package br.com.apt.ui.firstAccess.animations;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import br.com.apt.R;
import br.com.apt.application.BaseFragment;
import br.com.apt.util.Util;
import br.com.apt.util.VideoViewCustom;
import br.com.apt.widget.CustomButton;
import br.com.apt.widget.CustomTextView;

/**
 * Created by adminbs on 9/12/16.
 */
public class AnimationFragment extends BaseFragment implements View.OnClickListener{

    private VideoViewCustom video;
    private ImageView placeholderImageView;

    private static final int TIME_REPEAT_VIDEO = 5500;
    private static final int TIME_ANIMATE_BUTTONS = 1500;
    private static final int TIME_ANIMATE_INIT = 5000;

    private CustomButton codeAccess;
    private CustomButton doLogin;
    private LinearLayout linearAnimation;
    public static boolean loop = false;
    private boolean shouldFinish = false;

    public CustomTextView knowMeuApt;


    public static Fragment newInstance() {
        return new AnimationFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.animation_fragment, container, false);
    }


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final String pathVideo = "android.resource://" + getActivity().getPackageName() + "/" + R.raw.start_animation;

        final View view = getView();
        if(view != null){

            linearAnimation = (LinearLayout) view.findViewById(R.id.linearAnimation);
            linearAnimation.setAlpha(0);

            codeAccess = (CustomButton) view.findViewById(R.id.codeAccess);
            doLogin = (CustomButton) view.findViewById(R.id.doLogin);

            knowMeuApt = (CustomTextView) view.findViewById(R.id.knowMeuApt);
            knowMeuApt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse("https://meuapt.com.br/video/apt-video.mp4"));
                    startActivity(intent);
                }
            });

            DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();
            final int deviceWidth = displayMetrics.widthPixels;
            final int deviceHeight = displayMetrics.heightPixels;

            startCountDownByAnimationsButtons();

            placeholderImageView = (ImageView) view.findViewById(R.id.placeholderImageView);
            placeholderImageView.setVisibility(View.INVISIBLE);
            video = (VideoViewCustom) view.findViewById(R.id.video);
            video.setVideoURI(Uri.parse(pathVideo));
            video.setMediaController(null);
            video.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    video.start();
                }
            });

            if(deviceWidth <= 320 || deviceHeight <= 480){
                video.setDimensions(deviceWidth,deviceHeight);
                video.getHolder().setFixedSize(deviceWidth, deviceHeight);
            }


            AudioManager am = (AudioManager) getContext().getSystemService(Context.AUDIO_SERVICE);
            am.requestAudioFocus(null, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);

            video.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    if(shouldFinish){
                        video.stopPlayback();
                        video = null;
                        return;

                    }

                    video =  null;
                    video = (VideoViewCustom) view.findViewById(R.id.video);
                    video.setVideoURI(Uri.parse(pathVideo));
                    video.setMediaController(null);

                    if(deviceWidth <= 320 || deviceHeight <= 480){
                        video.setDimensions(deviceWidth,deviceHeight);
                        video.getHolder().setFixedSize(deviceWidth, deviceHeight);
                    }

                    video.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mp) {
                            video.seekTo(AnimationFragment.TIME_REPEAT_VIDEO);
                            video.start();
                        }
                    });
                }
            });

            video.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mp, int what, int extra) {
                    placeholderImageView.setVisibility(View.VISIBLE);
                    video.stopPlayback();
                    video.suspend();
                    shouldFinish = true;
                    return false;
                }
            });

            doLogin.setOnClickListener(this);
            codeAccess.setOnClickListener(this);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(AnimationFragment.loop && video != null){
            video.seekTo(AnimationFragment.TIME_REPEAT_VIDEO);
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void startCountDownByAnimationsButtons() {
        new CountDownTimer(AnimationFragment.TIME_ANIMATE_INIT, AnimationFragment.TIME_ANIMATE_INIT) {
            @Override
            public void onTick(long millisUntilFinished) {}

            @Override
            public void onFinish() {

                Animation a = new Animation() {
                    @Override
                    protected void applyTransformation(float interpolatedTime, Transformation t) {
                        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) linearAnimation.getLayoutParams();
                        params.bottomMargin = (int) ((Util.getSoftButtonsBarHeight(getActivity())) + 30 * interpolatedTime);
                        linearAnimation.setLayoutParams(params);
                    }
                };
                a.setDuration(1600);
                linearAnimation.startAnimation(a);


                linearAnimation.animate()
                        .alpha(1)
                        .setDuration(AnimationFragment.TIME_ANIMATE_BUTTONS)
                        .withLayer();
            }
        }.start();
    }

    @Override
    public void onClick(View v) {
        if(v == doLogin) {
            openActivity(BaseFragmentActivity.FRAGMENT_LOGIN);
        }else if(v == codeAccess){
            openActivity(BaseFragmentActivity.FRAGMENT_CODE);
        }
    }

    private void openActivity(int fragmentType) {
        startActivity(BaseFragmentActivity.newIntent(getContext(), fragmentType));
        getActivity().overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
    }
}
