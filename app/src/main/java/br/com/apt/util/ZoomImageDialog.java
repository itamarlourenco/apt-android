package br.com.apt.util;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.res.ResourcesCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;

import br.com.apt.R;
import br.com.apt.application.BaseActivity;
import br.com.apt.application.Config;
import br.com.apt.application.ZoomImageView;
import br.com.apt.ui.firstAccess.singup.DialogTermsOfUse;
import br.com.apt.widget.CustomButton;

public class ZoomImageDialog extends DialogFragment implements View.OnClickListener  {

    private static final String FRAG_TAG = "ZOOM_IMAGE";
    private int appArea;


    private SubsamplingScaleImageView zoomImageView;
    private Bitmap bitmap;

    /**
     * Shows an instance of this fragment to present the Terms of Use to the user.
     * @param fragmentManager a {@link FragmentManager} object to show this {@link DialogFragment}.
     */
    public static void show(FragmentManager fragmentManager, Bitmap bitmap, int area) {

        if (fragmentManager.findFragmentByTag(FRAG_TAG) == null) {
            ZoomImageDialog fragment = new ZoomImageDialog();
            fragment.show(fragmentManager, FRAG_TAG);
            fragment.setBitmap(bitmap);
            fragment.setAppArea(area);
        }
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }


    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null)
        {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_zoom_image, null);

        Window window = getDialog().getWindow();
        if(window != null){
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }


        zoomImageView = (SubsamplingScaleImageView) view.findViewById(R.id.zoomImageView);

        //TODO: set Bitmap
        zoomImageView.setImage(ImageSource.bitmap(bitmap));

        zoomImageView.setMaxScale(10f);

        CustomButton zoomCloseButton = (CustomButton) view.findViewById(R.id.zoomCloseButton);

        int color = R.drawable.selector_button_my_info;

        switch (this.appArea) {
            case ZoomImageView.APP_AREA_APT:
                color = R.drawable.selector_button_my_info;
                break;
            case ZoomImageView.APP_AREA_LOBBY:
                color = R.drawable.selector_more_visits;
                break;
        }

        zoomCloseButton.setBackground(
                ResourcesCompat.getDrawable(getResources(), color, null)
        );

        zoomCloseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeDialog();
            }
        });


        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.zoomCloseButton:
                closeDialog();
                break;
        }
    }

    private void closeDialog() {
        dismiss();
    }

    @Override
    public int getTheme() {
        return R.style.CustomDialog;
    }

    public int getAppArea() {
        return appArea;
    }

    public void setAppArea(int appArea) {
        this.appArea = appArea;
    }
}
