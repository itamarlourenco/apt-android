package br.com.apt.util;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import br.com.apt.R;
import br.com.apt.application.ZoomImageView;
import br.com.apt.widget.ViewHolderLoaderImageView;

/**
 * Created by itamarlourenco on 16/01/16.
 */
public class CustomImage {

    public static void loader(Context context, final ViewHolderLoaderImageView viewHolderLoaderImageView, String url){
        loader(context, viewHolderLoaderImageView, url, null);
    }

    public static void loader(Context context, final ViewHolderLoaderImageView viewHolderLoaderImageView, String url, final ImageView.ScaleType scaleType){
        if(viewHolderLoaderImageView.mainImageView == null || viewHolderLoaderImageView.imageViewProgressBar == null){
            Logger.e("Please add the include image_loader.xml in your view");
            return;
        }

        Picasso.with(context).load(url).into(viewHolderLoaderImageView.mainImageView, new Callback() {
            @Override
            public void onSuccess() {
                if(viewHolderLoaderImageView.imageViewProgressBar != null){
                    Util.changeVisibility(viewHolderLoaderImageView.imageViewProgressBar, View.GONE, true);
                    Util.changeVisibility(viewHolderLoaderImageView.mainImageView, View.VISIBLE, true);
                    if(scaleType != null){
                        viewHolderLoaderImageView.mainImageView.setScaleType(scaleType);
                    }
                }
            }

            @Override
            public void onError() {
                viewHolderLoaderImageView.mainImageView.setImageResource(R.drawable.image_place_holder);
            }
        });
    }

    public static void loader(Context context, View view, String url){
        loader(context, view, url, null);
    }

    public static void loader(Context context, View view, String url, final ImageView.ScaleType scaleType){
        final ZoomImageView mainImageView = (ZoomImageView) view.findViewById(R.id.mainImageView);
        mainImageView.setAppArea(ZoomImageView.APP_AREA_LOBBY);
        final ProgressBar imageViewProgressBar = (ProgressBar) view.findViewById(R.id.imageViewProgressBar);

        if(mainImageView == null || imageViewProgressBar == null){
            Logger.e("Please add the include image_loader.xml in your view");
            return;}


        Picasso.with(context).load(url).into(mainImageView, new Callback() {
            @Override
            public void onSuccess() {
                if(imageViewProgressBar != null){
                    Util.changeVisibility(imageViewProgressBar, View.GONE, true);
                    Util.changeVisibility(mainImageView, View.VISIBLE, true);

                    if(scaleType != null){
                        mainImageView.setScaleType(scaleType);
                    }
                }
            }

            @Override
            public void onError() {
                mainImageView.setImageResource(R.drawable.image_place_holder);
            }
        });
    }
}
