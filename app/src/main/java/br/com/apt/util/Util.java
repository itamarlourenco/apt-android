package br.com.apt.util;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.InputFilter;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.TimePicker;

import com.prolificinteractive.materialcalendarview.CalendarDay;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.ui.reservations.Installations;
import br.com.apt.widget.CustomTextView;
import br.com.apt.widget.CustomTextViewLight;

/**
 * Created by adminbs on 1/12/16.
 */
public class Util {

    public static final int DATE_EQUAL = 0;
    public static final int DATE_HIGHER = 1;
    public static final int DATE_LESSER = 2;


    public static void changeVisibility(View view, int visibility, boolean animate) {
        if (view == null || visibility == view.getVisibility()) {
            return;
        }
        try{
            view.clearAnimation();
            if (animate) {
                int animResourceId;
                if (visibility == View.VISIBLE) {
                    animResourceId = android.R.anim.fade_in;
                } else {
                    animResourceId = android.R.anim.fade_out;
                }
                Animation anim = AnimationUtils.loadAnimation(view.getContext(), animResourceId);
                view.setAnimation(anim);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        view.setVisibility(visibility);
    }
    public static ArrayList<View> getViewsByTag(ViewGroup root, String tag){
        ArrayList<View> views = new ArrayList<>();
        final int childCount = root.getChildCount();
        for (int i = 0; i < childCount; i++) {
            final View child = root.getChildAt(i);
            if (child instanceof ViewGroup) {
                views.addAll(getViewsByTag((ViewGroup) child, tag));
            }

            final Object tagObj = child.getTag();
            if (tagObj != null && tagObj.equals(tag)) {
                views.add(child);
            }

        }
        return views;
    }

    public static void setAllBackgroundTextViewByTag(int id, String tag, Activity activity) {
        ArrayList<View> arrayList = Util.getViewsByTag((ViewGroup) activity.getWindow().getDecorView().findViewById(android.R.id.content), tag);
        for (View textView : arrayList) {
            textView.setBackgroundResource(id);
        }
    }


    public static void showDialog(Activity activity, String message){
        showDialog(activity, message, null);
    }

    public static void showDialog(Activity activity, int resource){
        showDialog(activity, App.getContext().getString(resource), null);
    }
    public static void showDialog(Activity activity, String message, DialogInterface.OnClickListener listener){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setPositiveButton(App.getContext().getString(R.string.ok), listener);
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


    public static void hideFeedBackFragment(View view, int resource){
        if(view != null) {
            hideFeedBackFragment(view, view.findViewById(resource));
        }
    }

    public static void hideFeedBackFragment(View view, View show){
        View progressBarView = view.findViewById(R.id.progressBarFeedback);
        if(progressBarView != null){
            Util.changeVisibility(progressBarView, View.GONE, true);
        }

        CustomTextView feedback = (CustomTextView) view.findViewById(R.id.feedBack);
        if(feedback != null){
            Util.changeVisibility(feedback, View.GONE, true);
        }

        if(show != null){
            Util.changeVisibility(show, View.VISIBLE, true);
        }
    }

    public static void showFeedbackFragment(View view, String message) {
        if(view != null){
//            Util.changeVisibility(view.findViewById(R.id.include), View.VISIBLE, true);
            View progressBarView = view.findViewById(R.id.progressBarFeedback);
            if(progressBarView != null){
                Util.changeVisibility(progressBarView, View.GONE, true);
            }

            CustomTextView feedback = (CustomTextView) view.findViewById(R.id.feedBack);
            if(feedback != null){
                feedback.setText(message);
                Util.changeVisibility(feedback, View.VISIBLE, true);
            }
        }
    }

    public static long getDifferenceDay(Date date, Date dateCompare){
        long milliSeconds1 = date.getTime();
        long milliSeconds2 = dateCompare.getTime();
        long periodSeconds = (milliSeconds2 - milliSeconds1) / 1000;
        return periodSeconds / 60 / 60 / 24;
    }

    public static int convertToPositive(int convert){
        if(convert < 0){
            return convert * -1;
        }
        return convert;
    }

    public static String getItemString(Context context, int resource, int position){
        String[] array = context.getResources().getStringArray(resource);
        try{
            return array[position];
        }catch (Exception ignored){}
        return null;
    }

    public static String getNameByChat(String from){
        if(from.contains("@")){
            String[] split = from.split("@");
            if(split.length > 0){
                return split[0];
            }
        }
        return "";
    }

    public static void hideKeyboard(Context context, View view) {
        if(context != null){
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static String getAptoByUser(String user){
        return user.replaceAll("[\\D]", "");
    }

    public static void changeColorTextTimePicker(TimePicker timePicker, Context context){
        Resources system = Resources.getSystem();
        int hourId = system.getIdentifier("hour", "id", "android");
        int minuteId = system.getIdentifier("minute", "id", "android");
        int amPmId = system.getIdentifier("amPm", "id", "android");

        NumberPicker hourNumberPicker = (NumberPicker) timePicker.findViewById(hourId);
        NumberPicker minuteNumberPicker = (NumberPicker) timePicker.findViewById(minuteId);
        NumberPicker ampmNumberpicker = (NumberPicker) timePicker.findViewById(amPmId);

        setNumberTimePickerTextColour(hourNumberPicker, context);
        setNumberTimePickerTextColour(minuteNumberPicker, context);
        setNumberTimePickerTextColour(ampmNumberpicker, context);
    }

    private static void setNumberTimePickerTextColour(NumberPicker numberPicker, Context context){
        final int count = numberPicker.getChildCount();
        final int color = ContextCompat.getColor(context, R.color.text_default_color);

        for(int i = 0; i < count; i++){
            View child = numberPicker.getChildAt(i);

            try{
                Field field = numberPicker.getClass().getDeclaredField("mSelectorWheelPaint");
                field.setAccessible(true);

                ((Paint)field.get(numberPicker)).setColor(color);
                ((EditText)child).setTextColor(color);
                numberPicker.invalidate();
            }
            catch(NoSuchFieldException | IllegalAccessException | IllegalArgumentException e){
                Logger.e(e.getMessage());
            }
        }
    }

    public static boolean compareCalendarJustDay(Calendar calendarSelected, Calendar calendar) {
        return calendarSelected.get(Calendar.YEAR) == calendar.get(Calendar.YEAR)
                && calendarSelected.get(Calendar.MONTH) == calendar.get(Calendar.MONTH)
                && calendarSelected.get(Calendar.DAY_OF_MONTH) == calendar.get(Calendar.DAY_OF_MONTH);
    }

    public static String handleEvents(List<String> listEventsDay) {
        StringBuilder stringBuilder = new StringBuilder();
        for(String string: listEventsDay){

            stringBuilder.append(" ").append(string);
        }
        return stringBuilder.toString();
    }


    public static File resizeFile(String path, Bitmap bMap){

        Bitmap out = Bitmap.createScaledBitmap(bMap, (int)(bMap.getWidth() * 0.2), (int) (bMap.getHeight() * 0.2), false);
        File resizedFile = new File(path);
        OutputStream fOut=null;
        try {
            fOut = new BufferedOutputStream(new FileOutputStream(resizedFile));
            out.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.flush();
            fOut.close();
            bMap.recycle();
            out.recycle();
        } catch (Exception e) { // TODO

        }

        return resizedFile;
    }

    public static File resizeFile(String path, InputStream imageInputStream) {
        return resizeFile(path, BitmapFactory.decodeStream(imageInputStream));
    }

    public static File resizeFile(String path) {
       return resizeFile(path, BitmapFactory.decodeFile(path));
    }

    @Deprecated
    public static String convertToUTF8(String s) {
        return s;
    }

    public static String convertToUTF8params(String s) {
        String out;
        try {
            out = new String(s.getBytes("UTF-8"), "ISO-8859-1");
        } catch (Exception e) {
            return null;
        }
        return out;
    }

    public static int getSoftButtonsBarHeight(FragmentActivity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            DisplayMetrics metrics = new DisplayMetrics();
            activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
            int usableHeight = metrics.heightPixels;
            activity.getWindowManager().getDefaultDisplay().getRealMetrics(metrics);
            int realHeight = metrics.heightPixels;
            if (realHeight > usableHeight)
                return realHeight - usableHeight;
            else
                return 0;
        }
        return 0;
    }

    public static void setMargins (View v, int l, int t, int r, int b) {
        if (v.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) v.getLayoutParams();
            p.setMargins(l, t, r, b);
            v.requestLayout();
        }
    }

    public static float convertPixelsToDp(float px, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        return  px / ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static void playVideo(Activity activity){
        File sdCard = Environment.getExternalStorageDirectory();

        File dir = new File (sdCard.getAbsolutePath() + "/aptfolder");
        if(!dir.isDirectory()) {
            if(dir.mkdirs()){
                InputStream ins = activity.getResources().openRawResource(R.raw.video_know_apt);
                int size;
                try {
                    size = ins.available();

                    byte[] buffer = new byte[size];
                    ins.read(buffer);
                    ins.close();
                    FileOutputStream fos = new FileOutputStream(new File(dir, "MeuApt.mp4"));
                    fos.write(buffer);
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        File myvid = new File(dir, "MeuApt.mp4");
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.fromFile(myvid), "video/*");
        activity.startActivity(intent);
    }


    public static int compareDateWithNow(String d1)
    {
        try{
//            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date date1 = sdf.parse(d1);

            Date date2 = new Date();

            return Util.compareDates(date1,sdf.parse(sdf.format(date2)));
        }
        catch(ParseException ex){
            ex.printStackTrace();
        }
        return -1;
    }


    public static int compareDates(String d1,String d2)
    {
        try{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            Date date1 = sdf.parse(d1);
            Date date2 = sdf.parse(d2);
            return Util.compareDates(date1,date2);
        }
        catch(ParseException ex){
            ex.printStackTrace();
        }
        return -1;
    }

    public static int compareDates(Date date1,Date date2)
    {

        //date object is having 3 methods namely after,before and equals for comparing
        //after() will return true if and only if date1 is after date 2
        if(date1.after(date2)){
            return Util.DATE_HIGHER;
        }

        //before() will return true if and only if date1 is before date2
        if(date1.before(date2)){

            return Util.DATE_LESSER;
        }

        //equals() returns true if both the dates are equal
        if(date1.equals(date2)){
            return Util.DATE_EQUAL;
        }

        return -1;
    }



    public static boolean validateTime(String timeString) {
        String[] timeArr = timeString.split(":");

        if(timeArr.length == 2){

            int userHour = Integer.parseInt(timeArr[0]);
            int userTime = Integer.parseInt(timeArr[1]);

            if(userHour <= 23 && userHour >= 0 && userTime <= 59 && userTime >= 0){
                Calendar c = Calendar.getInstance();
                int hours = c.get(Calendar.HOUR_OF_DAY);
                int minute = c.get(Calendar.MINUTE);

                if(userHour > hours){
                    return true;
                }

                if(userHour >= hours && userTime >= minute){
                    return true;
                }
                return false;
            }
        }
        return false;
    }

    public static boolean validateAllTime(String timeString) {
        String[] timeArr = timeString.split(":");

        if(timeArr.length == 2){

            int userHour = Integer.parseInt(timeArr[0]);
            int userTime = Integer.parseInt(timeArr[1]);

            if(userHour <= 23 && userHour >= 0 && userTime <= 59 && userTime >= 0){
                return true;
            }
        }
        return false;
    }

    public static boolean compareHour(String startTime, String endTime){
        if(!TextUtils.isEmpty(endTime) && !TextUtils.isEmpty(startTime)){
            String pattern = "HH:mm";
            SimpleDateFormat sdf = new SimpleDateFormat(pattern);
            try {
                Date date1 = sdf.parse(startTime);
                Date date2 = sdf.parse(endTime);

                if(date1.compareTo(date2) > -1){
                    return false;
                }

                return true;

            } catch (ParseException e){
                // Exception handling goes here
            }
        }
        return false;
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean checkBirthDate(String birthDate) {
        try {
            SimpleDateFormat simpleDateFormatBirthDate = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
            Date dateBirthDate = simpleDateFormatBirthDate.parse(birthDate);

            if(dateBirthDate.before(new Date())){
                return true;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return false;
    }

    public static String dateUsToBr(String dateUs){
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        try {
            Date date = format.parse(dateUs);
            SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            return dateformat.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static String clearCpf(String text) {
        return text.replace(".", "").replace("-", "");

    }

    public static void openPdf(Activity activity, String url) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        activity.startActivity(browserIntent);
    }

    public static class ValidaCPF {
        public static boolean isCPF(String CPF) {
            if (CPF.equals("00000000000") || CPF.equals("11111111111") ||
                    CPF.equals("22222222222") || CPF.equals("33333333333") ||
                    CPF.equals("44444444444") || CPF.equals("55555555555") ||
                    CPF.equals("66666666666") || CPF.equals("77777777777") ||
                    CPF.equals("88888888888") || CPF.equals("99999999999") ||
                    (CPF.length() != 11))
                return(false);

            char dig10, dig11;
            int sm, i, r, num, peso;

            try {
                sm = 0;
                peso = 10;
                for (i=0; i<9; i++) {
                    num = (int) (CPF.charAt(i) - 48);
                    sm = sm + (num * peso);
                    peso = peso - 1;
                }

                r = 11 - (sm % 11);
                if ((r == 10) || (r == 11))
                    dig10 = '0';
                else dig10 = (char)(r + 48);

                sm = 0;
                peso = 11;
                for(i=0; i<10; i++) {
                    num = (int)(CPF.charAt(i) - 48);
                    sm = sm + (num * peso);
                    peso = peso - 1;
                }

                r = 11 - (sm % 11);
                if ((r == 10) || (r == 11))
                    dig11 = '0';
                else dig11 = (char)(r + 48);

                if ((dig10 == CPF.charAt(9)) && (dig11 == CPF.charAt(10)))
                    return(true);
                else return(false);
            } catch (InputMismatchException erro) {
                return(false);
            }
        }

        public static String imprimeCPF(String CPF) {
            return(CPF.substring(0, 3) + "." + CPF.substring(3, 6) + "." +
                    CPF.substring(6, 9) + "-" + CPF.substring(9, 11));
        }
    }

    public static String dateUsToBr(Context context, Date dateUs){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(context.getString(R.string.tickets_sent_date_date_fmt), Locale.US);
        return simpleDateFormat.format(dateUs);
    }

    public static boolean validateLicencePlate(String text) {
        Pattern pattern = Pattern.compile("[a-zA-Z]{3,3}-\\d{4,4}");
        Matcher matcher = pattern.matcher(text);
        return matcher.find();
    }


    public static boolean sendBitmapToFile(File finalFile, Bitmap bitmap){
        try {
            FileOutputStream fos = new FileOutputStream(finalFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.flush();
            fos.close();

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }


    public static void showPlaceholder(View view, Activity activity, int drawableId, String message){
        Util.showPlaceholder(view, activity, drawableId, message, true);
    }

    public static void showPlaceholder(View view, Activity activity, int drawableId, String message, boolean show){
        if(view == null){
            return;
        }

        View include = view.findViewById(R.id.placeholder);
        if(include != null){
            Util.changeVisibility(include, show ? View.VISIBLE : View.INVISIBLE, true);
            ImageView imagePlaceholder = (ImageView) view.findViewById(R.id.imagePlaceholder);
            imagePlaceholder.setImageDrawable(ContextCompat.getDrawable(activity, drawableId));

            ((CustomTextViewLight) view.findViewById(R.id.messagePlaceholder)).setText(message);
            (view.findViewById(R.id.titleMessage)).setVisibility(View.INVISIBLE);
        }
    }

    public static void hidePlaceholder(View view){
        if(view != null){
            View include = view.findViewById(R.id.placeholder);
            include.setVisibility(View.INVISIBLE);
        }
    }

    public static void handlePlaceholderError(View view, Activity activity){
        Util.handlePlaceholderError(view, activity, true);
    }

    public static void handlePlaceholderError(View view, Activity activity, boolean show){
        if(view == null){
            return;
        }

        View include = view.findViewById(R.id.placeholder);
        if(include != null){

            Util.changeVisibility(include, show ? View.VISIBLE : View.INVISIBLE, true);
        }

        CustomTextViewLight titleMessage = (CustomTextViewLight) view.findViewById(R.id.titleMessage);
        CustomTextViewLight messagePlaceholder = (CustomTextViewLight) view.findViewById(R.id.messagePlaceholder);

        if(titleMessage != null && messagePlaceholder != null){
            titleMessage.setTextColor(ContextCompat.getColor(App.getContext(), android.R.color.background_dark));
            messagePlaceholder.setTextColor(ContextCompat.getColor(App.getContext(), android.R.color.background_dark));
        }
    }


    public static String dateFormatted(Date date) {
        return Util.dateFormatted(date, "dd/MM/yyyy");
    }

    public static String dateFormatted(Date date, String format) {
        try{
            SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.US);
            return sdf.format(date);
        }catch (Exception ex){
            return "";
        }
    }

    public static String timeFormatted(Date date) {
        return Util.dateFormatted(date, "HH:mm:ss");
    }

    public static String timeFormatted(Date date, String format) {
        try{
            SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.US);
            return sdf.format(date);
        }catch (Exception ex){
            return "";
        }
    }


    public static File createImageFile() {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.CANADA).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        String path = System.getProperty("java.io.tmpdir");
        try {
            return File.createTempFile(imageFileName, ".jpg", new File(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void justifyListViewHeightBasedOnChildren (ListView listView) {

        ListAdapter adapter = listView.getAdapter();

        if (adapter == null) {
            return;
        }
        ViewGroup vg = listView;
        int totalHeight = 0;
        for (int i = 0; i < adapter.getCount(); i++) {
            View listItem = adapter.getView(i, null, vg);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams par = listView.getLayoutParams();
        par.height = totalHeight + (listView.getDividerHeight() * (adapter.getCount() - 1)) + 40;
        listView.setLayoutParams(par);
        listView.requestLayout();
    }

    public static String getFormatDateUS(Calendar calendar){
        return calendar.get(Calendar.YEAR) + "-" + calendar.get(Calendar.MONTH) + "-" + calendar.get(Calendar.DAY_OF_MONTH);
    }


    public static String getFormatDateUSCashFlowAndAccountFragment(Calendar calendar){
        return Util.getFormatDateUSCashFlowAndAccountFragment(calendar, false);
    }

    public static String getFormatDateUSCashFlowAndAccountFragment(Calendar calendar, boolean last){
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH);

        if(last){
            if(month == 1){
                day = 31;
            }
            if(month == 2){
                day = isAnoBissexto(calendar.get(Calendar.YEAR)) ? 29 : 28;
            }
            if(month == 3){
                day = 31;
            }
            if(month == 4){
                day = 30;
            }
            if(month == 5){
                day = 31;
            }
            if(month == 6){
                day = 30;
            }
            if(month == 7){
                day = 31;
            }
            if(month == 8){
                day = 31;
            }
            if(month == 9){
                day = 30;
            }
            if(month == 10){
                day = 31;
            }
            if(month == 11){
                day = 30;
            }
            if(month == 12){
                day = 31;
            }
        }


        return calendar.get(Calendar.YEAR) + "-" + month + "-" + (day < 10 ? "0" + day : day);
    }

    public static boolean isAnoBissexto(int ano) {
        if ( ( ano % 4 == 0 && ano % 100 != 0 ) || ( ano % 400 == 0 ) ){
            return true;
        }
        else{
            return false;
        }
    }

    public static final String md5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest.getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String generateName(){
        return generateName(null);
    }
    public static String generateName(String complement){
        String timeStamp;
        if(TextUtils.isEmpty(complement)){
            timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.CANADA).format(new Date()) + new Random().nextInt(999999);
            return Util.md5(timeStamp);
        }else{
            timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.CANADA).format(new Date()) + new Random().nextInt(999999) + "_" + complement;
            return Util.md5(timeStamp) + "_" + complement;
        }
    }

    public static void  removeDividerNumberPicker (NumberPicker picker, Context context) {
        java.lang.reflect.Field[] pickerFields = NumberPicker.class.getDeclaredFields();
        for (java.lang.reflect.Field pf : pickerFields) {
            if (pf.getName().equals("mSelectionDivider")) {
                pf.setAccessible(true);
                try {
                    pf.set(picker, ContextCompat.getDrawable(context, android.R.color.transparent));
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (Resources.NotFoundException e) {
                    e.printStackTrace();
                }
                catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    public static void setStartValueNumberPicker(NumberPicker numberPicker){
        Field f;
        try {
            f = NumberPicker.class.getDeclaredField("mInputText");
            f.setAccessible(true);
            EditText inputText = (EditText) f.get(numberPicker);
            inputText.setFilters(new InputFilter[0]);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String handleHTMLIcon(String icon){
        return "<html>" +
                "<body style='text-align:center; background:#2a292a; overflow:hidden;'>" +
                "<img height='22px' style='margin:auto;' src='"+ icon +"' />" +
                "</body>" +
                "</html>";
    }

    public static String handleHTMLIconSVG(String result){
        return "<html>" +
                "<head>" +
                "<style>" +
                "*{margin:0px; padding:0px;}" +
                "svg {width:30px; height:30px}" +
                "path {fill:#298CAD !important}" +
                "</style>" +
                "</head>" +
                "<body style='text-align:center; background:#DEDEDE; overflow:hidden;'>" +
                result +
                "</body>" +
                "</html>";
    }

    public static String getDayWeek(Calendar c){
        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
        String weekDay = "";

        if (Calendar.MONDAY == dayOfWeek) weekDay = "Segunda-Feira";
        else if (Calendar.TUESDAY == dayOfWeek) weekDay = "Terça-Feira";
        else if (Calendar.WEDNESDAY == dayOfWeek) weekDay = "Quarta-Feira";
        else if (Calendar.THURSDAY == dayOfWeek) weekDay = "Quinta-feira";
        else if (Calendar.FRIDAY == dayOfWeek) weekDay = "Sexta-feira";
        else if (Calendar.SATURDAY == dayOfWeek) weekDay = "Sábado";
        else if (Calendar.SUNDAY == dayOfWeek) weekDay = "Domingo";

        return weekDay;
    }

    public static String getMonthName(int month){
        switch (month){
            case 0:
                return "Janeiro";
            case 1:
                return "Fevereiro";
            case 2:
                return "Março";
            case 3:
                return "Abril";
            case 4:
                return "Maio";
            case 5:
                return "Junho";
            case 6:
                return "Julho";
            case 7:
                return "Agosto";
            case 8:
                return "Setembro";
            case 9:
                return "Outubro";
            case 10:
                return "Novembro";
            case 11:
                return "Dezembro";
        }
        return "";
    }

    public static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
        long diffInMillies = date2.getTime() - date1.getTime();
        return timeUnit.convert(diffInMillies,TimeUnit.MILLISECONDS);
    }

    public static String capitalizeString(final String string) {
        if (TextUtils.isEmpty(string)) {
            return string;
        }
        final char[] buffer = string.toLowerCase().toCharArray();
        boolean capitalizeNext = true;
        for (int i = 0; i < buffer.length; i++) {
            final char ch = buffer[i];
            if (Character.isWhitespace(ch)) {
                capitalizeNext = true;
            } else if (capitalizeNext) {
                buffer[i] = Character.toTitleCase(ch);
                capitalizeNext = false;
            }
        }
        return new String(buffer);
    }
}
