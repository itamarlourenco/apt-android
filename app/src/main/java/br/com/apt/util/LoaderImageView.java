package br.com.apt.util;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.AttributeSet;
import android.widget.ImageView;

import java.util.Timer;
import java.util.TimerTask;

import br.com.apt.R;

/**
 * Created by verificar on 3/30/17.
 */

public class LoaderImageView extends ImageView {

    static private Bitmap spriteBitmap;
    Activity myActivity = (Activity) getContext();
    private AsyncTask<Void, Void, Void> task = null;
    private boolean isRunning = false;

    public LoaderImageView(Context context) {
        super(context);
        initView();
    }

    public LoaderImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public LoaderImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    public LoaderImageView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initView();
    }

    private void initView() {
        if(LoaderImageView.spriteBitmap == null){
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 2;
            LoaderImageView.spriteBitmap = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.loading_sprite_small);
        }


        task = new AsyncTask<Void, Void, Void>(){
            int height;
            int imageY;
            Bitmap cropedBitmap;
            int multiplier = 0;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                isRunning = true;
            }

            @Override
            protected Void doInBackground(Void... params) {
                height = LoaderImageView.spriteBitmap.getHeight() / 74;
                while(isRunning) {
                    imageY = (height * multiplier);
                    final Bitmap cropedBitmap = Bitmap.createBitmap(LoaderImageView.spriteBitmap, 0, imageY, LoaderImageView.spriteBitmap.getWidth(), height);

                    myActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            setImageBitmap(cropedBitmap);
                        }
                    });

                    try {
                        if (multiplier == 73) {
                            Thread.sleep(300);
                            multiplier = 0;
                        } else {
                            multiplier++;
                            Thread.sleep(29);
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                setImageBitmap(cropedBitmap);
            }

        };

        task.execute();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        task.cancel(true);
        isRunning = false;
    }
}
