package br.com.apt.util;

import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import br.com.apt.application.App;

public class Logger {
    private static final String TAG = "br.com.apt.Logger";

    public static void d(Object message) {
        d(message, TAG);
    }

    public static void d(Object message, String tag) {
        Log.d(TAG, String.valueOf(message));
    }

    public static void e(Object message) {
        e(message, TAG);
    }

    public static void e(Object message, String tag) {
        Log.e(TAG, String.valueOf(message));
    }

    public static void i(Object message) {
        i(message, TAG);
    }

    public static void i(Object message, String tag) {
        Log.i(TAG, String.valueOf(message));
    }

    public static void w(Object message) {
        w(message, TAG);
    }

    public static void w(Object message, String tag) {
        Log.w(TAG, String.valueOf(message));
    }

    public static void v(Object message) {
        v(message, TAG);
    }

    public static void v(Object message, String tag) {
        Log.v(TAG, String.valueOf(message));
    }

    public static void t(Object message) {
        t(message, false);
    }

    public static void t(Object message, boolean type) {
        try {
            String str;
            if (type) {
                str = message.getClass().getName() + " - " + String.valueOf(message);
            } else {
                str = String.valueOf(message);
            }

            Toast.makeText(App.getContext(), str, Toast.LENGTH_LONG).show();
            d(String.valueOf(message));
        } catch (RuntimeException e) {
        }
    }
}