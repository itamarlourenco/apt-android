package br.com.apt.ui.tickets.tickets;

import com.google.gson.annotations.SerializedName;

import br.com.apt.application.SendRequest;

/**
 * Created by adminbs on 5/13/16.
 */
public class TicketsSend extends SendRequest {
    @SerializedName("ticket_id")
    private long ticketId;

    @SerializedName("status")
    private String status;

    public long getTicketId() {
        return ticketId;
    }

    public void setTicketId(long ticketId) {
        this.ticketId = ticketId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
