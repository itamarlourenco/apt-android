package br.com.apt.ui.lobby.lobbyFragments.visits;

import java.util.List;

import br.com.apt.application.BaseGson;
import br.com.apt.ui.lobby.lobbyFragments.deliveries.Delivery;

/**
 * Created by adminbs on 1/15/16.
 */
public class VisitRequest extends BaseGson {
    public List<Visit> Object;

    public List<Visit> getObject() {
        return Object;
    }

    public void setObject(List<Visit> object) {
        Object = object;
    }
}
