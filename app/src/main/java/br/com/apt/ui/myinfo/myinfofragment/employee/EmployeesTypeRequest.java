package br.com.apt.ui.myinfo.myinfofragment.employee;

import java.util.List;

import br.com.apt.application.BaseGson;
import br.com.apt.ui.myinfo.myinfofragment.AutoComplete;


/**
 * Created by adminbs on 11/7/16.
 */
public class EmployeesTypeRequest extends BaseGson {
    public List<AutoComplete> Object;

    public List<AutoComplete> getObject() {
        return Object;
    }
}

