package br.com.apt.ui.tickets.tickets;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import br.com.apt.application.BaseActivity;
import br.com.apt.ui.reservations.CalendarFragment;

public class TicketItemActivity extends BaseActivity {

    private static final String EXTRA_TICKET = "EXTRA_TICKET";

    public static Intent newIntent(Context context, Ticket ticket) {
        Intent intent = new Intent(context, TicketItemActivity.class);
        intent.putExtra(EXTRA_TICKET, ticket);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public Fragment getFragment(){
        Intent intent = getIntent();
        if(intent != null){
            Ticket ticket =  intent.getParcelableExtra(EXTRA_TICKET);
            return TicketItemFragment.newInstance(ticket);
        }
        return null;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}