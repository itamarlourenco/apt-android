package br.com.apt.ui.myinfo.myinfofragment.pet;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.Request;
import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.github.ybq.android.spinkit.SpinKitView;

import java.util.HashMap;
import java.util.List;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.ui.ExceptionWithoutResult;
import br.com.apt.ui.myinfo.myinfofragment.FragmentMyInfoAdd;
import br.com.apt.util.Logger;
import br.com.apt.util.Util;

/**
 * Created by fabricio.bezerra on 22/09/2016.
 */
public class MyPetListFragment extends FragmentMyInfoAdd implements Volley.Callback {

    private SwipeMenuListView listView;
    private List<Pet> pets;
    private SpinKitView loader;

    public static MyPetListFragment newInstance(){
        return new MyPetListFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list_my_pet, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final View view = getView();
        if (view != null) {
            loader = (SpinKitView) view.findViewById(R.id.loader);
            listView = (SwipeMenuListView) view.findViewById(R.id.listView);

            listView.setMenuCreator(new SwipeMenuCreator() {
                @Override
                public void create(SwipeMenu menu) {
                    SwipeMenuItem openItem = new SwipeMenuItem(getContext());
                    openItem.setBackground(new ColorDrawable(Color.rgb(0xF9, 0x3F, 0x25)));
                    openItem.setWidth(120);
                    openItem.setTitle("X");
                    openItem.setTitleSize(18);
                    openItem.setTitleColor(Color.BLACK);
                    menu.addMenuItem(openItem);
                }
            });

            listView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                    switch (index) {
                        case 0:
                            Pet pet = pets.get(position);
                            removeItem(pet.getId(), new CallbackDelete() {
                                @Override
                                public void delete() {
                                    getPets();
                                }
                            });
                            break;
                    }
                    return false;
                }
            });

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    startActivity(MyPetAddActivity.newIntent(getContext(), pets.get(position)));
                }
            });
            view.findViewById(R.id.newPet).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(MyPetAddActivity.newIntent(getContext()));
                    getActivity().overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
                }
            });
        }
    }

    public void getPets(){
        Volley volley = new Volley(this);
        volley.request(Request.Method.GET, null);
    }

    @Override
    public void result(String result, int type) {
        View view = getView();
        try{
            Util.hidePlaceholder(view);
            Util.changeVisibility(loader, View.GONE, true);
            Util.changeVisibility(listView, View.VISIBLE, true);
            PetRequest petRequest = App.getGson().fromJson(result, PetRequest.class);
            pets = petRequest.getObject();
            listView.setAdapter(new MyPetAdapter(getContext(), pets));
            if(pets == null){
                throw new ExceptionWithoutResult(getString(R.string.message_without_pet), R.drawable.meupet_no_have);
            }
        }catch (ExceptionWithoutResult exceptionWithoutResult){
            Util.showPlaceholder(view, getActivity(), exceptionWithoutResult.getDrawableId(), exceptionWithoutResult.getMessage());
        }catch (Exception exception){
            Util.handlePlaceholderError(view, getActivity());
        }
    }

    @Override
    public String uri() {
        return URI.PETS;
    }

    @Override
    public void onResume() {
        super.onResume();
        getPets();
    }

    @Override
    protected String getPagerTitle() {
        return App.getContext().getString(R.string.my_pet);
    }

    @Override
    protected int getPagerIcon() {
        return R.drawable.ico_meu_pet;
    }

    @Override
    protected HashMap<String, String> save() {
        return null;
    }

    @Override
    protected String getUri() {
        return URI.PETS;
    }

    @Override
    protected int getTypeRequest() {
        return 0;
    }

    @Override
    protected void saved() {

    }

    @Override
    protected void saved(String json) {

    }


}
