package br.com.apt.ui.myinfo.myinfofragment.pet;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import br.com.apt.R;
import br.com.apt.application.BaseActivity;
import br.com.apt.ui.reservations.CalendarFragment;

/**
 * Created by adminbs on 7/18/17.
 */

public class MyPetAddActivity extends BaseActivity {

    public static final String EXTRA_PET = "EXTRA_PET";
    private MyPetAddFragment myPetAddFragment = MyPetAddFragment.newInstance();

    public static Intent newIntent(Context context) {
        return new Intent(context, MyPetAddActivity.class);
    }

    public static Intent newIntent(Context context, Pet pet) {
        Intent intent = new Intent(context, MyPetAddActivity.class);
        intent.putExtra(EXTRA_PET, pet);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public Fragment getFragment(){
        Intent intent = getIntent();
        if(intent != null){
            Pet pet = intent.getParcelableExtra(MyPetAddActivity.EXTRA_PET);
            myPetAddFragment.setPetByAdd(pet);
        }
        return myPetAddFragment;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        myPetAddFragment.onActivityResultFragment(requestCode, resultCode, data);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_down, R.anim.slide_out_down);
    }
}