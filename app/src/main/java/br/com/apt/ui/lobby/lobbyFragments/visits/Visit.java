package br.com.apt.ui.lobby.lobbyFragments.visits;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

import br.com.apt.application.ModelObject;

/**
 * Created by adminbs on 1/11/16.
 */
public class Visit implements ModelObject {

    public static final String STATUS_RELEASED = "RELEASED";
    public static final String STATUS_NOT_RELEASED = "NOT_RELEASED";
    public static final String STATUS_WAITING = "WAITING";

    private long id;

    @SerializedName("nome")
    private String name;

    @SerializedName("descricao")
    private String description;

    @SerializedName("data")
    private Date date;

    private String type;

    @SerializedName("imagem")
    private String image;
    private String obs;

    @SerializedName("edificioId")
    private int buildId;

    @SerializedName("usuarioId")
    private String userId;

    @SerializedName("liberado")
    private int isFree;

    @SerializedName("usuarioIdCreated")
    private String userIdCreated;

    @SerializedName("agendado")
    private boolean scheduled;

    private Date data_liberado;

    private Date data_agendado;

    public Visit() {
    }

    public Visit(long id, String name, String description, Date date, String type, String image, String obs, int buildId, String userId, int isFree, String userIdCreated) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.date = date;
        this.image = image;
        this.type = type;
        this.obs = obs;
        this.buildId = buildId;
        this.userId = userId;
        this.isFree = isFree;
        this.userIdCreated = userIdCreated;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getObs() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }

    public int getBuildId() {
        return buildId;
    }

    public void setBuildId(int buildId) {
        this.buildId = buildId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int isFree() {
        return isFree;
    }

    public void setIsFree(int isFree) {
        this.isFree = isFree;
    }

    public String getUserIdCreated() {
        return userIdCreated;
    }

    public void setUserIdCreated(String userIdCreated) {
        this.userIdCreated = userIdCreated;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isScheduled() {
        return scheduled;
    }

    public void setScheduled(boolean scheduled) {
        this.scheduled = scheduled;
    }

    public Date getData_liberado() {
        return data_liberado;
    }

    public void setData_liberado(Date data_liberado) {
        this.data_liberado = data_liberado;
    }

    public Date getData_agendado() {
        return data_agendado;
    }

    public void setData_agendado(Date data_agendado) {
        this.data_agendado = data_agendado;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.name);
        dest.writeString(this.description);
        dest.writeLong(this.date != null ? this.date.getTime() : -1);
        dest.writeString(this.type);
        dest.writeString(this.image);
        dest.writeString(this.obs);
        dest.writeInt(this.buildId);
        dest.writeString(this.userId);
        dest.writeInt(this.isFree);
        dest.writeString(this.userIdCreated);
        dest.writeByte(this.scheduled ? (byte) 1 : (byte) 0);
        dest.writeLong(this.data_liberado != null ? this.data_liberado.getTime() : -1);
    }

    protected Visit(Parcel in) {
        this.id = in.readLong();
        this.name = in.readString();
        this.description = in.readString();
        long tmpDate = in.readLong();
        this.date = tmpDate == -1 ? null : new Date(tmpDate);
        this.type = in.readString();
        this.image = in.readString();
        this.obs = in.readString();
        this.buildId = in.readInt();
        this.userId = in.readString();
        this.isFree = in.readInt();
        this.userIdCreated = in.readString();
        this.scheduled = in.readByte() != 0;
        long tmpData_liberado = in.readLong();
        this.data_liberado = tmpData_liberado == -1 ? null : new Date(tmpData_liberado);
        long tmpData_agendado = in.readLong();
    }

    public static final Creator<Visit> CREATOR = new Creator<Visit>() {
        @Override
        public Visit createFromParcel(Parcel source) {
            return new Visit(source);
        }

        @Override
        public Visit[] newArray(int size) {
            return new Visit[size];
        }
    };
}