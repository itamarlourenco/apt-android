package br.com.apt.ui.myapt;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import br.com.apt.application.BaseActivity;
import br.com.apt.ui.reservations.CalendarFragment;

public class MyAptActivity extends BaseActivity {

    private Fragment myAptFragment;
    public static Intent newIntent(Context context) {
        return new Intent(context, MyAptActivity.class);
    }

    public static Intent newIntent(Context context, int position) {
        return intentWithPosition(new Intent(context, MyAptActivity.class), position);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public Fragment getFragment(){
        if(this.myAptFragment == null){
            this.myAptFragment =  MyAptFragment.newInstance(getPositionFragment());
        }
        return this.myAptFragment;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        MyAptFragment myAptFragment = (MyAptFragment) getFragment();
        myAptFragment.onActivityResultFragment(requestCode, resultCode, data);
    }
}