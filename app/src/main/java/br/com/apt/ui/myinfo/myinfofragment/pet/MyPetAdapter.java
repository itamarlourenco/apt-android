package br.com.apt.ui.myinfo.myinfofragment.pet;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;

import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.apt.R;
import br.com.apt.application.Volley;
import br.com.apt.widget.CustomTextViewLight;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by adminbs on 7/18/17.
 */

public class MyPetAdapter extends BaseAdapter {

    private Context context;
    private List<Pet> pets;

    public MyPetAdapter(Context context, List<Pet> pets) {
        this.context = context;
        this.pets = pets;
    }

    @Override
    public int getCount() {
        return pets != null ? pets.size() : 0;
    }

    @Override
    public Pet getItem(int position) {
        return pets.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        MyPetAdapter.ViewHolder holder;
        if(convertView == null){
            view = LayoutInflater.from(context).inflate(R.layout.adapter_my_pets, parent, false);
            holder = new MyPetAdapter.ViewHolder(view);
            view.setTag(holder);
        }else{
            view = convertView;
            holder = (MyPetAdapter.ViewHolder) view.getTag();
        }

        bindList(holder, position);
        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private void bindList(MyPetAdapter.ViewHolder holder, int position) {
        if(position % 2 == 0){
            holder.baseBackground.setBackground(ContextCompat.getDrawable(context, R.drawable.button_menu_list_dark));
        }else{
            holder.baseBackground.setBackground(ContextCompat.getDrawable(context, R.drawable.button_menu_list_light));
        }

        Pet pet = getItem(position);
        if(pet != null){
            Picasso.with(context).load(Volley.getUrlByImage(pet.getImage())).placeholder(R.drawable.placeholder_camera).into(holder.image);
            holder.title.setText(pet.getName());

            Pet.Breed breed = pet.getBreed();
            if(breed != null){
                holder.description.setText(breed.getName());
            }

            Pet.Type type = pet.getType();
            if(type != null){
                holder.label.setText(type.getName() != null ? type.getName().toUpperCase() : null);
            }
        }
    }

    public class ViewHolder{
        private RelativeLayout baseBackground;
        private CircleImageView image;
        private CustomTextViewLight title;
        private CustomTextViewLight description;
        private CustomTextViewLight label;

        public ViewHolder(View view) {
            baseBackground = (RelativeLayout) view.findViewById(R.id.baseBackground);
            image = (CircleImageView) view.findViewById(R.id.deliveryImage);
            title = (CustomTextViewLight) view.findViewById(R.id.deliveryTitle);
            description = (CustomTextViewLight) view.findViewById(R.id.deliveryDescription);
            label = (CustomTextViewLight) view.findViewById(R.id.deliveryLabel);
        }
    }
}
