package br.com.apt.ui.myapt.myaptFragments.poll;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.apt.application.ModelObject;
import br.com.apt.model.user.User;

/**
 * Created by adminbs on 1/12/16.
 */
public class Poll implements ModelObject{
    private int id;
    private String company_key;
    private String condominium_key;
    private String block_key;
    private String title;
    private String description;
    private Date start_at;
    private Date end_at;
    private int enabled;
    private String image_etag;
    private String image_url;
    private String type;
    private int qtd_units;
    private String created_user_id;
    private Date created_at;
    private Date updated_at;
    private List<Options> options;
    private int total_answers;
    private boolean voted;
    private UserVoted user_voted;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCompany_key() {
        return company_key;
    }

    public void setCompany_key(String company_key) {
        this.company_key = company_key;
    }

    public String getCondominium_key() {
        return condominium_key;
    }

    public void setCondominium_key(String condominium_key) {
        this.condominium_key = condominium_key;
    }

    public String getBlock_key() {
        return block_key;
    }

    public void setBlock_key(String block_key) {
        this.block_key = block_key;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartAt() {
        return start_at;
    }

    public void setStart_at(Date start_at) {
        this.start_at = start_at;
    }

    public Date getEndAt() {
        return end_at;
    }

    public void setEnd_at(Date end_at) {
        this.end_at = end_at;
    }

    public int getEnabled() {
        return enabled;
    }

    public void setEnabled(int enabled) {
        this.enabled = enabled;
    }

    public String getImage_etag() {
        return image_etag;
    }

    public void setImage_etag(String image_etag) {
        this.image_etag = image_etag;
    }

    public String getImageUrl() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getQtd_units() {
        return qtd_units;
    }

    public void setQtd_units(int qtd_units) {
        this.qtd_units = qtd_units;
    }

    public String getCreated_user_id() {
        return created_user_id;
    }

    public void setCreated_user_id(String created_user_id) {
        this.created_user_id = created_user_id;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public Date getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(Date updated_at) {
        this.updated_at = updated_at;
    }

    public List<Options> getOptions() {
        return options;
    }

    public void setOptions(List<Options> options) {
        this.options = options;
    }

    public int getTotal_answers() {
        return total_answers;
    }

    public void setTotal_answers(int total_answers) {
        this.total_answers = total_answers;
    }

    public boolean isVoted() {
        return voted;
    }

    public void setVoted(boolean voted) {
        this.voted = voted;
    }

    public UserVoted getUser_voted() {
        return user_voted;
    }

    public void setUser_voted(UserVoted user_voted) {
        this.user_voted = user_voted;
    }

    public static class Options implements Parcelable {

        private int id;
        private int poll_id;
        private String value;
        private String custom_option;
        private String image_etag;
        private String image_url;
        private Date created_at;
        private int total_answers;
        private float percentage;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getPoll_id() {
            return poll_id;
        }

        public void setPoll_id(int poll_id) {
            this.poll_id = poll_id;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getCustom_option() {
            return custom_option;
        }

        public void setCustom_option(String custom_option) {
            this.custom_option = custom_option;
        }

        public String getImage_etag() {
            return image_etag;
        }

        public void setImage_etag(String image_etag) {
            this.image_etag = image_etag;
        }

        public String getImage_url() {
            return image_url;
        }

        public void setImage_url(String image_url) {
            this.image_url = image_url;
        }

        public Date getCreated_at() {
            return created_at;
        }

        public void setCreated_at(Date created_at) {
            this.created_at = created_at;
        }

        public int getTotal_answers() {
            return total_answers;
        }

        public void setTotal_answers(int total_answers) {
            this.total_answers = total_answers;
        }

        public float getPercentage() {
            return percentage;
        }

        public void setPercentage(float percentage) {
            this.percentage = percentage;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.id);
            dest.writeInt(this.poll_id);
            dest.writeString(this.value);
            dest.writeString(this.custom_option);
            dest.writeString(this.image_etag);
            dest.writeString(this.image_url);
            dest.writeLong(this.created_at != null ? this.created_at.getTime() : -1);
            dest.writeInt(this.total_answers);
            dest.writeFloat(this.percentage);
        }

        public Options() {
        }

        protected Options(Parcel in) {
            this.id = in.readInt();
            this.poll_id = in.readInt();
            this.value = in.readString();
            this.custom_option = in.readString();
            this.image_etag = in.readString();
            this.image_url = in.readString();
            long tmpCreated_at = in.readLong();
            this.created_at = tmpCreated_at == -1 ? null : new Date(tmpCreated_at);
            this.total_answers = in.readInt();
            this.percentage = in.readFloat();
        }

        public static final Creator<Options> CREATOR = new Creator<Options>() {
            @Override
            public Options createFromParcel(Parcel source) {
                return new Options(source);
            }

            @Override
            public Options[] newArray(int size) {
                return new Options[size];
            }
        };
    }

    public static class UserVoted implements Parcelable {
        private String name;
        private String image;
        private Option option;

        protected UserVoted(Parcel in) {
            name = in.readString();
            image = in.readString();
            option = in.readParcelable(Option.class.getClassLoader());
        }

        public static final Creator<UserVoted> CREATOR = new Creator<UserVoted>() {
            @Override
            public UserVoted createFromParcel(Parcel in) {
                return new UserVoted(in);
            }

            @Override
            public UserVoted[] newArray(int size) {
                return new UserVoted[size];
            }
        };

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public Option getOption() {
            return option;
        }

        public void setOption(Option option) {
            this.option = option;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(this.name);
            parcel.writeString(this.image);
            parcel.writeParcelable(this.option, i);
        }

        public static class Option implements Parcelable {
            public int id;

            protected Option(Parcel in) {
                id = in.readInt();
            }

            public static final Creator<Option> CREATOR = new Creator<Option>() {
                @Override
                public Option createFromParcel(Parcel in) {
                    return new Option(in);
                }

                @Override
                public Option[] newArray(int size) {
                    return new Option[size];
                }
            };

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel parcel, int i) {
                parcel.writeInt(id);
            }
        }
    }

    public Poll() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.company_key);
        dest.writeString(this.condominium_key);
        dest.writeString(this.block_key);
        dest.writeString(this.title);
        dest.writeString(this.description);
        dest.writeLong(this.start_at != null ? this.start_at.getTime() : -1);
        dest.writeLong(this.end_at != null ? this.end_at.getTime() : -1);
        dest.writeInt(this.enabled);
        dest.writeString(this.image_etag);
        dest.writeString(this.image_url);
        dest.writeString(this.type);
        dest.writeInt(this.qtd_units);
        dest.writeString(this.created_user_id);
        dest.writeLong(this.created_at != null ? this.created_at.getTime() : -1);
        dest.writeLong(this.updated_at != null ? this.updated_at.getTime() : -1);
        dest.writeTypedList(this.options);
        dest.writeInt(this.total_answers);
        dest.writeByte(this.voted ? (byte) 1 : (byte) 0);
        dest.writeParcelable(this.user_voted, flags);
    }

    protected Poll(Parcel in) {
        this.id = in.readInt();
        this.company_key = in.readString();
        this.condominium_key = in.readString();
        this.block_key = in.readString();
        this.title = in.readString();
        this.description = in.readString();
        long tmpStart_at = in.readLong();
        this.start_at = tmpStart_at == -1 ? null : new Date(tmpStart_at);
        long tmpEnd_at = in.readLong();
        this.end_at = tmpEnd_at == -1 ? null : new Date(tmpEnd_at);
        this.enabled = in.readInt();
        this.image_etag = in.readString();
        this.image_url = in.readString();
        this.type = in.readString();
        this.qtd_units = in.readInt();
        this.created_user_id = in.readString();
        long tmpCreated_at = in.readLong();
        this.created_at = tmpCreated_at == -1 ? null : new Date(tmpCreated_at);
        long tmpUpdated_at = in.readLong();
        this.updated_at = tmpUpdated_at == -1 ? null : new Date(tmpUpdated_at);
        this.options = in.createTypedArrayList(Options.CREATOR);
        this.total_answers = in.readInt();
        this.voted = in.readByte() != 0;
        this.user_voted = in.readParcelable(UserVoted.class.getClassLoader());
    }

    public static final Creator<Poll> CREATOR = new Creator<Poll>() {
        @Override
        public Poll createFromParcel(Parcel source) {
            return new Poll(source);
        }

        @Override
        public Poll[] newArray(int size) {
            return new Poll[size];
        }
    };
}