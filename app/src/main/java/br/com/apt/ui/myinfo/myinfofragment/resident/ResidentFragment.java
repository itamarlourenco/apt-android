package br.com.apt.ui.myinfo.myinfofragment.resident;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.github.ybq.android.spinkit.SpinKitView;

import java.util.HashMap;
import java.util.List;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.model.user.User;
import br.com.apt.ui.ExceptionWithoutResult;
import br.com.apt.ui.myinfo.myinfofragment.FragmentMyInfoAdd;
import br.com.apt.ui.myinfo.myinfofragment.NewUser;
import br.com.apt.ui.myinfo.myinfofragment.pet.MyPetAddActivity;
import br.com.apt.ui.myinfo.myinfofragment.pet.Pet;
import br.com.apt.util.Util;
import br.com.apt.widget.PagerItemFragment;

/**
 * Created by fabricio.bezerra on 22/09/2016.
 */
public class ResidentFragment extends FragmentMyInfoAdd implements Volley.Callback {
    private SwipeMenuListView listView;
    private List<NewUser> residents;
    private SpinKitView loader;

    public static PagerItemFragment newInstance(){
        return new ResidentFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_resident, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if (view != null) {
            listView = (SwipeMenuListView) view.findViewById(R.id.listView);
            loader = (SpinKitView) view.findViewById(R.id.loader);

            listView.setMenuCreator(new SwipeMenuCreator() {
                @Override
                public void create(SwipeMenu menu) {
                    SwipeMenuItem openItem = new SwipeMenuItem(getContext());
                    openItem.setBackground(new ColorDrawable(Color.rgb(0xF9, 0x3F, 0x25)));
                    openItem.setWidth(120);
                    openItem.setTitle("X");
                    openItem.setTitleSize(18);
                    openItem.setTitleColor(Color.BLACK);
                    menu.addMenuItem(openItem);
                }
            });

            listView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                    switch (index) {
                        case 0:
                            NewUser newUser = residents.get(position);
                            removeItem(newUser.getId(), new CallbackDelete() {
                                @Override
                                public void delete() {
                                    getResidents();
                                }
                            });
                            break;
                    }
                    return false;
                }
            });

            view.findViewById(R.id.sendResident).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(ResidentsAddActivity.newIntent(getContext()));
                    getActivity().overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
                }
            });

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    NewUser newUser = residents.get(position);
                    startActivity(ResidentsAddActivity.newIntent(getContext(), newUser));
                    getActivity().overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
                }
            });
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getResidents();
    }

    public void getResidents() {
        Volley volley = new Volley(this);
        volley.request();
    }

    @Override
    public void result(String result, int type) {
        View view = getView();
        try{
            Util.hidePlaceholder(view);
            Util.changeVisibility(loader, View.GONE, true);
            Util.changeVisibility(listView, View.VISIBLE, true);
            ResidentRequest residentRequest = App.getGson().fromJson(result, ResidentRequest.class);
            residents = residentRequest.getObject();
            listView.setAdapter(new ResidentAdapter(getContext(), residents));

            if(residents == null){
                throw new ExceptionWithoutResult(getString(R.string.message_error_resident), R.drawable.img_notificacoes_no_itens);
            }
        }catch (ExceptionWithoutResult exceptionWithoutResult) {
            Util.showPlaceholder(view, getActivity(), exceptionWithoutResult.getDrawableId(), exceptionWithoutResult.getMessage());
        }catch (Exception exception){
            Util.handlePlaceholderError(view, getActivity());
        }
    }

    @Override
    public String uri() {
        return URI.RESIDENTS;
    }

    @Override
    protected String getPagerTitle() {
        return App.getContext().getString(R.string.resident);
    }

    @Override
    protected int getPagerIcon() {
        return R.drawable.ico_moradores;
    }

    @Override
    protected HashMap<String, String> save() {
        return null;
    }

    @Override
    protected String getUri() {
        return URI.RESIDENTS;
    }

    @Override
    protected int getTypeRequest() {
        return 0;
    }

    @Override
    protected void saved() {
    }

    @Override
    protected void saved(String json) {
    }
}
