package br.com.apt.ui.myapt.myaptFragments.plan;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;

import br.com.apt.R;
import br.com.apt.application.BaseActivity;
import br.com.apt.ui.myinfo.myinfofragment.pet.MyPetAddActivity;
import br.com.apt.ui.myinfo.myinfofragment.pet.MyPetAddFragment;
import br.com.apt.ui.myinfo.myinfofragment.pet.Pet;

/**
 * Created by adminbs on 2/23/18.
 */

public class PlantAddActivity extends BaseActivity {

    public static final String EXTRA_PLANT = "EXTRA_PLANT";
    private PlantAddFragment plantAddFragment = PlantAddFragment.newInstance();

    public static Intent newIntent(Context context, Plant plant) {
        Intent intent = new Intent(context, PlantAddActivity.class);
        intent.putExtra(EXTRA_PLANT, plant);
        return intent;
    }

    public static Intent newIntent(Context context) {
        return new Intent(context, PlantAddActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public Fragment getFragment(){
        Intent intent = getIntent();
        if(intent != null){
            Plant plant = intent.getParcelableExtra(PlantAddActivity.EXTRA_PLANT);
            plantAddFragment.setPlant(plant);
        }

        return plantAddFragment;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        plantAddFragment.onActivityResultFragment(requestCode, resultCode, data);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_down, R.anim.slide_out_down);
    }
}