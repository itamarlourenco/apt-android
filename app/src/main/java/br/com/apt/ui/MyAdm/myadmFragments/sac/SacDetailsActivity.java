package br.com.apt.ui.MyAdm.myadmFragments.sac;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import br.com.apt.R;
import br.com.apt.application.BaseActivity;
import br.com.apt.ui.MyAdm.Occurrences;
import br.com.apt.ui.reservations.CalendarFragment;

/**
 * Created by adminbs on 8/4/17.
 */

public class SacDetailsActivity extends BaseActivity{

    private static final String EXTRA_OCCURRENCE = "EXTRA_OCCURRENCE";
    private static final String EXTRA_IS_NOTIFICATION = "EXTRA_IS_NOTIFICATION";

    public static Intent newIntent(Context context, Occurrences occurrences) {
        Intent intent = new Intent(context, SacDetailsActivity.class);
        intent.putExtra(SacDetailsActivity.EXTRA_OCCURRENCE, occurrences);
        return intent;
    }

    public static Intent newIntent(Context context, Occurrences occurrences, boolean isBoolean) {
        Intent intent = new Intent(context, SacDetailsActivity.class);
        intent.putExtra(SacDetailsActivity.EXTRA_OCCURRENCE, occurrences);
        intent.putExtra(SacDetailsActivity.EXTRA_IS_NOTIFICATION, isBoolean);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public Fragment getFragment(){
        Intent intent = getIntent();
        if (intent != null) {
            Occurrences occurrences = intent.getParcelableExtra(SacDetailsActivity.EXTRA_OCCURRENCE);
            SacDetailsFragment sacDetailsFragment = SacDetailsFragment.newInstance();
            sacDetailsFragment.setOccurrence(occurrences);

            boolean isNotification = intent.getBooleanExtra(EXTRA_IS_NOTIFICATION, false);
            if (isNotification) {
                setTitle(getString(R.string.notification));
                setTitleToolBar(getString(R.string.notification));
            }

            return sacDetailsFragment;
        }

        return null;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void finish() {
        super.finish();
    }
}
