package br.com.apt.ui.MyAdm;

import java.util.List;

import br.com.apt.application.BaseGsonMeuAdm;

/**
 * Created by adminbs on 7/31/17.
 */

public class OccurrencesRequest extends BaseGsonMeuAdm {
    private List<Occurrences> object;

    public List<Occurrences> getObject() {
        return object;
    }

    public void setObject(List<Occurrences> object) {
        this.object = object;
    }
}