package br.com.apt.ui.myinfo.myinfofragment.vehicle;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;

import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.apt.R;
import br.com.apt.application.BaseFragment;
import br.com.apt.application.Volley;
import br.com.apt.ui.myinfo.myinfofragment.employee.Employees;
import br.com.apt.ui.myinfo.myinfofragment.employee.EmployeesAdapter;
import br.com.apt.widget.CustomTextViewLight;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by adminbs on 7/20/17.
 */

class VehicleAdapter extends BaseAdapter {

    private Context context;
    private List<Vehicle> vehiclesList;

    public VehicleAdapter(Context context, List<Vehicle> vehiclesList) {
        this.context = context;
        this.vehiclesList = vehiclesList;
    }

    @Override
    public int getCount() {
        return vehiclesList != null ? vehiclesList.size() : 0;
    }

    @Override
    public Vehicle getItem(int position) {
        return vehiclesList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        VehicleAdapter.ViewHolder holder;
        if(convertView == null){
            view = LayoutInflater.from(context).inflate(R.layout.adapter_employees, parent, false);
            holder = new VehicleAdapter.ViewHolder(view);
            view.setTag(holder);
        }else{
            view = convertView;
            holder = (VehicleAdapter.ViewHolder) view.getTag();
        }

        bindList(holder, position);
        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private void bindList(VehicleAdapter.ViewHolder holder, int position) {
        if(position % 2 == 0){
            holder.baseBackground.setBackground(ContextCompat.getDrawable(context, R.drawable.button_menu_list_dark));
        }else{
            holder.baseBackground.setBackground(ContextCompat.getDrawable(context, R.drawable.button_menu_list_light));
        }

        Vehicle vehicle = getItem(position);
        if(vehicle != null){
            Picasso.with(context).load(Volley.getUrlByImage(vehicle.getImage())).placeholder(R.drawable.placeholder_camera).into(holder.image);

            Vehicle.CarModel carModel = vehicle.getCarModel();
            if(carModel != null){
                holder.title.setText(carModel.getModelo());
            }

            Vehicle.CarBrand carBrand = vehicle.getCarBrand();
            if(carBrand != null){
                holder.description.setText(carBrand.getMarca());
                Vehicle.CarYear car_year = vehicle.getCar_year();
                if(car_year != null){
                    holder.description.setText(carBrand.getMarca() + " - " + car_year.getAno());
                }

                holder.vehicleLabel.setVisibility(View.VISIBLE);
                holder.vehicleLabel.setText(vehicle.getType().equals(Vehicle.TYPE_MOTORCYCLE) ? context.getString(R.string.motorcicle) : context.getString(R.string.vehicle));
            }


        }
    }

    public class ViewHolder{
        private RelativeLayout baseBackground;
        private CircleImageView image;
        private CustomTextViewLight title;
        private CustomTextViewLight description;
        private CustomTextViewLight vehicleLabel;

        public ViewHolder(View view) {
            baseBackground = (RelativeLayout) view.findViewById(R.id.baseBackground);
            image = (CircleImageView) view.findViewById(R.id.deliveryImage);
            title = (CustomTextViewLight) view.findViewById(R.id.deliveryTitle);
            description = (CustomTextViewLight) view.findViewById(R.id.deliveryDescription);
            vehicleLabel = (CustomTextViewLight) view.findViewById(R.id.vehicleLabel);
        }
    }
}
