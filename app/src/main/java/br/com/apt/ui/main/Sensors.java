package br.com.apt.ui.main;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.concurrent.ExecutionException;

import br.com.apt.application.BaseGson;
import br.com.apt.application.ModelObject;
import br.com.apt.application.SendRequest;
import br.com.apt.model.Condominium;
import br.com.apt.util.Logger;

/**
 * Created by adminbs on 5/31/16.
 */
public class Sensors extends SendRequest implements ModelObject {
    public long id;

    @SerializedName("condominium_id")
    public long condominiumId;

    @SerializedName("sensor_type_id")
    public long sensorTypeId;

    public String value;

    @SerializedName("created_at")
    public Date createdAt;

    private Type type;
    private Condominium condominium;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getCondominiumId() {
        return condominiumId;
    }

    public void setCondominiumId(long condominiumId) {
        this.condominiumId = condominiumId;
    }

    public long getSensorTypeId() {
        return sensorTypeId;
    }

    public void setSensorTypeId(long sensorTypeId) {
        this.sensorTypeId = sensorTypeId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Condominium getCondominium() {
        return condominium;
    }

    public void setCondominium(Condominium condominium) {
        this.condominium = condominium;
    }

    public class Type{
        public long id;
        public String name;
        public String icon;
        public String unit;

        @SerializedName("created_at")
        public Date createdAt;

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getIcon() {
            return icon;
        }

        public void setIcon(String icon) {
            this.icon = icon;
        }

        public String getUnit() {
            return unit;
        }

        public void setUnit(String unit) {
            this.unit = unit;
        }

        public Date getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(Date createdAt) {
            this.createdAt = createdAt;
        }
    }

    protected Sensors(Parcel in) {

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        try{
            dest.writeLong(id);
            dest.writeLong(condominiumId);
            dest.writeLong(sensorTypeId);
            dest.writeString(value);
            dest.writeLong(createdAt != null ? createdAt.getTime() : -1L);
            dest.writeValue(type);
            dest.writeValue(condominium);
        }catch (Exception e){
            Logger.e(e.getMessage());
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Sensors> CREATOR = new Parcelable.Creator<Sensors>() {
        @Override
        public Sensors createFromParcel(Parcel in) {
            return new Sensors(in);
        }

        @Override
        public Sensors[] newArray(int size) {
            return new Sensors[size];
        }
    };
}