package br.com.apt.ui.setting.settingFragment.password;

import com.google.gson.annotations.SerializedName;

import br.com.apt.application.SendRequest;

/**
 * Created by adminbs on 1/20/16.
 */
public class PasswordSend extends SendRequest {
    private String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
