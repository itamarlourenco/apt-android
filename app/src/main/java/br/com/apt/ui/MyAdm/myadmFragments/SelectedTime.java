package br.com.apt.ui.MyAdm.myadmFragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import br.com.apt.R;
import br.com.apt.application.BaseActivity;
import br.com.apt.ui.MyAdm.myadmFragments.cashflow.CashFlowAndAccountFragment;
import br.com.apt.ui.reservations.CalendarFragment;

/**
 * Created by adminbs on 9/5/17.
 */

public class SelectedTime extends BaseActivity {
    public static final String EXTRA_YEAR = "EXTRA_YEAR";
    public static final String EXTRA_MONTH = "EXTRA_MONTH";
    public static final String EXTRA_TYPE = "EXTRA_TYPE";


    public static Intent newIntent(Context context, int month, int year, String type) {
        Intent intent = new Intent(context, SelectedTime.class);
        intent.putExtra(EXTRA_YEAR, year);
        intent.putExtra(EXTRA_MONTH, month);
        intent.putExtra(EXTRA_TYPE, type);

        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public Fragment getFragment(){
        Intent intent = getIntent();
        if(intent != null){
            int year = intent.getIntExtra(EXTRA_YEAR, 0);
            int month = intent.getIntExtra(EXTRA_MONTH, 0);
            String type = intent.getStringExtra(EXTRA_TYPE);

            if(type.equals(SelectTime.ACCOUNT)){
               setTitleToolBar(getString(R.string.accounts));
                setTitle(getString(R.string.accounts));
            }else if(type.equals(SelectTime.CASH_FLOW)){
                setTitleToolBar(getString(R.string.cash_flow));
                setTitle(getString(R.string.cash_flow));
            }

            return CashFlowAndAccountFragment.newInstance(month, year, type);
        }
        return null;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_down, R.anim.slide_out_down);
    }
}
