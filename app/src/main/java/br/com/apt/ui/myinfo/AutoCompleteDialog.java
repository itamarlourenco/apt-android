package br.com.apt.ui.myinfo;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import br.com.apt.R;
import br.com.apt.ui.myinfo.myinfofragment.AutoComplete;
import br.com.apt.util.Logger;
import br.com.apt.widget.CustomEditText;
import br.com.apt.widget.CustomTextView;
import br.com.apt.widget.CustomTextViewBold;
import br.com.apt.widget.CustomTextViewLight;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * {@link DialogFragment} implementation that presents the Terms of Use to the user.
 */
public class AutoCompleteDialog extends DialogFragment implements View.OnClickListener {

    /** Tag for this fragment. */
    private static final String FRAG_TAG = "autoComplete";
    private static List<AutoComplete> list;
    private static List<AutoComplete> listed;
    private static AutoCompleteDialog.Click clickListener;
    private CustomEditText search;
    private static AutoCompleteAdapter autoCompleteAdapter;
    private static String title;

    /**
     * Shows an instance of this fragment to present the Terms of Use to the user.
     * @param fragmentManager a {@link FragmentManager} object to show this {@link DialogFragment}.
     */
    public static void show(FragmentManager fragmentManager, List<AutoComplete> list, AutoCompleteDialog.Click click, String title) {
        if (fragmentManager.findFragmentByTag(FRAG_TAG) == null) {
            AutoCompleteDialog fragment = new AutoCompleteDialog();
            AutoCompleteDialog.list = list;
            AutoCompleteDialog.listed = list;
            AutoCompleteDialog.clickListener = click;
            AutoCompleteDialog.title = title;
            fragment.show(fragmentManager, FRAG_TAG);
        }
    }
    public static void show(FragmentManager fragmentManager, List<AutoComplete> list, AutoCompleteDialog.Click click) {
        show(fragmentManager, list, click, null);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Window window = getDialog().getWindow();
        if(window != null){
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }

        View view = inflater.inflate(R.layout.fragment_auto_complete, container, false);
        view.findViewById(R.id.closeDialog).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeDialog();
            }
        });

        ListView listView = (ListView) view.findViewById(R.id.listView);
        AutoCompleteDialog.autoCompleteAdapter = new AutoCompleteAdapter();
        listView.setAdapter(AutoCompleteDialog.autoCompleteAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                AutoComplete autoComplete = AutoCompleteDialog.listed.get(position);
                clickListener.click(autoComplete);
                closeDialog();
            }
        });

        view.findViewById(R.id.upClose).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeDialog();
            }
        });

        CustomTextViewLight title = (CustomTextViewLight) view.findViewById(R.id.title);
        if(!TextUtils.isEmpty(AutoCompleteDialog.title)){
            title.setText(AutoCompleteDialog.title);
        }
        search = (CustomEditText) view.findViewById(R.id.search);

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                AutoCompleteDialog.autoCompleteAdapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        search.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == KeyEvent.KEYCODE_DEL){
                    AutoCompleteDialog.autoCompleteAdapter.getFilter().filter("");
                }
                return false;
            }
        });

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.closeDialog:
                closeDialog();
                break;
        }
    }

    private void closeDialog() {
        dismiss();
    }

    @Override
    public int getTheme() {
        return R.style.CustomDialog;
    }


    public class AutoCompleteAdapter extends BaseAdapter implements Filterable{
        private FriendFilter friendFilter;

        public AutoCompleteAdapter() {
            getFilter();
        }

        @Override
        public int getCount() {
            return AutoCompleteDialog.listed == null ? 0 : AutoCompleteDialog.listed.size();
        }

        @Override
        public AutoComplete getItem(int position) {
            return AutoCompleteDialog.listed.get(position);
        }

        @Override
        public long getItemId(int position) {
            return getItem(position).getId();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;
            if(convertView == null) {
                convertView = getActivity().getLayoutInflater().inflate(R.layout.adapter_auto_complete, parent, false);

                viewHolder = new ViewHolder();
                viewHolder.item = (LinearLayout) convertView.findViewById(R.id.item);
                viewHolder.name = (CustomTextViewLight) convertView.findViewById(R.id.name);
                viewHolder.image = (CircleImageView) convertView.findViewById(R.id.profile_image);

                convertView.setTag(viewHolder);
            }else{
                viewHolder = (ViewHolder) convertView.getTag();
            }

            AutoComplete autoComplete = getItem(position);
            if(autoComplete != null){
                viewHolder.name.setText(autoComplete.getName());
            }

            return convertView;
        }

        @Override
        public Filter getFilter() {
            if (friendFilter == null) {
                friendFilter = new FriendFilter();
            }

            return friendFilter;
        }


        public class ViewHolder{
            public LinearLayout item;
            public CustomTextViewLight name;
            public CircleImageView image;
        }

        private class FriendFilter extends Filter {

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint != null && constraint.length() > 0) {

                    ArrayList<AutoComplete> tempList = new ArrayList<>();

                    for (AutoComplete autoComplete: AutoCompleteDialog.list) {
                        if (autoComplete.getName().toLowerCase().contains(constraint.toString().toLowerCase())) {
                            tempList.add(autoComplete);
                        }
                    }

                    filterResults.count = tempList.size();
                    filterResults.values = tempList;
                } else {
                    filterResults.count = AutoCompleteDialog.list.size();
                    filterResults.values = AutoCompleteDialog.list;
                }

                return filterResults;
            }


            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                AutoCompleteDialog.listed = (ArrayList<AutoComplete>) results.values;
                notifyDataSetChanged();
            }
        }
    }


    public interface Click{
        void click(AutoComplete autoComplete);
    }
}
