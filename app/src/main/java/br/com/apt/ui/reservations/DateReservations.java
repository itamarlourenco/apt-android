package br.com.apt.ui.reservations;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import br.com.apt.util.Logger;

/**
 * Created by adminbs on 9/9/17.
 */

public class DateReservations {
    private String dateInit;
    private String dateEnd;

    public DateReservations(String dateInit, String dateEnd) {
        this.dateInit = dateInit;
        this.dateEnd = dateEnd;
    }

    public String getDateInit() {
        return dateInit;
    }

    public void setDateInit(String dateInit) {
        this.dateInit = dateInit;
    }

    public String getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }

    public void rangeTime(String fraction){
        DateFormat df = new SimpleDateFormat("HH:mm:ss", Locale.US);

        Calendar startDate = Calendar.getInstance();
        String[] splitStartDate = dateInit.split(":");
        if(splitStartDate.length >= 2){
            int hours = Integer.parseInt(splitStartDate[0]);
            int minute = Integer.parseInt(splitStartDate[1]);
            int seconds = Integer.parseInt(splitStartDate[2]);

            startDate.set(Calendar.HOUR_OF_DAY, hours);
            startDate.set(Calendar.MINUTE, minute);
            startDate.set(Calendar.SECOND, seconds);
        }

        Calendar endDate = Calendar.getInstance();
        String[] splitEndDate = dateEnd.split(":");
        if(splitEndDate.length >= 2){
            int hours = Integer.parseInt(splitStartDate[0]);
            int minute = Integer.parseInt(splitStartDate[1]);
            int seconds = Integer.parseInt(splitStartDate[2]);

            endDate.set(Calendar.HOUR_OF_DAY, hours);
            endDate.set(Calendar.MINUTE, minute);
            endDate.set(Calendar.SECOND, seconds);
        }

        String[] split = fraction.split(":");
        if(split.length >= 2){
            try{
                int hours = Integer.parseInt(split[0]);
                int minute = Integer.parseInt(split[1]);
                int seconds = Integer.parseInt(split[2]);

                Calendar cal = Calendar.getInstance();
                cal.set(Calendar.HOUR_OF_DAY, 0);
                cal.set(Calendar.MINUTE, 0);
                cal.set(Calendar.SECOND, 0);

                while (cal.get(Calendar.HOUR) >= startDate.get(Calendar.HOUR) &&
                        cal.get(Calendar.MINUTE) >= startDate.get(Calendar.MINUTE) &&
                        cal.get(Calendar.SECOND) >= startDate.get(Calendar.SECOND) &&

                        cal.get(Calendar.HOUR) <= endDate.get(Calendar.HOUR) &&
                        cal.get(Calendar.MINUTE) <= endDate.get(Calendar.MINUTE) &&
                        cal.get(Calendar.SECOND) <= endDate.get(Calendar.SECOND)){
                    if(hours > 0){
                        cal.add(Calendar.HOUR, hours);
                    }
                    if(minute > 0){
                        cal.add(Calendar.MINUTE, minute);
                    }
                    if(seconds > 0){
                        cal.add(Calendar.SECOND, seconds);
                    }

                    Logger.t(df.format(cal.getTime()));
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}
