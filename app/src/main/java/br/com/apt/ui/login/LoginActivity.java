package br.com.apt.ui.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import br.com.apt.application.App;
import br.com.apt.application.BaseActivity;
import br.com.apt.ui.firstAccess.animations.AnimationActivity;
import br.com.apt.ui.reservations.CalendarFragment;

public class LoginActivity extends BaseActivity {

    public static final String PUT_USERNAME = "EXTRA_USERNAME";
    public static final String PUT_PASSWORD = "EXTRA_PASSWORD";

    public static Intent newIntent(Context context) {
        return new Intent(context, LoginActivity.class);
    }

    public static Intent newIntent(Context context, String username, String passowrd) {
        Intent intent =  new Intent(context, LoginActivity.class);
        intent.putExtra(PUT_USERNAME, username);
        intent.putExtra(PUT_PASSWORD, passowrd);

        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.getAppPreferences().setFirstAccess(true);

        startActivity(AnimationActivity.newIntent(this));
        finish();
        return;
//
//        if(getToolBar() != null){
//            getToolBar().setVisibility(View.INVISIBLE);
//        }
//
//
//        User user = getUser();
//        if(user != null){
//            startActivity(MainActivity.newIntent(this));
//            finish();
//        }
    }

    @Override
    public Fragment getFragment() {
        return LoginFragment.newInstance();
    }

    @Override
    protected boolean showNavigationDrawer() {
        return false;
    }

    @Override
    protected boolean showToolbarLogo() {
        return true;
    }

    @Override
    protected boolean checkLogin() {
        return false;
    }
}
