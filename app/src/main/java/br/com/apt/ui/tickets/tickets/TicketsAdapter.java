package br.com.apt.ui.tickets.tickets;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import br.com.apt.R;
import br.com.apt.application.Volley;
import br.com.apt.ui.lobby.lobbyFragments.deliveries.DeliveriesListViewAdapter;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomTextView;
import br.com.apt.widget.CustomTextViewLight;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by adminbs on 7/14/17.
 */

public class TicketsAdapter extends BaseAdapter {
    private List<Ticket> tickets;
    private Context context;

    public TicketsAdapter(Context context, List<Ticket> tickets) {
        this.tickets = tickets;
        this.context = context;
    }

    @Override
    public int getCount() {
        return tickets.size();
    }

    @Override
    public Ticket getItem(int position) {
        return tickets.get(position);
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getId();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        TicketsAdapter.ViewHolder holder;
        if(convertView == null){
            view = LayoutInflater.from(context).inflate(R.layout.adapter_tickets, parent, false);
            holder = new TicketsAdapter.ViewHolder(view);
            view.setTag(holder);
        }else{
            view = convertView;
            holder = (TicketsAdapter.ViewHolder) view.getTag();
        }

        bindList(holder, position);
        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private void bindList(ViewHolder holder, int position) {
        if(position % 2 == 0){
            holder.baseBackground.setBackground(ContextCompat.getDrawable(context, R.drawable.button_menu_list_dark));
        }else{
            holder.baseBackground.setBackground(ContextCompat.getDrawable(context, R.drawable.button_menu_list_light));
        }

        Ticket ticket = getItem(position);
        if(ticket != null){

            Picasso.with(context).load(Volley.getUrlByImage(ticket.getImageString())).placeholder(R.drawable.placeholder_camera).into(holder.image);

            holder.title.setText(ticket.getTitle());

            holder.description.setText(
                    String.format(context.getText(R.string.description_delivery).toString(), Util.dateFormatted(ticket.getCreatedAt()), Util.timeFormatted(ticket.getCreatedAt()))
            );

            switch (ticket.getStatus()) {
                case Ticket.TODO:
                    holder.label.setText(context.getString(R.string.sending).toUpperCase());
                    holder.label.setBackground(ContextCompat.getDrawable(context, R.drawable.border_radius_blue));
                    holder.label.setTextColor(ContextCompat.getColor(context, R.color.faded_blue));
                    break;
                case Ticket.PROGRESS:
                    holder.label.setText(context.getString(Ticket.translateStatus(ticket.getStatus())).toUpperCase());
                    holder.label.setBackground(ContextCompat.getDrawable(context, R.drawable.border_radius_yellow));
                    holder.label.setTextColor(ContextCompat.getColor(context, R.color.leather));
                    break;
                case Ticket.DONE:
                    holder.label.setText(context.getString(Ticket.translateStatus(ticket.getStatus())).toUpperCase());
                    holder.label.setBackground(ContextCompat.getDrawable(context, R.drawable.border_radius_green));
                    holder.label.setTextColor(ContextCompat.getColor(context, R.color.pale_olive_green));
                    break;
            }
        }
    }

    public class ViewHolder{
        private RelativeLayout baseBackground;
        private CircleImageView image;
        private CustomTextViewLight title;
        private CustomTextViewLight description;
        private CustomTextViewLight label;

        public ViewHolder(View view) {
            baseBackground = (RelativeLayout) view.findViewById(R.id.baseBackground);
            image = (CircleImageView) view.findViewById(R.id.deliveryImage);
            title = (CustomTextViewLight) view.findViewById(R.id.deliveryTitle);
            description = (CustomTextViewLight) view.findViewById(R.id.deliveryDescription);
            label = (CustomTextViewLight) view.findViewById(R.id.deliveryLabel);
        }
    }
}
