package br.com.apt.ui.firstAccess.animations;

import android.annotation.TargetApi;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.VideoView;

import com.android.volley.Request;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import org.json.JSONObject;

import java.io.IOException;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.AppPreferences;
import br.com.apt.application.BaseFragment;
import br.com.apt.application.Config;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.model.user.User;
import br.com.apt.model.user.UserData;
import br.com.apt.model.user.UserMeuadmRequest;
import br.com.apt.model.user.UserRequest;
import br.com.apt.ui.firstAccess.singup.DialogTermsOfUse;
import br.com.apt.ui.login.Login;
import br.com.apt.ui.main.MainActivity;
import br.com.apt.util.Logger;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomTextViewLight;
import br.com.apt.widget.SendSuccessDialog;

/**
 * Created by adminbs on 10/3/16.
 */
public class FinishSingUpFragment extends BaseFragment implements Volley.Callback {
    private VideoView videoView;
    private Volley volley;
    private String username;
    private String password;
    private String token;

    public static Fragment newInstance() {
        return new FinishSingUpFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.finish_sing_up_fragment, container, false);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Intent intent = getActivity().getIntent();
        Bundle bundle = intent.getExtras();

        username = bundle.getString(FinishSingUpActivity.EXTRA_USERNAME);
        password = bundle.getString(FinishSingUpActivity.EXTRA_PASSWORD);
        final String name = bundle.getString(FinishSingUpActivity.EXTRA_NAME);
        final String apto = bundle.getString(FinishSingUpActivity.EXTRA_APTO);

        View view = getView();
        if(view != null){
            ((CustomTextViewLight) view.findViewById(R.id.name)).setText(name);
            ((CustomTextViewLight) view.findViewById(R.id.apto)).setText(String.format(getString(R.string.fromApartment), apto));

            CustomTextViewLight terms = (CustomTextViewLight) view.findViewById(R.id.terms);
            terms.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DialogTermsOfUse.show(getFragmentManager());
                }
            });
            SpannableString content = new SpannableString(terms.getText());
            content.setSpan(new UnderlineSpan(), 51, terms.getText().length(), 0);
            terms.setText(content);


            (view.findViewById(R.id.buttonFinish)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    doLogin(username, password);
                }
            });
        }
    }

    private void doLogin(String email, String password){
        try{
            volley = new Volley(this);
            volley.showDialog(getActivity());
            getTokenGcm(email, password);
        }catch (Exception e){
            View view = getView();
            if(view != null){
                Util.showDialog(getActivity(), e.getMessage());
            }
        }
    }

    protected void getTokenGcm(final String email, final String password) {
        new AsyncTask<Void, Void, Void>(){
            @Override
            protected Void doInBackground(Void... params) {
                InstanceID instanceID = InstanceID.getInstance(FinishSingUpFragment.this.getActivity());
                try {
                    token = instanceID.getToken(Config.GCM.SENDER_ID, GoogleCloudMessaging.INSTANCE_ID_SCOPE);
                    sendRegistrationToServer(token);
                    Login login = new Login(email, password, token);
                    volley.request(Request.Method.POST, login.toJson());
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return null;
            }
        }.execute();
    }

    private void sendRegistrationToServer(String token) {
        AppPreferences appPreferences = new AppPreferences();
        appPreferences.setAPNS(token);
    }

    @Override
    public void result(String result, int type) {
        View view = getView();
        if(result == null){
            Logger.t(getString(R.string.internet_error));
        }

        UserRequest user = App.getGson().fromJson(result, UserRequest.class);

        if(user != null && user.getStatus() == Volley.STATUS_OK){
            UserData.saveUser(user);
            getActivity().finish();
            startActivity(MainActivity.newIntent(getContext()));
        }else{
            if(view != null){
                checkMeuAdm(username, password);
            }
        }
    }

    @Override
    public String uri() {
        return URI.AUTHENTICATE;
    }


    private void checkMeuAdm(final String username, final String password) {
        Volley.Callback callback = new Volley.Callback() {
            @Override
            public void result(String result, int type) {
                UserMeuadmRequest userMeuadm = App.getGson().fromJson(result, UserMeuadmRequest.class);
                if(userMeuadm != null){
                    if(userMeuadm.getCode() == Volley.STATUS_OK){
                        User user = User.adapterUserMeuadm(userMeuadm.getObject(), token, username, password, getActivity());
                        if(user != null){
                            UserData.clear();
                            UserData.saveUser(user);
                            startActivity(MainActivity.newIntent(getContext()));
                            getActivity().finish();
                        }
                        return;
                    }

                    String messageError = getString(R.string.message_error_login_request);
                    if(!TextUtils.isEmpty(userMeuadm.getMessage())){
                        messageError = userMeuadm.getMessage();
                    }
                    SendSuccessDialog.show(messageError, messageError, getFragmentManager(), null, SendSuccessDialog.TYPE_ERROR);
                }
            }

            @Override
            public String uri() {
                return URI.AUTHENTICATE;
            }
        };
        Volley volley = new Volley(callback);
        volley.showDialog(getActivity());
        volley.isMeuADM(true);
        volley.setLoginMeuAdm(username);
        volley.setPasswordMeuAdm(password);
        try {
            JSONObject device = new JSONObject();
            device.put("device", "android");
            device.put("device_id", token);
            volley.request(Request.Method.POST, device.toString());
        } catch (Exception e) {
            e.printStackTrace();
            SendSuccessDialog.show(getString(R.string.error_generic), null, getFragmentManager(), null, SendSuccessDialog.TYPE_ERROR);
        }
    }
}