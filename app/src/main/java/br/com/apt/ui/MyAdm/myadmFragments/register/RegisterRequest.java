package br.com.apt.ui.MyAdm.myadmFragments.register;

import java.util.List;

import br.com.apt.application.BaseGsonMeuAdm;
import br.com.apt.ui.lobby.lobbyFragments.deliveries.Delivery;

/**
 * Created by adminbs on 7/31/17.
 */

public class RegisterRequest extends BaseGsonMeuAdm {
    public Register object;

    public Register getObject() {
        return object;
    }

    public void setObject(Register object) {
        this.object = object;
    }
}
