package br.com.apt.ui.MyAdm.myadmFragments.Billet;

import br.com.apt.application.BaseGsonMeuAdm;

/**
 * Created by adminbs on 7/31/17.
 */

public class QuotasRequest extends BaseGsonMeuAdm {
    private Quotas object;

    public Quotas getObject() {
        return object;
    }

    public void setObject(Quotas object) {
        this.object = object;
    }
}