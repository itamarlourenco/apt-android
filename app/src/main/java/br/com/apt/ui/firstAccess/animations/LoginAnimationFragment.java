package br.com.apt.ui.firstAccess.animations;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.Request;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import br.com.apt.BuildConfig;
import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.AppPreferences;
import br.com.apt.application.BaseFragment;
import br.com.apt.application.Config;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.application.services.LocationNotificationService;
import br.com.apt.model.user.User;
import br.com.apt.model.user.UserData;
import br.com.apt.model.user.UserMeuadm;
import br.com.apt.model.user.UserMeuadmRequest;
import br.com.apt.model.user.UserRequest;
import br.com.apt.ui.login.Login;
import br.com.apt.ui.login.LoginActivity;
import br.com.apt.ui.main.MainActivity;
import br.com.apt.util.Logger;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomButton;
import br.com.apt.widget.CustomEditText;
import br.com.apt.widget.CustomTextView;
import br.com.apt.widget.CustomTextViewLight;
import br.com.apt.widget.SendSuccessDialog;

/**
 * Created by adminbs on 9/12/16.
 */
public class LoginAnimationFragment extends BaseFragment implements View.OnClickListener{

    private CustomEditText email;
    private CustomEditText password;
    private CustomButton buttonLogin;
    private String token;


    private static LoginAnimationListener loginAnimationListener;
    public static LoginAnimationFragment newInstance(LoginAnimationListener loginAnimationListener){
        LoginAnimationFragment.loginAnimationListener = loginAnimationListener;
        return new LoginAnimationFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.login_animate, container, false);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if(view != null){
            CustomTextViewLight forgetPassword = (CustomTextViewLight) view.findViewById(R.id.forgetPassword);
            forgetPassword.setPaintFlags(forgetPassword.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            forgetPassword.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    loginAnimationListener.forgetPassword(v);
                }
            });

            email = (CustomEditText) view.findViewById(R.id.email);
            password = (CustomEditText) view.findViewById(R.id.password);
            buttonLogin = (CustomButton) view.findViewById(R.id.buttonLogin);


            if(App.isDeviceHml()){
                email.setText("manolo@gmail.com");
                password.setText("qqqq");
            }

            password.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    executeLogin();
                    return true;
                }
            });

            buttonLogin.setOnClickListener(this);

            Intent intent = getActivity().getIntent();
            if(intent != null){
                String autoUsername = intent.getStringExtra(LoginActivity.PUT_USERNAME);
                String autoPassword = intent.getStringExtra(LoginActivity.PUT_PASSWORD);

                if(TextUtils.isEmpty(autoUsername) || TextUtils.isEmpty(autoPassword)){
                    Logger.d("Auto-Login not do");
                    return;
                }

                email.setText(autoUsername);
                password.setText(autoPassword);
                executeLogin();
            }

            new ArrayList<>();
        }
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.buttonLogin){
            executeLogin();
        }
    }

    private void executeLogin() {
        try{
            if(TextUtils.isEmpty(email.getText())){
                email.requestFocus();
                throw new Exception(getString(R.string.message_error_login_email));
            }

            if(TextUtils.isEmpty(password.getText())){
                password.requestFocus();
                throw new Exception(getString(R.string.message_error_login_password));
            }
            getTokenGcm();
        }catch (Exception e){
            View view = getView();
            if(view != null){
                Util.showDialog(getActivity(), e.getMessage());
            }
        }
    }


    protected void getTokenGcm() {
        new AsyncTask<Void, Void, Void>(){
            @Override
            protected Void doInBackground(Void... params) {
                InstanceID instanceID = InstanceID.getInstance(getContext());
                try {
                    token = instanceID.getToken(Config.GCM.SENDER_ID, GoogleCloudMessaging.INSTANCE_ID_SCOPE);
                    checkMeuAdm();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return null;
            }
        }.execute();
    }

    private void checkMeuAdm() {
        Volley.Callback callback = new Volley.Callback() {
            @Override
            public void result(String result, int type) {
                setTextButtonLogin(getString(R.string.enter));
                UserMeuadmRequest userMeuadm = App.getGson().fromJson(result, UserMeuadmRequest.class);
                if(userMeuadm != null){
                    if(userMeuadm.getCode() == Volley.STATUS_OK){
                        User user = User.adapterUserMeuadm(userMeuadm.getObject(), token, email.getText().toString(), password.getText().toString(), getActivity());
                        if(user != null){
                            UserData.clear();
                            UserData.saveUser(user);
                            startActivity(MainActivity.newIntent(getContext()));
                            getActivity().finish();
                            getInfoCondominium();
                        }
                        return;
                    }

                    String messageError = getString(R.string.message_error_login_request);
                    if(!TextUtils.isEmpty(userMeuadm.getMessage())){
                        messageError = userMeuadm.getMessage();
                    }
                    SendSuccessDialog.show(getString(R.string.error_generic), messageError, getFragmentManager(), null, SendSuccessDialog.TYPE_ERROR);
                }
            }

            @Override
            public String uri() {
                return URI.AUTHENTICATE;
            }
        };
        Volley volley = new Volley(callback);
        volley.isMeuADM(true);
        volley.setLoginMeuAdm(email.getText().toString());
        volley.setPasswordMeuAdm(password.getText().toString());
        setTextButtonLogin(getString(R.string.waiting));
        try {
            JSONObject device = new JSONObject();
            device.put("device", "android");
            device.put("device_id", token);
            volley.request(Request.Method.POST, device.toString());
        } catch (Exception e) {
            e.printStackTrace();
            SendSuccessDialog.show(getString(R.string.error_generic), null, getFragmentManager(), null, SendSuccessDialog.TYPE_ERROR);
        }
    }

    public void setTextButtonLogin(final String textButtonLogin) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                buttonLogin.setEnabled(!buttonLogin.isEnabled());
                buttonLogin.setText(textButtonLogin);
            }
        });
    }

    public interface LoginAnimationListener{
        void forgetPassword(View view);
    }

    public void getInfoCondominium(){
        Volley volley = new Volley(new Volley.Callback() {
            @Override
            public void result(String result, int type) {
                try{
                    JSONObject jsonObject = new JSONObject(result);
                    JSONObject object = jsonObject.getJSONObject("Object");
                    if(object != null){
                        float latitude = Float.parseFloat(object.getString("latitude"));
                        float longitude = Float.parseFloat(object.getString("longitude"));

                        (new LocationNotificationService.ManegeData()).saveData(latitude, longitude);
                    }
                }catch (Exception e){
                    Logger.e(e.getMessage());
                }
            }

            @Override
            public String uri() {
                User user = getUser();
                return String.format(URI.CONDOMINIUM_ID, user.getCondominiumId());
            }
        });
        volley.request();
    }
}
