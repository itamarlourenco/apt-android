package br.com.apt.ui.consumption;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import br.com.apt.R;
import br.com.apt.application.BaseActivity;
import br.com.apt.ui.reservations.CalendarFragment;

public class ConsumptionActivity extends BaseActivity {

    public static Intent newIntent(Context context) {
        return new Intent(context, ConsumptionActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public ConsumptionFragment getFragment(){
        return ConsumptionFragment.newInstance();
    }

    @Override
    protected int getTitleToolbarColor() {
        return R.color.consumption_color;
    }
}