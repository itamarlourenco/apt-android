package br.com.apt.ui.reservations;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.github.ybq.android.spinkit.SpinKitView;

import java.util.List;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.BaseFragment;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.model.user.User;
import br.com.apt.util.Util;
import br.com.apt.widget.SendSuccessDialog;

/**
 * Created by adminbs on 9/6/17.
 */

public class InstallationsFragment extends BaseFragment implements Volley.Callback {

    private GridView gridView;
    private SpinKitView loader;
    private List<Installations> installationsList;

    public static InstallationsFragment newInstance(){
        return new InstallationsFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_installations, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if(view != null){
            gridView = (GridView) view.findViewById(R.id.gridView);
            gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    openCalendar(position);
                }
            });
            loader = (SpinKitView) view.findViewById(R.id.loader);

            Volley volley = new Volley(this);
            volley.isReservation(true);
            volley.request();
        }
    }

    public void openCalendar(int position) {
        startActivity(CalendarActivity.newIntent(getContext(), installationsList.get(position)));
        getActivity().overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
    }

    @Override
    public void result(String result, int type) {
        try{
            InstallationsRequest installationsRequest = App.getGson().fromJson(result, InstallationsRequest.class);
            installationsList = installationsRequest.getObject();

            gridView.setAdapter(new InstallationsAdapter(this, getContext(), installationsList));
            Util.changeVisibility(gridView, View.VISIBLE, true);
            Util.changeVisibility(loader, View.GONE, true);
        }catch (Exception e){
            SendSuccessDialog.show(getString(R.string.error_generic), null, getFragmentManager(), new SendSuccessDialog.Actions() {
                @Override
                public void back() {
                    getActivity().finish();
                }
            }, SendSuccessDialog.TYPE_ERROR);
            e.printStackTrace();
        }
    }

    @Override
    public String uri() {
        User user = getUser();
        return String.format(URI.INSTALLATIONS_LIST, user.getCondominiumId(), user.getCompanyKey());
    }
}


