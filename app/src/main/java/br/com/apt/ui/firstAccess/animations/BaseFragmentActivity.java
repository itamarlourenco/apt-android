package br.com.apt.ui.firstAccess.animations;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;

import br.com.apt.R;
import br.com.apt.application.BaseActivity;
import br.com.apt.application.BaseFragment;
import br.com.apt.application.FullScreenBaseActivity;

/**
 * Created by adminbs on 9/15/16.
 */
public class BaseFragmentActivity extends FullScreenBaseActivity implements View.OnClickListener, NotHaveCodeAnimationFragment.NotHaveCodeListener, CodeAnimationsFragment.CodeAnimationsListener, ForgetAnimationFragment.ForgetAnimationListener, LoginAnimationFragment.LoginAnimationListener {

    private LoginAnimationFragment loginFragmentAnimation = LoginAnimationFragment.newInstance(this);
    private ForgetAnimationFragment forgetAnimationFragment = ForgetAnimationFragment.newInstance(this);
    private CodeAnimationsFragment codeAnimationsFragment = CodeAnimationsFragment.newInstance(this);
    private NotHaveCodeAnimationFragment notHaveCodeAnimationFragment = NotHaveCodeAnimationFragment.newInstance(this);
    private boolean showCode = false;

    public static final int FRAGMENT_LOGIN = 1;
    public static final int FRAGMENT_CODE = 2;
    private static final String EXTRA_FRAGMENT = "EXTRA_FRAGMENT";

    public static final String PUT_USERNAME = "EXTRA_USERNAME";
    public static final String PUT_SHOW_CODE = "EXTRA_SHOWCODE";
    public static final String PUT_PASSWORD = "EXTRA_PASSWORD";

    public static Intent newIntent(Context context) {
        return new Intent(context, BaseFragmentActivity.class);
    }

    public static Intent newIntent(Context context,boolean shouldShowForm) {
        Intent intent =  new Intent(context, BaseFragmentActivity.class);
        intent.putExtra(PUT_SHOW_CODE, shouldShowForm);
        return intent;
    }

    public static Intent newIntent(Context context, String username, String password, int fragmentType) {
        Intent intent =  new Intent(context, BaseFragmentActivity.class);
        intent.putExtra(BaseFragmentActivity.EXTRA_FRAGMENT, fragmentType);
        intent.putExtra(PUT_USERNAME, username);
        intent.putExtra(PUT_PASSWORD, password);

        return intent;
    }

    public static Intent newIntent(Context context, int fragmentType){
        Intent intent = new Intent(context, BaseFragmentActivity.class);
        intent.putExtra(BaseFragmentActivity.EXTRA_FRAGMENT, fragmentType);
        return intent;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        (findViewById(R.id.baseTransparent)).setOnClickListener(this);

        Intent intent = getIntent();
        if(intent != null){
            int fragmentType = intent.getIntExtra(BaseFragmentActivity.EXTRA_FRAGMENT, 0);
            this.showCode = intent.getBooleanExtra(PUT_SHOW_CODE,false);
            switch (fragmentType){
                case BaseFragmentActivity.FRAGMENT_LOGIN:
                    handleFragment(loginFragmentAnimation);
                    break;
                case BaseFragmentActivity.FRAGMENT_CODE:
                    handleFragment(codeAnimationsFragment);
                    break;
            }
        }


    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if(this.showCode == true){
            this.notHaveCode(null);
        }
    }

    @Override
    protected int getIdLayoutActivity() {
        return R.layout.layout_base_fragment;
    }

    @Override
    protected boolean checkLogin() {
        return false;
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_down, R.anim.slide_out_down);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.baseTransparent){
            finish();
        }
    }

    private void handleFragment(BaseFragment baseFragment) {
        handleFragment(baseFragment, 0, 0);
    }
    private void handleFragment(BaseFragment baseFragment, int firstAnimation, int secondAnimation) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(firstAnimation, secondAnimation);
        fragmentTransaction.replace(R.id.fragmentContainerAnimation, baseFragment);
        fragmentTransaction.commit();
    }

    //Listeners
    @Override
    public void forgetPassword(View view) {
        handleFragment(forgetAnimationFragment, R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    public void back(View view) {
        handleFragment(loginFragmentAnimation, R.anim.slide_out_left, R.anim.slide_in_right);
    }

    @Override
    public void notHaveCode(View view) {
        handleFragment(notHaveCodeAnimationFragment, R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    public void backToCode(View view) {
        handleFragment(codeAnimationsFragment, R.anim.slide_out_left, R.anim.slide_in_right);
    }
}
