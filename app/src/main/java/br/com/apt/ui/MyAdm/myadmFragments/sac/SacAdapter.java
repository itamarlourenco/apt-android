package br.com.apt.ui.MyAdm.myadmFragments.sac;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;

import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.apt.R;
import br.com.apt.application.Volley;
import br.com.apt.ui.MyAdm.Occurrences;
import br.com.apt.ui.lobby.lobbyFragments.deliveries.DeliveriesListViewAdapter;
import br.com.apt.ui.lobby.lobbyFragments.deliveries.Delivery;
import br.com.apt.util.Logger;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomTextViewLight;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by adminbs on 7/31/17.
 */

public class SacAdapter extends BaseAdapter {
    private Context context;
    private List<Occurrences> occurrences;

    public SacAdapter(Context context, List<Occurrences> occurrences) {
        this.context = context;
        this.occurrences = occurrences;
    }

    @Override
    public int getCount() {
        return occurrences != null ? occurrences.size() : 0;
    }

    @Override
    public Occurrences getItem(int position) {
        return occurrences.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        SacAdapter.ViewHolder holder;

        if(convertView == null){
            view = LayoutInflater.from(context).inflate(R.layout.row_sac, parent, false);
            holder = new SacAdapter.ViewHolder(view);
            view.setTag(holder);
        }else{
            view = convertView;
            holder = (SacAdapter.ViewHolder) view.getTag();
        }

        bindList(holder, position);
        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private void bindList(SacAdapter.ViewHolder holder, int position) {
        Occurrences item = getItem(position);

        if(position % 2 == 0){
            holder.deliveryBaseBackground.setBackground(ContextCompat.getDrawable(context, R.drawable.button_menu_list_dark));
        }else{
            holder.deliveryBaseBackground.setBackground(ContextCompat.getDrawable(context, R.drawable.button_menu_list_light));
        }

        holder.deliveryTitle.setText(item.getTitle());
        holder.deliveryDescription.setText(
                String.format(context.getText(R.string.description_sac).toString(), Util.dateFormatted(item.getCreatedAt()), Util.timeFormatted(item.getCreatedAt()))
        );

        if(item.isNotified()){
            holder.notified.setVisibility(View.VISIBLE);
        }else{
            holder.notified.setVisibility(View.GONE);
        }

        String status = item.getStatus();
        if(!TextUtils.isEmpty(status)){
            if(status.contains("Andamento")){
                holder.deliveryLabel.setText("ANDAMENTO");
                holder.deliveryLabel.setBackground(ContextCompat.getDrawable(context, R.drawable.border_radius_full_green));
                holder.deliveryLabel.setTextColor(ContextCompat.getColor(context, R.color.sac_andamento_text_color));
            }else if(status.contains("Finalizado")){
                holder.deliveryLabel.setText("FINALIZADO");
                holder.deliveryLabel.setBackground(ContextCompat.getDrawable(context, R.drawable.border_radius_blue));
                holder.deliveryLabel.setTextColor(ContextCompat.getColor(context, R.color.sac_andamento_text_color));
            }else{
                holder.deliveryLabel.setText("ABERTO");
                holder.deliveryLabel.setBackground(ContextCompat.getDrawable(context, R.drawable.border_radius_yellow));
                holder.deliveryLabel.setTextColor(ContextCompat.getColor(context, R.color.sac_andamento_text_color));
            }
        }
    }

    public class ViewHolder{
        private RelativeLayout deliveryBaseBackground;
        private CircleImageView deliveryImage;
        private CustomTextViewLight deliveryTitle;
        private CustomTextViewLight deliveryDescription;
        private CustomTextViewLight deliveryLabel;
        private View notified;

        public ViewHolder(View view) {
            deliveryBaseBackground = (RelativeLayout) view.findViewById(R.id.deliveryBaseBackground);
            deliveryImage = (CircleImageView) view.findViewById(R.id.deliveryImage);
            deliveryTitle = (CustomTextViewLight) view.findViewById(R.id.deliveryTitle);
            deliveryDescription = (CustomTextViewLight) view.findViewById(R.id.deliveryDescription);
            deliveryLabel = (CustomTextViewLight) view.findViewById(R.id.deliveryLabel);
            notified = view.findViewById(R.id.notified);
        }
    }
}
