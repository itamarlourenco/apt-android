package br.com.apt.ui.MyAdm.myadmFragments.Billet;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import br.com.apt.application.BaseActivity;
import br.com.apt.ui.reservations.CalendarFragment;

/**
 * Created by adminbs on 8/1/17.
 */

public class BankActivity extends BaseActivity {

    private static final String EXTRA_BILLET_DETAILS = "EXTRA_BILLET_DETAILS";

    public static Intent newIntent(Context context, BilletDetails billetDetails) {
        Intent intent = new Intent(context, BankActivity.class);
        intent.putExtra(BankActivity.EXTRA_BILLET_DETAILS, billetDetails);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public Fragment getFragment(){
        Intent intent = getIntent();
        if(intent != null){
            BilletDetails billetDetails = intent.getParcelableExtra(EXTRA_BILLET_DETAILS);
            return BankFragment.newInstance(billetDetails);
        }

        return null;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void finish() {
        super.finish();
    }
}
