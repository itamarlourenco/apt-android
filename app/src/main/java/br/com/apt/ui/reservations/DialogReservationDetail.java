package br.com.apt.ui.reservations;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.annotation.RequiresApi;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebView;
import android.widget.ImageView;

import com.android.volley.Request;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.model.user.User;
import br.com.apt.model.user.UserData;
import br.com.apt.ui.MyAdm.Occurrences;
import br.com.apt.util.Logger;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomTextViewBold;
import br.com.apt.widget.CustomTextViewLight;
import br.com.apt.widget.SendSuccessDialog;

/**
 * Created by adminbs on 9/10/17.
 */

public class DialogReservationDetail extends DialogFragment {

    private static final String FRAG_TAG = "dialogReservationDetailDetailsTag";
    private CustomTextViewBold title;
    private CustomTextViewLight description;
    private CustomTextViewLight dateLabel;
    private CustomTextViewLight nameInstallations;
    private CustomTextViewBold nameCondominium;
    private CustomTextViewLight descriptionReservation;
    private CustomTextViewLight textLabelCalendar;
    private CustomTextViewLight times;
    private ImageView icon;
    private ImageView iconTitle;
    private ReservationsFragment parent;

    private Reservations reservations;


    /**
     * Shows an instance of this fragment to present the Terms of Use to the user.
     * @param fragmentManager a {@link FragmentManager} object to show this {@link DialogFragment}.
     */
    public static void show(ReservationsFragment parent, FragmentManager fragmentManager, Reservations reservations) {
        if (fragmentManager.findFragmentByTag(FRAG_TAG) == null) {
            DialogReservationDetail fragment = new DialogReservationDetail();
            fragment.setReservations(reservations);
            fragment.setParent(parent);
            fragment.show(fragmentManager, FRAG_TAG);
        }
    }

    public void setReservations(Reservations reservations) {
        this.reservations = reservations;
    }

    public void setParent(ReservationsFragment parent) {
        this.parent = parent;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reservation_detail, container, false);
        Window window = getDialog().getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if (view != null) {
            title = (CustomTextViewBold) view.findViewById(R.id.title);
            description = (CustomTextViewLight) view.findViewById(R.id.description);
            nameCondominium = (CustomTextViewBold) view.findViewById(R.id.nameCondominium);
            descriptionReservation = (CustomTextViewLight) view.findViewById(R.id.descriptionReservation);
            nameInstallations = (CustomTextViewLight) view.findViewById(R.id.nameInstallations);
            dateLabel = (CustomTextViewLight) view.findViewById(R.id.dateLabel);
            textLabelCalendar = (CustomTextViewLight) view.findViewById(R.id.textLabelCalendar);
            times = (CustomTextViewLight) view.findViewById(R.id.times);
            icon = (ImageView) view.findViewById(R.id.icon);
            iconTitle = (ImageView) view.findViewById(R.id.iconTitle);


            final Installations installation = reservations.getInstallation();
            if (installation != null) {

                Picasso.with(getContext()).load(Installations.handleUrlIcon(installation.getIcon())).into(icon);

                if(TextUtils.equals(reservations.getApproved(), Reservations.TYPE_CREATED)){
                    title.setText("AGUARDANDO CONFIRMAÇÃO");
                    title.setTextColor(Color.parseColor("#c98f33"));
                    description.setText(R.string.reservation_description_created);
                    iconTitle.setVisibility(View.VISIBLE);
                }

                if(TextUtils.equals(reservations.getApproved(), Reservations.TYPE_APPROVED)){
                    title.setText("RESERVA CONFIRMADA");
                    title.setTextColor(Color.parseColor("#2583ad"));
                    description.setText(R.string.reservation_description_approved);
                    iconTitle.setVisibility(View.VISIBLE);
                    iconTitle.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.reservation_approved));
                }

                if(TextUtils.equals(reservations.getApproved(), Reservations.TYPE_NOT_APPROVED)){
                    title.setText("RESERVA RECUSADA");
                    title.setTextColor(Color.parseColor("#9e5954"));
                    description.setText(R.string.reservation_not_reproved);
                    iconTitle.setVisibility(View.VISIBLE);
                    iconTitle.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.reservation_not_approved));
                }

                SimpleDateFormat simpleDateFormat = App.getSimpleDateFormat();
                Date currentDate = Calendar.getInstance().getTime();
                try {
                    Date parseEnd = simpleDateFormat.parse(reservations.getEnd());
                    if(currentDate.compareTo(parseEnd) > 0){

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


                User user = UserData.getUser();
                if (user != null) {
                    br.com.apt.model.build.Build build = user.getBuild();
                    nameCondominium.setText(build.getName());
                }
                nameInstallations.setText(installation.getName());
                descriptionReservation.setText(Html.fromHtml("Reserva em nome de <font color='#298CAD'>"+reservations.getCreated_external_user_name()+"</font> apartamento <font color='#298CAD'>"+reservations.getUnit().getExternal_id()+" bloco "+reservations.getBlock().getName()+"</font>"));

                try {
                    final Date parseStart = simpleDateFormat.parse(reservations.getStart());
                    final Date parseEnd = simpleDateFormat.parse(reservations.getEnd());
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(parseStart);

                    dateLabel.setText(
                            String.format(
                                    getString(R.string.reservation_label_day),
                                    Util.getDayWeek(calendar),
                                    String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)),
                                    Util.getMonthName(calendar.get(Calendar.MONTH)),
                                    String.valueOf(calendar.get(Calendar.YEAR))
                            )
                    );
                    textLabelCalendar.setText(String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)));
                    times.setText(
                            String.format(getString(R.string.time_reservation_label), reservations.getStart().split(" ")[1], reservations.getEnd().split(" ")[1])
                    );


                    final String descriptionReservationCreateEventAndShared = (descriptionReservation.getText() + "\n\n" + nameInstallations.getText() + "\n\n" + dateLabel.getText() + "\n\n" + times.getText());

                    view.findViewById(R.id.shared).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent shareIntent = new Intent(Intent.ACTION_SEND);
                            shareIntent.setType("text/plain");
                            shareIntent.putExtra(Intent.EXTRA_TEXT, descriptionReservationCreateEventAndShared);
                            shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Reserva MeuAPT - "+ nameInstallations.getText() );
                            startActivity(Intent.createChooser(shareIntent, "Compartilhar..."));
                        }
                    });

                    view.findViewById(R.id.addCalendar).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(Intent.ACTION_INSERT)
                                    .setData(CalendarContract.Events.CONTENT_URI)
                                    .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, parseStart.getTime())
                                    .putExtra(CalendarContract.EXTRA_EVENT_END_TIME, parseEnd.getTime())
                                    .putExtra(CalendarContract.Events.TITLE, "Reserva MeuAPT - "+ nameInstallations.getText())
                                    .putExtra(CalendarContract.Events.DESCRIPTION, descriptionReservationCreateEventAndShared)
                                    .putExtra(CalendarContract.Events.AVAILABILITY, CalendarContract.Events.AVAILABILITY_BUSY);
                            startActivity(intent);
                        }
                    });

                    view.findViewById(R.id.regulation).setOnClickListener(new View.OnClickListener(){
                        @Override
                        public void onClick(View v) {
                            getTerms();
                        }
                    });

                    view.findViewById(R.id.cancelReservation).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            SendSuccessDialog.show(getString(R.string.ask_remove_reservation), getString(R.string.ask_remove_reservation_description), getFragmentManager(), new SendSuccessDialog.Actions() {
                                @Override
                                public void back() {
                                    Volley volley = new Volley(new Volley.Callback() {
                                        @Override
                                        public void result(String result, int type) {
                                            if (result != null) {
                                                closeDialog();
                                            }
                                        }

                                        @Override
                                        public String uri() {
                                            return String.format(URI.RESERVATION_CANCEL, reservations.getId());
                                        }
                                    });
                                    volley.isReservation(true);
                                    volley.showDialog(getActivity());
                                    volley.request(Request.Method.POST, null);
                                }
                            }, SendSuccessDialog.TYPE_DYNAMIC_ERROR);

                        }
                    });

                    if (TextUtils.equals(reservations.getApproved(), Reservations.TYPE_NOT_APPROVED)) {
                        view.findViewById(R.id.cancelReservation).setVisibility(View.GONE);
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
    }

    private void closeDialog() {
        parent.getReservations();
        dismiss();
    }

    @Override
    public int getTheme() {
        return R.style.CustomDialog;
    }

    public void getTerms() {
        Volley volley = new Volley(new Volley.Callback() {
            @Override
            public void result(String result, int type) {
                String title = "";
                String rule = "";
                if (result != null) {
                    RulesProceduresRequest rulesProceduresRequest = App.getGson().fromJson(result, RulesProceduresRequest.class);
                    List<RulesProcedures> object = rulesProceduresRequest.getObject();
                    if (object != null && object.size() > 0) {
                        RulesProcedures rulesProcedures = object.get(0);
                        title = rulesProcedures.getText();
                        rule = rulesProcedures.getRule();
                    }else{
                        rule = getString(R.string.without_term);
                    }
                }else{
                    rule = getString(R.string.without_term);
                }

                new AlertDialog.Builder(getContext())
                        .setTitle(title)
                        .setMessage(rule)
                        .setPositiveButton("Fechar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                            }
                        })
                        .show();
            }

            @Override
            public String uri() {
                return String.format(URI.RULE_PROCEDURE, reservations.getInstallation().getId());
            }
        });
        volley.isReservation(true);
        volley.showDialog(getActivity());
        volley.request();
    }
}
