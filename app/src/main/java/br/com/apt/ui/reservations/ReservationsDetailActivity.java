package br.com.apt.ui.reservations;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import br.com.apt.application.BaseActivity;

/**
 * Created by andre on 5/15/17.
 */

public class ReservationsDetailActivity extends BaseActivity {

    private static final String EXTRA_RESERVATION = "EXTRA_RESERVATION";

    public static Intent newIntent(Context context, Reservations reservations) {
        Intent intent = new Intent(context, ReservationsDetailActivity.class);
        intent.putExtra(EXTRA_RESERVATION, reservations);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public Fragment getFragment() {
        Intent intent = getIntent();
        if (intent != null) {
            Reservations reservations = intent.getParcelableExtra(EXTRA_RESERVATION);
            return ReservationsDetailFragment.newInstance(reservations);
        }
        return null;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}