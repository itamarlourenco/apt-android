package br.com.apt.ui.neighborhood.neighborhoodFragment.model;

import com.google.gson.annotations.SerializedName;

import br.com.apt.application.SendRequest;
import br.com.apt.ui.main.SensorsRequest;

/**
 * Created by adminbs on 1/21/16.
 */
public class SendNeighborhoodDelivery extends SendRequest {

    @SerializedName("type_service_id")
    private String typeServiceId;

    public String getTypeServiceId() {
        return typeServiceId;
    }

    public void setTypeServiceId(String typeServiceId) {
        this.typeServiceId = typeServiceId;
    }

}
