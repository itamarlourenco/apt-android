package br.com.apt.ui.main;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;
import java.util.List;

import br.com.apt.application.BaseGson;
import br.com.apt.application.ModelObject;

/**
 * Created by adminbs on 11/15/16.
 */
public class DisabledFunction  implements ModelObject, Parcelable {
    public static final String LOBBY = "LOBBY";
    public static final String RESERVATIONS = "RESERVATIONS";
    public static final String NOTIFICATION = "NOTIFICATION";
    public static final String TRAVEL = "TRAVEL";
    public static final String MY_APT = "MY_APT";
    public static final String TICKETS = "TICKETS";

    private int condominium_id;
    private Date created_at;
    private String function;
    private int id;

    public int getCondominiumId() {
        return condominium_id;
    }

    public void setCondominiumId(int condominium_id) {
        this.condominium_id = condominium_id;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public String getFunction() {
        return function;
    }

    public void setFunction(String function) {
        this.function = function;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    protected DisabledFunction(Parcel in) {
        condominium_id = in.readInt();
        long tmpCreated_at = in.readLong();
        created_at = tmpCreated_at != -1 ? new Date(tmpCreated_at) : null;
        function = in.readString();
        id = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(condominium_id);
        dest.writeLong(created_at != null ? created_at.getTime() : -1L);
        dest.writeString(function);
        dest.writeInt(id);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<DisabledFunction> CREATOR = new Parcelable.Creator<DisabledFunction>() {
        @Override
        public DisabledFunction createFromParcel(Parcel in) {
            return new DisabledFunction(in);
        }

        @Override
        public DisabledFunction[] newArray(int size) {
            return new DisabledFunction[size];
        }
    };

    public class DisabledFunctionRequest  extends BaseGson {
        private List<DisabledFunction> Object;

        public List<DisabledFunction> getObject() {
            return Object;
        }

        public void setObject(List<DisabledFunction> object) {
            Object = object;
        }
    }

}
