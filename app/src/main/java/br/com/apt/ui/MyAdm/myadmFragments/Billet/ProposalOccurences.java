package br.com.apt.ui.MyAdm.myadmFragments.Billet;

import br.com.apt.application.SendRequest;

/**
 * Created by adminbs on 8/2/17.
 */

public class ProposalOccurences extends SendRequest{

    private String title;
    private String body;
    private String condominium_key;
    private String block_key;
    private String subject_uuid;
    private String unit_key;

    public ProposalOccurences(String title,  String condominium_key, String subject_uuid, String block_key, String unit_key, int proposal) {
        this.title = title;
        this.condominium_key = condominium_key;
        this.block_key = block_key;
        this.unit_key = unit_key;
        this.subject_uuid = subject_uuid;
        this.body = handleBody(proposal);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }


    public String getCondominium_key() {
        return condominium_key;
    }

    public void setCondominium_key(String condominium_key) {
        this.condominium_key = condominium_key;
    }

    public String getBlock_key() {
        return block_key;
    }

    public void setBlock_key(String block_key) {
        this.block_key = block_key;
    }

    public String getUnit_key() {
        return unit_key;
    }

    public void setUnit_key(String unit_key) {
        this.unit_key = unit_key;
    }

    public String getSubject_uuid() {
        return subject_uuid;
    }

    public void setSubject_uuid(String subject_uuid) {
        this.subject_uuid = subject_uuid;
    }

    public String handleBody(int proposal){
        return "Bloco: " +
                block_key +
                "\n" +
                "Unidade: " +
                unit_key +
                "\n" +
                "Solicito o envio de uma prévia de acordo de débitos em até " +
                proposal +
                "x. " +
                "\n" +
                "OBSERVAÇÃO: " +
                "\n" +
                "Sujeito à análise e aprovação. " +
                "\n" +
                "Valores apresentados são principais e ainda não contemplam multa, juros ou outras incidências.";
    }
}
