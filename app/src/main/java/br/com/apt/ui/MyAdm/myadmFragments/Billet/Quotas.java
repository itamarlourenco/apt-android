package br.com.apt.ui.MyAdm.myadmFragments.Billet;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.apt.model.reservations.Unit;

/**
 * Created by adminbs on 8/1/17.
 */

public class Quotas {
    private String unit_open;
    private String receipt_open;
    private String amount_open;
    private List<Units> units;

    public String getUnit_open() {
        return unit_open;
    }

    public void setUnit_open(String unit_open) {
        this.unit_open = unit_open;
    }

    public String getReceipt_open() {
        return receipt_open;
    }

    public void setReceipt_open(String receipt_open) {
        this.receipt_open = receipt_open;
    }

    public String getAmount_open() {
        return amount_open;
    }

    public void setAmount_open(String amount_open) {
        this.amount_open = amount_open;
    }

    public List<Units> getUnits() {
        return units;
    }

    public void setUnits(List<Units> units) {
        this.units = units;
    }

    public static class Units{
        private String block;
        private String unit;
        private String owner;
        private String total;
        private List<Receipt> receipt;

        public String getBlock() {
            return block;
        }

        public void setBlock(String block) {
            this.block = block;
        }

        public String getUnit() {
            return unit;
        }

        public void setUnit(String unit) {
            this.unit = unit;
        }

        public String getOwner() {
            return owner;
        }

        public void setOwner(String owner) {
            this.owner = owner;
        }

        public String getTotal() {
            return total;
        }

        public void setTotal(String total) {
            this.total = total;
        }

        public List<Receipt> getReceipt() {
            return receipt;
        }

        public void setReceipt(List<Receipt> receipt) {
            this.receipt = receipt;
        }
    }

    public static class Receipt{
        private String status;
        private String receipt;
        private Date limit;
        private String emission;
        private String total;
        private List<History> history;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getReceipt() {
            return receipt;
        }

        public void setReceipt(String receipt) {
            this.receipt = receipt;
        }

        public Date getLimit() {
            return limit;
        }

        public void setLimit(Date limit) {
            this.limit = limit;
        }

        public String getEmission() {
            return emission;
        }

        public void setEmission(String emission) {
            this.emission = emission;
        }

        public String getTotal() {
            return total;
        }

        public void setTotal(String total) {
            this.total = total;
        }

        public List<History> getHistory() {
            return history;
        }

        public void setHistory(List<History> history) {
            this.history = history;
        }
    }

    public static class History{
        private String item;
        private String total;

        public String getItem() {
            return item;
        }

        public void setItem(String item) {
            this.item = item;
        }

        public String getTotal() {
            return total;
        }

        public void setTotal(String total) {
            this.total = total;
        }
    }

    public List<Receipt> getAllReceipt(){
        List<Receipt> receiptListAll = new ArrayList<>();
        if(units != null){
            for(Units unit: units){
                List<Receipt> receiptList = unit.getReceipt();
                if(receiptList != null){
                    for(Receipt receiptItem: receiptList){
                        receiptListAll.add(receiptItem);
                    }
                }
            }
        }

        return receiptListAll;
    }
}
