package br.com.apt.ui.firstAccess.singup;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import br.com.apt.BuildConfig;
import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.Config;
import br.com.apt.util.Logger;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomButton;

/**
 * {@link DialogFragment} implementation that presents the Terms of Use to the user.
 */
public class DialogTermsOfUse extends DialogFragment implements View.OnClickListener {

    /** Tag for this fragment. */
    private static final String FRAG_TAG = "termsDialogFragmentTag";

    /**
     * Shows an instance of this fragment to present the Terms of Use to the user.
     * @param fragmentManager a {@link FragmentManager} object to show this {@link DialogFragment}.
     */
    public static void show(FragmentManager fragmentManager) {
        if (fragmentManager.findFragmentByTag(FRAG_TAG) == null) {
            DialogTermsOfUse fragment = new DialogTermsOfUse();
            fragment.show(fragmentManager, FRAG_TAG);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_terms, null);
        CustomButton closeDialog = (CustomButton) view.findViewById(R.id.closeDialog);

        final WebView webView = (WebView) view.findViewById(R.id.webView);

        final ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        webView.getSettings().setJavaScriptEnabled(true);

        String url = Config.About.URL;
        if(App.isAdbens()){
            url += "?type=adbens";
        }


        webView.loadUrl(url);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                Util.changeVisibility(progressBar, View.GONE, false);
                Util.changeVisibility(webView, View.VISIBLE, true);
            }
        });

        closeDialog.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.closeDialog:
                closeDialog();
                break;
        }
    }

    private void closeDialog() {
        dismiss();
    }

    @Override
    public int getTheme() {
        return R.style.CustomDialog;
    }
}
