package br.com.apt.ui.myCondominium.myCondominiumFragments.infoCondominium;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by adminbs on 9/3/17.
 */

public class Condominium implements Parcelable {
    private long id;
    private String name;
    private String address;
    private String street;
    private String street_number;
    private String neighborhood;
    private String zip_code;
    private String city;
    private String state;
    private String manager_name;
    private String manager_email;
    private String manager_phone;
    private long apartment_quantity;
    private long store_quantity;
    private long group_quantity;
    private String document;
    private Date date_closing;
    private long ins_debtor;
    private String category;
    private List<Blocks> blocks;

    class Blocks{
        private String name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getStreet_number() {
        return street_number;
    }

    public void setStreet_number(String street_number) {
        this.street_number = street_number;
    }

    public String getNeighborhood() {
        return neighborhood;
    }

    public void setNeighborhood(String neighborhood) {
        this.neighborhood = neighborhood;
    }

    public String getZip_code() {
        return zip_code;
    }

    public void setZip_code(String zip_code) {
        this.zip_code = zip_code;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getManager_name() {
        return manager_name;
    }

    public void setManager_name(String manager_name) {
        this.manager_name = manager_name;
    }

    public String getManager_email() {
        return manager_email;
    }

    public void setManager_email(String manager_email) {
        this.manager_email = manager_email;
    }

    public String getManager_phone() {
        return manager_phone;
    }

    public void setManager_phone(String manager_phone) {
        this.manager_phone = manager_phone;
    }

    public long getApartment_quantity() {
        return apartment_quantity;
    }

    public void setApartment_quantity(long apartment_quantity) {
        this.apartment_quantity = apartment_quantity;
    }

    public long getStore_quantity() {
        return store_quantity;
    }

    public void setStore_quantity(long store_quantity) {
        this.store_quantity = store_quantity;
    }

    public long getGroup_quantity() {
        return group_quantity;
    }

    public void setGroup_quantity(long group_quantity) {
        this.group_quantity = group_quantity;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public Date getDate_closing() {
        return date_closing;
    }

    public void setDate_closing(Date date_closing) {
        this.date_closing = date_closing;
    }

    public long getIns_debtor() {
        return ins_debtor;
    }

    public void setIns_debtor(long ins_debtor) {
        this.ins_debtor = ins_debtor;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<Blocks> getBlocks() {
        return blocks;
    }

    public void setBlocks(List<Blocks> blocks) {
        this.blocks = blocks;
    }

    protected Condominium(Parcel in) {
        id = in.readLong();
        name = in.readString();
        address = in.readString();
        street = in.readString();
        street_number = in.readString();
        neighborhood = in.readString();
        zip_code = in.readString();
        city = in.readString();
        state = in.readString();
        manager_name = in.readString();
        manager_email = in.readString();
        manager_phone = in.readString();
        apartment_quantity = in.readLong();
        store_quantity = in.readLong();
        group_quantity = in.readLong();
        document = in.readString();
        long tmpDate_closing = in.readLong();
        date_closing = tmpDate_closing != -1 ? new Date(tmpDate_closing) : null;
        ins_debtor = in.readLong();
        category = in.readString();
        if (in.readByte() == 0x01) {
            blocks = new ArrayList<Blocks>();
            in.readList(blocks, Blocks.class.getClassLoader());
        } else {
            blocks = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(name);
        dest.writeString(address);
        dest.writeString(street);
        dest.writeString(street_number);
        dest.writeString(neighborhood);
        dest.writeString(zip_code);
        dest.writeString(city);
        dest.writeString(state);
        dest.writeString(manager_name);
        dest.writeString(manager_email);
        dest.writeString(manager_phone);
        dest.writeLong(apartment_quantity);
        dest.writeLong(store_quantity);
        dest.writeLong(group_quantity);
        dest.writeString(document);
        dest.writeLong(date_closing != null ? date_closing.getTime() : -1L);
        dest.writeLong(ins_debtor);
        dest.writeString(category);
        if (blocks == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(blocks);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Condominium> CREATOR = new Parcelable.Creator<Condominium>() {
        @Override
        public Condominium createFromParcel(Parcel in) {
            return new Condominium(in);
        }

        @Override
        public Condominium[] newArray(int size) {
            return new Condominium[size];
        }
    };
}
