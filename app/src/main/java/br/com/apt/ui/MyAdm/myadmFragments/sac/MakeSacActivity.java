package br.com.apt.ui.MyAdm.myadmFragments.sac;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import br.com.apt.R;
import br.com.apt.application.BaseActivity;
import br.com.apt.ui.reservations.CalendarFragment;

/**
 * Created by adminbs on 8/3/17.
 */

public class MakeSacActivity extends BaseActivity {

    public static final String EXTRA_ICON = "EXTRA_ICON";
    public static final String EXTRA_THEME_UUID = "EXTRA_THEME_UUID";
    public static final String EXTRA_NAME = "EXTRA_NAME";

    public static Intent newIntent(Context context, int icon, String subjectUuid, String name) {
        Intent intent = new Intent(context, MakeSacActivity.class);
        intent.putExtra(MakeSacActivity.EXTRA_ICON, icon);
        intent.putExtra(MakeSacActivity.EXTRA_THEME_UUID, subjectUuid);
        intent.putExtra(MakeSacActivity.EXTRA_NAME, name);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public Fragment getFragment(){
        Intent intent = getIntent();
        if (intent != null) {
            String getThemeUiID = intent.getStringExtra(MakeSacActivity.EXTRA_THEME_UUID);
            String name = intent.getStringExtra(MakeSacActivity.EXTRA_NAME);
            int icon = intent.getIntExtra(MakeSacActivity.EXTRA_ICON, 0);

            MakeSacFragment makeSacFragment = MakeSacFragment.newInstance();
            makeSacFragment.setIcon(icon);
            makeSacFragment.setName(name);
            makeSacFragment.setThemeUiID(getThemeUiID);
            return makeSacFragment;
        }

        return null;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_down, R.anim.slide_out_down);
    }
}