package br.com.apt.ui.MyAdm.myadmFragments.cashflow;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.activeandroid.query.Select;
import com.github.ybq.android.spinkit.SpinKitView;

import java.util.Calendar;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.BaseFragment;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.model.user.User;
import br.com.apt.model.user.UserData;
import br.com.apt.ui.MyAdm.myadmFragments.SelectTime;
import br.com.apt.util.Util;
import br.com.apt.widget.SendSuccessDialog;

/**
 * Created by adminbs on 7/21/17.
 */

public class CashFlowAndAccountFragment extends BaseFragment implements Volley.Callback {

    private WebView webView;
    private SpinKitView loader;
    private User user = UserData.getUser();

    private int year;
    private int month;
    private String type;

    public static CashFlowAndAccountFragment newInstance(int month, int year, String type){
        CashFlowAndAccountFragment cashFlowFragment = new CashFlowAndAccountFragment();
        cashFlowFragment.setMonth(month);
        cashFlowFragment.setYear(year);
        cashFlowFragment.setType(type);

        return cashFlowFragment;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_cash_flow, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if(view != null){
            webView = (WebView) view.findViewById(R.id.webView);
            loader = (SpinKitView) view.findViewById(R.id.loader);
            WebSettings webSettings = webView.getSettings();
            webSettings.setJavaScriptEnabled(true);
            webView.clearCache(true);


            Volley volley = new Volley(this);
            volley.isMeuADM(true);
            volley.request();
        }
    }

    @Override
    public void result(String result, int type) {
        try{
            if(result != null){
                Util.changeVisibility(loader, View.GONE, true);
                Util.changeVisibility(webView, View.VISIBLE, true);

                CashFlowRequest cashFlowRequest = App.getGson().fromJson(result, CashFlowRequest.class);
                if(TextUtils.isEmpty(cashFlowRequest.getObject())){
                    SendSuccessDialog.show(getString(R.string.not_exist_cash_flow), null, getFragmentManager(), new SendSuccessDialog.Actions() {
                        @Override
                        public void back() {
                            getActivity().finish();
                        }
                    }, SendSuccessDialog.TYPE_ERROR);
                    return;
                }
                webView.loadData(cashFlowRequest.getObject(),  "text/html; charset=utf-8", "UTF-8");
            }else{
                SendSuccessDialog.show(getString(R.string.not_exist_cash_flow), null, getFragmentManager(), new SendSuccessDialog.Actions() {
                    @Override
                    public void back() {
                        getActivity().finish();
                    }
                }, SendSuccessDialog.TYPE_ERROR);
            }
        }catch (Exception e){
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    SendSuccessDialog.show(getString(R.string.error_generic), null, getFragmentManager(), new SendSuccessDialog.Actions() {
                        @Override
                        public void back() {
                            getActivity().finish();
                        }
                    }, SendSuccessDialog.TYPE_ERROR);
                }
            });
        }
    }

    @Override
    public String uri() {
        Calendar firstDayOfMonth = Calendar.getInstance();
        firstDayOfMonth.set(Calendar.DAY_OF_MONTH, 1);
        firstDayOfMonth.set(Calendar.MONTH, month+1);
        firstDayOfMonth.set(Calendar.YEAR, year);

        Calendar lastDayOfMonth = Calendar.getInstance();
        lastDayOfMonth.set(Calendar.MONTH, month+1);
        lastDayOfMonth.set(Calendar.YEAR, year);
        lastDayOfMonth.set(Calendar.DAY_OF_MONTH, lastDayOfMonth.getActualMaximum(Calendar.DAY_OF_MONTH));

        return String.format(type.equals(SelectTime.CASH_FLOW) ? URI.CASH_FLOW : URI.ACCCONTABILITY, user.getCondominiumId(), Util.getFormatDateUSCashFlowAndAccountFragment(lastDayOfMonth, true), Util.getFormatDateUSCashFlowAndAccountFragment(firstDayOfMonth));
    }
}
