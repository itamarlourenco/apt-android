package br.com.apt.ui.setting.settingFragment.password;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.BaseGson;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.model.user.User;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomEditText;
import br.com.apt.widget.CustomTextView;
import br.com.apt.widget.PagerItemFragment;

/**
 * Created by adminbs on 1/12/16.
 */
public class PasswordFragment extends PagerItemFragment implements Volley.Callback, View.OnClickListener {

    private CustomEditText password = null;
    private CustomEditText confirmPassword = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_password_setting, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if(view != null){
            view.findViewById(R.id.buttonSaveSetting).setOnClickListener(this);
            password = (CustomEditText) view.findViewById(R.id.password);
            confirmPassword = (CustomEditText) view.findViewById(R.id.confirmPassword);
        }
    }

    @Override
    protected String getPagerTitle() {
        return App.getContext().getString(R.string.password);
    }

    @Override
    protected int getPagerIcon() {
        return R.drawable.ic_setting_guard;
    }

    @Override
    public void result(String result, int type) {
        try {
            View view = getView();
            if(result == null){
                throw new Exception(getString(R.string.internet_error));
            }

            BaseGson baseGson = App.getGson().fromJson(result, BaseGson.class);
            if(view == null || baseGson == null){
                throw new Exception(getString(R.string.error_generic));
            }

            if(baseGson.getStatus() == Volley.STATUS_OK){
                Util.showDialog(getActivity(), R.string.password_alter_with_success);
                confirmPassword.setText("");
                password.setText("");
            }
        }catch (Exception e){
            Util.showDialog(getActivity(), e.getMessage());
        }
    }

    @Override
    public String uri() {
        return URI.PASSWORD;
    }

    @Override
    public void onClick(View v) {
        User user = getUser();
        View view = getView();
        if(user != null && view != null){
            String stringPassword = password.getText().toString();
            String stringConfirmPassword = confirmPassword.getText().toString();

            try{
                if(TextUtils.isEmpty(stringConfirmPassword)){
                   password.setFocusable(true);
                   throw new Exception(getString(R.string.error_input_passord));
                }

                if(!TextUtils.equals(stringPassword, stringConfirmPassword)){
                    throw new Exception(getString(R.string.password_not_correct));
                }

                PasswordSend passwordSend = new PasswordSend();
                passwordSend.setPassword(confirmPassword.getText().toString());

                Volley volley = new Volley(this);
                volley.request(Request.Method.POST, passwordSend.toJson());
                volley.showDialog(getActivity());
            }catch (Exception e){
                Util.showDialog(getActivity(), e.getMessage());
            }
        }
    }
}
