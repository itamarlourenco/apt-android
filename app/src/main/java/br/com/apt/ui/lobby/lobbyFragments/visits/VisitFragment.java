package br.com.apt.ui.lobby.lobbyFragments.visits;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.android.volley.Request;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.util.List;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.BaseFragment;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.application.ZoomImageView;
import br.com.apt.model.user.User;
import br.com.apt.ui.lobby.lobbyFragments.deliveries.Delivery;
import br.com.apt.util.CustomImage;
import br.com.apt.util.Logger;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomTextView;
import dmax.dialog.SpotsDialog;

/**
 * Created by adminbs on 6/1/16.
 */
public class VisitFragment extends BaseFragment implements View.OnClickListener, Volley.Callback {

    private static final String ARGS_VISIT = "VISIT";
    private Visit visit;
    private static final int REQUEST_AUTHORIZE = 1;
    private SpotsDialog dialog;


    public static VisitFragment newInstance(Visit visit) {
        VisitFragment introFragment = new VisitFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARGS_VISIT, visit);
        introFragment.setArguments(args);
        return introFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.visit_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Bundle bundle = this.getArguments();
        visit = bundle.getParcelable(ARGS_VISIT);
        View view = getView();

        if(visit != null && view != null){
            CustomTextView visitName = (CustomTextView) view.findViewById(R.id.visit);
            CustomTextView typeVisit = (CustomTextView) view.findViewById(R.id.typeVisit);
            CustomTextView note = (CustomTextView) view.findViewById(R.id.note);
            ZoomImageView visitImage = (ZoomImageView) view.findViewById(R.id.visitImage);
            visitImage.setAppArea(ZoomImageView.APP_AREA_LOBBY);

            (view.findViewById(R.id.noAuthorize)).setOnClickListener(this);
            (view.findViewById(R.id.authorize)).setOnClickListener(this);

            visitName.setText(visit.getName());
            typeVisit.setText(visit.getType());
            note.setText(visit.getObs());
            Picasso.with(getContext()).load(Volley.getUrlByImage(visit.getImage())).into(visitImage);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.noAuthorize:
                noAuthorized();
                break;
            case R.id.authorize:
                authorized();
                break;
        }
    }

    private void noAuthorized() {
        authorizeResident(visit.getId(), false);
    }

    private void authorized() {
        authorizeResident(visit.getId(), true);
    }

    private void authorizeResident(long id, boolean authorize){
        Volley volley = new Volley(this);
        AuthorizeSend authorizeSend = new AuthorizeSend();
        authorizeSend.setVisitId(id);
        authorizeSend.setReleased(authorize);


        volley.request(Request.Method.PUT, authorizeSend.toJson(), VisitFragment.REQUEST_AUTHORIZE);

        dialog = new SpotsDialog(getActivity(), R.style.AlertDialogCustom);
        dialog.show();
    }

    private void handleFeedBackAuthorization(String result) {
        if(dialog != null && dialog.isShowing()){
            dialog.dismiss();
            AuthorizeRequest authorizeRequest = App.getGson().fromJson(result, AuthorizeRequest.class);
            if(authorizeRequest != null){
                List<Visit> visit = authorizeRequest.getObject();
                if(visit != null && visit.size() > 0){
                    Visit oneVisits = visit.get(0);
                    final Activity activity = getActivity();
                    if(oneVisits.isFree() == 0){
                        Util.showDialog(getActivity(), getString(R.string.resident_not_authorize), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if(activity != null){
                                    activity.finish();
                                }
                            }
                        });
                    }else{
                        Util.showDialog(getActivity(), getString(R.string.resident_authorize), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if(activity != null){
                                    activity.finish();
                                }
                            }
                        });
                    }
                }
            }
        }
    }

    @Override
    public void result(String result, int type) {
        handleFeedBackAuthorization(result);
    }

    @Override
    public String uri() {
        return URI.VISITS;
    }
}
