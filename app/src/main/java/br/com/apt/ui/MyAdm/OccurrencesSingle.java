package br.com.apt.ui.MyAdm;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

/**
 * Created by adminbs on 7/31/17.
 */

public class OccurrencesSingle {

    @SerializedName("id")
    public String id;

    @SerializedName("protocol_number")
    public int protocolNumber;

    @SerializedName("title")
    public String title;

    @SerializedName("body")
    public String body;

    @SerializedName("type")
    public String type;

    @SerializedName("status")
    public String status;

    @SerializedName("view_by")
    public int viewBy;

    @SerializedName("closed")
    public boolean closed;

    @SerializedName("deadline")
    public String deadline;

    @SerializedName("deadline_counter")
    public String deadlineCounter;

    @SerializedName("created_at")
    public Date createdAt;

    @SerializedName("updated_at")
    public String updatedAt;

    @SerializedName("condominium")
    public Occurrences.Condominium condominium;

    @SerializedName("theme")
    public Occurrences.Theme theme;

    @SerializedName("subject")
    public Occurrences.Subject subject;

    @SerializedName("priority")
    public Occurrences.Priority priority;

    @SerializedName("created_by")
    public Occurrences.User createdBy;

    @SerializedName("update_by")
    public Occurrences.User updatedBy;

    @SerializedName("owned_by")
    public List<Occurrences.User> ownedBy;

    @SerializedName("rating")
    public int rating;

    @SerializedName("attachments")
    public List<OccurrencesSingle.Attachments> attachments;



    public static class Attachments{
        private String photo_name;
        private String photo_etag;
        private String photo_url;

        public String getPhoto_name() {
            return photo_name;
        }

        public void setPhoto_name(String photo_name) {
            this.photo_name = photo_name;
        }

        public String getPhoto_etag() {
            return photo_etag;
        }

        public void setPhoto_etag(String photo_etag) {
            this.photo_etag = photo_etag;
        }

        public String getPhoto_url() {
            return photo_url;
        }

        public void setPhoto_url(String photo_url) {
            this.photo_url = photo_url;
        }
    }
}