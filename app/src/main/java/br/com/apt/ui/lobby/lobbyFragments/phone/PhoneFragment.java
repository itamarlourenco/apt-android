package br.com.apt.ui.lobby.lobbyFragments.phone;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;
import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.model.user.User;
import br.com.apt.widget.PagerItemFragment;

public class PhoneFragment extends PagerItemFragment implements View.OnClickListener {

    private int REQUEST_CALL_PHONE = 1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_phone, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if(view != null){
            view.findViewById(R.id.callToLobby).setOnClickListener(this);
        }
    }

    @Override
    protected String getPagerTitle() {
        return App.getContext().getString(R.string.phone);
    }

    @Override
    protected int getPagerIcon() {
        return R.drawable.ico_telefone;
    }

    @Override
    public void onClick(View v) {
        if(callPhoneGranted())
            call_action();
    }

    private boolean callPhoneGranted() {
        if(Build.VERSION.SDK_INT >= 23) {
            if(ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {

//                if(shouldShowRequestPermissionRationale(Manifest.permission.CALL_PHONE)) {
//                    Toast.makeText(getActivity(), "Permissão de telefone é necessária para completar a ligação.", Toast.LENGTH_LONG).show();
//                }

                requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, REQUEST_CALL_PHONE);
                return false;
            }
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == REQUEST_CALL_PHONE) {
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                call_action();
            } else {
                Toast.makeText(getActivity(), "Permissão não concedida.", Toast.LENGTH_SHORT).show();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void call_action() {
        User user = getUser();
        if(user != null){

            Volley volley = new Volley(new Volley.Callback() {
                @Override
                public void result(String result, int type) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        JSONObject object = jsonObject.getJSONObject("Object");
                        String phone = object.getString("phone");
                        startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone )));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public String uri() {
                    return URI.CONDOMINIUM;
                }
            });
            volley.showDialog(getActivity());
            volley.request();
        }
    }
}
