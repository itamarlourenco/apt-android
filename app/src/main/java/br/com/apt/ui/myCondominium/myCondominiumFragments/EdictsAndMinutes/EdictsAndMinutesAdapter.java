package br.com.apt.ui.myCondominium.myCondominiumFragments.EdictsAndMinutes;

import android.content.Context;
import android.media.Image;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;

import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.List;

import br.com.apt.R;
import br.com.apt.application.Volley;
import br.com.apt.ui.lobby.lobbyFragments.deliveries.DeliveriesListViewAdapter;
import br.com.apt.ui.lobby.lobbyFragments.deliveries.Delivery;
import br.com.apt.ui.myCondominium.myCondominiumFragments.Attachment;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomTextViewLight;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by adminbs on 9/3/17.
 */

public class EdictsAndMinutesAdapter extends BaseAdapter {
    private Context context;
    private List<Attachment> attachments;

    public EdictsAndMinutesAdapter(Context context, List<Attachment> attachments) {
        this.context = context;
        this.attachments = attachments;
    }

    @Override
    public int getCount() {
        return attachments != null ? attachments.size() : 0;
    }

    @Override
    public Attachment getItem(int position) {
        return attachments.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        EdictsAndMinutesAdapter.ViewHolder holder;

        if(convertView == null){
            view = LayoutInflater.from(context).inflate(R.layout.row_edicts_minutes, parent, false);
            holder = new EdictsAndMinutesAdapter.ViewHolder(view);
            view.setTag(holder);
        }else{
            view = convertView;
            holder = (EdictsAndMinutesAdapter.ViewHolder) view.getTag();
        }

        bindList(holder, position);
        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private void bindList(EdictsAndMinutesAdapter.ViewHolder holder, int position) {
        Attachment item = getItem(position);

        if(position % 2 == 0){
            holder.deliveryBaseBackground.setBackground(ContextCompat.getDrawable(context, R.drawable.button_menu_list_dark));
        }else{
            holder.deliveryBaseBackground.setBackground(ContextCompat.getDrawable(context, R.drawable.button_menu_list_light));
        }

        if(item.getType().equals(EdictsAndMinutes.TYPE_NOTICE)){
            holder.deliveryLabel.setText(R.string.edicts);
            holder.image.setImageResource(R.drawable.ico_edital);
        }else{
            holder.deliveryLabel.setText(R.string.minutes);
            holder.image.setImageResource(R.drawable.ico_atas);
        }

        SimpleDateFormat dateFormatter = new SimpleDateFormat(
                "dd/MM/yyyy");
        holder.deliveryDate.setText(dateFormatter.format(item.getEvent_date()));
        holder.deliveryTitle.setText(item.getTitle());
    }

    public class ViewHolder{
        private RelativeLayout deliveryBaseBackground;
        private CircleImageView deliveryImage;
        private CustomTextViewLight deliveryTitle;
        private CustomTextViewLight deliveryDescription;
        private CustomTextViewLight deliveryLabel;
        private CustomTextViewLight deliveryDate;
        private ImageView image;

        public ViewHolder(View view) {
            image = (ImageView) view.findViewById(R.id.image);
            deliveryBaseBackground = (RelativeLayout) view.findViewById(R.id.deliveryBaseBackground);
            deliveryImage = (CircleImageView) view.findViewById(R.id.deliveryImage);
            deliveryDate = (CustomTextViewLight) view.findViewById(R.id.deliveryDate);
            deliveryTitle = (CustomTextViewLight) view.findViewById(R.id.deliveryTitle);
            deliveryDescription = (CustomTextViewLight) view.findViewById(R.id.deliveryDescription);
            deliveryLabel = (CustomTextViewLight) view.findViewById(R.id.deliveryLabel);
        }
    }
}
