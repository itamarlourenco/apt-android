package br.com.apt.ui.myapt.myaptFragments.plan;

import java.util.List;

import br.com.apt.application.BaseGson;
import br.com.apt.application.BaseGsonMeuAdm;
import br.com.apt.ui.myapt.myaptFragments.notice.Notice;

/**
 * Created by adminbs on 2/23/18.
 */

public class PlantRequest extends BaseGsonMeuAdm{
    public List<Plant> object;

    public List<Plant> getObject() {
        return object;
    }

}
