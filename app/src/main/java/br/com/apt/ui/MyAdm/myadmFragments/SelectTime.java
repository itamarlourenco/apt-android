package br.com.apt.ui.MyAdm.myadmFragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.NumberPicker;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.ui.MyAdm.myadmFragments.cashflow.CashFlowAndAccountFragment;
import br.com.apt.util.Util;
import br.com.apt.widget.SendSuccessDialog;

/**
 * Created by adminbs on 9/5/17.
 */

public class SelectTime extends br.com.apt.widget.PagerItemFragment {

    public static final String CASH_FLOW = "CASH_FLOW";
    public static final String ACCOUNT = "ACCOUNT";

    private String isClass;
    private NumberPicker month;
    private NumberPicker year;
    private String[] arrayMonth;
    private List<String> arrayYear = new ArrayList<>();

    public SelectTime setIsClass(String isClass) {
        this.isClass = isClass;
        return this;
    }

    @Override
    protected String getPagerTitle() {
        if(this.isClass.equals(CASH_FLOW)){
            return App.getContext().getString(R.string.cash_flow);
        }
        return App.getContext().getString(R.string.account);
    }

    @Override
    protected int getPagerIcon() {
        if(this.isClass.equals(ACCOUNT)){
            return R.drawable.ico_menu_fulxo_caixa;
        }
        return R.drawable.ico_menu_prestacao_contas;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_selected_date, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        arrayMonth = getResources().getStringArray(R.array.months);
        if(view != null){
            month = (NumberPicker) view.findViewById(R.id.month);
            month.setMinValue(0);
            month.setMaxValue(arrayMonth.length-1);
            month.setFormatter(new NumberPicker.Formatter() {
                @Override
                public String format(int value) {
                    return arrayMonth[value].toUpperCase();
                }
            });
            month.setValue(Calendar.getInstance().get(Calendar.MONTH));
            month.setSelected(false);
            Util.removeDividerNumberPicker(month, getContext());
            Util.setStartValueNumberPicker(month);


            year = (NumberPicker) view.findViewById(R.id.year);
            for(int i=Calendar.getInstance().get(Calendar.YEAR); i>=2016; i--){
                arrayYear.add(String.valueOf(i));
            }
            year.setMinValue(0);
            year.setMaxValue(arrayYear.size()-1);
            year.setFormatter(new NumberPicker.Formatter() {
                @Override
                public String format(int value) {
                    return arrayYear.get(value);
                }
            });
            year.setSelected(false);
            Util.removeDividerNumberPicker(year, getContext());
            Util.setStartValueNumberPicker(year);

            view.findViewById(R.id.consult).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Date date = new Date();
                    int y = date.getYear() + 1900;
                    int m = date.getMonth();

                    if(Integer.parseInt(arrayYear.get(year.getValue())) >= y && month.getValue() > m) {
                        SendSuccessDialog.show(getString(R.string.error_date_movimentation), null, getFragmentManager(), null, SendSuccessDialog.TYPE_ERROR);
                    } else {
                        openItem();
                    }
                }
            });
        }
    }

    private void openItem(){
        startActivity(SelectedTime.newIntent(getContext(), month.getValue(), Integer.parseInt(arrayYear.get(year.getValue())), this.isClass));
        getActivity().overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
    }
}
