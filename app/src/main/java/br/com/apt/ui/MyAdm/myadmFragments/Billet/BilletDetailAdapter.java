package br.com.apt.ui.MyAdm.myadmFragments.Billet;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

import br.com.apt.R;
import br.com.apt.widget.CustomTextViewLight;

/**
 * Created by adminbs on 8/1/17.
 */

public class BilletDetailAdapter extends BaseAdapter {

    private Context context;
    private List<BilletDetails.Items> items;

    public BilletDetailAdapter(Context context, List<BilletDetails.Items> items) {
        this.context = context;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items != null ? items.size() : 0;
    }

    @Override
    public BilletDetails.Items getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        BilletDetailAdapter.ViewHolder holder;

        if(convertView == null){
            view = LayoutInflater.from(context).inflate(R.layout.row_billet_items, parent, false);
            holder = new BilletDetailAdapter.ViewHolder(view);
            view.setTag(holder);
        }else{
            view = convertView;
            holder = (BilletDetailAdapter.ViewHolder) view.getTag();
        }

        bindList(holder, position);
        return view;
    }

    private void bindList(ViewHolder holder, int position) {
        BilletDetails.Items items = getItem(position);

        holder.description.setText(items.getHistory());
        holder.value.setText(context.getString(R.string.money) + " " + items.getAmount());
    }

    public class ViewHolder{
        private CustomTextViewLight description;
        private CustomTextViewLight value;

        public ViewHolder(View view) {
            description = (CustomTextViewLight) view.findViewById(R.id.description);
            value = (CustomTextViewLight) view.findViewById(R.id.valueBillet);
        }
    }
}
