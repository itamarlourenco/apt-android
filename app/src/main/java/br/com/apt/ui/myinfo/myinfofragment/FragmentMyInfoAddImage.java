package br.com.apt.ui.myinfo.myinfofragment;

import android.content.Intent;
import android.net.Uri;

import java.util.HashMap;

/**
 * Created by adminbs on 11/9/16.
 */
public abstract class FragmentMyInfoAddImage extends FragmentMyInfoAdd {
    @Override
    protected abstract String getPagerTitle();

    @Override
    protected abstract int getPagerIcon();

    @Override
    protected abstract  HashMap<String, String> save();

    @Override
    protected abstract String getUri();

    @Override
    protected abstract int getTypeRequest();

    @Override
    protected abstract void saved();

    protected abstract void setConfigPhotos(Uri mCurrentPhotoUri, String mCurrentPhotoPath);

    public abstract void onActivityResultFragment(int requestCode, int resultCode, Intent data);

    @Override
    public abstract Uri getCurrentPhotoUri();

    @Override
    public abstract String getCurrentPhotoPath();
}
