package br.com.apt.ui.myinfo.myinfofragment.vehicle;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;
import java.util.List;

import br.com.apt.application.BaseGson;
import br.com.apt.application.ModelObject;
import br.com.apt.ui.myinfo.myinfofragment.NewUser;

/**
 * Created by fabriciooliveira on 9/28/16.
 */
public class VehicleRequest extends BaseGson {
    public List<Vehicle> Object;

    public List<Vehicle> getObject() {
        return Object;
    }
}
