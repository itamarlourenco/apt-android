package br.com.apt.ui.myinfo.myinfofragment.vehicle;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.android.volley.Request;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.List;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.BaseFragment;
import br.com.apt.application.BaseGson;
import br.com.apt.application.PicturesHelper;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.ui.myinfo.AutoCompleteDialog;
import br.com.apt.ui.myinfo.myinfofragment.AutoComplete;
import br.com.apt.ui.myinfo.myinfofragment.NewUser;
import br.com.apt.ui.myinfo.myinfofragment.employee.EmployeesAddFragment;
import br.com.apt.util.Mask;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomButton;
import br.com.apt.widget.CustomEditText;
import br.com.apt.widget.SendSuccessDialog;
import siclo.com.ezphotopicker.api.EZPhotoPick;
import siclo.com.ezphotopicker.api.EZPhotoPickStorage;

import static br.com.apt.R.id.view;

/**
 * Created by adminbs on 7/20/17.
 */

public class VehicleAddFragment extends BaseFragment implements Volley.Callback {
    private Vehicle vehicle;
    private static ImageView image;
    private Bitmap bitmap = null;

    private Spinner type;
    private CustomEditText brand;
    private CustomEditText model;
    private CustomEditText color;
    private CustomEditText plaque;
    private CustomEditText year;
    private RelativeLayout canvasAlterImage;
    private List<AutoComplete> brandList;
    private String typeVehicle = Vehicle.TYPE_CAR;
    private int brandId = 0;
    private int modelId = 0;
    private int yearId = 0;
    private List<AutoComplete> modelList;
    private List<AutoComplete> yearList;
    private int typeNumber = Vehicle.TYPE_NO_FILTER_NUMBER;

    public static VehicleAddFragment newInstance(){
        return new VehicleAddFragment();
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if(view != null){
            image =  (ImageView) view.findViewById(R.id.image);
            type = (Spinner) view.findViewById(R.id.type);
            brand = (CustomEditText) view.findViewById(R.id.brand);
            model = (CustomEditText) view.findViewById(R.id.model);
            color = (CustomEditText) view.findViewById(R.id.color);
            plaque = (CustomEditText) view.findViewById(R.id.plaque);
            plaque.addTextChangedListener(Mask.insert("###-####", plaque));
            year = (CustomEditText) view.findViewById(R.id.year);

            type = (Spinner) view.findViewById(R.id.type_car);
            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(), R.array.type_car, R.layout.spinner_gender);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            type.setAdapter(adapter);
            type.setSelection(0);
            type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if(position == 0){
                        typeVehicle = Vehicle.TYPE_CAR;
                        typeNumber = Vehicle.TYPE_CAR_NUMBER;
                    }else{
                        typeVehicle = Vehicle.TYPE_MOTORCYCLE;
                        typeNumber = Vehicle.TYPE_MOTORCYCLE_NUMBER;
                    }
                    getBrands();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            canvasAlterImage = (RelativeLayout) view.findViewById(R.id.canvasAlterImage);
            canvasAlterImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alterImage();
                }
            });

            brand.setOnClickListener(new OnClickListener(){
                @Override
                public void onClick(View v) {
                    openDialogBrand();
                }
            });
            brand.setOnFocusChangeListener(new OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if(hasFocus){
                        openDialogBrand();
                    }
                }
            });

            model.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    openDialogModel();
                }
            });
            model.setOnFocusChangeListener(new OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if(hasFocus){
                        openDialogModel();
                    }
                }
            });

            year.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    openDialogYear();
                }
            });
            year.setOnFocusChangeListener(new View.OnFocusChangeListener(){
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if(hasFocus){
                        openDialogYear();
                    }
                }
            });

            view.findViewById(R.id.sendVehicle).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    sendVehicle();
                }
            });
        }

        getBrands();
        willBeEdited();
    }

    private void willBeEdited() {
        if(vehicle != null){
            if(vehicle.getType().equals(Vehicle.TYPE_CAR)){
                type.setSelection(0);
            }

            if(vehicle.getType().equals(Vehicle.TYPE_MOTORCYCLE)){
                type.setSelection(1);
            }

            Vehicle.CarBrand carBrand = vehicle.getCarBrand();
            if(carBrand != null){
                brandId = carBrand.getId();
                brand.setText(vehicle.getCarBrand().getMarca());
            }

            Vehicle.CarModel carModel = vehicle.getCarModel();
            if(carModel != null){
                modelId = vehicle.getCar_model_id();
                model.setText(carModel.getModelo());
            }

            color.setText(vehicle.getColor());
            plaque.setText(vehicle.getLicensePlate());

            Vehicle.CarYear carYear = vehicle.getCarYear();
            if(carYear != null){
                year.setText(vehicle.getCarYear().getAno());
                yearId = vehicle.getCarYearId();
            }

            Picasso.with(getContext()).load(Volley.getUrlByImage(vehicle.getImage())).placeholder(R.drawable.placeholder_camera).into(image);
        }
    }

    private void sendVehicle(){
        try{
            if(brandId <= 0){
                throw new Exception(getString(R.string.error_brand));
            }

            if(modelId <= 0){
                throw new Exception(getString(R.string.error_model));
            }

            if(yearId <= 0){
                throw new Exception(getString(R.string.error_year));
            }

            if(TextUtils.isEmpty(color.getText())){
                throw new Exception(getString(R.string.error_color_veicle));
            }

            if(TextUtils.isEmpty(plaque.getText())){
                throw new Exception(getString(R.string.error_plaque));
            }

            HashMap<String, String> hashMap = new HashMap<>();
            if(vehicle != null && vehicle.getId() > 0){
                hashMap.put("id", String.valueOf(vehicle.getId()));
            }
            hashMap.put("type", typeVehicle);
            hashMap.put("car_model_id", String.valueOf(modelId));
            hashMap.put("car_year_id", String.valueOf(yearId));
            hashMap.put("color", color.getText().toString());
            hashMap.put("license_plate", plaque.getText().toString());

            File finalFile = Util.createImageFile();
            if(bitmap != null && finalFile != null){
                Util.sendBitmapToFile(finalFile, bitmap);
            }

            Volley volley = new Volley(this);
            volley.showDialog(getActivity());
            volley.request(Request.Method.POST, hashMap, 0, finalFile, "image");
        }catch (Exception e){
            SendSuccessDialog.show(e.getMessage(), null, getFragmentManager(), null, SendSuccessDialog.TYPE_ERROR);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_add_vehicle, container, false);
    }

    public void onActivityResultFragment(int requestCode, int resultCode, Intent data) {
        if(requestCode == EZPhotoPick.PHOTO_PICK_REQUEST_CODE && resultCode != 0 && VehicleAddFragment.image != null){
            try {
                EZPhotoPickStorage ezPhotoPickStorage = new EZPhotoPickStorage(getActivity());
                bitmap = ezPhotoPickStorage.loadLatestStoredPhotoBitmap();

                VehicleAddFragment.image.setImageBitmap(bitmap);
                VehicleAddFragment.image.setVisibility(View.VISIBLE);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void alterImage(){
        CharSequence[] options = {getString(R.string.takePhoto), getString(R.string.chooseGallery)};
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity());

        builder.setTitle("")
                .setItems(options, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if(which == 0) {
                            PicturesHelper.Camera(getActivity());
                        } else if(which == 1) {
                            PicturesHelper.Gallery(getActivity());
                        }else{
                            image.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.placeholder_camera));
                        }
                    }
                });
        builder.create().show();
    }

    public void getBrands() {
        Volley volley = new Volley(new Volley.Callback() {
            @Override
            public void result(String result, int type) {
                if(result != null){
                    VehicleBrandRequest vehicleBrandRequest = App.getGson().fromJson(result, VehicleBrandRequest.class);
                    if(vehicleBrandRequest != null){
                        brandList = vehicleBrandRequest.getObject();
                    }
                }
            }
            @Override
            public String uri() {
                return URI.VEHICLE_BRAND +"?id=" + typeNumber;
            }
        });
        volley.showDialog(getActivity());
        volley.request(Request.Method.GET, null);
    }


    public void openDialogBrand(){
        AutoCompleteDialog.show(getFragmentManager(), brandList, new AutoCompleteDialog.Click() {
            @Override
            public void click(AutoComplete autoComplete) {
                brandId = autoComplete.getId();
                brand.setText(autoComplete.getName());

                getModels();
            }
        }, getString(R.string.choose_brand));
    }

    public void getModels() {
        Volley volley = new Volley(new Volley.Callback() {
            @Override
            public void result(String result, int type) {
                if(result != null){
                    VehicleBrandRequest vehicleBrandRequest = App.getGson().fromJson(result, VehicleBrandRequest.class);
                    if(vehicleBrandRequest != null){
                        modelList = vehicleBrandRequest.getObject();
                    }
                }
            }
            @Override
            public String uri() {
                return URI.VEHICLE_MODEL;
            }
        });
        volley.showDialog(getActivity());
        JSONObject jsonObject = new JSONObject();
        try{
            jsonObject.put("brand_id", brandId);
        }catch (Exception e){}
        volley.request(Request.Method.POST, jsonObject.toString());
    }

    public void openDialogModel(){
        AutoCompleteDialog.show(getFragmentManager(), modelList, new AutoCompleteDialog.Click() {
            @Override
            public void click(AutoComplete autoComplete) {
                modelId = autoComplete.getId();
                model.setText(autoComplete.getName());
                getYears();
            }
        }, getString(R.string.choose_model));
    }

    private void getYears() {
        Volley volley = new Volley(new Volley.Callback() {
            @Override
            public void result(String result, int type) {
                if(result != null){
                    VehicleBrandRequest vehicleBrandRequest = App.getGson().fromJson(result, VehicleBrandRequest.class);
                    if(vehicleBrandRequest != null){
                        yearList = vehicleBrandRequest.getObject();
                    }
                }
            }
            @Override
            public String uri() {
                return URI.VEHICLE_YEAR;
            }
        });
        volley.showDialog(getActivity());
        JSONObject jsonObject = new JSONObject();
        try{
            jsonObject.put("model_id", modelId);
        }catch (Exception e){}
        volley.request(Request.Method.POST, jsonObject.toString());
    }

    private void openDialogYear() {
        AutoCompleteDialog.show(getFragmentManager(), yearList, new AutoCompleteDialog.Click() {
            @Override
            public void click(AutoComplete autoComplete) {
                yearId = autoComplete.getId();
                year.setText(autoComplete.getName());
            }
        }, getString(R.string.choose_year));
    }

    @Override
    public void result(String result, int type) {
        if(result != null){
            BaseGson baseGson = App.getGson().fromJson(result, BaseGson.class);
            if(baseGson.getStatus() == Volley.STATUS_OK){
                SendSuccessDialog.show(getString(R.string.success_vehicle), null, getFragmentManager(), new SendSuccessDialog.Actions(){
                    @Override
                    public void back() {
                        getActivity().finish();
                    }
                });
            }else{
                SendSuccessDialog.show(getString(R.string.error_generic), null, getFragmentManager(), null, SendSuccessDialog.TYPE_ERROR);
            }
        }
    }

    @Override
    public String uri() {
        return URI.VEHICLE;
    }
}
