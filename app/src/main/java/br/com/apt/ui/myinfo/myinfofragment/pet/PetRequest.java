package br.com.apt.ui.myinfo.myinfofragment.pet;

import android.os.Parcel;

import java.util.Date;
import java.util.List;

import br.com.apt.application.BaseGson;
import br.com.apt.application.ModelObject;
import br.com.apt.ui.myinfo.myinfofragment.NewUser;

/**
 * Created by fabricio.bezerra on 25/09/2016.
 */
public class PetRequest extends BaseGson {
    public List<Pet> Object;

    public List<Pet> getObject() {
        return Object;
    }
}

