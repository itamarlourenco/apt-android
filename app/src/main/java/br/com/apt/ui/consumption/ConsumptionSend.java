package br.com.apt.ui.consumption;

import com.google.gson.annotations.SerializedName;

import br.com.apt.application.SendRequest;

/**
 * Created by itamarlourenco on 16/01/16.
 */
public class ConsumptionSend extends SendRequest {
    private long id;

    @SerializedName("edificioID")
    private String buildId;

    @SerializedName("blocoId")
    private String blockId;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getBuildId() {
        return buildId;
    }

    public void setBuildId(String buildId) {
        this.buildId = buildId;
    }

    public String getBlockId() {
        return blockId;
    }

    public void setBlockId(String blockId) {
        this.blockId = blockId;
    }
}
