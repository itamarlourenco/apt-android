package br.com.apt.ui.lobby.lobbyFragments.safety;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import br.com.apt.application.ModelObject;

/**
 * Created by adminbs on 1/11/16.
 */
public class Safe implements ModelObject {

    private long id;

    @SerializedName("edificioId")
    private String buildId;

    @SerializedName("ipCamera")
    private String ipCamera;

    @SerializedName("descricao")
    private String description;

    @SerializedName("tipoCamera")
    private int tipeCamera;

    @SerializedName("idBloco")
    private int blockId;

    public Safe() {
    }

    public Safe(long id, String buildId, String ipCamera, String description, int tipeCamera, int blockId) {
        this.id = id;
        this.buildId = buildId;
        this.ipCamera = ipCamera;
        this.description = description;
        this.tipeCamera = tipeCamera;
        this.blockId = blockId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getBuildId() {
        return buildId;
    }

    public void setBuildId(String buildId) {
        this.buildId = buildId;
    }

    public String getIpCamera() {
        return ipCamera;
    }

    public void setIpCamera(String ipCamera) {
        this.ipCamera = ipCamera;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getTipeCamera() {
        return tipeCamera;
    }

    public void setTipeCamera(int tipeCamera) {
        this.tipeCamera = tipeCamera;
    }

    public int getBlockId() {
        return blockId;
    }

    public void setBlockId(int blockId) {
        this.blockId = blockId;
    }

    protected Safe(Parcel in) {
        id = in.readLong();
        buildId = in.readString();
        ipCamera = in.readString();
        description = in.readString();
        tipeCamera = in.readInt();
        blockId = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(buildId);
        dest.writeString(ipCamera);
        dest.writeString(description);
        dest.writeInt(tipeCamera);
        dest.writeInt(blockId);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Safe> CREATOR = new Parcelable.Creator<Safe>() {
        @Override
        public Safe createFromParcel(Parcel in) {
            return new Safe(in);
        }

        @Override
        public Safe[] newArray(int size) {
            return new Safe[size];
        }
    };
}