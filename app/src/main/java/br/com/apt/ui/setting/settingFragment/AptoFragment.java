package br.com.apt.ui.setting.settingFragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.widget.PagerItemFragment;

/**
 * Created by adminbs on 1/12/16.
 */
public class AptoFragment extends PagerItemFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_user_setting, container, false);
    }

    @Override
    protected String getPagerTitle() {
        return App.getContext().getString(R.string.apto);
    }

    @Override
    protected int getPagerIcon() {
        return R.drawable.ic_setting_key;
    }
}
