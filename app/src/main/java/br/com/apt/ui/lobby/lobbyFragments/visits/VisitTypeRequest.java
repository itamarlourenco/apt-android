package br.com.apt.ui.lobby.lobbyFragments.visits;

import java.util.List;

import br.com.apt.application.BaseGson;
import br.com.apt.ui.myinfo.myinfofragment.AutoComplete;

/**
 * Created by adminbs on 11/26/17.
 */

public class VisitTypeRequest extends BaseGson {
    public List<AutoComplete> Object;

    public List<AutoComplete> getObject() {
        return Object;
    }
}
