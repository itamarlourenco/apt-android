package br.com.apt.ui.myapt.myaptFragments.notice;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import br.com.apt.application.BaseActivity;
import br.com.apt.ui.reservations.CalendarFragment;

/**
 * Created by adminbs on 7/14/17.
 */

public class NoticesActivity extends BaseActivity {

    private static final String EXTRA_NOTICE = "EXTRA_NOTICE";

    public static Intent newIntent(Context context, Notice notice) {
        Intent intent = new Intent(context, NoticesActivity.class);
        intent.putExtra(EXTRA_NOTICE, notice);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public Fragment getFragment(){
        Intent intent = getIntent();
        if(intent != null){
            Notice notice =  intent.getParcelableExtra(EXTRA_NOTICE);
            return NoticeItemFragment.newInstance(notice);
        }
        return null;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
