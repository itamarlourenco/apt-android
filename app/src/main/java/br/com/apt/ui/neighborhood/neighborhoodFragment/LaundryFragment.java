package br.com.apt.ui.neighborhood.neighborhoodFragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.android.volley.Request;

import java.util.ArrayList;
import java.util.List;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.Volley;
import br.com.apt.model.user.User;
import br.com.apt.ui.neighborhood.neighborhoodFragment.adapter.NeighborhoodAdapter;
import br.com.apt.ui.neighborhood.neighborhoodFragment.model.Neighborhood;
import br.com.apt.ui.neighborhood.neighborhoodFragment.model.SendNeighborhoodDelivery;
import br.com.apt.widget.PagerItemFragment;

public class LaundryFragment extends NeighborhoodsFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_neighborhood, container, false);
    }

    @Override
    protected String getPagerTitle() {
        return App.getContext().getString(R.string.neighborhood_laundry);
    }

    @Override
    protected int getPagerIcon() {
        return R.drawable.ic_neighborhood_laundry;
    }

    @Override
    public String getStringService() {
        return "3";
    }
}
