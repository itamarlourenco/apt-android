package br.com.apt.ui.myCondominium.myCondominiumFragments.legislation;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import com.github.ybq.android.spinkit.SpinKitView;
import java.util.List;
import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.model.user.User;
import br.com.apt.ui.myCondominium.myCondominiumFragments.Attachments;
import br.com.apt.ui.myCondominium.myCondominiumFragments.AttachmentsRequest;
import br.com.apt.util.Util;
import br.com.apt.widget.PagerItemFragment;
import br.com.apt.widget.SendSuccessDialog;

/**
 * Created by meuapt on 07/03/18.
 */

public class LegislationFragment extends PagerItemFragment implements Volley.Callback {

    private User user = getUser();
    private ListView listView;
    private SpinKitView loader;
    private List<Attachments> attachmentsList;

    @Override
    protected String getPagerTitle() {
        return App.getContext().getString(R.string.legislation);
    }

    @Override
    protected int getPagerIcon() {
        return R.drawable.ico_docs_legais;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_legislation, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if(view != null){
            view.findViewById(R.id.civilCode).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    webView("CÓDIGO CIVIL", "http://www.planalto.gov.br/ccivil_03/leis/2002/L10406.htm");
                }
            });
            view.findViewById(R.id.condominiumLaw).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    webView("LEI DO CONDOMíNIO", "http://www.planalto.gov.br/ccivil_03/leis/L4591.htm");
                }
            });
            view.findViewById(R.id.individualLaw).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    webView("LEI DO INQUILINATO", "http://www.planalto.gov.br/ccivil_03/leis/L8245.htm");
                }
            });
        }
    }

    private void webView(String title, String url) {
        Intent intent = new Intent(getContext(), LegislationActivity.class);
        intent.putExtra("title", title);
        intent.putExtra("url", url);
        startActivity(intent);
    }

    @Override
    public void result(String result, int type) {
        try{
            Util.changeVisibility(loader, View.GONE, true);
            Util.changeVisibility(listView, View.VISIBLE, true);

            AttachmentsRequest attachmentsRequest = App.getGson().fromJson(result, AttachmentsRequest.class);
            attachmentsList = attachmentsRequest.getObject();
            listView.setAdapter(new LegislationAdapter(getContext(), attachmentsList));
        }catch (Exception e){
            SendSuccessDialog.show(getString(R.string.error_generic), null, getFragmentManager(), new SendSuccessDialog.Actions() {
                @Override
                public void back() {
                    getActivity().finish();
                }
            }, SendSuccessDialog.TYPE_ERROR);
        }
    }

    @Override
    public String uri() {
        return String.format(URI.DOCUMENTS, user.getCondominiumId());
    }
}
