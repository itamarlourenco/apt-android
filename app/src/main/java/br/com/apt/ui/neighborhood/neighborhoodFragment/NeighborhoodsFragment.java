package br.com.apt.ui.neighborhood.neighborhoodFragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.android.volley.Request;

import java.util.ArrayList;
import java.util.List;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.model.user.User;
import br.com.apt.ui.neighborhood.neighborhoodFragment.adapter.NeighborhoodAdapter;
import br.com.apt.ui.neighborhood.neighborhoodFragment.model.Neighborhood;
import br.com.apt.ui.neighborhood.neighborhoodFragment.model.RequestNeighborhoodDelivery;
import br.com.apt.ui.neighborhood.neighborhoodFragment.model.SendNeighborhoodDelivery;
import br.com.apt.util.Util;
import br.com.apt.widget.PagerItemFragment;

/**
 * Created by adminbs on 1/21/16.
 */
public class NeighborhoodsFragment extends PagerItemFragment implements Volley.Callback {

    private ListView listView;
    private List<Neighborhood> neighborhoodList = new ArrayList<>();

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        User user = getUser();

        if(view != null && user != null){
            SendNeighborhoodDelivery sendNeighborhood = new SendNeighborhoodDelivery();
            sendNeighborhood.setTypeServiceId(getStringService());
            Volley volley = new Volley(this);
            volley.request(Request.Method.POST, sendNeighborhood.toJson());
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_neighborhood, container, false);
    }

    @Override
    protected String getPagerTitle() {
        return App.getContext().getString(R.string.neighborhood_delivery);
    }

    @Override
    protected int getPagerIcon() {
        return R.drawable.ic_neighborhood_delivery;
    }

    @Override
    public void result(String result, int type) {
        View view = getView();
        try{
            if(result == null){
                throw new Exception(getString(R.string.internet_error));
            }
            RequestNeighborhoodDelivery requestNeighborhoodDelivery = App.getGson().fromJson(result, RequestNeighborhoodDelivery.class);
            if(requestNeighborhoodDelivery == null || view == null){
                throw new Exception(getString(R.string.error_generic));
            }

            if(requestNeighborhoodDelivery.getStatus() == Volley.STATUS_OK){
                neighborhoodList = requestNeighborhoodDelivery.getObject();
                listView = (ListView) view.findViewById(R.id.listView);
                listView.setAdapter(new NeighborhoodAdapter(neighborhoodList, getContext()));
                Util.hideFeedBackFragment(view, listView);
            }else{
                throw new Exception(requestNeighborhoodDelivery.getMessage());
            }
        }catch (Exception e){
            Util.showFeedbackFragment(view, e.getMessage());
        }
    }

    @Override
    public String uri() {
        return URI.SERVICES;
    }

    public String getStringService() {
        return "2";
    }
}
