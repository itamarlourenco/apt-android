package br.com.apt.ui.myapt.myaptFragments.notice;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.apt.R;
import br.com.apt.application.Volley;
import br.com.apt.ui.lobby.lobbyFragments.visits.Visit;
import br.com.apt.ui.lobby.lobbyFragments.visits.VisitsListViewAdapter;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomTextView;
import br.com.apt.widget.CustomTextViewLight;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by adminbs on 7/14/17.
 */

public class NoticesAdapter extends BaseAdapter {

    private List<Notice> noticesList;
    private Context context;

    public NoticesAdapter(Context context, List<Notice> noticesList) {
        this.noticesList = noticesList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return noticesList == null ? 0 : noticesList.size();
    }

    @Override
    public Notice getItem(int position) {
        return noticesList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        NoticesAdapter.ViewHolder holder;

        if(convertView == null){
            view = LayoutInflater.from(context).inflate(R.layout.row_notices, parent, false);
            holder = new NoticesAdapter.ViewHolder(view);
            view.setTag(holder);
        }else{
            view = convertView;
            holder = (NoticesAdapter.ViewHolder) view.getTag();
        }

        bindList(holder, position);
        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void bindList(NoticesAdapter.ViewHolder holder, int position){
        Notice notice = getItem(position);

        if(position % 2 == 0){
            holder.baseBackground.setBackground(ContextCompat.getDrawable(context, R.drawable.button_menu_list_dark));
        }else{
            holder.baseBackground.setBackground(ContextCompat.getDrawable(context, R.drawable.button_menu_list_light));
        }

        Picasso.with(context).load(Volley.getUrlByImage(notice.getImage())).placeholder(R.drawable.placeholder_camera).into(holder.image);
        holder.title.setText(notice.getTitle());
        holder.description.setText(notice.getUserName());
        holder.label.setText(
                String.format(context.getText(R.string.hours_and_time).toString(), Util.dateFormatted(notice.getCreatedAt()), Util.timeFormatted(notice.getCreatedAt(), "HH:mm"))
        );

        if(notice.isRead()){
            holder.read.setVisibility(View.GONE);
        }else{
            holder.read.setVisibility(View.VISIBLE);
        }
    }

    public class ViewHolder{
        private RelativeLayout baseBackground;
        private CircleImageView image;
        private CustomTextViewLight title;
        private CustomTextViewLight description;
        private CustomTextViewLight label;
        private View read;

        public ViewHolder(View view) {
            baseBackground = (RelativeLayout) view.findViewById(R.id.baseBackground);
            image = (CircleImageView) view.findViewById(R.id.image);
            title = (CustomTextViewLight) view.findViewById(R.id.title);
            description = (CustomTextViewLight) view.findViewById(R.id.description);
            label = (CustomTextViewLight) view.findViewById(R.id.label);
            read = view.findViewById(R.id.read);
        }
    }
}
