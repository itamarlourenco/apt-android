package br.com.apt.ui.reservations;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.prolificinteractive.materialcalendarview.CalendarDay;

import br.com.apt.R;
import br.com.apt.application.BaseActivity;

/**
 * Created by adminbs on 9/7/17.
 */

public class TimeListActivity extends BaseActivity {

    private static final String EXTRA_INSTALLATIONS = "EXTRA_INSTALLATIONS";
    private static final String EXTRA_DATE = "EXTRA_DATE";

    public static Intent newIntent(Context context, Installations installations, CalendarDay date) {
        Intent intent = new Intent(context, TimeListActivity.class);
        intent.putExtra(EXTRA_INSTALLATIONS, installations);
        intent.putExtra(EXTRA_DATE, date);

        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public Fragment getFragment() {
        Intent intent = getIntent();
        if(intent != null){
            Installations installations = intent.getParcelableExtra(EXTRA_INSTALLATIONS);
            CalendarDay date  = intent.getParcelableExtra(EXTRA_DATE);

            TimeListFragment timeListFragment = TimeListFragment.newInstance();
            timeListFragment.setInstallations(installations);
            timeListFragment.setDate(date);

            return timeListFragment;
        }
        return null;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_down, R.anim.slide_out_down);
    }
}