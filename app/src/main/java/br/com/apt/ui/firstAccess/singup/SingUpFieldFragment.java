package br.com.apt.ui.firstAccess.singup;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.com.apt.BuildConfig;
import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.BaseFragment;
import br.com.apt.ui.lobby.lobbyFragments.deliveries.Delivery;
import br.com.apt.util.Mask;
import br.com.apt.widget.CustomEditText;

/**
 * Created by adminbs on 9/14/16.
 */
public class SingUpFieldFragment extends BaseFragment {

    public static CustomEditText name;
    public static CustomEditText email;
    public static CustomEditText phone;
    public static CustomEditText cpf;


    public static SingUpFieldFragment newInstance() {
        return new SingUpFieldFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.sing_up_field, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if(view != null){
            name = (CustomEditText) view.findViewById(R.id.name);
            name.setSelected(false);
            email = (CustomEditText) view.findViewById(R.id.email);
            email.setSelected(false);
            phone = (CustomEditText) view.findViewById(R.id.phone);
            phone.setSelected(false);

            cpf = (CustomEditText) view.findViewById(R.id.cpf);
            cpf.setSelected(false);

            phone.addTextChangedListener(Mask.insert("(##)#####-####", phone));
            cpf.addTextChangedListener(Mask.insert("###.###.###-##", cpf));

            if(App.isDeviceHml()){
                name.setText("Itamar Developer");
                email.setText("itamar.developer@gmail.com");
                phone.setText("11 975400 9414");
                cpf.setText("354.743.988-40");
            }
        }
    }
}
