package br.com.apt.ui.firstAccess.singup;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.UnderlineSpan;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.VideoView;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.android.volley.Request;
import com.viewpagerindicator.CirclePageIndicator;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.HashMap;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.BaseFragment;
import br.com.apt.application.BaseGson;
import br.com.apt.application.BaseGsonMeuAdm;
import br.com.apt.application.Config;
import br.com.apt.application.SendRequest;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.ui.MyAdm.Occurrences;
import br.com.apt.ui.firstAccess.animations.CodeAnimationsFragment;
import br.com.apt.ui.firstAccess.animations.FinishSingUpActivity;
import br.com.apt.ui.myapt.myaptFragments.plan.PlantAddFragment;
import br.com.apt.util.CheckCpf;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomButton;
import br.com.apt.widget.CustomTextView;


public class SingUpFragment extends BaseFragment implements View.OnClickListener, ViewPager.OnPageChangeListener{
    private ViewPager viewPager;
    private CirclePageIndicator pagerViewIndicator;
    private Volley volley;

    private static final int PAGE_SING_UP = 0;
    private static final int PAGE_PICTURES = 1;
    private static final int PAGE_PASSWORD = 2;

    private CodeAnimationsFragment.Code code ;

    private CustomButton buttonNext;
    private static SingUpPhotoFragment singUpPhotoFragment;
    private VideoView videoView;

    public static SingUpFragment newInstance() {
        return new SingUpFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.sing_up_fragment, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        SingUpFragment.singUpPhotoFragment = SingUpPhotoFragment.newInstance();


        View view = getView();
        if (view != null) {
            Intent intent = getActivity().getIntent();
            Bundle extras = intent.getExtras();

            code = extras.getParcelable(SingUpActivity.EXTRA_CODE);

            viewPager = (ViewPager) view.findViewById(R.id.viewPager);
            pagerViewIndicator = (CirclePageIndicator) view.findViewById(R.id.pagerViewIndicator);

            viewPager.addOnPageChangeListener(this);
            viewPager.setAdapter(new SingUpAdapter(getChildFragmentManager()));

            pagerViewIndicator.setViewPager(viewPager);

            buttonNext = (CustomButton) view.findViewById(R.id.buttonNext);
            buttonNext.setOnClickListener(this);

            Util.setMargins(buttonNext, 20, 20, 20, Util.getSoftButtonsBarHeight(getActivity()) + 40);

            CustomTextView customTextView = (CustomTextView) view.findViewById(R.id.textTerms);

            SpannableString content = new SpannableString(customTextView.getText());
            content.setSpan(new UnderlineSpan(), 56, customTextView.getText().length(), 0);
            customTextView.setText(content);

            customTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openTermsOfUse();
                }
            });
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonNext:
                next();
                break;
        }
    }

    private void next() {
        switch (viewPager.getCurrentItem()){
            case SingUpFragment.PAGE_SING_UP:
                viewPager.setCurrentItem(SingUpFragment.PAGE_PICTURES);
                doSingUp();
                break;
            case SingUpFragment.PAGE_PICTURES:
                viewPager.setCurrentItem(SingUpFragment.PAGE_PASSWORD);
                break;
            case SingUpFragment.PAGE_PASSWORD:
                doSingUp();
                break;
        }
    }

    private void openTermsOfUse() {
        DialogTermsOfUse.show(getFragmentManager());
    }

    public void doSingUp(){
        final String name = SingUpFieldFragment.name.getText().toString();
        final String email = SingUpFieldFragment.email.getText().toString();
        final String phone = SingUpFieldFragment.phone.getText().toString();
        final String cpf = SingUpFieldFragment.cpf.getText().toString();

        final String password = SingUpPasswordFragment.password.getText().toString();
        final String confirmPassword = SingUpPasswordFragment.confirmPassword.getText().toString();

        if(viewPager.getCurrentItem() == SingUpFragment.PAGE_PICTURES) {
            if (TextUtils.isEmpty(name)) {
                Util.showDialog(getActivity(), getString(R.string.please_enter_with_name), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SingUpFieldFragment.name.requestFocus();
                        viewPager.setCurrentItem(SingUpFragment.PAGE_SING_UP);
                    }
                });
                return;
            }

            if (!Util.isValidEmail(email) || TextUtils.isEmpty(email)) {
                Util.showDialog(getActivity(), getString(R.string.please_enter_with_email), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SingUpFieldFragment.email.requestFocus();
                        viewPager.setCurrentItem(SingUpFragment.PAGE_SING_UP);
                    }
                });
                return;
            }

            if(!TextUtils.isEmpty(cpf)){
                if(!CheckCpf.isCPF(cpf.replace(".", "").replace("-", ""))){
                    Util.showDialog(getActivity(), getString(R.string.please_enter_with_cpf), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            SingUpFieldFragment.email.requestFocus();
                            viewPager.setCurrentItem(SingUpFragment.PAGE_SING_UP);
                        }
                    });
                    return;
                }
            }
        }


        if(viewPager.getCurrentItem() == SingUpFragment.PAGE_PASSWORD) {
            if (!password.equals(confirmPassword)) {
                Util.showDialog(getActivity(), getString(R.string.please_enter_same_password));
                return;
            }

            if (TextUtils.isEmpty(password)) {
                Util.showDialog(getActivity(), getString(R.string.please_enter_with_password));
                return;
            }

            if(password.length() < 4){
                Util.showDialog(getActivity(), getString(R.string.please_enter_with_password_4));
                return;
            }
        }

        if(viewPager.getCurrentItem() != SingUpFragment.PAGE_PASSWORD) {
            return;
        }


        volley = new Volley(new Volley.Callback() {
            @Override
            public void result(String result, int type) {
                if(result != null){
                    BaseGsonMeuAdm baseGson = App.getGson().fromJson(result, BaseGsonMeuAdm.class);
                    if(baseGson.getCode() == Volley.STATUS_OK){
                        getActivity().startActivity(FinishSingUpActivity.newIntent(getActivity(), email, password, name, code.getApto()));
                        getActivity().finish();
                    }else{
                        Util.showDialog(getActivity(), baseGson.getMessage().equals("") ? getString(R.string.error_generic_reservation) : baseGson.getMessage());
                    }
                }
            }

            @Override
            public String uri() {
                return URI.REGISTER_MEUADM;
            }
        });

        volley.showDialog(getActivity());
        (new TaskUploadS3(SingUpFragment.this)).execute();
    }

    private static class TaskUploadS3 extends AsyncTask<Void, Void, Void> {

        private SingUpFragment singUpFragment;

        public TaskUploadS3(SingUpFragment singUpFragment) {
            this.singUpFragment = singUpFragment;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            final File finalPhotoPlant = Util.createImageFile();
            if(SingUpPhotoFragment.bitmap != null && finalPhotoPlant != null){
                Util.sendBitmapToFile(finalPhotoPlant, SingUpPhotoFragment.bitmap);


                String namePhoto = Util.generateName("user_android");
                SingUpPhotoFragment.urlImage = Config.Amazon.handleLocation(namePhoto);

                AmazonS3Client s3Client = new AmazonS3Client(new BasicAWSCredentials(Config.Amazon.ACCESS_KEY, Config.Amazon.PUBLIC_KEY));
                PutObjectRequest requestBillet = new PutObjectRequest(Config.Amazon.BUCKET, String.format(Config.Amazon.PATH, namePhoto), finalPhotoPlant);
                s3Client.putObject(requestBillet);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            singUpFragment.executePost();
        }
    }

    public void executePost(){
        final String name = SingUpFieldFragment.name.getText().toString();
        final String email = SingUpFieldFragment.email.getText().toString();
        final String phone = SingUpFieldFragment.phone.getText().toString();
        final String cpf = SingUpFieldFragment.cpf.getText().toString();

        final String password = SingUpPasswordFragment.password.getText().toString();


        String handleCpf = "";
        if(!TextUtils.isEmpty(cpf)){
            handleCpf = cpf.replace(".", "").replace("-", "");
        }

        SingUpSent singUpSent = new SingUpSent(
                code.getCompanyKey(),
                name,
                handleCpf,
                email,
                code.getCondominiumKey(),
                password,
                new SingUpSent.Condominium(code.getCondominium()),
                new SingUpSent.Block(code.getBlockKey(), code.getBloco()),
                new SingUpSent.Unit(code.getUnitKey()),
                new SingUpSent.Photo(SingUpPhotoFragment.urlImage, "0")
        );
        volley.isMeuADM(true);
        volley.request(Request.Method.POST, singUpSent.toJson());
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    public void onActivityResultFragment(int requestCode, int resultCode, Intent data) {
        if(SingUpFragment.singUpPhotoFragment != null){
            SingUpFragment.singUpPhotoFragment.onActivityResultFragment(requestCode, resultCode, data);
        }
    }

    private class SingUpAdapter extends FragmentPagerAdapter {

        public SingUpAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public BaseFragment getItem(int position) {
            switch (position){
                case SingUpFragment.PAGE_SING_UP:
                    return SingUpFieldFragment.newInstance();
                case SingUpFragment.PAGE_PICTURES:
                    return SingUpFragment.singUpPhotoFragment;
                case SingUpFragment.PAGE_PASSWORD:
                    return SingUpPasswordFragment.newInstance();
            }

            return SingUpFieldFragment.newInstance();
        }

        @Override
        public int getCount() {
            return new int[]{SingUpFragment.PAGE_SING_UP, SingUpFragment.PAGE_PICTURES, SingUpFragment.PAGE_PASSWORD}.length;
        }
    }


    private static class SingUpSent extends SendRequest {
        private String company_key;
        private String name;
        private String document;
        private String email;
        private String condominium_key;
        private String password;
        private Condominium condominium;
        private Block block;
        private Unit unit;
        private Photo photo;

        public SingUpSent(String company_key, String name, String document, String email, String condominium_key, String password, Condominium condominium, Block block, Unit unit, Photo photo) {
            this.company_key = company_key;
            this.name = name;
            this.document = document;
            this.email = email;
            this.condominium_key = condominium_key;
            this.password = password;
            this.condominium = condominium;
            this.block = block;
            this.unit = unit;
            this.photo = photo;
        }

        public static class Photo{
            private String url;
            private String etag;

            public Photo(String url, String etag) {
                this.url = url;
                this.etag = etag;
            }

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }

            public String getEtag() {
                return etag;
            }

            public void setEtag(String etag) {
                this.etag = etag;
            }
        }

        public static class Condominium{
            private String name;

            public Condominium(String name) {
                this.name = name;
            }
        }

        public static class Block{
            private String id;
            private String name;

            public Block(String id, String name) {
                this.id = id;
                this.name = name;
            }
        }

        public static class Unit{
            private String id;

            public Unit(String id) {
                this.id = id;
            }
        }
    }

}
