package br.com.apt.ui.reservations;

import java.util.List;

import br.com.apt.application.BaseGsonMeuAdm;
import br.com.apt.ui.myapt.myaptFragments.build.Apartment;

/**
 * Created by adminbs on 9/10/17.
 */

public class RulesProceduresRequest extends BaseGsonMeuAdm {
    public List<RulesProcedures> object;

    public List<RulesProcedures> getObject() {
        return object;
    }
}
