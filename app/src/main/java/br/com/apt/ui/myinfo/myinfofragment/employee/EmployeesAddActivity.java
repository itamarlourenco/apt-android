package br.com.apt.ui.myinfo.myinfofragment.employee;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import br.com.apt.R;
import br.com.apt.application.BaseActivity;
import br.com.apt.ui.reservations.CalendarFragment;

/**
 * Created by adminbs on 7/19/17.
 */

public class EmployeesAddActivity extends BaseActivity {

    public static final String EXTRA_EMPLOYEES = "EXTRA_EMPLOYEES";
    private EmployeesAddFragment employeesAddFragment = EmployeesAddFragment.newInstance();

    public static Intent newIntent(Context context) {
        return new Intent(context, EmployeesAddActivity.class);
    }

    public static Intent newIntent(Context context, Employees employees) {
        Intent intent = new Intent(context, EmployeesAddActivity.class);
        intent.putExtra(EXTRA_EMPLOYEES, employees);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public Fragment getFragment(){
        Intent intent = getIntent();
        if(intent != null){
            Employees employees = intent.getParcelableExtra(EmployeesAddActivity.EXTRA_EMPLOYEES);
            employeesAddFragment.setEmployees(employees);
        }
        return employeesAddFragment;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        employeesAddFragment.onActivityResultFragment(requestCode, resultCode, data);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_down, R.anim.slide_out_down);
    }
}