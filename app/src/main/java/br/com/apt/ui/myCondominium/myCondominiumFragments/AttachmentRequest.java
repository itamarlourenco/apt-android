package br.com.apt.ui.myCondominium.myCondominiumFragments;

import java.util.List;

import br.com.apt.application.BaseGsonMeuAdm;

/**
 * Created by adminbs on 8/6/17.
 */

public class AttachmentRequest extends BaseGsonMeuAdm {

    private Attachments object;

    public Attachments getObject() {
        return object;
    }

    public void setObject(Attachments object) {
        this.object = object;
    }
}
