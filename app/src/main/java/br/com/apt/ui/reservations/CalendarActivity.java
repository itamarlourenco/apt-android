package br.com.apt.ui.reservations;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import br.com.apt.R;
import br.com.apt.application.BaseActivity;

/**
 * Created by adminbs on 9/6/17.
 */

public class CalendarActivity  extends BaseActivity {

    private static final String EXTRA_INSTALLATIONS = "EXTRA_INSTALLATIONS";

    public static Intent newIntent(Context context, Installations installations) {
        Intent intent = new Intent(context, CalendarActivity.class);
        intent.putExtra(EXTRA_INSTALLATIONS, installations);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public CalendarFragment getFragment() {
        Intent intent = getIntent();
        if(intent != null){
            Installations installations = intent.getParcelableExtra(EXTRA_INSTALLATIONS);
            return CalendarFragment.newInstance(installations);
        }
        return null;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_down, R.anim.slide_out_down);
    }
}