package br.com.apt.ui.forgotPassword;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;

import org.json.JSONObject;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.BaseFragment;
import br.com.apt.application.BaseGson;
import br.com.apt.application.SendRequest;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.model.user.User;
import br.com.apt.util.Logger;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomEditText;

public class ForgotPasswordFragment extends BaseFragment implements View.OnClickListener, Volley.Callback {

    public CustomEditText userName;

    public static ForgotPasswordFragment newInstance() {
        return new ForgotPasswordFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.forgot_password_fragment, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if (view != null) {
            view.findViewById(R.id.back).setOnClickListener(this);
            view.findViewById(R.id.recoverPassword).setOnClickListener(this);
            userName = (CustomEditText) view.findViewById(R.id.userName);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                backActivity();
                break;

            case R.id.recoverPassword:
                recoverPassword();
                break;
        }
    }

    private void backActivity() {
        getActivity().finish();
    }

    private void recoverPassword() {
        if(TextUtils.isEmpty(userName.getText())){
            Util.showDialog(getActivity(), R.string.error_lost_passowrd);
            return;
        }

        LostPassword lostPassword = new LostPassword();
        lostPassword.setUsername(userName.getText().toString());

        Volley volley = new Volley(this);
        volley.showDialog(getActivity());
        volley.request(Request.Method.PUT, lostPassword.toJson());
    }

    @Override
    public void result(String result, int type) {
        if(result != null){
            BaseGson baseGson = App.getGson().fromJson(result, BaseGson.class);
            Util.showDialog(getActivity(), baseGson.getMessage());
        }
    }

    @Override
    public String uri() {
        return URI.LOST_PASSWORD;
    }

    public class LostPassword extends SendRequest{
        public String username;

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }
    }
}
