package br.com.apt.ui.MyAdm.myadmFragments.administrator;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.github.ybq.android.spinkit.SpinKitView;
import com.squareup.picasso.Picasso;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.ui.ExceptionWithoutResult;
import br.com.apt.util.Util;
import br.com.apt.widget.Base64CircleImageView;
import br.com.apt.widget.CustomButton;
import br.com.apt.widget.CustomTextViewLight;
import br.com.apt.widget.PagerItemFragment;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by adminbs on 7/21/17.
 */

public class AdministratorFragment extends PagerItemFragment implements Volley.Callback{
    private CustomTextViewLight foundation;
    private CustomTextViewLight properties;
    private CustomTextViewLight employers;
    private ImageView logo;
    private Base64CircleImageView image;
    private CustomTextViewLight name;
    private CustomTextViewLight text;
    private CustomButton knowMore;

    private SpinKitView loader;
    private RelativeLayout canvasAdm;
    private CompanyData companyData;

    @Override
    protected String getPagerTitle() {
        return App.getContext().getString(R.string.administrator);
    }

    @Override
    protected int getPagerIcon() {
        return R.drawable.ico_menu_administradora;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_adm_main, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if (view != null) {
            logo = (ImageView) view.findViewById(R.id.administrationMainLogo);
            foundation = (CustomTextViewLight) view.findViewById(R.id.administrationMainFoundation);
            properties = (CustomTextViewLight) view.findViewById(R.id.administrationMainProperties);
            employers = (CustomTextViewLight) view.findViewById(R.id.administrationMainEmployers);
            image = (Base64CircleImageView) view.findViewById(R.id.administrationMainManagerImage);
            name = (CustomTextViewLight) view.findViewById(R.id.administrationMainManagerName);
            text = (CustomTextViewLight) view.findViewById(R.id.administrationMainManagerText);
            knowMore = (CustomButton) view.findViewById(R.id.administrationMainKnowMore);
            logo = (ImageView) view.findViewById(R.id.administrationMainLogo);
            knowMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    knowMore();
                }
            });

            loader = (SpinKitView) view.findViewById(R.id.loader);
            canvasAdm = (RelativeLayout) view.findViewById(R.id.canvasAdm);
        }

        Volley volley = new Volley(this);
        volley.isMeuADM(true);
        volley.request();
    }

    private void knowMore(){
        startActivity(AdministratorKnowMoreActivity.newIntent(getContext(), companyData));
        getActivity().overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
    }

    @Override
    public void result(String result, int type) {
        View view = getView();
        try{
            Util.changeVisibility(loader, View.GONE, true);
            Util.changeVisibility(canvasAdm, View.VISIBLE, true);
            CompanyDataRequest companyDataRequest = App.getGson().fromJson(result, CompanyDataRequest.class);
            companyData = companyDataRequest.getObject();

            if(companyData == null){
                throw new ExceptionWithoutResult(getString(R.string.message_without_visits), R.drawable.img_visita_no_have);
            }

            foundation.setText(companyData.founded_year);
            properties.setText(companyData.properties);
            employers.setText(companyData.employees);
            name.setText(companyData.manager_name);
            text.setText(companyData.manager_email);

            Picasso.with(getContext()).load(companyData.getManagerPhoto()).placeholder(R.drawable.default_camera).into(image);
            Picasso.with(getContext()).load(companyData.logo_url).placeholder(R.drawable.default_camera).into(logo);
            Picasso.with(getContext()).load(companyData.logo_url).placeholder(R.drawable.default_camera).into(image);


        }catch (Exception exception){
            Util.handlePlaceholderError(view, getActivity());
        }
    }

    @Override
    public String uri() {
        return URI.COMPANY;
    }
}
