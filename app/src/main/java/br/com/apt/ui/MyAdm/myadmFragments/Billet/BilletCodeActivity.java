package br.com.apt.ui.MyAdm.myadmFragments.Billet;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import br.com.apt.application.BaseActivity;
import br.com.apt.ui.reservations.CalendarFragment;

/**
 * Created by adminbs on 7/31/17.
 */

public class BilletCodeActivity extends BaseActivity {

    private static final String EXTRA_BILLET_ITEM = "EXTRA_BILLET_ITEM";

    public static Intent newIntent(Context context, Billet.Items items) {
        Intent intent = new Intent(context, BilletCodeActivity.class);
        intent.putExtra(BilletCodeActivity.EXTRA_BILLET_ITEM, items);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public Fragment getFragment(){
        Intent intent = getIntent();
        if(intent != null){
            Billet.Items items =  intent.getParcelableExtra(EXTRA_BILLET_ITEM);
            return BilletCodeFragment.newInstance(items);
        }

        return null;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void finish() {
        super.finish();
    }
}