package br.com.apt.ui.MyAdm.myadmFragments.register;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.android.volley.Request;
import com.github.ybq.android.spinkit.SpinKitView;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.model.user.User;
import br.com.apt.ui.MyAdm.OccurrencesSend;
import br.com.apt.ui.MyAdm.OccurrenceRequest;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomEditText;
import br.com.apt.widget.PagerItemFragment;
import br.com.apt.widget.SendSuccessDialog;

/**
 * Created by adminbs on 7/21/17.
 */

public class RegisterFragment extends PagerItemFragment implements Volley.Callback {

    private CustomEditText name;
    private CustomEditText email;
    private CustomEditText document;
    private CustomEditText mobile;
    private CustomEditText zipcode;
    private CustomEditText neighborhood;
    private CustomEditText street;
    private CustomEditText number;
    private CustomEditText city;
    private CustomEditText state;
    private CustomEditText country;
    private CustomEditText complement;
    private CustomEditText billetZipcode;
    private CustomEditText billetAddress;
    private CustomEditText billetComplement;
    private CustomEditText billetNumber;
    private CustomEditText billetNeighborhood;
    private CustomEditText billetState;
    private CustomEditText billetCity;
    private LinearLayout baseRegister;
    private SpinKitView loader;

    private Register register;


    @Override
    protected String getPagerTitle() {
        return App.getContext().getString(R.string.register);
    }

    @Override
    protected int getPagerIcon() {
        return R.drawable.ico_menu_cadastro;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_meuadm_register, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if(view != null){
            name = (CustomEditText) view.findViewById(R.id.name);
            email = (CustomEditText) view.findViewById(R.id.email);
            document = (CustomEditText) view.findViewById(R.id.document);
            mobile = (CustomEditText) view.findViewById(R.id.mobile);
            zipcode = (CustomEditText) view.findViewById(R.id.zipcode);
            neighborhood = (CustomEditText) view.findViewById(R.id.neighborhood);
            street = (CustomEditText) view.findViewById(R.id.street);
            number = (CustomEditText) view.findViewById(R.id.number);
            city = (CustomEditText) view.findViewById(R.id.city);
            state = (CustomEditText) view.findViewById(R.id.state);
            country = (CustomEditText) view.findViewById(R.id.country);
            complement = (CustomEditText) view.findViewById(R.id.complement);
            baseRegister = (LinearLayout) view.findViewById(R.id.baseRegister);
            loader = (SpinKitView) view.findViewById(R.id.loader);
            baseRegister = (LinearLayout) view.findViewById(R.id.baseRegister);
            billetZipcode = (CustomEditText) view.findViewById(R.id.billetZipcode);
            billetAddress = (CustomEditText) view.findViewById(R.id.billetAddress);
            billetComplement = (CustomEditText) view.findViewById(R.id.billetComplement);
            billetNumber = (CustomEditText) view.findViewById(R.id.billetNumber);
            billetNeighborhood = (CustomEditText) view.findViewById(R.id.billetNeighborhood);
            billetState = (CustomEditText) view.findViewById(R.id.billetState);
            billetCity = (CustomEditText) view.findViewById(R.id.billetCity);
            view.findViewById(R.id.buttonSaveUser).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sendObjectOccurrence();
                }
            });

            Volley volley = new Volley(this);
            volley.isMeuADM(true);
            volley.request();
        }
    }

    @Override
    public void result(String result, int type) {
        try{
            Util.changeVisibility(loader, View.GONE, true);
            Util.changeVisibility(baseRegister, View.VISIBLE, true);

            RegisterRequest registerRequest = App.getGson().fromJson(result, RegisterRequest.class);
            register = registerRequest.getObject();
            if(register != null){
                name.setText(register.getName());
                email.setText(register.getEmail());
                Register.Document registerDocument = register.getDocument();
                if(registerDocument != null){
                    document.setText(registerDocument.getNumber());
                }
                mobile.setText(register.getMobile());
                Register.Addressdetail addressdetail = register.getAddressdetail();
                if(addressdetail != null){
                    zipcode.setText(addressdetail.getZipCode());
                    neighborhood.setText(addressdetail.getNeighborhood());
                    street.setText(addressdetail.getStreet());
                    number.setText(addressdetail.getNumber());
                    city.setText(addressdetail.getCity());
                    state.setText(addressdetail.getState());
                    country.setText(addressdetail.getCountry());
                    complement.setText(addressdetail.getComplement());
                }
            }
        }catch (Exception e){
            SendSuccessDialog.show(getString(R.string.error_generic), null, getFragmentManager(), null, SendSuccessDialog.TYPE_ERROR);
        }
    }

    @Override
    public String uri() {
        return URI.USER_EXTERNAL;
    }

    private void sendObjectOccurrence() {
        User user = getUser();
        if(user != null){
            Register.Document document = new Register.Document(this.register.getDocument().getType(), this.document.getText().toString());
            Register.Addressdetail addressdetail = new Register.Addressdetail(
                    this.zipcode.getText().toString(),
                    this.neighborhood.getText().toString(),
                    this.street.getText().toString(),
                    this.number.getText().toString(),
                    this.city.getText().toString(),
                    this.state.getText().toString(),
                    this.country.getText().toString(),
                    this.complement.getText().toString()
            );
            Register.AttachmentAddress attachmentAddress = new Register.AttachmentAddress(
                    this.billetZipcode.getText().toString(),
                    this.billetAddress.getText().toString(),
                    this.billetComplement.getText().toString(),
                    this.billetNumber.getText().toString(),
                    this.billetNeighborhood.getText().toString(),
                    this.billetState.getText().toString(),
                    this.billetCity.getText().toString()
            );
            Register register = new Register(
                    document,
                    this.name.getText().toString(),
                    this.mobile.getText().toString(),
                    this.email.getText().toString(),
                    addressdetail,
                    attachmentAddress
            );

            long condominiumId = Integer.parseInt(user.getCondominiumId());
            String unit_key = user.getApto();

            String registerBody = register.handleObjectByBoby();


            OccurrencesSend occurrences = new OccurrencesSend(getString(R.string.title_register), registerBody, condominiumId, OccurrencesSend.SUBJECT_UUID_REGISTER, unit_key);

            Volley volley = new Volley(new Volley.Callback() {
                @Override
                public void result(String result, int type) {
                    try{
                        OccurrenceRequest occurrencesRequest = App.getGson().fromJson(result, OccurrenceRequest.class);
                        SendSuccessDialog.show(occurrencesRequest.getMessage(), getString(R.string.occurrence_send_success), getFragmentManager());
                    }catch (Exception e){
                        SendSuccessDialog.show(getString(R.string.error_generic), null, getFragmentManager(), null, SendSuccessDialog.TYPE_ERROR);
                    }
                }

                @Override
                public String uri() {
                    return URI.OCCURRENCES;
                }
            });
            volley.showDialog(getActivity());
            volley.isMeuADM(true);
            volley.request(Request.Method.POST, occurrences.toJson());
        }
    }
}
