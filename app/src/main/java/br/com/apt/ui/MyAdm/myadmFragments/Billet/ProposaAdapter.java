package br.com.apt.ui.MyAdm.myadmFragments.Billet;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import br.com.apt.R;
import br.com.apt.widget.CustomTextViewLight;

/**
 * Created by adminbs on 8/1/17.
 */

class ProposaAdapter extends BaseAdapter {

    private Context context;
    private int portion;

    public ProposaAdapter(Context context, int portion) {
        this.context = context;
        this.portion = portion;
    }

    @Override
    public int getCount() {
        return portion;
    }

    @Override
    public Integer getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view;
        ProposaAdapter.ViewHolder holder;

        if(convertView == null){
            view = LayoutInflater.from(context).inflate(R.layout.row_proposa, parent, false);
            holder = new ProposaAdapter.ViewHolder(view);
            view.setTag(holder);
        }else{
            view = convertView;
            holder = (ProposaAdapter.ViewHolder) view.getTag();
        }

        bindList(holder, position);
        return view;
    }

    private void bindList(ProposaAdapter.ViewHolder holder, int position) {
        Integer item = (getItem(position) + 1);
        holder.text.setText(String.format(context.getString(R.string.portion), item));
    }

    public class ViewHolder{
        private CustomTextViewLight text;

        public ViewHolder(View view) {
            text = (CustomTextViewLight) view.findViewById(R.id.text);
        }
    }
}
