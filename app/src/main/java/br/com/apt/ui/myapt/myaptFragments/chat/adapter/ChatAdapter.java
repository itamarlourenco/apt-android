package br.com.apt.ui.myapt.myaptFragments.chat.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import org.jivesoftware.smack.RosterEntry;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import br.com.apt.R;
import br.com.apt.ui.myapt.myaptFragments.chat.Chat;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomTextView;

/**
 * Created by adminbs on 1/21/16.
 */
public class ChatAdapter extends BaseAdapter {
    private List<RosterEntry> entries;
    private Context context;

    public ChatAdapter(Context context, List<RosterEntry> entries) {
        this.context = context;
        this.entries = new ArrayList<>(entries);
    }

    @Override
    public int getCount() {
        return entries == null ? 0 : entries.size();
    }

    @Override
    public RosterEntry getItem(int position) {
        return entries.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if(convertView == null) {
            View view = LayoutInflater.from(context).inflate(R.layout.adapter_chat, parent, false);
            convertView = view;

            viewHolder = new ViewHolder();
            viewHolder.title = (CustomTextView) view.findViewById(R.id.title);
            viewHolder.imageView = (ImageView) view.findViewById(R.id.imageView);
            viewHolder.subTitle = (CustomTextView) view.findViewById(R.id.subTitle);
            viewHolder.canvas = (LinearLayout) view.findViewById(R.id.canvas);

            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }

        RosterEntry item = getItem(position);
        if(item != null){
            viewHolder.title.setText(item.getName());
            String apto = "Apto. " + Integer.parseInt(Util.getAptoByUser(item.getUser()));
            viewHolder.subTitle.setText(apto);
        }

        return convertView;
    }

    public class ViewHolder{
        public ImageView imageView;
        public CustomTextView title;
        public CustomTextView subTitle;
        public LinearLayout canvas;
    }
}