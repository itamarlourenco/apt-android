package br.com.apt.ui.neighborhood.neighborhoodFragment.model;

import java.util.List;

import br.com.apt.application.BaseGson;
import br.com.apt.application.SendRequest;

/**
 * Created by adminbs on 1/21/16.
 */
public class RequestNeighborhoodDelivery extends BaseGson {
    private List<Neighborhood> Object;

    public List<Neighborhood> getObject() {
        return Object;
    }

    public void setObject(List<Neighborhood> object) {
        Object = object;
    }
}
