package br.com.apt.ui.myapt;

import android.content.Intent;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.ui.myapt.myaptFragments.build.BuildFragment;
import br.com.apt.ui.myapt.myaptFragments.notice.NoticesFragment;
import br.com.apt.ui.myapt.myaptFragments.plan.PlantListFragment;
import br.com.apt.ui.myapt.myaptFragments.poll.PollsFragment;
import br.com.apt.widget.PagerFragment;
import br.com.apt.widget.PagerItemFragment;


public class MyAptFragment extends PagerFragment implements Observer {

    private PlantListFragment plan = new PlantListFragment();
    private ArrayList<PagerItemFragment> pagerItems = null;


    public static MyAptFragment newInstance() {
        return new MyAptFragment();
    }

    public static PagerFragment newInstance(int position) {
        return instanceWithPosition(new MyAptFragment(), position);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        App.getNotificationSharedPreferences().addObserver(this);
    }

    @Override
    public ArrayList<PagerItemFragment> getPagerItems() {
        if(pagerItems == null) {
            pagerItems = new ArrayList<>();

            pagerItems.add(new NoticesFragment());
            //pagerItems.add(new ChatFragment());
            pagerItems.add(new PollsFragment());
            pagerItems.add(new BuildFragment());
            pagerItems.add(plan);
        }
        return pagerItems;
    }

    @Override
    protected int getDrawableSelected() {
        return R.drawable.background_tab_selected_my_apt;
    }

    @Override
    public void update(Observable observable, Object data) {

        int pos = this.getPositionFragment();
        switch (pos){
            case 0:
                ((NoticesFragment)getPagerItems().get(pos)).onCounterUpdate();
                break;
            case 1:
                //((TicketsFragment)getPagerItems().get(pos)).onCounterUpdate();
                break;
            case 2:
                //((PollsFragment)getPagerItems().get(pos)).onCounterUpdate();
                break;
            case 3:
                break;
            case 4:
                break;
        }

        this.updateIndicator();
    }

    public void onActivityResultFragment(int requestCode, int resultCode, Intent data) {
        if(plan != null){
            plan.onActivityResultFragment(requestCode, resultCode, data);
        }
    }

    /**
     * Callback interface for responding to Notifications Counter
     */
    public interface CounterUpdateListener {

        /**
         * This method will be invoked when the app gets a notification
         * it should update the content of the page in display
         * maybe update counters?
         */
        public void onCounterUpdate();
    }

}
