package br.com.apt.ui.myinfo.myinfofragment.vehicle;

import java.util.List;

import br.com.apt.application.BaseGson;
import br.com.apt.ui.myinfo.myinfofragment.AutoComplete;

/**
 * Created by fabriciooliveira on 9/28/16.
 */
public class VehicleBrandRequest extends BaseGson {
    public List<AutoComplete> Object;

    public List<AutoComplete> getObject() {
        return Object;
    }
}
