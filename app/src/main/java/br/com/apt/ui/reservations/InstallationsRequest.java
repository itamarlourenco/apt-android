package br.com.apt.ui.reservations;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;
import java.util.List;

import br.com.apt.application.BaseGson;
import br.com.apt.application.BaseGsonMeuAdm;
import br.com.apt.ui.neighborhood.neighborhoodFragment.model.Neighborhood;

/**
 * Created by adminbs on 9/6/17.
 */

public class InstallationsRequest extends BaseGsonMeuAdm {
    private List<Installations> object;

    public List<Installations> getObject() {
        return object;
    }

    public void setObject(List<Installations> object) {
        this.object = object;
    }
}
