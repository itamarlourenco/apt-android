package br.com.apt.ui.myapt.myaptFragments.plan;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;
import br.com.apt.R;
import br.com.apt.application.Volley;
import br.com.apt.ui.myinfo.myinfofragment.AutoComplete;
import br.com.apt.ui.myinfo.myinfofragment.FragmentMyInfoAdd;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomButton;
import br.com.apt.widget.CustomEditText;

public class PlantViewFragment extends FragmentMyInfoAdd {

    private Plant plant;
    private ImageView image;
    private List<AutoComplete> plantTypes;
    private CustomEditText name;
    private CustomEditText update;
    private Plant.PlantType plantType;
    private String plantTypeUuiidSelectd = "";
    private CustomButton editPlant;
    private Bitmap bitmapPhoto;
    private String urlImage;
    private Volley volley;
    private TextInputLayout baseLastUpdate;

    public static PlantViewFragment newInstance(){
        return new PlantViewFragment();
    }

    public static PlantViewFragment newInstance(Plant plant){
        PlantViewFragment plantViewFragment = new PlantViewFragment();
        plantViewFragment.setPlant(plant);
        return plantViewFragment;
    }

    public void setPlant(Plant plant) {
        this.plant = plant;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_view_plant, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        image = (ImageView) view.findViewById(R.id.image);
        name = (CustomEditText) view.findViewById(R.id.name);
        update = (CustomEditText) view.findViewById(R.id.update);
        editPlant = (CustomButton) view.findViewById(R.id.editPlant);
        baseLastUpdate = (TextInputLayout) view.findViewById(R.id.baseLastUpdate);
        update.setEnabled(false);

        Picasso.with(getContext()).load(plant.getImage()).placeholder(R.drawable.default_camera).into(image);
        plantType = plant.getPlantType();
        if(plantType != null){
            name.setText(plantType.getTitle());
            plantTypeUuiidSelectd = plantType.getUuid();
        }
        update.setText(Util.dateFormatted(plant.getUpdatedAt()));

        editPlant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(PlantAddActivity.newIntent(getContext(), plant));
            }
        });
    }

    @Override
    protected String getPagerTitle() {
        return null;
    }

    @Override
    protected int getPagerIcon() {
        return 0;
    }

    @Override
    protected HashMap<String, String> save() {
        return null;
    }

    @Override
    protected String getUri() {
        return null;
    }

    @Override
    protected int getTypeRequest() {
        return 0;
    }

    @Override
    protected void saved() {

    }

    @Override
    protected void saved(String json) {

    }
}
