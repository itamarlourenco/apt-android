package br.com.apt.ui.MyAdm.myadmFragments.administrator;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import br.com.apt.R;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.util.Logger;

/**
 * Created by adminbs on 7/21/17.
 */

public class CompanyData implements Parcelable {

    @SerializedName("company_key")
    public String companyKey;

    @SerializedName("name")
    public String name;

    @SerializedName("address")
    public String address;

    @SerializedName("city")
    public String city;

    @SerializedName("state")
    public String state;

    @SerializedName("zip_code")
    public String zip_code;

    @SerializedName("neighborhood")
    public String neighborhood;

    @SerializedName("email")
    public String email;

    @SerializedName("phone")
    public String phone;

    @SerializedName("fax")
    public String fax;

    @SerializedName("manager_name")
    public String manager_name;

    @SerializedName("manager_email")
    public String manager_email;

    @SerializedName("manager_phone")
    public String manager_phone;

    @SerializedName("cnpj")
    public String cnpj;

    @SerializedName("id")
    public String id;

    @SerializedName("description")
    public String description;

    @SerializedName("logo_url")
    public String logo_url;

    @SerializedName("url_site")
    public String url_site;

    @SerializedName("founded_year")
    public String founded_year;

    @SerializedName("properties")
    public String properties;

    @SerializedName("employees")
    public String employees;

    @SerializedName("resident_deal_amount_parcel")
    public int residentDealAmountParcel;

    @SerializedName("company_app_presentation_text")
    public String companyAppPresentationText;


    @SerializedName("manager_photo")
    public String managerPhoto;



    public String getCompanyKey() {
        return companyKey;
    }

    public void setCompanyKey(String companyKey) {
        this.companyKey = companyKey;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip_code() {
        return zip_code;
    }

    public void setZip_code(String zip_code) {
        this.zip_code = zip_code;
    }

    public String getNeighborhood() {
        return neighborhood;
    }

    public void setNeighborhood(String neighborhood) {
        this.neighborhood = neighborhood;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getManager_name() {
        return manager_name;
    }

    public void setManager_name(String manager_name) {
        this.manager_name = manager_name;
    }

    public String getManager_email() {
        return manager_email;
    }

    public void setManager_email(String manager_email) {
        this.manager_email = manager_email;
    }

    public String getManager_phone() {
        return manager_phone;
    }

    public void setManager_phone(String manager_phone) {
        this.manager_phone = manager_phone;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLogo_url() {
        return logo_url;
    }

    public void setLogo_url(String logo_url) {
        this.logo_url = logo_url;
    }

    public String getUrl_site() {
        return url_site;
    }

    public void setUrl_site(String url_site) {
        this.url_site = url_site;
    }

    public String getFounded_year() {
        return founded_year;
    }

    public void setFounded_year(String founded_year) {
        this.founded_year = founded_year;
    }

    public String getProperties() {
        return properties;
    }

    public void setProperties(String properties) {
        this.properties = properties;
    }

    public String getEmployees() {
        return employees;
    }

    public void setEmployees(String employees) {
        this.employees = employees;
    }

    public int getResidentDealAmountParcel() {
        return residentDealAmountParcel;
    }

    public void setResidentDealAmountParcel(int residentDealAmountParcel) {
        this.residentDealAmountParcel = residentDealAmountParcel;
    }

    public String getCompanyAppPresentationText() {
        return companyAppPresentationText;
    }

    public void setCompanyAppPresentationText(String companyAppPresentationText) {
        this.companyAppPresentationText = companyAppPresentationText;
    }

    public String getManagerPhoto() {
        return managerPhoto;
    }

    public void setManagerPhoto(String managerPhoto) {
        this.managerPhoto = managerPhoto;
    }

    protected CompanyData(Parcel in) {
        companyKey = in.readString();
        name = in.readString();
        address = in.readString();
        city = in.readString();
        state = in.readString();
        zip_code = in.readString();
        neighborhood = in.readString();
        email = in.readString();
        phone = in.readString();
        fax = in.readString();
        manager_name = in.readString();
        manager_email = in.readString();
        manager_phone = in.readString();
        cnpj = in.readString();
        id = in.readString();
        description = in.readString();
        logo_url = in.readString();
        url_site = in.readString();
        founded_year = in.readString();
        properties = in.readString();
        employees = in.readString();
        residentDealAmountParcel = in.readInt();
        companyAppPresentationText = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(companyKey);
        dest.writeString(name);
        dest.writeString(address);
        dest.writeString(city);
        dest.writeString(state);
        dest.writeString(zip_code);
        dest.writeString(neighborhood);
        dest.writeString(email);
        dest.writeString(phone);
        dest.writeString(fax);
        dest.writeString(manager_name);
        dest.writeString(manager_email);
        dest.writeString(manager_phone);
        dest.writeString(cnpj);
        dest.writeString(id);
        dest.writeString(description);
        dest.writeString(logo_url);
        dest.writeString(url_site);
        dest.writeString(founded_year);
        dest.writeString(properties);
        dest.writeString(employees);
        dest.writeInt(residentDealAmountParcel);
        dest.writeString(companyAppPresentationText);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<CompanyData> CREATOR = new Parcelable.Creator<CompanyData>() {
        @Override
        public CompanyData createFromParcel(Parcel in) {
            return new CompanyData(in);
        }

        @Override
        public CompanyData[] newArray(int size) {
            return new CompanyData[size];
        }
    };
}