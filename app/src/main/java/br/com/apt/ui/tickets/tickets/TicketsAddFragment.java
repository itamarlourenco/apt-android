package br.com.apt.ui.tickets.tickets;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.android.volley.Request;

import java.io.File;
import java.util.HashMap;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.BaseFragment;
import br.com.apt.application.BaseGson;
import br.com.apt.application.PicturesHelper;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomButton;
import br.com.apt.widget.CustomEditText;
import br.com.apt.widget.SendSuccessDialog;
import siclo.com.ezphotopicker.api.EZPhotoPick;
import siclo.com.ezphotopicker.api.EZPhotoPickStorage;


public class TicketsAddFragment extends BaseFragment {

    private Bitmap bitmap = null;
    private RelativeLayout canvasAlterImage;
    private CustomEditText subject;
    private CustomEditText message;
    private static ImageView image;
    private CustomButton sendTicket;

    public static TicketsAddFragment newInstance() {
        return new TicketsAddFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_add_tickets, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if(view != null) {
            canvasAlterImage = (RelativeLayout) view.findViewById(R.id.canvasAlterImage);
            subject = (CustomEditText) view.findViewById(R.id.subject);
            message = (CustomEditText) view.findViewById(R.id.message);
            sendTicket = (CustomButton) view.findViewById(R.id.sendTicket);
            image = (ImageView) view.findViewById(R.id.image);
            sendTicket.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //sendTickets();
                }
            });
            canvasAlterImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alterImage();
                }
            });
        }else{
            SendSuccessDialog.show(getString(R.string.error_generic), null, getFragmentManager(), null, SendSuccessDialog.TYPE_ERROR);
        }
    }


    public void alterImage(){
        CharSequence[] options = {getString(R.string.takePhoto), getString(R.string.chooseGallery)};
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity());

        builder.setTitle("")
                .setItems(options, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if(which == 0) {
                            PicturesHelper.Camera(getActivity());
                        } else if(which == 1) {
                            PicturesHelper.Gallery(getActivity());
                        }else{
                            image.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.placeholder_camera));
                        }
                    }
                });
        builder.create().show();
    }



    private void sendTickets() {
        try{
            if(TextUtils.isEmpty(subject.getText())){
                throw new Exception(getString(R.string.error_subject));
            }

            if(TextUtils.isEmpty(message.getText())){
                throw new Exception(getString(R.string.error_message));
            }

            HashMap<String, String> hashMap = new HashMap<>();

            String editTextSubscriptString = subject.getText().toString();
            String editTextMessageString = message.getText().toString();

            hashMap.put("title", editTextSubscriptString);
            hashMap.put("description", editTextMessageString);
            File finalFile = Util.createImageFile();
            if(bitmap != null && finalFile != null){
                Util.sendBitmapToFile(finalFile, bitmap);
            }

            Volley volley = new Volley(new Volley.Callback() {
                @Override
                public void result(String result, int type) {
                    if(result != null){
                        BaseGson baseGson = App.getGson().fromJson(result, BaseGson.class);

                        if(baseGson.getStatus() == Volley.STATUS_OK){
                            SendSuccessDialog.show(getString(R.string.ticket_add_success), baseGson.getMessage(), getFragmentManager(), new SendSuccessDialog.Actions(){
                                @Override
                                public void back() {
                                    getActivity().finish();
                                }
                            });
                        }else{
                            SendSuccessDialog.show(getString(R.string.error_generic), null, getFragmentManager(), null, SendSuccessDialog.TYPE_ERROR);
                        }

                    }
                    sendTicket.setEnabled(true);
                }

                @Override
                public String uri() {
                    return URI.TICKETS;
                }
            });
            volley.showDialog(getActivity());
            volley.request(Request.Method.POST, hashMap, 0, finalFile, "image");

        } catch (Exception e) {
            SendSuccessDialog.show(e.getMessage(), null, getFragmentManager(), null, SendSuccessDialog.TYPE_ERROR);
        }
    }

    public void onActivityResultFragment(int requestCode, int resultCode, Intent data) {
        if(requestCode == EZPhotoPick.PHOTO_PICK_REQUEST_CODE && resultCode != 0 && TicketsAddFragment.image != null){
            try {
                EZPhotoPickStorage ezPhotoPickStorage = new EZPhotoPickStorage(getActivity());
                bitmap = ezPhotoPickStorage.loadLatestStoredPhotoBitmap();

                TicketsAddFragment.image.setImageBitmap(bitmap);
                TicketsAddFragment.image.setVisibility(View.VISIBLE);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}