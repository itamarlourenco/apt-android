package br.com.apt.ui.reservations;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by adminbs on 9/9/17.
 */

public class TimeSelected implements Parcelable {
    private String start;
    private String end;

    public TimeSelected() {

    }

    public TimeSelected(String start, String end) {
        this.start = start;
        this.end = end;
    }

    public String getStart() {
        return start;
    }

    public String handleStart(){
        try{
            String[] split = start.split("-");
            return split[0].trim();
        }catch (Exception e){
            return "";
        }
    }

    public String handleEnd(){
        try{
            String[] split = end.split("-");
            return split[1].trim();
        }catch (Exception e){
            return "";
        }
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    protected TimeSelected(Parcel in) {
        start = in.readString();
        end = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(start);
        dest.writeString(end);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<TimeSelected> CREATOR = new Parcelable.Creator<TimeSelected>() {
        @Override
        public TimeSelected createFromParcel(Parcel in) {
            return new TimeSelected(in);
        }

        @Override
        public TimeSelected[] newArray(int size) {
            return new TimeSelected[size];
        }
    };
}