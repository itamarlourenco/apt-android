package br.com.apt.ui.lobby.lobbyFragments.safety;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import br.com.apt.R;
import br.com.apt.application.BaseFragment;
import br.com.apt.application.Volley;
import br.com.apt.widget.CustomTextView;
import br.com.apt.widget.CustomTextViewLight;

/**
 * Created by itamarlourenco on 10/01/16.
 */
public class SafeFragment extends BaseFragment{

    private static final String ARGS_SAFE = "SAFE";

    public static SafeFragment newInstance(Safe safe) {
        SafeFragment introFragment = new SafeFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARGS_SAFE, safe);
        introFragment.setArguments(args);
        return introFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_safe, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Bundle bundle = this.getArguments();
        Safe safe = bundle.getParcelable(ARGS_SAFE);
        View view = getView();
        if(safe != null && view != null){
            ImageView imageSafety = (ImageView) view.findViewById(R.id.imageSafety);
            CustomTextViewLight titleSafety = (CustomTextViewLight) view.findViewById(R.id.titleSafety);

            Picasso.with(getContext()).load(safe.getIpCamera()).into(imageSafety);
            titleSafety.setText(safe.getDescription());
        }
    }
}
