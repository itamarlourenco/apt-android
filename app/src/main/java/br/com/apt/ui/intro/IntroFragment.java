package br.com.apt.ui.intro;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import br.com.apt.R;
import br.com.apt.widget.CustomTextView;

public class IntroFragment extends Fragment {

    private static final String ARGS_POSITION = "POSITION_FRAGMENT";

    public static IntroFragment newIntent(int position) {
        IntroFragment introFragment = new IntroFragment();
        Bundle args = new Bundle();
        args.putInt(ARGS_POSITION, position);
        introFragment.setArguments(args);
        return introFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Bundle bundle = this.getArguments();
        int position = bundle.getInt(ARGS_POSITION, 0);
        ViewGroup viewGroup = (ViewGroup) inflater.inflate(R.layout.intro_page_fragment, container, false);

        switch (position) {
            case 0:
                changeInformation(viewGroup, R.drawable.ic_home, R.string.intro_text_home);
                break;
            case 1:
                changeInformation(viewGroup, R.drawable.ic_device, R.string.intro_text_device);
                break;
            case 2:
                changeInformation(viewGroup, R.drawable.ic_chat, R.string.intro_text_chat);
                break;
            case 3:
                changeInformation(viewGroup, R.drawable.ic_location, R.string.intro_text_location);
                break;
            case 4:
                break;
        }

        return viewGroup;
    }

    private void changeInformation(ViewGroup view, int drawable, int string) {
        if (view != null) {
            ((ImageView) view.findViewById(R.id.intro_ic)).setImageResource(drawable);
            ((CustomTextView) view.findViewById(R.id.intro_text)).setText(getString(string));
        }
    }
}
