package br.com.apt.ui.reservations;

import android.media.Image;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.volley.Request;
import com.github.ybq.android.spinkit.SpinKitView;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.BaseFragment;
import br.com.apt.application.BaseGsonMeuAdm;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.model.ReservationError;
import br.com.apt.model.ReservationErrorRequest;
import br.com.apt.model.user.User;
import br.com.apt.ui.main.MainActivity;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomTextViewLight;
import br.com.apt.widget.SendSuccessDialog;

/**
 * Created by adminbs on 9/6/17.
 */

public class TermsFragment extends BaseFragment implements Volley.Callback {

    private Installations installations;
    private CalendarDay calendarDay;
    private TimeSelected timesSelected;

    private ImageView icon;
    private CustomTextViewLight installationName;
    private CustomTextViewLight labelCalendar;
    private CustomTextViewLight dateLabel;
    private CustomTextViewLight timeLabel;
    private CustomTextViewLight text;
    private CustomTextViewLight rule;
    private CustomTextViewLight reservationAccept;
    private LinearLayout content;
    private SpinKitView loader;

    public static TermsFragment newInstance(Installations installations, CalendarDay calendarDay, TimeSelected timesSelected){
        TermsFragment termsFragment = new TermsFragment();
        termsFragment.setInstallations(installations);
        termsFragment.setCalendarDay(calendarDay);
        termsFragment.setTimesSelected(timesSelected);

        return  termsFragment;
    }

    public void setInstallations(Installations installations) {
        this.installations = installations;
    }

    public void setCalendarDay(CalendarDay calendarDay) {
        this.calendarDay = calendarDay;
    }

    public void setTimesSelected(TimeSelected timesSelected) {
        this.timesSelected = timesSelected;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_terms_reservations, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if (view != null) {
            icon = (ImageView) view.findViewById(R.id.icon);
            installationName = (CustomTextViewLight) view.findViewById(R.id.installation_name);
            labelCalendar = (CustomTextViewLight) view.findViewById(R.id.labelCalendar);
            dateLabel = (CustomTextViewLight) view.findViewById(R.id.dateLabel);
            labelCalendar = (CustomTextViewLight) view.findViewById(R.id.labelCalendar);
            timeLabel = (CustomTextViewLight) view.findViewById(R.id.timeLabel);
            loader = (SpinKitView) view.findViewById(R.id.loader);
            text = (CustomTextViewLight) view.findViewById(R.id.text);
            rule = (CustomTextViewLight) view.findViewById(R.id.rule);
            reservationAccept = (CustomTextViewLight) view.findViewById(R.id.reservation_accept);
            content = (LinearLayout) view.findViewById(R.id.content);


            reservationAccept.setText(Html.fromHtml("Ao clicar em <font color='#2583ad'>“solicitar a reserva”</font> você \n" +
                    "está aceitando os termos de uso da área"));


            getTerms();

            if (installations != null) {
                installationName.setText(installations.getName());
                Picasso.with(getContext()).load(Installations.handleUrlIcon(installations.getIcon())).into(icon);
            }

            Calendar calendar = calendarDay.getCalendar();
            dateLabel.setText(
                    String.format(
                            getString(R.string.reservation_label_day),
                            Util.getDayWeek(calendar),
                            String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)),
                            Util.getMonthName(calendar.get(Calendar.MONTH)),
                            String.valueOf(calendar.get(Calendar.YEAR))
                    )
            );
            labelCalendar.setText(String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)));
            timeLabel.setText(
                    String.format(getString(R.string.time_reservation_label),
                            !TextUtils.isEmpty(timesSelected.getStart()) ? timesSelected.getStart().replace("-", "às") : ""
                    )
            );

            view.findViewById(R.id.confirmReservation).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    createReservation();
                }
            });
        }
    }

    private void createReservation() {
        User user = getUser();
        if (user != null) {
            final Reservations reservations = new Reservations();
            reservations.setCreated_user_id(user.getId());
            reservations.setCreated_external_user_name(user.getName());
            reservations.setNote("");
            reservations.setInstallation_id((int) installations.getId());
            reservations.setBlock_id(user.getBlockId().trim());
            reservations.setUnit_id(user.getApto().trim());

            String date = new SimpleDateFormat("yyyy-MM-dd", Locale.US).format(calendarDay.getDate());
            String dateStart = date + " " + timesSelected.handleStart() + ":00";
            //String dateEnd = date + " " + timesSelected.handleEnd() + ":00";

            reservations.setStart(dateStart);
            //reservations.setEnd(dateEnd);

            Reservations.Block block = new Reservations.Block();
            block.setExternal_id(user.getBlockId());
            block.setName(user.getBuild().getNameBlock());
            reservations.setBlock(block);

            Reservations.Unit unit = new Reservations.Unit();
            unit.setExternal_id(user.getApto());
            unit.setName(user.getName());
            reservations.setUnit(unit);


            Volley volley = new Volley(new Volley.Callback() {
                @Override
                public void result(String result, int typse) {
                    try{
                        BaseGsonMeuAdm baseGsonMeuAdm = App.getGson().fromJson(result, BaseGsonMeuAdm.class);
                        if(baseGsonMeuAdm.getStatus() == Volley.STATUS_OK){
                            String title;
                            String message;

                            title = getString(R.string.reservation_ok);
                            message = getString(R.string.reservation_ok_message);

                            if(installations != null && installations.getAuto_approved() == 1){
                                title = getString(R.string.reservation_ok_with_auto_approved);
                                message = getString(R.string.reservation_ok_message_with_auto_approved);
                            }

                            SendSuccessDialog.show(title, message, getFragmentManager(), new SendSuccessDialog.Actions() {
                                @Override
                                public void back() {
                                    startActivity(MainActivity.newIntent(getContext()));
                                    getActivity().finish();
                                }
                            });
                        }else{
                            ReservationErrorRequest reservationErrorRequest = App.getGson().fromJson(result, ReservationErrorRequest.class);
                            List<ReservationError> object = reservationErrorRequest.getObject();
                            StringBuilder messages = new StringBuilder();
                            for(ReservationError reservationError: object){
                                messages.append(reservationError.getMessage()).append("\n\n");
                            }

                            SendSuccessDialog.show(getString(R.string.error_generic), String.valueOf(messages), getFragmentManager(), new SendSuccessDialog.Actions() {
                                @Override
                                public void back() {
                                    getActivity().finish();
                                }
                            }, SendSuccessDialog.TYPE_ERROR);
                        }
                    }catch (Exception e){
                        SendSuccessDialog.show(getString(R.string.error_generic), null, getFragmentManager(), new SendSuccessDialog.Actions() {
                            @Override
                            public void back() {
                                getActivity().finish();
                            }
                        }, SendSuccessDialog.TYPE_ERROR);
                    }
                }

                @Override
                public String uri() {
                    return URI.RESERVATION;
                }
            });
            volley.showDialog(getActivity());
            volley.isReservation(true);
            volley.request(Request.Method.POST, reservations.toJson());
        }
    }

    public void getTerms() {
        Volley volley = new Volley(this);
        volley.isReservation(true);
        volley.request();
    }

    @Override
    public void result(String result, int type) {
        loader.setVisibility(View.GONE);
        content.setVisibility(View.VISIBLE);
        if (result != null) {
            RulesProceduresRequest rulesProceduresRequest = App.getGson().fromJson(result, RulesProceduresRequest.class);
            List<RulesProcedures> object = rulesProceduresRequest.getObject();
            StringBuilder ruleBuilder = new StringBuilder();
            if (object != null && object.size() > 0) {
                for(RulesProcedures rulesProcedures: object){
                    ruleBuilder.append(rulesProcedures.getText()).append("\n\n").append(rulesProcedures.getRule()).append("\n\n");
                }
                text.setText(ruleBuilder.toString());
            }else{
                text.setText(getString(R.string.without_term));
            }
        }else{
            text.setText(getString(R.string.without_term));
        }
    }

    @Override
    public String uri() {
        return String.format(URI.RULE_PROCEDURE, installations.getId());
    }
}

