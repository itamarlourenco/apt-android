package br.com.apt.ui.reservations;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by adminbs on 9/7/17.
 */

public class Times {
    private String time;
    private Date date;
    private boolean selected;
    private boolean first;
    private boolean last;
    private boolean disabled = false;

    public Times(){

    }

    public Times(String time, Date date, boolean selected, boolean first, boolean last) {
        this.time = time;
        this.selected = selected;
        this.first = first;
        this.date = date;
        this.last = last;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public boolean isFirst() {
        return first;
    }

    public void setFirst(boolean first) {
        this.first = first;
    }

    public boolean isLast() {
        return last;
    }

    public void setLast(boolean last) {
        this.last = last;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean enabled) {
        this.disabled = enabled;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
