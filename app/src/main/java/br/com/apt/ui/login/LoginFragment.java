package br.com.apt.ui.login;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.android.volley.Request;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import java.io.IOException;

import br.com.apt.BuildConfig;
import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.AppPreferences;
import br.com.apt.application.BaseFragment;
import br.com.apt.application.Config;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.model.user.UserData;
import br.com.apt.model.user.UserRequest;
import br.com.apt.ui.firstAccess.FirstAccessActivity;
import br.com.apt.ui.forgotPassword.ForgotPasswordActivity;
import br.com.apt.ui.main.MainActivity;
import br.com.apt.util.Logger;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomEditText;
import dmax.dialog.SpotsDialog;

public class LoginFragment extends BaseFragment implements View.OnClickListener, Volley.Callback {

    private CustomEditText userName;
    private CustomEditText password;

    private Volley volley;

    @Override
    public String uri() {
        return URI.AUTHENTICATE;
    }

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.login_fragment, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if (view != null) {
            view.findViewById(R.id.forgotPassword).setOnClickListener(this);
            view.findViewById(R.id.firstAccess).setOnClickListener(this);
            view.findViewById(R.id.submit).setOnClickListener(this);

            userName = (CustomEditText) view.findViewById(R.id.userName);
            password = (CustomEditText) view.findViewById(R.id.password);

            if(BuildConfig.DEBUG){
                userName.setText("itamar.developer@gmail.com");
                password.setText("6949519");
            }

            password.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    executeLogin();
                    return true;
                }
            });


            Intent intent = getActivity().getIntent();
            if(intent != null){
                String autoUsername = intent.getStringExtra(LoginActivity.PUT_USERNAME);
                String autoPassword = intent.getStringExtra(LoginActivity.PUT_PASSWORD);

                if(TextUtils.isEmpty(autoUsername) || TextUtils.isEmpty(autoPassword)){
                    Logger.d("Auto-Login not do");
                    return;
                }

                userName.setText(autoUsername);
                password.setText(autoPassword);
                executeLogin();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.forgotPassword:
                startForgotPassword();
                break;

            case R.id.firstAccess:
                startFirstAccess();
                break;

            case R.id.submit:
                executeLogin();
                break;
        }
    }

    private void executeLogin() {
        try{
            if(TextUtils.isEmpty(userName.getText())){
                userName.requestFocus();
                throw new Exception(getString(R.string.message_error_login_email));
            }

            if(TextUtils.isEmpty(password.getText())){
                password.requestFocus();
                throw new Exception(getString(R.string.message_error_login_password));
            }

            volley = new Volley(this);
            volley.showDialog(getActivity());
            getTokenGcm();
        }catch (Exception e){
            View view = getView();
            if(view != null){
                Util.showDialog(getActivity(), e.getMessage());
            }
        }
    }

    private void startFirstAccess() {
        startActivity(FirstAccessActivity.newIntent(getActivity()));
    }

    private void startForgotPassword() {
        startActivity(ForgotPasswordActivity.newIntent(getActivity()));
    }

    @Override
    public void result(String result, int type) {
        View view = getView();
        if(result == null){
            Logger.t(getString(R.string.internet_error));
        }

        UserRequest user = App.getGson().fromJson(result, UserRequest.class);

        if(user != null && user.getStatus() == Volley.STATUS_OK){
            UserData.saveUser(user);
            getActivity().finish();
            startActivity(MainActivity.newIntent(getContext()));
        }else{
            if(view != null){
                String messageError = getString(R.string.message_error_login_request);
                if(user != null && !TextUtils.isEmpty(user.getMessage())){
                    messageError = user.getMessage();
                }
                Util.showDialog(getActivity(), messageError);
            }
        }
    }



    protected void getTokenGcm() {
        final String stringEmail = userName.getText().toString();
        final String stringPassword = password.getText().toString();
        new AsyncTask<Void, Void, Void>(){
            @Override
            protected Void doInBackground(Void... params) {
                InstanceID instanceID = InstanceID.getInstance(getContext());
                try {
                    String token = instanceID.getToken(Config.GCM.SENDER_ID, GoogleCloudMessaging.INSTANCE_ID_SCOPE);
                    sendRegistrationToServer(token);
                    Login login = new Login(stringEmail, stringPassword, token);
                    volley.request(Request.Method.POST, login.toJson());
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return null;
            }
        }.execute();
    }

    private void sendRegistrationToServer(String token) {
        AppPreferences appPreferences = new AppPreferences();
        appPreferences.setAPNS(token);
    }
}
