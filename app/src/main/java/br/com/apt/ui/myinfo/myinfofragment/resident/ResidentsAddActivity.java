package br.com.apt.ui.myinfo.myinfofragment.resident;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import br.com.apt.R;
import br.com.apt.application.BaseActivity;
import br.com.apt.ui.myinfo.myinfofragment.NewUser;

/**
 * Created by adminbs on 12/29/17.
 */

public class ResidentsAddActivity extends BaseActivity {

    public static final String EXTRA_RESIDENT = "EXTRA_RESIDENT";
    private ResidentAddFragment residentAddFragment = ResidentAddFragment.newInstance();

    public static Intent newIntent(Context context) {
        return new Intent(context, ResidentsAddActivity.class);
    }

    public static Intent newIntent(Context context, NewUser resident) {
        Intent intent = new Intent(context, ResidentsAddActivity.class);
        intent.putExtra(EXTRA_RESIDENT, resident);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public Fragment getFragment(){
        Intent intent = getIntent();
        if(intent != null){
            NewUser resident = intent.getParcelableExtra(ResidentsAddActivity.EXTRA_RESIDENT);
            residentAddFragment.setResident(resident);
        }
        return residentAddFragment;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        residentAddFragment.onActivityResultFragment(requestCode, resultCode, data);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_down, R.anim.slide_out_down);
    }
}