package br.com.apt.ui.tickets.tickets;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.BaseFragment;
import br.com.apt.ui.myapt.MyAptFragment;
import br.com.apt.widget.PagerItemFragment;


public class TicketsFragment extends BaseFragment implements MyAptFragment.CounterUpdateListener{

    public static TicketsAddFragment ticketsAddFragment = TicketsAddFragment.newInstance();
    private TicketsListFragment listFragment;

    public static TicketsFragment newIntent(){
        return new TicketsFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_tickets, container, false);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        listFragment = TicketsListFragment.newInstance();
        TicketsFragment.handleTicketsFragments(listFragment, getActivity());
    }

    public static void handleTicketsFragments(BaseFragment baseFragment, FragmentActivity fragmentActivity) {
        TicketsFragment.handleTicketsFragments(baseFragment, fragmentActivity, 0, 0);
    }
    public static void handleTicketsFragments(BaseFragment baseFragment, FragmentActivity fragmentActivity, int firstAnimation, int secondAnimation) {
        FragmentTransaction fragmentTransaction = fragmentActivity.getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(firstAnimation, secondAnimation);
        fragmentTransaction.replace(R.id.fragmentContainerAnimation, baseFragment);
        fragmentTransaction.commit();
    }

    public void onActivityResultFragment(int requestCode, int resultCode, Intent data) {
        ticketsAddFragment.onActivityResultFragment(requestCode, resultCode, data);
    }

    @Override
    public void onCounterUpdate() {
        //listFragment.updateList();
    }
}
