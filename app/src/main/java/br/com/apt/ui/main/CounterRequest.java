package br.com.apt.ui.main;

import java.util.List;

import br.com.apt.application.BaseGson;

/**
 * Created by adminbs on 1/21/16.
 */
public class CounterRequest extends BaseGson {
    private Counter Object;

    public Counter getObject() {
        return Object;
    }

    public void setObject(Counter object) {
        Object = object;
    }
}
