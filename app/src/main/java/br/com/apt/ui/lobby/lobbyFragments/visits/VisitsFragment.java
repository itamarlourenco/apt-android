package br.com.apt.ui.lobby.lobbyFragments.visits;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.android.volley.Request;
import com.squareup.picasso.Picasso;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.List;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.BaseFragment;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.application.notification.NotificationSharedPreferences;
import br.com.apt.model.user.User;
import br.com.apt.ui.lobby.LobbyFragment;
import br.com.apt.ui.lobby.lobbyFragments.deliveries.DeliveriesFragment;
import br.com.apt.ui.lobby.lobbyFragments.deliveries.DeliveriesListFragment;
import br.com.apt.ui.lobby.lobbyFragments.deliveries.DeliveryFragment;
import br.com.apt.util.Logger;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomTextView;
import br.com.apt.widget.PagerItemFragment;
import br.com.apt.widget.ViewPagerCustom;
import dmax.dialog.SpotsDialog;

public class VisitsFragment extends PagerItemFragment{

    private static VisitsListFragment visitsListFragment;

    public static VisitsFragment newInstance(){
        return new VisitsFragment();
    }

    @Override
    protected String getPagerTitle() {
        return App.getContext().getString(R.string.visits_fragment);
    }

    @Override
    protected int getPagerIcon() {
        return R.drawable.ico_visita;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_visits, container, false);
    }

    @Override
    protected int getIndicator() {
        return App.getNotificationSharedPreferences().getCountVisit();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        visitsListFragment = VisitsListFragment.newInstance(this);
        VisitsFragment.handleDeliveriesFragments(visitsListFragment, getActivity());
    }

    public static void handleDeliveriesFragments(BaseFragment baseFragment, FragmentActivity activity){
        VisitsFragment.handleDeliveriesFragments(R.id.fragmentContainerAnimationVisits, baseFragment, activity);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        updateTitle();
    }

    public void onActivityResultFragment(int requestCode, int resultCode, Intent data) {
        if(visitsListFragment != null){
            visitsListFragment.onActivityResultFragment(requestCode, resultCode, data);
        }
    }
}
