package br.com.apt.ui.main;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;

import br.com.apt.application.BaseActivity;
import br.com.apt.application.services.NotificationService;
import br.com.apt.ui.main.navigationDrawer.NavigationDrawerMenu;
import br.com.apt.ui.reservations.CalendarFragment;
import br.com.apt.util.Util;

public class MainActivity extends BaseActivity {

    public static final int MY_PERMISSIONS_REQUEST_CAMERA = 0;

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String[] permissions = new String[]{
            Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
        };

        if(getToolBar() != null){
            getToolBar().setBackgroundColor(ContextCompat.getColor(this, android.R.color.transparent));
        }

        if(!Util.hasPermissions(this, permissions)){
            ActivityCompat.requestPermissions(this, permissions, MY_PERMISSIONS_REQUEST_CAMERA);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        NotificationService.startService(getApplicationContext());
    }

    @Override
    public Fragment getFragment() {
        return MainFragment.newInstance();
    }


    public static void openActivity(Context context, NavigationDrawerMenu navigationDrawerMenu) {
        if(navigationDrawerMenu.intent() != null){
            context.startActivity(navigationDrawerMenu.intent());
        }
    }

    @Override
    protected boolean showToolbarLogo() {
        return true;
    }
}