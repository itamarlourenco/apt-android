package br.com.apt.ui.neighborhood.neighborhoodFragment.adapter;

import android.Manifest;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import br.com.apt.R;
import br.com.apt.application.Volley;
import br.com.apt.ui.neighborhood.neighborhoodFragment.model.Neighborhood;
import br.com.apt.util.CustomImage;
import br.com.apt.util.Logger;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomTextView;
import br.com.apt.widget.ViewHolderLoaderImageView;

public class NeighborhoodAdapter extends BaseAdapter {

    private List<Neighborhood> neighborhoodList = new ArrayList<>();
    private Context context;
    private ImageView buttonCall;

    public NeighborhoodAdapter(List<Neighborhood> neighborhoodList, Context context) {
        this.neighborhoodList = neighborhoodList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return neighborhoodList == null ? 0 : neighborhoodList.size();
    }

    @Override
    public Neighborhood getItem(int position) {
        return neighborhoodList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {

            View view = LayoutInflater.from(context).inflate(R.layout.adapter_neighborhood, parent, false);
            convertView = view;

            viewHolder = new ViewHolder(view);
            viewHolder.title = (CustomTextView) view.findViewById(R.id.title);
            viewHolder.description = (CustomTextView) view.findViewById(R.id.description);
            viewHolder.phone = (CustomTextView) view.findViewById(R.id.phone);
            viewHolder.buttonCall = (ImageView) view.findViewById(R.id.buttonCall);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        final Neighborhood neighborhood = getItem(position);
        if (neighborhood != null) {
            viewHolder.description.setText(neighborhood.getAddress());
            viewHolder.title.setText(neighborhood.getName());

            if(neighborhood.getId() == 1){
                viewHolder.buttonCall.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_go_to));
            }

            viewHolder.buttonCall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!TextUtils.isEmpty(neighborhood.getPhone())) {
                        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            return;
                        }
                        if(neighborhood.getId() == 1){
                            Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse("google.navigation:q=" + URLEncoder.encode(neighborhood.getAddress())));
                            context.startActivity(intent);
                        }else{
                            context.startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + neighborhood.getPhone())));
                        }
                    }
                }
            });

            CustomImage.loader(context, viewHolder, neighborhood.getImage(), ImageView.ScaleType.CENTER_CROP);
        }


        return convertView;
    }

    public class ViewHolder extends ViewHolderLoaderImageView {
        public CustomTextView title;
        public CustomTextView description;
        public CustomTextView phone;
        public ImageView buttonCall;

        public ViewHolder(View view) {
            super(view);
        }
    }
}
