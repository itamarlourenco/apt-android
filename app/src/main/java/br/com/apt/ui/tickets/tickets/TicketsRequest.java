package br.com.apt.ui.tickets.tickets;

import java.util.List;

import br.com.apt.application.BaseGson;

/**
 * Created by adminbs on 5/11/16.
 */
public class TicketsRequest extends BaseGson {
    private List<Ticket> Object;

    public List<Ticket> getObject() {
        return Object;
    }

    public void setObject(List<Ticket> object) {
        Object = object;
    }
}
