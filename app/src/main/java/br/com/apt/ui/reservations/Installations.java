package br.com.apt.ui.reservations;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

/**
 * Created by adminbs on 9/6/17.
 */

public class Installations implements Parcelable {
    private long id;
    private String company_key;
    private String condominium_id;
    private String name;
    private String icon;
    private String color;
    private String fraction_rental_time;
    private String rental_lock_time;
    private long amount_lock;
    private String opening_time;
    private String closed_time;
    private int maximum_day_reservation;
    private int minimum_day_reservation;
    private int cancellation_period;
    private int enabled_use;
    private int auto_approved;
    private int all_day;
    private Date created_at;
    private String work_days;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCompany_key() {
        return company_key;
    }

    public void setCompany_key(String company_key) {
        this.company_key = company_key;
    }

    public String getCondominium_id() {
        return condominium_id;
    }

    public void setCondominium_id(String condominium_id) {
        this.condominium_id = condominium_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getFraction_rental_time() {
        return fraction_rental_time;
    }

    public void setFraction_rental_time(String fraction_rental_time) {
        this.fraction_rental_time = fraction_rental_time;
    }

    public String getRental_lock_time() {
        return rental_lock_time;
    }

    public void setRental_lock_time(String rental_lock_time) {
        this.rental_lock_time = rental_lock_time;
    }

    public long getAmount_lock() {
        return amount_lock;
    }

    public void setAmount_lock(long amount_lock) {
        this.amount_lock = amount_lock;
    }

    public String getOpening_time() {
        return opening_time;
    }

    public void setOpening_time(String opening_time) {
        this.opening_time = opening_time;
    }

    public String getClosed_time() {
        return closed_time;
    }

    public void setClosed_time(String closed_time) {
        this.closed_time = closed_time;
    }

    public int getMaximum_day_reservation() {
        return maximum_day_reservation;
    }

    public void setMaximum_day_reservation(int maximum_day_reservation) {
        this.maximum_day_reservation = maximum_day_reservation;
    }

    public int getMinimum_day_reservation() {
        return minimum_day_reservation;
    }

    public void setMinimum_day_reservation(int minimum_day_reservation) {
        this.minimum_day_reservation = minimum_day_reservation;
    }

    public int getCancellation_period() {
        return cancellation_period;
    }

    public void setCancellation_period(int cancellation_period) {
        this.cancellation_period = cancellation_period;
    }

    public int getEnabled_use() {
        return enabled_use;
    }

    public void setEnabled_use(int enabled_use) {
        this.enabled_use = enabled_use;
    }

    public int getAuto_approved() {
        return auto_approved;
    }

    public void setAuto_approved(int auto_approved) {
        this.auto_approved = auto_approved;
    }

    public boolean isAllDay() {
        return all_day > 0;
    }

    public void setAll_day(int all_day) {
        this.all_day = all_day;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public String getWork_days() {
        return work_days;
    }

    public void setWork_days(String work_days) {
        this.work_days = work_days;
    }

    public static Creator<Installations> getCREATOR() {
        return CREATOR;
    }

    public static String handleUrlIcon(String icon){
        return "https://reservas.meuapt.com.br" + icon;
    }

    protected Installations(Parcel in) {
        id = in.readLong();
        company_key = in.readString();
        condominium_id = in.readString();
        name = in.readString();
        icon = in.readString();
        color = in.readString();
        fraction_rental_time = in.readString();
        rental_lock_time = in.readString();
        amount_lock = in.readLong();
        opening_time = in.readString();
        closed_time = in.readString();
        maximum_day_reservation = in.readInt();
        minimum_day_reservation = in.readInt();
        cancellation_period = in.readInt();
        enabled_use = in.readInt();
        auto_approved = in.readInt();
        all_day = in.readInt();
        long tmpCreated_at = in.readLong();
        created_at = tmpCreated_at != -1 ? new Date(tmpCreated_at) : null;
        work_days = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(company_key);
        dest.writeString(condominium_id);
        dest.writeString(name);
        dest.writeString(icon);
        dest.writeString(color);
        dest.writeString(fraction_rental_time);
        dest.writeString(rental_lock_time);
        dest.writeLong(amount_lock);
        dest.writeString(opening_time);
        dest.writeString(closed_time);
        dest.writeInt(maximum_day_reservation);
        dest.writeInt(minimum_day_reservation);
        dest.writeInt(cancellation_period);
        dest.writeInt(enabled_use);
        dest.writeInt(auto_approved);
        dest.writeInt(all_day);
        dest.writeLong(created_at != null ? created_at.getTime() : -1L);
        dest.writeString(work_days);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Installations> CREATOR = new Parcelable.Creator<Installations>() {
        @Override
        public Installations createFromParcel(Parcel in) {
            return new Installations(in);
        }

        @Override
        public Installations[] newArray(int size) {
            return new Installations[size];
        }
    };
}