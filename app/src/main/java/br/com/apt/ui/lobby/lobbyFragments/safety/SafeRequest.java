package br.com.apt.ui.lobby.lobbyFragments.safety;

import java.util.List;

import br.com.apt.application.BaseGson;

/**
 * Created by itamarlourenco on 16/01/16.
 */
public class SafeRequest extends BaseGson {
    public List<Safe> Object;

    public List<Safe> getObject() {
        return Object;
    }
}
