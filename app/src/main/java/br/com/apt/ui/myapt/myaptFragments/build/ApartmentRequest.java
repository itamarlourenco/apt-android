package br.com.apt.ui.myapt.myaptFragments.build;

import java.util.List;

import br.com.apt.application.BaseGson;
import br.com.apt.ui.myapt.myaptFragments.notice.Notice;

/**
 * Created by adminbs on 5/10/16.
 */
public class ApartmentRequest extends BaseGson {
    public Apartment Object;

    public Apartment getObject() {
        return Object;
    }
}
