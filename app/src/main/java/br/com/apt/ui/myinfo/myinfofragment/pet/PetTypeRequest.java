package br.com.apt.ui.myinfo.myinfofragment.pet;

import java.util.List;

import br.com.apt.application.BaseGson;
import br.com.apt.ui.myinfo.myinfofragment.AutoComplete;

/**
 * Created by fabricio.bezerra on 25/09/2016.
 */
public class PetTypeRequest extends BaseGson {
    public List<AutoComplete> Object;

    public List<AutoComplete> getObject() {
        return Object;
    }
}

