package br.com.apt.ui.lobby.lobbyFragments.deliveries;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.squareup.picasso.Picasso;

import br.com.apt.R;
import br.com.apt.application.BaseActivity;
import br.com.apt.application.BaseFragment;
import br.com.apt.application.Volley;
import br.com.apt.application.ZoomImageView;
import br.com.apt.util.Logger;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomButton;
import br.com.apt.widget.CustomEditTextLight;
import br.com.apt.widget.CustomTextView;
import br.com.apt.widget.CustomTextViewBold;
import br.com.apt.widget.CustomTextViewLight;
import br.com.apt.widget.SendSuccessDialog;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by itamarlourenco on 10/01/16.
 */
public class DeliveryFragment extends BaseFragment {

    private Delivery delivery;

    public void setDelivery(Delivery delivery) {
        this.delivery = delivery;
    }

    public static DeliveryFragment newInstance(Delivery delivery) {
        DeliveryFragment introFragment = new DeliveryFragment();
        introFragment.setDelivery(delivery);
        return introFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.delivery_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if(view != null && delivery != null){
            ZoomImageView deliveryImage = (ZoomImageView) view.findViewById(R.id.deliveryImage);
            CircleImageView userWirhdrawImage = (CircleImageView) view.findViewById(R.id.userWitdrawImage);
            CustomTextViewLight sender = (CustomTextViewLight) view.findViewById(R.id.sender);
            CustomTextViewLight input = (CustomTextViewLight) view.findViewById(R.id.input);
            CustomTextViewLight withdrawal = (CustomTextViewLight) view.findViewById(R.id.withdrawal);
            CustomTextViewLight statusDescription = (CustomTextViewLight) view.findViewById(R.id.statusDescription);
            CustomTextViewLight labelWithdraw = (CustomTextViewLight) view.findViewById(R.id.labelWithdraw);
            CustomTextViewLight nameUser = (CustomTextViewLight) view.findViewById(R.id.nameUser);
            CustomButton historyButton = (CustomButton) view.findViewById(R.id.historyButton);
            LinearLayout baseStatus = (LinearLayout) view.findViewById(R.id.baseStatus);
            RelativeLayout baseWithdrawBy = (RelativeLayout) view.findViewById(R.id.baseWithdrawBy);


            Picasso.with(getContext()).load(Volley.getUrlByImage(delivery.getImage())).placeholder(R.drawable.placeholder_camera).into(deliveryImage);
            sender.setText(delivery.getNameUser());
            input.setText(
                    String.format(getContext().getText(R.string.description_delivery_without_input).toString(), Util.dateFormatted(delivery.getDate()), Util.timeFormatted(delivery.getDate()))
            );
            statusDescription.setText(getString(R.string.status_description));
            labelWithdraw.setText(getString(R.string.waiting_visits));

            if(delivery.isWithdrawn()){
                withdrawal.setText(
                        String.format(
                                getContext().getText(R.string.description_delivery_without_input).toString(),
                                Util.dateFormatted(delivery.getDateWithdrawn()),
                                Util.timeFormatted(delivery.getDateWithdrawn())
                        )
                );
                baseStatus.setVisibility(View.GONE);
                labelWithdraw.setText(getString(R.string.withdrawal));

                baseWithdrawBy.setVisibility(View.VISIBLE);
                nameUser.setText(delivery.getUserWithDraw());
                Picasso.with(getContext()).load(Volley.getUrlByImage(delivery.getUserWithdrawImage())).placeholder(R.drawable.placeholder_camera).into(userWirhdrawImage);
            }

            historyButton.setOnClickListener(new OnClickListener(){
                @Override
                public void onClick(View v) {
                    getActivity().finish();
                }
            });
        }else{
            SendSuccessDialog.show(getString(R.string.error_generic), null, getFragmentManager(), new SendSuccessDialog.Actions() {
                @Override
                public void back() {
                    getActivity().finish();
                }
            }, SendSuccessDialog.TYPE_ERROR);
        }
    }
}
