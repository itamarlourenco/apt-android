package br.com.apt.ui.MyAdm.myadmFragments.Billet;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.android.volley.Request;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.BaseFragment;
import br.com.apt.application.BaseGsonMeuAdm;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.model.user.User;
import br.com.apt.model.user.UserData;
import br.com.apt.ui.MyAdm.OccurrenceRequest;
import br.com.apt.ui.MyAdm.OccurrencesSend;
import br.com.apt.ui.MyAdm.myadmFragments.administrator.CompanyData;
import br.com.apt.ui.MyAdm.myadmFragments.administrator.CompanyDataRequest;
import br.com.apt.util.Logger;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomTextViewLight;
import br.com.apt.widget.SendSuccessDialog;

/**
 * Created by adminbs on 8/1/17.
 */

public class QuotasFragment extends BaseFragment implements Volley.Callback{

    private ListView listView;
    private ListView proposalList;
    private User user = UserData.getUser();
    private CustomTextViewLight totalDebits;
    private LinearLayout list;
    private LinearLayout proposalListBase;

    public static Fragment newInstance() {
        return new QuotasFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_quotas, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if(view != null && user != null){

            listView = (ListView) view.findViewById(R.id.listView);
            totalDebits = (CustomTextViewLight) view.findViewById(R.id.totalDebits);
            list = (LinearLayout) view.findViewById(R.id.list);
            proposalList = (ListView) view.findViewById(R.id.proposalList);
            proposalList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    int portion = (position + 1);
                    final ProposalOccurences proposalOccurences = new ProposalOccurences(
                            getString(R.string.prosal_title),
                            user.getCondominiumId(),
                            OccurrencesSend.SUBJECT_UUID_PROPOSAL,
                            user.getBlockId(),
                            user.getApto(),
                            portion
                    );


                    SendSuccessDialog.show(getString(R.string.send_proposal), proposalOccurences.getBody(), getFragmentManager(), new SendSuccessDialog.Actions() {
                        @Override
                        public void back() {
                            sendProposal(proposalOccurences);
                        }
                    }, SendSuccessDialog.TYPE_DYNAMIC);
                }
            });
            proposalListBase = (LinearLayout) view.findViewById(R.id.proposalListBase);

            handleProposalList();

            view.findViewById(R.id.proposal).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    toggleView();
                }
            });

            Volley volley = new Volley(this);
            volley.isMeuADM(true);
            volley.showDialog(getActivity());
            volley.request();
        }
    }

    private void sendProposal(ProposalOccurences proposalOccurences) {
        Volley volley = new Volley(new Volley.Callback() {
            @Override
            public void result(String result, int type) {
                try{
                    BaseGsonMeuAdm baseGsonMeuAdm = App.getGson().fromJson(result, BaseGsonMeuAdm.class);
                    SendSuccessDialog.show(baseGsonMeuAdm.getMessage(), null, getFragmentManager(), new SendSuccessDialog.Actions() {
                        @Override
                        public void back() {
                            getActivity().finish();
                        }
                    }, SendSuccessDialog.TYPE_DIALOG);

                }catch (Exception e){
                    SendSuccessDialog.show(getString(R.string.error_generic), null, getFragmentManager(), new SendSuccessDialog.Actions() {
                        @Override
                        public void back() {
                            getActivity().finish();
                        }
                    }, SendSuccessDialog.TYPE_ERROR);
                }
            }

            @Override
            public String uri() {
                return URI.OCCURRENCES;
            }
        });
        volley.isMeuADM(true);
        volley.showDialog(getActivity());
        volley.request(Request.Method.POST, proposalOccurences.toJson());
    }

    private void handleProposalList() {
        Volley volley = new Volley(new Volley.Callback() {
            @Override
            public void result(String result, int type) {
                CompanyDataRequest companyDataRequest = App.getGson().fromJson(result, CompanyDataRequest.class);
                CompanyData companyData = companyDataRequest.getObject();

                proposalList.setAdapter(new ProposaAdapter(getContext(), companyData.residentDealAmountParcel));
            }

            @Override
            public String uri() {
                return URI.COMPANY;
            }
        });
        volley.isMeuADM(true);
        volley.request();
    }

    private void toggleView() {
        if(list.getVisibility() == View.VISIBLE){
            Util.changeVisibility(list, View.GONE, true);
            Util.changeVisibility(proposalListBase, View.VISIBLE, true);
        }else{
            Util.changeVisibility(list, View.VISIBLE, true);
            Util.changeVisibility(proposalListBase, View.GONE, true);
        }
    }

    @Override
    public void result(String result, int type) {
        try{
            QuotasRequest quotasRequest = App.getGson().fromJson(result, QuotasRequest.class);
            Quotas quotas = quotasRequest.getObject();
            if(quotas != null){
                totalDebits.setText(new StringBuffer(getString(R.string.money)).append(" ").append(quotas.getAmount_open()));
                listView.setAdapter(new QuotasAdapter(getContext(), quotas.getAllReceipt()));
            }
        }catch (Exception e){
            SendSuccessDialog.show(getString(R.string.error_generic), null, getFragmentManager(), new SendSuccessDialog.Actions() {
                @Override
                public void back() {
                    getActivity().finish();
                }
            }, SendSuccessDialog.TYPE_ERROR);
        }
    }

    @Override
    public String uri() {
        return String.format(URI.FINANCE, user.getCondominiumId());
    }

}