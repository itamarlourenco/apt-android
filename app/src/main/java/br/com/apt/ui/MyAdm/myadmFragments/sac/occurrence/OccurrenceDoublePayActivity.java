package br.com.apt.ui.MyAdm.myadmFragments.sac.occurrence;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import br.com.apt.application.BaseActivity;
import br.com.apt.ui.reservations.CalendarFragment;

/**
 * Created by adminbs on 8/2/17.
 */

public class OccurrenceDoublePayActivity extends BaseActivity {

    private OccurrenceDoubleFragment occurrenceDoubleFragment = OccurrenceDoubleFragment.newInstance();

    public static Intent newIntent(Context context) {
        return new Intent(context, OccurrenceDoublePayActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public Fragment getFragment(){
        return occurrenceDoubleFragment;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        occurrenceDoubleFragment.onActivityResultFragment(requestCode, resultCode, data);
    }

    @Override
    public void finish() {
        super.finish();
    }
}
