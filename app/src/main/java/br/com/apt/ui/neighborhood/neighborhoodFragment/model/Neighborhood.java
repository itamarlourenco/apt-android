package br.com.apt.ui.neighborhood.neighborhoodFragment.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import br.com.apt.application.ModelObject;

/**
 * Created by adminbs on 1/21/16.
 */
public class Neighborhood implements ModelObject{
    private long id;

    @SerializedName("nome")
    private String name;

    @SerializedName("endereco")
    private String address;

    @SerializedName("telefone")
    private String phone;

    @SerializedName("imagem")
    private String image;

    @SerializedName("edificioId")
    private long buildId;

    @SerializedName("tipoServicoId")
    private long typeServiceId;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public long getBuildId() {
        return buildId;
    }

    public void setBuildId(long buildId) {
        this.buildId = buildId;
    }

    public long getTypeServiceId() {
        return typeServiceId;
    }

    public void setTypeServiceId(long typeServiceId) {
        this.typeServiceId = typeServiceId;
    }

    protected Neighborhood(Parcel in) {
        id = in.readLong();
        name = in.readString();
        address = in.readString();
        phone = in.readString();
        image = in.readString();
        buildId = in.readLong();
        typeServiceId = in.readLong();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(name);
        dest.writeString(address);
        dest.writeString(phone);
        dest.writeString(image);
        dest.writeLong(buildId);
        dest.writeLong(typeServiceId);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Neighborhood> CREATOR = new Parcelable.Creator<Neighborhood>() {
        @Override
        public Neighborhood createFromParcel(Parcel in) {
            return new Neighborhood(in);
        }

        @Override
        public Neighborhood[] newArray(int size) {
            return new Neighborhood[size];
        }
    };
}