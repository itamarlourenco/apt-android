package br.com.apt.ui.lobby.lobbyFragments.visits;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import br.com.apt.application.BaseActivity;
import br.com.apt.ui.reservations.CalendarFragment;

/**
 * Created by adminbs on 7/13/17.
 */
public class VisitsDetailsActivity extends BaseActivity {

    private static final String EXTRA_VISIT = "EXTRA_VISIT";

    public static Intent newIntent(Context context, Visit visit) {
        Intent intent = new Intent(context, VisitsDetailsActivity.class);
        intent.putExtra(EXTRA_VISIT, visit);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public Fragment getFragment(){
        Intent intent = getIntent();
        if(intent != null){
            Visit visits =  intent.getParcelableExtra(EXTRA_VISIT);
            return VisitsDetailsFragment.newInstance(visits);
        }

        return null;
    }
}
