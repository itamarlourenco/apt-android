package br.com.apt.ui.MyAdm.myadmFragments.sac;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import br.com.apt.R;
import br.com.apt.application.BaseActivity;
import br.com.apt.ui.MyAdm.myadmFragments.sac.billet.BilletFragment;
import br.com.apt.ui.reservations.CalendarFragment;

/**
 * Created by adminbs on 7/31/17.
 */

public class SolicitationAddActivity extends BaseActivity {
    public static Intent newIntent(Context context) {
        return new Intent(context, SolicitationAddActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public Fragment getFragment(){
        return SolicitationAddFragment.newInstance();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(BilletFragment.isGoToBillet()){
            finish();
        }
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_down, R.anim.slide_out_down);
    }
}