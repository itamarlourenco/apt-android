package br.com.apt.ui.myCondominium;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import br.com.apt.R;
import br.com.apt.application.BaseFragment;

/**
 * Created by adminbs on 8/6/17.
 */

public class OpenPDFFragment extends BaseFragment{

    private String url;


    public static OpenPDFFragment newInstance(){
        return new OpenPDFFragment();
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.open_pdf, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if (view != null) {
            WebView webView = (WebView) view.findViewById(R.id.webView);
            webView.getSettings().setJavaScriptEnabled(true);
            webView.loadUrl("https://drive.google.com/viewerng/viewer?embedded=true&url=" + getUrl());
        }
    }
}
