package br.com.apt.ui.setting.settingFragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;

import org.w3c.dom.Text;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.BaseGson;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.model.user.User;
import br.com.apt.model.user.UserData;
import br.com.apt.ui.setting.Setting;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomEditText;
import br.com.apt.widget.CustomTextView;
import br.com.apt.widget.PagerItemFragment;

/**
 * Created by adminbs on 1/12/16.
 */
public class UserSettingFragment extends PagerItemFragment {
    private CustomEditText name;
    private CustomEditText email;
    private CustomEditText phone;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_user_setting, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        User user = getUser();
        if(view != null && user != null){

            (view.findViewById(R.id.buttonSaveUser)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    updateUser();
                }
            });

            name = (CustomEditText) view.findViewById(R.id.name);
            name.setText(user.getName());

            email = (CustomEditText) view.findViewById(R.id.email);
            email.setText(user.getEmail());

            phone = (CustomEditText) view.findViewById(R.id.phone);
            phone.setText(user.getPhone());
        }
    }

    @Override
    protected String getPagerTitle() {
        return App.getContext().getString(R.string.user);
    }

    @Override
    protected int getPagerIcon() {
        return R.drawable.ic_setting_user;
    }

    public void updateUser(){
        Setting setting = new Setting();
        setting.setName(name.getText().toString());
        setting.setUsername(email.getText().toString());
        setting.setPhone(phone.getText().toString());

        Volley volley = new Volley(new Volley.Callback() {
            @Override
            public void result(String result, int type) {
                if(result != null){
                    BaseGson baseGson = App.getGson().fromJson(result, BaseGson.class);
                    Util.showDialog(getActivity(), baseGson.getMessage());

                    User user = getUser();
                    user.setName(name.getText().toString());
                    user.setEmail(email.getText().toString());
                    user.setPhone(phone.getText().toString());

                    UserData.saveUser(user);
                }
            }

            @Override
            public String uri() {
                return URI.USER;
            }
        });
        volley.showDialog(getActivity());
        volley.request(Request.Method.POST, setting.toJson());
    }

}
