package br.com.apt.ui.lobby.lobbyFragments.visits;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.android.volley.Request;
import com.squareup.picasso.Picasso;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.BaseActivity;
import br.com.apt.application.BaseFragment;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.application.ZoomImageView;
import br.com.apt.application.services.NotificationService;
import br.com.apt.ui.ExceptionWithoutResult;
import br.com.apt.util.Logger;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomTextView;
import br.com.apt.widget.CustomTextViewLight;
import br.com.apt.widget.SendSuccessDialog;

/**
 * Created by adminbs on 7/13/17.
 */

public class VisitsDetailsFragment extends BaseFragment {

    private Visit visits;
    private boolean isAuthorized;

    public void setVisits(Visit visits) {
        this.visits = visits;
    }

    public static VisitsDetailsFragment newInstance(Visit visit) {
        VisitsDetailsFragment visitsDetailsFragment = new VisitsDetailsFragment();
        visitsDetailsFragment.setVisits(visit);
        return visitsDetailsFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.visits_details_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState) ;
        View view = getView();
        if(view != null && visits != null){
            CustomTextViewLight name = (CustomTextViewLight)  view.findViewById(R.id.name);
            CustomTextViewLight type = (CustomTextViewLight) view.findViewById(R.id.type);
            CustomTextViewLight arrival = (CustomTextViewLight) view.findViewById(R.id.arrival);
            CustomTextViewLight mLabelReleased = (CustomTextViewLight) view.findViewById(R.id.mLabelReleased);
            CustomTextViewLight released = (CustomTextViewLight) view.findViewById(R.id.released);
            CustomTextViewLight statusDescription = (CustomTextViewLight) view.findViewById(R.id.statusDescription);
            ZoomImageView visitsImage = (ZoomImageView) view.findViewById(R.id.visitsImage);
            LinearLayout buttonActions = (LinearLayout) view.findViewById(R.id.buttonActions);

            view.findViewById(R.id.visitsHistory).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    getActivity().finish();
                }
            });

            view.findViewById(R.id.authorized).setOnClickListener(new OnClickListener(){
                @Override
                public void onClick(View v) {
                    authorized();
                }
            });

            view.findViewById(R.id.refuse).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    refuse();
                }
            });


            name.setText(String.format(getString(R.string.visits_details), visits.getName()));
            type.setText(visits.getType());
            arrival.setText(Util.dateFormatted(visits.getDate()) + " às \n"+ Util.timeFormatted(visits.getDate(), "HH:mm"));
            Picasso.with(getContext()).load(Volley.getUrlByImage(visits.getImage())).placeholder(R.drawable.placeholder_camera).into(visitsImage);


            if(visits.isFree() == VisitsListViewAdapter.WAITING_VISITS){
                statusDescription.setText(R.string.visitor_waiting_for);
                mLabelReleased.setText(getString(R.string.waiting_visits));
                released.setText(getString(R.string.visitor_waiting_for));
                released.setVisibility(View.INVISIBLE);
                buttonActions.setVisibility(View.VISIBLE);
            }

            if(visits.isFree() == VisitsListViewAdapter.IS_FREE){
                statusDescription.setText(R.string.visitor_released);
                mLabelReleased.setText(getString(R.string.released));
                released.setText(Util.dateFormatted(visits.getData_liberado()) + " às \n"+ Util.timeFormatted(visits.getData_liberado(), "HH:mm"));
                buttonActions.setVisibility(View.GONE);
            }

            if(visits.isFree() == VisitsListViewAdapter.NOT_FREE){
                statusDescription.setText(R.string.refused_visitor);
                mLabelReleased.setText(getString(R.string.refused));
                released.setText(Util.dateFormatted(visits.getData_liberado()) + " às \n"+ Util.timeFormatted(visits.getData_liberado(), "HH:mm"));
                buttonActions.setVisibility(View.GONE);
            }

            if(visits.isScheduled()){
                statusDescription.setText(R.string.scheduled_visitor);
                mLabelReleased.setText(getString(R.string.scheduled));
                released.setText(Util.dateFormatted(visits.getData_liberado()) + " às \n"+ Util.timeFormatted(visits.getData_liberado(), "HH:mm"));
                buttonActions.setVisibility(View.GONE);
            }

        }else{
            SendSuccessDialog.show(getString(R.string.error_generic), null, getFragmentManager(), new SendSuccessDialog.Actions() {
                @Override
                public void back() {
                    getActivity().finish();
                }
            }, SendSuccessDialog.TYPE_ERROR);
        }
    }

    public void authorized(){
        SendSuccessDialog.show(getString(R.string.authorizedVisitWant), null, getFragmentManager(), new SendSuccessDialog.Actions() {
            @Override
            public void back() {
                authorizeResident(visits.getId(), true);
                isAuthorized = true;
            }
        }, SendSuccessDialog.TYPE_DYNAMIC);
    }

    public void refuse(){
        SendSuccessDialog.show(getString(R.string.notAuthorizedVisitWant), null, getFragmentManager(), new SendSuccessDialog.Actions() {
            @Override
            public void back() {
                authorizeResident(visits.getId(), false);
                isAuthorized = false;
            }
        }, SendSuccessDialog.TYPE_DYNAMIC_ERROR);
    }

    private void authorizeResident(long id, boolean authorize){
        Volley volley = new Volley(new Volley.Callback() {
            @Override
            public void result(String result, int type) {
                NotificationService.startService(getContext());
                try{
                    AuthorizeRequest authorizeRequest = App.getGson().fromJson(result, AuthorizeRequest.class);
                    if(authorizeRequest.getStatus() == Volley.STATUS_OK){
                        SendSuccessDialog.show(authorizeRequest.getMessage(), null, getFragmentManager(), new SendSuccessDialog.Actions() {
                            @Override
                            public void back() {
                                getActivity().finish();
                            }
                        }, isAuthorized ? SendSuccessDialog.TYPE_DIALOG : SendSuccessDialog.TYPE_ERROR);
                    }
                }catch (Exception e){
                    SendSuccessDialog.show(getString(R.string.error_generic), null, getFragmentManager(), null, SendSuccessDialog.TYPE_ERROR);
                }
            }

            @Override
            public String uri() {
                return URI.VISITS;
            }
        });
        AuthorizeSend authorizeSend = new AuthorizeSend();
        authorizeSend.setVisitId(id);
        authorizeSend.setReleased(authorize);
        volley.request(Request.Method.PUT, authorizeSend.toJson());
        volley.showDialog(getActivity());
    }
}