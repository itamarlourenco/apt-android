package br.com.apt.ui.reservations;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.apt.R;
import br.com.apt.application.Volley;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomTextViewLight;

/**
 * Created by adminbs on 9/6/17.
 */

public class InstallationsAdapter extends BaseAdapter {
    private Context context;
    private List<Installations> installations;
    private InstallationsFragment parent;


    public InstallationsAdapter(InstallationsFragment parent, Context context, List<Installations> installations) {
        this.context = context;
        this.installations = installations;
        this.parent = parent;
    }

    @Override
    public int getCount() {
        return installations != null ? installations.size() : 0;
    }

    @Override
    public Installations getItem(int position) {
        return installations.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        InstallationsAdapter.ViewHolder holder;

        if(convertView == null){
            view = LayoutInflater.from(context).inflate(R.layout.row_installation, parent, false);
            holder = new InstallationsAdapter.ViewHolder(view);
            view.setTag(holder);
        }else{
            view = convertView;
            holder = (InstallationsAdapter.ViewHolder) view.getTag();
        }

        bindList(holder, position);
        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private void bindList(final InstallationsAdapter.ViewHolder holder, final int position) {
        Installations installations = getItem(position);
        Picasso.with(context).load(Installations.handleUrlIcon(installations.getIcon())).into(holder.icon);
        holder.label.setText(installations.getName());
    }

    public class ViewHolder{
        private ImageView icon;
        private CustomTextViewLight label;

        public ViewHolder(View view) {
            icon = (ImageView) view.findViewById(R.id.icon);
            label = (CustomTextViewLight) view.findViewById(R.id.label);
        }
    }
}
