package br.com.apt.ui.regulation;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import br.com.apt.R;
import br.com.apt.application.BaseActivity;
import br.com.apt.application.Config;
import br.com.apt.model.user.User;
import br.com.apt.model.user.UserData;

/**
 * Created by adminbs on 5/16/16.
 */
public class RegulationActivity extends BaseActivity {

    public static Intent newIntent(Context context) {
        return new Intent(context, RegulationActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        User user = UserData.getUser();

        WebView webView = (WebView) findViewById(R.id.webView);
        final ProgressBar loader = (ProgressBar) findViewById(R.id.loader);
        webView.loadUrl(Config.Regulation.URL(user != null ? user.getBlockId() : "0"));
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.setWebViewClient(new WebViewClient() {
            public void onPageFinished(WebView view, String url) {
                loader.setVisibility(View.GONE);
            }
        });
    }

    @Override
    protected int getTitleToolbarColor() {
        return R.color.about_color;
    }

    @Override
    protected int getIdLayoutActivity() {
        return R.layout.about_activity;
    }
}