package br.com.apt.ui.myapt.myaptFragments.plan;

import android.app.VoiceInteractor;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.android.volley.Request;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.BaseGsonMeuAdm;
import br.com.apt.application.Config;
import br.com.apt.application.PicturesHelper;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.model.user.User;
import br.com.apt.ui.myinfo.AutoCompleteDialog;
import br.com.apt.ui.myinfo.myinfofragment.AutoComplete;
import br.com.apt.ui.myinfo.myinfofragment.FragmentMyInfoAdd;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomButton;
import br.com.apt.widget.CustomEditText;
import br.com.apt.widget.SendSuccessDialog;
import siclo.com.ezphotopicker.api.EZPhotoPick;
import siclo.com.ezphotopicker.api.EZPhotoPickStorage;

/**
 * Created by adminbs on 2/23/18.
 */

public class PlantAddFragment extends FragmentMyInfoAdd{

    private Plant plant;
    private ImageView image;
    private List<AutoComplete> plantTypes;
    private CustomEditText name;
    private CustomEditText update;
    private Plant.PlantType plantType;
    private String plantTypeUuiidSelectd = "";
    private CustomButton sendPlant;
    private CustomButton deletePlant;
    private Bitmap bitmapPhoto;
    private String urlImage;
    private Volley volley;
    private boolean editPlant;
    private TextInputLayout baseLastUpdate;

    public static PlantAddFragment newInstance(){
        return new PlantAddFragment();
    }

    public static PlantAddFragment newInstance(Plant plant){
        PlantAddFragment plantAddFragment = new PlantAddFragment();
        plantAddFragment.setPlant(plant);
        return plantAddFragment;
    }


    public void setPlant(Plant plant) {
        this.plant = plant;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_add_plant, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        image = (ImageView) view.findViewById(R.id.image);
        name = (CustomEditText) view.findViewById(R.id.name);
        update = (CustomEditText) view.findViewById(R.id.update);
        sendPlant = (CustomButton) view.findViewById(R.id.sendPlant);
        deletePlant = (CustomButton) view.findViewById(R.id.deletePlant);
        baseLastUpdate = (TextInputLayout) view.findViewById(R.id.baseLastUpdate);
        update.setEnabled(false);
        editPlant = (plant != null && !TextUtils.isEmpty(plant.getUuid()));

        if(editPlant){
            baseLastUpdate.setVisibility(View.VISIBLE);
        }

        if(editPlant){
            Picasso.with(getContext()).load(plant.getImage()).placeholder(R.drawable.default_camera).into(image);
            plantType = plant.getPlantType();
            if(plantType != null){
                name.setText(plantType.getTitle());
                plantTypeUuiidSelectd = plantType.getUuid();
            }
            update.setText(Util.dateFormatted(plant.getUpdatedAt()));
            deletePlant.setVisibility(View.VISIBLE);
        }

        name.setOnFocusChangeListener(new View.OnFocusChangeListener(){
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    openDialogPlantTypes();
                }
            }
        });
        name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialogPlantTypes();
            }
        });

        sendPlant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendPlant();
            }
        });

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CharSequence[] options = {getString(R.string.takePhoto), getString(R.string.chooseGallery)};
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                builder.setTitle("")
                        .setItems(options, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                bitmapPhoto = null;
                                if(which == 0) {
                                    PicturesHelper.Camera(getActivity());
                                } else if(which == 1) {
                                    PicturesHelper.Gallery(getActivity());
                                }else{
                                    image.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.placeholder_camera));
                                }
                            }
                        });
                builder.create().show();
            }
        });

        deletePlant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SendSuccessDialog.show(getString(R.string.wanna_delete_plant), null, getFragmentManager(), new SendSuccessDialog.Actions() {
                    @Override
                    public void back() {
                        deletePlant();
                    }
                }, SendSuccessDialog.TYPE_DYNAMIC_ERROR);

            }
        });

        getPlantsType();
    }

    @Override
    protected String getPagerTitle() {
        return null;
    }

    @Override
    protected int getPagerIcon() {
        return 0;
    }

    @Override
    protected HashMap<String, String> save() {
        return null;
    }

    @Override
    protected String getUri() {
        return null;
    }

    @Override
    protected int getTypeRequest() {
        return 0;
    }

    @Override
    protected void saved() {

    }

    @Override
    protected void saved(String json) {

    }

    private void openDialogPlantTypes() {
        AutoCompleteDialog.show(getFragmentManager(), plantTypes, new AutoCompleteDialog.Click() {
            @Override
            public void click(AutoComplete autoComplete) {
                plantTypeUuiidSelectd = autoComplete.getUuid();
                name.setText(autoComplete.getTitle());
            }
        }, getString(R.string.choose_plant_type));
    }

    public void getPlantsType() {
        name.setText(R.string.waiting);
        Volley volley = new Volley(new Volley.Callback() {
            @Override
            public void result(String result, int type) {
                if(plantType != null){
                    name.setText(plantType.getTitle());
                }else{
                    name.setText("");
                }

                PlantTypeRequest plantTypeRequest = App.getGson().fromJson(result, PlantTypeRequest.class);
                if(plantTypeRequest != null){
                    plantTypes = plantTypeRequest.getObject();
                }
            }

            @Override
            public String uri() {
                return URI.PLANT_TYPE;
            }
        });
        volley.isMeuADM(true);
        volley.request();
    }


    public void sendPlant(){
        if(TextUtils.isEmpty(plantTypeUuiidSelectd)){
            SendSuccessDialog.show(getString(R.string.error_plant_type), null, getFragmentManager());
            return;
        }

        if(bitmapPhoto == null && !editPlant){
            SendSuccessDialog.show(getString(R.string.error_plant_photo), null, getFragmentManager());
            return;
        }

        volley = new Volley(new Volley.Callback() {
            @Override
            public void result(String result, int type) {
                if(result != null){
                    BaseGsonMeuAdm baseGson = App.getGson().fromJson(result, BaseGsonMeuAdm.class);
                    if(baseGson.getCode() == Volley.STATUS_OK){
                        SendSuccessDialog.show(baseGson.getMessage(), "", getFragmentManager(), new SendSuccessDialog.Actions(){
                            @Override
                            public void back() {
                                getActivity().finish();
                            }
                        });
                    }else{
                        SendSuccessDialog.show(getString(R.string.error_generic), null, getFragmentManager(), null, SendSuccessDialog.TYPE_ERROR);
                    }
                }
            }

            @Override
            public String uri() {
                String uuid = editPlant ? "/" + plant.getUuid() : "";
                return URI.PLANT + uuid;
            }
        });
        volley.showDialog(getActivity());
        (new TaskUploadS3(this)).execute();
    }

    private static class TaskUploadS3 extends AsyncTask<Void, Void, Void>{

        private PlantAddFragment currentFragment;

        public TaskUploadS3(PlantAddFragment currentFragment) {
            this.currentFragment = currentFragment;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            final File finalPhotoPlant = Util.createImageFile();
            if(currentFragment.bitmapPhoto != null && finalPhotoPlant != null){
                Util.sendBitmapToFile(finalPhotoPlant, currentFragment.bitmapPhoto);
            }

            String namePhoto = Util.generateName("plant");
            currentFragment.urlImage = Config.Amazon.handleLocation(namePhoto);
            AmazonS3Client s3Client = new AmazonS3Client(new BasicAWSCredentials(Config.Amazon.ACCESS_KEY, Config.Amazon.PUBLIC_KEY));
            PutObjectRequest requestBillet = new PutObjectRequest(Config.Amazon.BUCKET, String.format(Config.Amazon.PATH, namePhoto), finalPhotoPlant);
            s3Client.putObject(requestBillet);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            this.currentFragment.requestPlant();
        }
    }

    private void requestPlant(){
        final User user = getUser();
        ArrayList<Plant.Units> units = new ArrayList<>();
        units.add(new Plant.Units(0, user.getApto()));

        if(bitmapPhoto == null){
            urlImage = plant.getImage();
        }

        Plant plant = new Plant(
                user.getCondominiumId(),
                0,
                user.getCurrentBlock(),
                Plant.AREA,
                plantTypeUuiidSelectd,
                urlImage,
                units
        );

        volley.isMeuADM(true);
        volley.request(editPlant ? Request.Method.PUT : Request.Method.POST, plant.toJson());
    }

    @Override
    public void onActivityResultFragment(int requestCode, int resultCode, Intent data) {
        if(requestCode == EZPhotoPick.PHOTO_PICK_REQUEST_CODE && resultCode != 0){
            try {
                EZPhotoPickStorage ezPhotoPickStorage = new EZPhotoPickStorage(getActivity());
                bitmapPhoto = ezPhotoPickStorage.loadLatestStoredPhotoBitmap();
                image.setImageBitmap(bitmapPhoto);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void deletePlant(){
        Volley volley = new Volley(new Volley.Callback() {
            @Override
            public void result(String result, int type) {
                if(result != null){
                    BaseGsonMeuAdm baseGson = App.getGson().fromJson(result, BaseGsonMeuAdm.class);
                    if(baseGson.getCode() == Volley.STATUS_OK){
                        SendSuccessDialog.show(baseGson.getMessage(), "", getFragmentManager(), new SendSuccessDialog.Actions(){
                            @Override
                            public void back() {
                                getActivity().finish();
                            }
                        });
                    }else{
                        SendSuccessDialog.show(getString(R.string.error_generic), null, getFragmentManager(), null, SendSuccessDialog.TYPE_ERROR);
                    }
                }
            }

            @Override
            public String uri() {
                String uuid = plant.getUuid();
                return URI.PLANT + "/" + uuid;
            }
        });
        volley.showDialog(getActivity());
        volley.isMeuADM(true);
        volley.request(Request.Method.DELETE, null);
    }
}