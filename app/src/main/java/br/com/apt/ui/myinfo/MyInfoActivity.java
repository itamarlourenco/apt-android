package br.com.apt.ui.myinfo;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import br.com.apt.application.BaseActivity;
import br.com.apt.ui.reservations.CalendarFragment;

/**
 * Created by fabricio.bezerra on 22/09/2016.
 */
public class MyInfoActivity extends BaseActivity {

    public static Intent newIntent(Context context) {
        return new Intent(context, MyInfoActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public Fragment getFragment(){
        return MyInfoFragment.newInstance();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        MyInfoFragment myInfoFragment = (MyInfoFragment) getFragment();
        myInfoFragment.onActivityResultFragment(requestCode, resultCode, data);
    }
}
