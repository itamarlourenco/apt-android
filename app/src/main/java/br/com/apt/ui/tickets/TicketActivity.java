package br.com.apt.ui.tickets;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import br.com.apt.application.BaseActivity;
import br.com.apt.ui.MyAdm.myadmFragments.sac.SacFragment;
import br.com.apt.ui.reservations.CalendarFragment;

public class TicketActivity extends BaseActivity {

    private static final String TICKET_IS_NOTIFICATION = "TICKET_IS_NOTIFICATION";

    public static Intent newIntent(Context context) {
        return new Intent(context, TicketActivity.class);
    }

    public static Intent newIntent(Context context, boolean isNotification) {
        if(isNotification){
            Intent intent = new Intent(context, TicketActivity.class);
            intent.putExtra(TICKET_IS_NOTIFICATION, true);

            return intent;
        }
        return new Intent(context, TicketActivity.class);
    }

    public static Intent newIntent(Context context, int position) {
        return intentWithPosition(new Intent(context, TicketActivity.class), position);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public Fragment getFragment(){
        Intent intent = getIntent();
        if(intent != null){
            boolean isNotification = intent.getBooleanExtra(TICKET_IS_NOTIFICATION, false);

            SacFragment sacFragment = new SacFragment();
            sacFragment.isNotification(isNotification);
            return sacFragment;
        }


        return null;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}