package br.com.apt.ui.main;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.apt.application.BaseGson;
import br.com.apt.application.ModelObject;

/**
 * Created by adminbs on 1/15/16.
 */
public class SensorsRequest extends BaseGson implements ModelObject{
    public ArrayList<Sensors> Object;
    public ArrayList<Sensors> getObject() {
        return Object;
    }

    public void setObject(ArrayList<Sensors> object) {
        Object = object;
    }

    protected SensorsRequest(Parcel in) {
        if (in.readByte() == 0x01) {
            Object = new ArrayList<Sensors>();
            in.readList(Object, Sensors.class.getClassLoader());
        } else {
            Object = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (Object == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(Object);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<SensorsRequest> CREATOR = new Parcelable.Creator<SensorsRequest>() {
        @Override
        public SensorsRequest createFromParcel(Parcel in) {
            return new SensorsRequest(in);
        }

        @Override
        public SensorsRequest[] newArray(int size) {
            return new SensorsRequest[size];
        }
    };
}