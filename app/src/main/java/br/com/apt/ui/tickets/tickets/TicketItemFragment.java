package br.com.apt.ui.tickets.tickets;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.squareup.picasso.Picasso;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.BaseFragment;
import br.com.apt.application.BaseGson;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.application.ZoomImageView;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomButton;
import br.com.apt.widget.CustomTextView;
import br.com.apt.widget.CustomTextViewLight;


public class TicketItemFragment extends BaseFragment{

    private Ticket ticket;

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    public static TicketItemFragment newInstance(Ticket ticket) {
        TicketItemFragment ticketItemFragment = new TicketItemFragment();
        ticketItemFragment.setTicket(ticket);
        return ticketItemFragment;
    }

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_item_tickets, container, false);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if(view != null){
            ZoomImageView image = (ZoomImageView) view.findViewById(R.id.image);
            CustomTextViewLight title = (CustomTextViewLight) view.findViewById(R.id.title);
            CustomTextViewLight message = (CustomTextViewLight) view.findViewById(R.id.message);
            CustomTextViewLight status = (CustomTextViewLight) view.findViewById(R.id.status);
            CustomTextViewLight statusDescription = (CustomTextViewLight) view.findViewById(R.id.statusDescription);
            CustomButton close = (CustomButton) view.findViewById(R.id.close);

            Picasso.with(getContext()).load(Volley.getUrlByImage(ticket.getImageString())).placeholder(R.drawable.placeholder_camera).into(image);
            title.setText(ticket.getTitle());
            message.setText(ticket.getDescription());
            close.setOnClickListener(new OnClickListener(){
                @Override
                public void onClick(View v) {
                    getActivity().finish();
                }
            });

            switch (ticket.getStatus()) {
                case Ticket.TODO:
                    statusDescription.setText(getString(R.string.sending).toUpperCase());
                    status.setTextColor(ContextCompat.getColor(getContext(), R.color.faded_blue));
                    break;
                case Ticket.PROGRESS:
                    statusDescription.setText(getString(Ticket.translateStatus(ticket.getStatus())).toUpperCase());
                    status.setTextColor(ContextCompat.getColor(getContext(), R.color.leather));
                    break;
                case Ticket.DONE:
                    statusDescription.setText(getString(Ticket.translateStatus(ticket.getStatus())).toUpperCase());
                    status.setTextColor(ContextCompat.getColor(getContext(), R.color.pale_olive_green));
                    break;
            }
        }
    }
}