package br.com.apt.ui.myinfo.myinfofragment.resident;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

import br.com.apt.R;
import br.com.apt.application.ModelObject;

/**
 * Created by fabricio.bezerra on 25/09/2016.
 */
public class Resident implements ModelObject {

    public static final String TODO = "TODO";
    public static final String PROGRESS = "PROGRESS";
    public static final String DONE = "DONE";

    @SerializedName("pet_id")
    private int id;

    @SerializedName("pet_name")
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Resident() {

    }

    protected Resident(Parcel in) {
        id = in.readInt();
        name = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
    }

    @SuppressWarnings("unused")
    public static final Creator<Resident> CREATOR = new Creator<Resident>() {
        @Override
        public Resident createFromParcel(Parcel in) {
            return new Resident(in);
        }

        @Override
        public Resident[] newArray(int size) {
            return new Resident[size];
        }
    };

    public static int translateStatus(String status){
        switch (status){
            case Resident.TODO:
                return R.string.todo;

            case Resident.PROGRESS:
                return R.string.progress;

            case Resident.DONE:
                return R.string.done;
        }

        return R.string.todo;
    }
}
