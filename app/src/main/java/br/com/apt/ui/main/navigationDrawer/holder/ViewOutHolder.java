package br.com.apt.ui.main.navigationDrawer.holder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.com.apt.R;
import br.com.apt.ui.main.navigationDrawer.NavigationDrawerMenuAdapter;
import br.com.apt.widget.CustomTextView;

/**
 * Created by itamarlourenco on 04/01/16.
 */
public class ViewOutHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private NavigationDrawerMenuAdapter.OnClickItemMenu onClickItemMenu;
    public CustomTextView out;


    public ViewOutHolder(View itemView, NavigationDrawerMenuAdapter.OnClickItemMenu clickItemMenu) {
        super(itemView);
        onClickItemMenu = clickItemMenu;
        out = (CustomTextView) itemView.findViewById(R.id.out);
        out.setOnClickListener(this);
    }

    public static View getView(Context context, ViewGroup viewGroup){
        return LayoutInflater.from(context).inflate(R.layout.view_out_holder, viewGroup, false);
    }

    public void onClick(View v) {
        onClickItemMenu.clickClose(v, getAdapterPosition());
    }
}
