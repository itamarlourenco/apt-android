package br.com.apt.ui.MyAdm.myadmFragments.sac;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;

import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.apt.R;
import br.com.apt.application.Volley;
import br.com.apt.ui.MyAdm.Occurrences;
import br.com.apt.ui.lobby.lobbyFragments.deliveries.DeliveriesListViewAdapter;
import br.com.apt.ui.lobby.lobbyFragments.deliveries.Delivery;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomTextViewLight;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by adminbs on 8/4/17.
 */

class HistoryAdapter extends BaseAdapter {

    private Context context;
    private List<History> historyList;

    public HistoryAdapter(Context context, List<History> historyList) {
        this.context = context;
        this.historyList = historyList;
    }

    @Override
    public int getCount() {
        return historyList != null ? historyList.size() : 0;
    }

    @Override
    public History getItem(int position) {
        return historyList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        HistoryAdapter.ViewHolder holder;

        if(convertView == null){
            view = LayoutInflater.from(context).inflate(R.layout.row_history, parent, false);
            holder = new HistoryAdapter.ViewHolder(view);
            view.setTag(holder);
        }else{
            view = convertView;
            holder = (HistoryAdapter.ViewHolder) view.getTag();
        }

        bindList(holder, position);
        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private void bindList(HistoryAdapter.ViewHolder holder, int position) {
        History item = getItem(position);

        holder.message.setText(item.getComment());
        holder.by.setText(item.getBy());
        holder.dateTime.setText(
                new StringBuffer(context.getString(R.string.day))
                    .append(" ")
                    .append(Util.dateFormatted(item.getChangedAt()))
                    .append(" às ").append(Util.timeFormatted(item.getChangedAt()))
        );

        Occurrences.User user = item.getUser();
        if (user != null) {
            Occurrences.User.Photo photo = user.getPhoto();
            if (photo != null && !TextUtils.isEmpty(photo.url)) {
                try{
                    String[] base64string = photo.url.split(",");

                    byte[] decodedString;
                    if(base64string.length > 1){
                        decodedString = Base64.decode(base64string[1], Base64.DEFAULT);
                    }else{
                        decodedString = Base64.decode(photo.url, Base64.DEFAULT);
                    }

                    Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0,decodedString.length);
                    holder.userImage.setImageBitmap(decodedByte);
                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        }
    }

    public class ViewHolder{
        private CircleImageView userImage;
        private CustomTextViewLight message;
        private CustomTextViewLight by;
        private CustomTextViewLight dateTime;

        public ViewHolder(View view) {
            userImage = (CircleImageView) view.findViewById(R.id.userImage);
            message = (CustomTextViewLight) view.findViewById(R.id.message);
            by = (CustomTextViewLight) view.findViewById(R.id.by);
            dateTime = (CustomTextViewLight) view.findViewById(R.id.dateTime);
        }
    }
}
