package br.com.apt.ui.firstAccess.singup;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.com.apt.R;
import br.com.apt.application.BaseFragment;
import br.com.apt.widget.CustomEditText;

/**
 * Created by adminbs on 9/15/16.
 */
public class SingUpPasswordFragment extends BaseFragment {

    public static CustomEditText password;
    public static CustomEditText confirmPassword;

    public static SingUpPasswordFragment newInstance() {
        return new SingUpPasswordFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.sing_up_password, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if(view != null){
            password = (CustomEditText) view.findViewById(R.id.password);
            confirmPassword = (CustomEditText) view.findViewById(R.id.confirmPassword);
        }
    }
}

