package br.com.apt.ui.MyAdm.myadmFragments.sac.occurrence;

import br.com.apt.application.BaseGson;
import br.com.apt.application.BaseGsonMeuAdm;
import br.com.apt.application.SendRequest;

/**
 * Created by adminbs on 8/4/17.
 */

public class OccurrenceSubject {

    private String id;
    private String name;

    public OccurrenceSubject(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
