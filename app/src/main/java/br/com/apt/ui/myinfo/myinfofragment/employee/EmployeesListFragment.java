package br.com.apt.ui.myinfo.myinfofragment.employee;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.Request;
import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.github.ybq.android.spinkit.SpinKitView;

import java.util.HashMap;
import java.util.List;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.ui.ExceptionWithoutResult;
import br.com.apt.ui.myinfo.myinfofragment.FragmentMyInfoAddImage;
import br.com.apt.ui.myinfo.myinfofragment.pet.MyPetAddActivity;
import br.com.apt.ui.myinfo.myinfofragment.pet.Pet;
import br.com.apt.util.Util;
import br.com.apt.widget.PagerItemFragment;

public class EmployeesListFragment extends FragmentMyInfoAddImage implements Volley.Callback{

    private SwipeMenuListView listView;
    private List<Employees> employees;
    private SpinKitView loader;


    public static PagerItemFragment newInstance(){
        return new EmployeesListFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_employee, container, false);
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if (view != null) {
            loader = (SpinKitView) view.findViewById(R.id.loader);
            listView = (SwipeMenuListView) view.findViewById(R.id.listView);

            listView.setMenuCreator(new SwipeMenuCreator() {
                @Override
                public void create(SwipeMenu menu) {
                    SwipeMenuItem openItem = new SwipeMenuItem(getContext());
                    openItem.setBackground(new ColorDrawable(Color.rgb(0xF9, 0x3F, 0x25)));
                    openItem.setWidth(120);
                    openItem.setTitle("X");
                    openItem.setTitleSize(18);
                    openItem.setTitleColor(Color.BLACK);
                    menu.addMenuItem(openItem);
                }
            });

            listView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                    switch (index) {
                        case 0:
                            Employees employee = employees.get(index);
                            removeItem(employee.getId(), new CallbackDelete() {
                                @Override
                                public void delete() {
                                    getEmployees();
                                }
                            });
                            break;
                    }
                    return false;
                }
            });


            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    startActivity(EmployeesAddActivity.newIntent(getContext(), employees.get(position)));
                }
            });
            view.findViewById(R.id.newEmployee).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(EmployeesAddActivity.newIntent(getContext()));
                    getActivity().overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
                }
            });
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getEmployees();
    }

    private void getEmployees(){
        Volley volley = new Volley(this);
        volley.request(Request.Method.GET, null);
    }

    @Override
    public void result(String result, int type) {
        View view = getView();
        try{
            Util.hidePlaceholder(view);
            Util.changeVisibility(loader, View.GONE, true);
            Util.changeVisibility(listView, View.VISIBLE, true);
            EmployeesRequest employeesRequest = App.getGson().fromJson(result, EmployeesRequest.class);
            employees = employeesRequest.getObject();

            if(employees == null){
                throw new ExceptionWithoutResult(getString(R.string.message_without_employees), R.drawable.img_empregados_no_have);
            }
            listView.setAdapter(new EmployeesAdapter(getContext(), employees));
        }catch (ExceptionWithoutResult exceptionWithoutResult){
            Util.showPlaceholder(view, getActivity(), exceptionWithoutResult.getDrawableId(), exceptionWithoutResult.getMessage());
        }catch (Exception exception){
            Util.handlePlaceholderError(view, getActivity());
        }

    }

    @Override
    public String uri() {
        return URI.EMPLOYEES;
    }



    @Override
    protected String getPagerTitle() {
        return App.getContext().getString(R.string.employees);
    }

    @Override
    protected int getPagerIcon() {
        return R.drawable.ico_empregados;
    }

    @Override
    protected HashMap<String, String> save() {
        return null;
    }

    @Override
    protected String getUri() {
        return URI.EMPLOYEES;
    }

    @Override
    protected int getTypeRequest() {
        return 0;
    }

    @Override
    protected void saved() {

    }

    @Override
    protected void saved(String json) {

    }

    @Override
    protected void setConfigPhotos(Uri mCurrentPhotoUri, String mCurrentPhotoPath) {

    }

    @Override
    public void onActivityResultFragment(int requestCode, int resultCode, Intent data) {

    }

    @Override
    public Uri getCurrentPhotoUri() {
        return null;
    }

    @Override
    public String getCurrentPhotoPath() {
        return null;
    }
}
