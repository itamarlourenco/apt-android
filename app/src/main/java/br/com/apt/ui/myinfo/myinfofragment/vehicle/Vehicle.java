package br.com.apt.ui.myinfo.myinfofragment.vehicle;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

import br.com.apt.R;
import br.com.apt.application.ModelObject;

/**
 * Created by fabriciooliveira on 9/28/16.
 */
public class Vehicle implements ModelObject, Parcelable {
    public static final String TYPE_MOTORCYCLE = "MOTORCYCLE";
    public static final String TYPE_CAR = "CAR";

    public static final int TYPE_MOTORCYCLE_NUMBER = 2;
    public static final int TYPE_CAR_NUMBER = 1;
    public static final int TYPE_NO_FILTER_NUMBER = 0;



    private int apartment_id;
    private CarBrand car_brand;
    private CarModel car_model;
    private int car_model_id;
    private CarYear car_year;
    private int car_year_id;
    private String color;
    private Date created_at;
    private int id;
    private String image;
    private String license_plate;
    private String type;

    public CarBrand getCar_brand() {
        return car_brand;
    }

    public CarModel getCar_model() {
        return car_model;
    }

    public CarYear getCar_year() {
        return car_year;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLicensePlate() {
        return license_plate;
    }

    public void setLicense_plate(String license_plate) {
        this.license_plate = license_plate;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getCarYearId() {
        return car_year_id;
    }

    public void setCar_year_id(int car_year_id) {
        this.car_year_id = car_year_id;
    }

    public CarYear getCarYear() {
        return car_year;
    }

    public void setCar_year(CarYear car_year) {
        this.car_year = car_year;
    }

    public int getCar_model_id() {
        return car_model_id;
    }

    public void setCar_model_id(int car_model_id) {
        this.car_model_id = car_model_id;
    }

    public CarModel getCarModel() {
        return car_model;
    }

    public void setCar_model(CarModel car_model) {
        this.car_model = car_model;
    }

    public CarBrand getCarBrand() {
        return car_brand;
    }

    public void setCar_brand(CarBrand car_brand) {
        this.car_brand = car_brand;
    }

    public int getApartment_id() {
        return apartment_id;
    }

    public void setApartment_id(int apartment_id) {
        this.apartment_id = apartment_id;
    }

    public static class CarBrand implements Parcelable {
        private int codigo_marca;
        private int id;
        private String marca;
        private int tipo;

        public int getCodigo_marca() {
            return codigo_marca;
        }

        public void setCodigo_marca(int codigo_marca) {
            this.codigo_marca = codigo_marca;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getMarca() {
            return marca;
        }

        public void setMarca(String marca) {
            this.marca = marca;
        }

        public int getTipo() {
            return tipo;
        }

        public void setTipo(int tipo) {
            this.tipo = tipo;
        }

        protected CarBrand(Parcel in) {
            codigo_marca = in.readInt();
            id = in.readInt();
            marca = in.readString();
            tipo = in.readInt();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(codigo_marca);
            dest.writeInt(id);
            dest.writeString(marca);
            dest.writeInt(tipo);
        }

        @SuppressWarnings("unused")
        public static final Parcelable.Creator<CarBrand> CREATOR = new Parcelable.Creator<CarBrand>() {
            @Override
            public CarBrand createFromParcel(Parcel in) {
                return new CarBrand(in);
            }

            @Override
            public CarBrand[] newArray(int size) {
                return new CarBrand[size];
            }
        };
    }

    public static class CarModel implements Parcelable {
        private String codigo_fipe;
        private int codigo_marca;
        private int codigo_modelo;
        private String modelo;

        public String getCodigo_fipe() {
            return codigo_fipe;
        }

        public void setCodigo_fipe(String codigo_fipe) {
            this.codigo_fipe = codigo_fipe;
        }

        public int getCodigo_marca() {
            return codigo_marca;
        }

        public void setCodigo_marca(int codigo_marca) {
            this.codigo_marca = codigo_marca;
        }

        public int getCodigoModelo() {
            return codigo_modelo;
        }

        public void setCodigo_modelo(int codigo_modelo) {
            this.codigo_modelo = codigo_modelo;
        }

        public String getModelo() {
            return modelo;
        }

        public void setModelo(String modelo) {
            this.modelo = modelo;
        }

        protected CarModel(Parcel in) {
            codigo_fipe = in.readString();
            codigo_marca = in.readInt();
            codigo_modelo = in.readInt();
            modelo = in.readString();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(codigo_fipe);
            dest.writeInt(codigo_marca);
            dest.writeInt(codigo_modelo);
            dest.writeString(modelo);
        }

        @SuppressWarnings("unused")
        public static final Parcelable.Creator<CarModel> CREATOR = new Parcelable.Creator<CarModel>() {
            @Override
            public CarModel createFromParcel(Parcel in) {
                return new CarModel(in);
            }

            @Override
            public CarModel[] newArray(int size) {
                return new CarModel[size];
            }
        };
    }

    public static class CarYear implements Parcelable {
        private String ano;
        private String codigo_fipe;
        private int codigo_modelo;
        private String combustivel;
        private int id_ano;
        private int valor;

        public String getAno() {
            return ano;
        }

        public void setAno(String ano) {
            this.ano = ano;
        }

        public String getCodigo_fipe() {
            return codigo_fipe;
        }

        public void setCodigo_fipe(String codigo_fipe) {
            this.codigo_fipe = codigo_fipe;
        }

        public int getCodigo_modelo() {
            return codigo_modelo;
        }

        public void setCodigo_modelo(int codigo_modelo) {
            this.codigo_modelo = codigo_modelo;
        }

        public String getCombustivel() {
            return combustivel;
        }

        public void setCombustivel(String combustivel) {
            this.combustivel = combustivel;
        }

        public int getId_ano() {
            return id_ano;
        }

        public void setId_ano(int id_ano) {
            this.id_ano = id_ano;
        }

        public int getValor() {
            return valor;
        }

        public void setValor(int valor) {
            this.valor = valor;
        }

        protected CarYear(Parcel in) {
            ano = in.readString();
            codigo_fipe = in.readString();
            codigo_modelo = in.readInt();
            combustivel = in.readString();
            id_ano = in.readInt();
            valor = in.readInt();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(ano);
            dest.writeString(codigo_fipe);
            dest.writeInt(codigo_modelo);
            dest.writeString(combustivel);
            dest.writeInt(id_ano);
            dest.writeInt(valor);
        }

        @SuppressWarnings("unused")
        public static final Parcelable.Creator<CarYear> CREATOR = new Parcelable.Creator<CarYear>() {
            @Override
            public CarYear createFromParcel(Parcel in) {
                return new CarYear(in);
            }

            @Override
            public CarYear[] newArray(int size) {
                return new CarYear[size];
            }
        };
    }

    protected Vehicle(Parcel in) {
        apartment_id = in.readInt();
        car_brand = (CarBrand) in.readValue(CarBrand.class.getClassLoader());
        car_model = (CarModel) in.readValue(CarModel.class.getClassLoader());
        car_model_id = in.readInt();
        car_year = (CarYear) in.readValue(CarYear.class.getClassLoader());
        car_year_id = in.readInt();
        color = in.readString();
        long tmpCreated_at = in.readLong();
        created_at = tmpCreated_at != -1 ? new Date(tmpCreated_at) : null;
        id = in.readInt();
        image = in.readString();
        license_plate = in.readString();
        type = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(apartment_id);
        dest.writeValue(car_brand);
        dest.writeValue(car_model);
        dest.writeInt(car_model_id);
        dest.writeValue(car_year);
        dest.writeInt(car_year_id);
        dest.writeString(color);
        dest.writeLong(created_at != null ? created_at.getTime() : -1L);
        dest.writeInt(id);
        dest.writeString(image);
        dest.writeString(license_plate);
        dest.writeString(type);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Vehicle> CREATOR = new Parcelable.Creator<Vehicle>() {
        @Override
        public Vehicle createFromParcel(Parcel in) {
            return new Vehicle(in);
        }

        @Override
        public Vehicle[] newArray(int size) {
            return new Vehicle[size];
        }
    };
}