package br.com.apt.ui.main.navigationDrawer;

import android.content.Intent;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.ui.MyAdm.MyAdmActivity;
import br.com.apt.ui.about.AboutActivity;
import br.com.apt.ui.consumption.ConsumptionActivity;
import br.com.apt.ui.lobby.LobbyActivity;
import br.com.apt.ui.myCondominium.MyCondominiumActivity;
import br.com.apt.ui.myapt.MyAptActivity;
import br.com.apt.ui.myinfo.MyInfoActivity;
import br.com.apt.ui.neighborhood.NeighborhoodActivity;
import br.com.apt.ui.reservations.ReservationsActivity;
import br.com.apt.ui.setting.SettingActivity;
import br.com.apt.ui.tickets.TicketActivity;
import br.com.apt.ui.trip.TripActivity;

public enum NavigationDrawerMenu {
    TOP{
        @Override
        public int icon() {
            return 0;
        }

        @Override
        public boolean show() {
            return true;
        }

        @Override
        public Intent intent() {
            return null;
        }

        @Override
        public int id() {
            return 0;
        }
    },
    SETTING{
        @Override
        public String toString() {
            return App.getContext().getString(R.string.myinfoActivityTitleUp);
        }

        @Override
        public int icon() {
            return R.drawable.ico_meusdados_empregados_homem_selected;
        }

        @Override
        public boolean show() {
            return true;
        }

        @Override
        public Intent intent() {
            return MyInfoActivity.newIntent(App.getContext());
        }

        @Override
        public int id() {
            return 0;
        }
    },
//    ABOUT{
//        @Override
//        public String toString() {
//            return App.getContext().getString(R.string.about);
//        }
//
//        @Override
//        public int icon() {
//            return R.drawable.ic_about;
//        }
//
//        @Override
//        public boolean show() {
//            return true;
//        }
//
//        @Override
//        public Intent intent() {
//            return AboutActivity.newIntent(App.getContext());
//        }
//
//        public int id(){
//            return 0;
//        }
//    },
    LOBBY{
        @Override
        public String toString() {
            return App.getContext().getString(R.string.lobby);
        }

        @Override
        public int icon() {
            return R.drawable.ico_home_portaria;
        }

        @Override
        public boolean show() {
            return true;
        }

        @Override
        public Intent intent() {
            return LobbyActivity.newIntent(App.getContext());
        }

        @Override
        public int id() {
            return R.id.relativeLobby;
        }
    },
    CONDOMINIUM{
        @Override
        public String toString() {
            return App.getContext().getString(R.string.condominium_home);
        }

        @Override
        public int icon() {
            return 0;//R.drawable.ico_home_condominio;
        }

        @Override
        public boolean show() {
            return true;
        }

        @Override
        public Intent intent() {
            return MyCondominiumActivity.newIntent(App.getContext());
        }

        @Override
        public int id() {
            return R.id.relativeCondominium;
        }
    },
//    CONSUMPTION{
//        @Override
//        public String toString() {
//            return App.getContext().getString(R.string.consumption);
//        }
//
//        @Override
//        public int icon() {
//            return R.drawable.ic_consumption;
//        }
//
//        @Override3
//        public boolean show() {
//            return true;
//        }
//
//        @Override
//        public Intent intent() {
//            return ConsumptionActivity.newIntent(App.getContext());
//        }
//
//        @Override
//        public int id() {
//            return R.id.relativeConsumption;
//        }
//    },
    RESERVATIONS{
        @Override
        public String toString() {
            return App.getContext().getString(R.string.reservations);
        }

        @Override
        public int icon() {
            return R.drawable.ico_home_reservas;
        }

        @Override
        public boolean show() {
            return true;
        }

        @Override
        public Intent intent() {
            return ReservationsActivity.newIntent(App.getContext());
        }

        @Override
        public int id() {
            return R.id.relativeReservations;
        }
    },
    MYAPT{
        @Override
        public String toString() {
            return App.getContext().getString(R.string.my_apt);
        }

        @Override
        public int icon() {
            return R.drawable.ic_apt;
        }

        @Override
        public boolean show() {
            return true;
        }

        @Override
        public Intent intent() {
            return MyAptActivity.newIntent(App.getContext());
        }

        @Override
        public int id() {
            return R.id.relativeMyApt;
        }
    },
    ADMINISTRATOR{
        @Override
        public String toString() {
            return App.getContext().getString(R.string.administrator);
        }

        @Override
        public int icon() {
            return R.drawable.ic_trip;
        }

        @Override
        public boolean show() {
            return true;
        }

        @Override
        public Intent intent() {
            return MyAdmActivity.newIntent(App.getContext());
        }

        @Override
        public int id() {
            return R.id.relativeAdministrator;
        }
    },
    TICKET{
        @Override
        public int icon() {
            return R.drawable.ico_home_notificacoes;
        }

        @Override
        public boolean show() {
            return true;
        }

        @Override
        public Intent intent() {
            return TicketActivity.newIntent(App.getContext(), true);
        }

        @Override
        public int id() {
            return R.id.relativeNotification;
        }
    },
    OUT{
        @Override
        public int icon() {
            return 0;
        }

        @Override
        public boolean show() {
            return true;
        }

        @Override
        public Intent intent() {
            return null;
        }

        @Override
        public int id() {
            return 0;
        }
    };

    public abstract int icon();
    public abstract boolean show();
    public abstract Intent intent();
    public abstract int id();
}