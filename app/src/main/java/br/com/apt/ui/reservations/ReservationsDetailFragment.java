package br.com.apt.ui.reservations;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.volley.Request;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.BaseFragment;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.model.user.User;
import br.com.apt.model.user.UserData;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomTextViewBold;
import br.com.apt.widget.CustomTextViewLight;
import br.com.apt.widget.SendSuccessDialog;

/**
 * Created by adminbs on 9/10/17.
 */

public class ReservationsDetailFragment extends BaseFragment {

    private Reservations reservations;
    private Installations installation;
    private CustomTextViewBold title;
    private CustomTextViewLight description;
    private CustomTextViewLight dateLabel;
    private CustomTextViewLight nameInstallations;
    private CustomTextViewBold nameCondominium;
    private CustomTextViewLight descriptionReservation;
    private CustomTextViewLight textLabelCalendar;
    private CustomTextViewLight times;
    private CustomTextViewLight labelShared;
    private ImageView iconShared;
    private ImageView icon;
    private ImageView iconTitle;
    private ImageView iconCalendar;
    private LinearLayout shared;
    private LinearLayout addCalendar;
    private String descriptionReservationCreateEventAndShared;
    private Date parseStart;
    private Date parseEnd;

    public static ReservationsDetailFragment newInstance(Reservations reservations){
        ReservationsDetailFragment reservationsDetailFragment = new ReservationsDetailFragment();
        reservationsDetailFragment.setReservations(reservations);
        return reservationsDetailFragment;
    }

    public void setReservations(Reservations reservations) {
        this.reservations = reservations;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_reservation_detail, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if (view != null) {
            title = (CustomTextViewBold) view.findViewById(R.id.title);
            description = (CustomTextViewLight) view.findViewById(R.id.description);
            nameCondominium = (CustomTextViewBold) view.findViewById(R.id.nameCondominium);
            descriptionReservation = (CustomTextViewLight) view.findViewById(R.id.descriptionReservation);
            nameInstallations = (CustomTextViewLight) view.findViewById(R.id.nameInstallations);
            dateLabel = (CustomTextViewLight) view.findViewById(R.id.dateLabel);
            textLabelCalendar = (CustomTextViewLight) view.findViewById(R.id.textLabelCalendar);
            times = (CustomTextViewLight) view.findViewById(R.id.times);
            icon = (ImageView) view.findViewById(R.id.icon);
            iconTitle = (ImageView) view.findViewById(R.id.iconTitle);
            labelShared = (CustomTextViewLight) view.findViewById(R.id.labelShared);
            addCalendar = (LinearLayout) view.findViewById(R.id.addCalendar);
            shared = (LinearLayout) view.findViewById(R.id.shared);
            iconShared = (ImageView) view.findViewById(R.id.iconShared);
            iconShared.setAlpha(.1f);
            iconCalendar = (ImageView) view.findViewById(R.id.iconCalendar);
            iconCalendar.setAlpha(.1f);



            Date currentDate = Calendar.getInstance().getTime();
            try {
                SimpleDateFormat simpleDateFormat = App.getSimpleDateFormat();
                Date parseEnd = simpleDateFormat.parse(reservations.getEnd());
                if(currentDate.compareTo(parseEnd) > 0){
                    view.findViewById(R.id.cancelReservation).setVisibility(View.GONE);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }


            installation = reservations.getInstallation();
            if (installation != null) {
                Picasso.with(getContext()).load(Installations.handleUrlIcon(installation.getIcon())).into(icon);
                handleStatusReservation();


                User user = UserData.getUser();
                if (user != null) {
                    br.com.apt.model.build.Build build = user.getBuild();
                    nameCondominium.setText(build.getName());
                }
                nameInstallations.setText(installation.getName());
                Reservations.Unit unit = reservations.getUnit();
                Reservations.Block block = reservations.getBlock();

                if(unit != null && block != null){
                    descriptionReservation.setText(Html.fromHtml(
                            "Reserva em nome de <font color='#298CAD'>"+reservations.getCreated_external_user_name()+"</font> apartamento <font color='#298CAD'>"+unit.getExternal_id()+" bloco "+block.getName()+"</font>"
                    ));
                }


                SimpleDateFormat simpleDateFormat = App.getSimpleDateFormat();
                try {
                    parseStart = simpleDateFormat.parse(reservations.getStart());
                    parseEnd = simpleDateFormat.parse(reservations.getEnd());
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(parseStart);

                    dateLabel.setText(
                            String.format(
                                    getString(R.string.reservation_label_day),
                                    Util.getDayWeek(calendar),
                                    String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)),
                                    Util.getMonthName(calendar.get(Calendar.MONTH)),
                                    String.valueOf(calendar.get(Calendar.YEAR))
                            )
                    );
                    textLabelCalendar.setText(String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)));
                    Date dateStart = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(reservations.getStart());
                    Date dateEnd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(reservations.getEnd());

                    times.setText(
                            String.format(getString(R.string.time_reservation_label), Util.timeFormatted(dateStart, "HH:mm").replace(":", "h"), Util.timeFormatted(dateEnd, "HH:mm").replace(":", "h"))
                    );
                    descriptionReservationCreateEventAndShared = "Olá,\n\n" +
                            "Reservei o espaço \""+nameInstallations.getText()+"\" para "+dateLabel.getText()+" " + times.getText().toString().toLowerCase() +".\n\n" +
                            "Você está convidado(a)!\n\n\n\n" +
                            "Enviado pelo MeuAPT (http://www.meuapt.com.br)";

                    view.findViewById(R.id.regulation).setOnClickListener(new View.OnClickListener(){
                        @Override
                        public void onClick(View v) {
                            getTerms();
                        }
                    });

                    view.findViewById(R.id.cancelReservation).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            SendSuccessDialog.show(getString(R.string.ask_remove_reservation), getString(R.string.ask_remove_reservation_description), getFragmentManager(), new SendSuccessDialog.Actions() {
                                @Override
                                public void back() {
                                    Volley volley = new Volley(new Volley.Callback() {
                                        @Override
                                        public void result(String result, int type) {
                                            if (result != null) {
                                                getActivity().finish();
                                            }
                                        }

                                        @Override
                                        public String uri() {
                                            return String.format(URI.RESERVATION_CANCEL, reservations.getId());
                                        }
                                    });
                                    volley.isReservation(true);
                                    volley.showDialog(getActivity());
                                    volley.request(Request.Method.POST, null);
                                }
                            }, SendSuccessDialog.TYPE_DYNAMIC_ERROR);

                        }
                    });

                    if (TextUtils.equals(reservations.getApproved(), Reservations.TYPE_NOT_APPROVED)) {
                        view.findViewById(R.id.cancelReservation).setVisibility(View.GONE);
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        readReservation();
    }

    private void readReservation(){
        Volley volley = new Volley(new Volley.Callback() {
            @Override
            public void result(String result, int type) {
                if(result != null){}
            }

            @Override
            public String uri() {
                return String.format(URI.READ_RESERVATION, reservations.getId());
            }
        });
        volley.isReservation(true);
        volley.request(Request.Method.POST, "");

    }

    private void getTerms() {
        Volley volley = new Volley(new Volley.Callback() {
            @Override
            public void result(String result, int type) {
                String title = "";
                String rule = "";
                if (result != null) {
                    RulesProceduresRequest rulesProceduresRequest = App.getGson().fromJson(result, RulesProceduresRequest.class);
                    List<RulesProcedures> object = rulesProceduresRequest.getObject();
                    StringBuilder ruleBuilder = new StringBuilder();
                    if (object != null && object.size() > 0) {
                        for(RulesProcedures rulesProcedures: object){
                            ruleBuilder.append(rulesProcedures.getText()).append("\n\n").append(rulesProcedures.getRule()).append("\n\n");
                        }

                        rule = ruleBuilder.toString();
                    }else{
                        rule = getString(R.string.without_term);
                    }
                }else{
                    rule = getString(R.string.without_term);
                }

                new AlertDialog.Builder(getContext())
                        .setTitle(title)
                        .setMessage(rule)
                        .setPositiveButton(getString(R.string.close), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                            }
                        })
                        .show();
            }

            @Override
            public String uri() {
                return String.format(URI.RULE_PROCEDURE, reservations.getInstallation().getId());
            }
        });
        volley.isReservation(true);
        volley.showDialog(getActivity());
        volley.request();
    }

    private void handleStatusReservation() {
        if(TextUtils.equals(reservations.getApproved(), Reservations.TYPE_CREATED)){
            title.setText("AGUARDANDO CONFIRMAÇÃO");
            title.setTextColor(Color.parseColor("#c98f33"));
            description.setText(R.string.reservation_description_created);
            iconTitle.setVisibility(View.VISIBLE);
        }

        if(TextUtils.equals(reservations.getApproved(), Reservations.TYPE_APPROVED)){
            title.setText("RESERVA CONFIRMADA");
            title.setTextColor(Color.parseColor("#2583ad"));
            description.setText(R.string.reservation_description_approved);
            iconTitle.setVisibility(View.VISIBLE);
            iconTitle.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.reservation_approved));

            iconShared.setAlpha(1f);
            iconCalendar.setAlpha(1f);

            shared.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.setType("text/plain");
                    shareIntent.putExtra(Intent.EXTRA_TEXT, descriptionReservationCreateEventAndShared);
                    shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Reserva MeuAPT - "+ nameInstallations.getText() );
                    startActivity(Intent.createChooser(shareIntent, "Compartilhar..."));
                }
            });

            addCalendar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_INSERT)
                            .setData(CalendarContract.Events.CONTENT_URI)
                            .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, parseStart.getTime())
                            .putExtra(CalendarContract.EXTRA_EVENT_END_TIME, parseEnd.getTime())
                            .putExtra(CalendarContract.Events.TITLE, "Reserva MeuAPT - "+ nameInstallations.getText())
                            .putExtra(CalendarContract.Events.DESCRIPTION, descriptionReservationCreateEventAndShared)
                            .putExtra(CalendarContract.Events.AVAILABILITY, CalendarContract.Events.AVAILABILITY_BUSY);
                    startActivity(intent);
                }
            });
        }

        if(TextUtils.equals(reservations.getApproved(), Reservations.TYPE_NOT_APPROVED)){
            title.setText("RESERVA RECUSADA");
            title.setTextColor(Color.parseColor("#9e5954"));
            description.setText(String.format(getString(R.string.reservation_not_reproved), reservations.getReason()));
            iconTitle.setVisibility(View.VISIBLE);
            iconTitle.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.reservation_not_approved));
        }
    }
}