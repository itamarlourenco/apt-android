package br.com.apt.ui.lobby;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import br.com.apt.application.BaseActivity;
import br.com.apt.ui.reservations.CalendarFragment;

public class LobbyActivity extends BaseActivity {

    public static Intent newIntent(Context context) {
        return new Intent(context, LobbyActivity.class);
    }

    public static Intent newIntent(Context context, int position) {
        return intentWithPosition(new Intent(context, LobbyActivity.class), position);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public Fragment getFragment(){
        return LobbyFragment.newInstance(getPositionFragment());
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        LobbyFragment lobbyFragment = (LobbyFragment) getFragment();
        lobbyFragment.onActivityResultFragment(requestCode, resultCode, data);
    }
}