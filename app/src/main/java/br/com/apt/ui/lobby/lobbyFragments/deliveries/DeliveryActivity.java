package br.com.apt.ui.lobby.lobbyFragments.deliveries;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import br.com.apt.application.BaseActivity;
import br.com.apt.ui.reservations.CalendarFragment;

/**
 * Created by itamarlourenco on 10/01/16.
 */
public class DeliveryActivity extends BaseActivity {

    private static final String EXTRA_DELIVERY = "EXTRA_DELIVERY";

    public static Intent newIntent(Context context, Delivery delivery) {
        Intent intent = new Intent(context, DeliveryActivity.class);
        intent.putExtra(EXTRA_DELIVERY, delivery);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public Fragment getFragment(){
        Intent intent = getIntent();
        if(intent != null){
            Delivery delivery =  intent.getParcelableExtra(EXTRA_DELIVERY);
            return DeliveryFragment.newInstance(delivery);
        }

        return null;
    }
}
