package br.com.apt.ui.forgotPassword;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import br.com.apt.application.BaseActivity;
import br.com.apt.ui.reservations.CalendarFragment;

public class ForgotPasswordActivity extends BaseActivity {

    public static Intent newIntent(Context context) {
        return new Intent(context, ForgotPasswordActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public Fragment getFragment() {
        return ForgotPasswordFragment.newInstance();
    }

    @Override
    protected boolean showNavigationDrawer() {
        return false;
    }

    @Override
    protected boolean showToolbarLogo() {
        return true;
    }

    @Override
    protected boolean checkLogin() {
        return false;
    }
}