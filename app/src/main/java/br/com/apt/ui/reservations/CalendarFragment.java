package br.com.apt.ui.reservations;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.squareup.picasso.Picasso;

import java.util.Calendar;

import br.com.apt.R;
import br.com.apt.application.BaseFragment;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomTextViewLight;
import br.com.apt.widget.calendarView.MaterialCalendarViewCustom;

/**
 * Created by adminbs on 9/6/17.
 */

public class CalendarFragment extends BaseFragment implements OnDateSelectedListener {

    private MaterialCalendarViewCustom calendarView;
    private Installations installations;
    private ImageView icon;
    private CustomTextViewLight installationName;

    public static CalendarFragment newInstance(Installations installations){
        CalendarFragment calendarFragment = new CalendarFragment();
        calendarFragment.setInstallations(installations);
        return calendarFragment;
    }

    public void setInstallations(Installations installations) {
        this.installations = installations;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_calendar, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if(view != null){
            calendarView = (MaterialCalendarViewCustom) view.findViewById(R.id.calendarView);
            calendarView.setSelectionMode(MaterialCalendarView.SELECTION_MODE_SINGLE);
            calendarView.setOnDateChangedListener(this);
            calendarView.addDecorators(new DecoratorCalendar.AllDecoratorDays(getContext()));
            calendarView.addDecorators(new DecoratorCalendar.ClickDecoratorDay(getContext()));
            installations.setMinimum_day_reservation(installations.getMinimum_day_reservation() -1);
            installations.setMaximum_day_reservation(installations.getMaximum_day_reservation() -1);
            calendarView.addDecorators(new DecoratorCalendar.DisabledDays(getContext(), installations));
            calendarView.setTitleMonths(R.array.months_uppercase);
            icon = (ImageView) view.findViewById(R.id.icon);
            installationName = (CustomTextViewLight) view.findViewById(R.id.installation_name);

            if(installations != null){
                installationName.setText(installations.getName());
                Picasso.with(getContext()).load(Installations.handleUrlIcon(installations.getIcon())).into(icon);
            }

        }
    }

    @Override
    public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
        Calendar calendar = Calendar.getInstance();
        Calendar dayCalendar = date.getCalendar();

        if(calendar.compareTo(dayCalendar) < 0){
            calendarView.addDecorators(new DecoratorCalendar.ClickDecoratorTextviewColor(date));
            calendarView.addDecorators(new DecoratorCalendar.AllDecoratorDays(getContext()));
            calendarView.addDecorators(new DecoratorCalendar.DisabledDays(getContext(), installations));

            startActivity(TimeListActivity.newIntent(getContext(), installations, date));
            getActivity().overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
        }
    }
}

