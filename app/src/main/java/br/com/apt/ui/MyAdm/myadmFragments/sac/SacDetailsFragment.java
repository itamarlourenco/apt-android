package br.com.apt.ui.MyAdm.myadmFragments.sac;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.android.volley.Request;

import java.util.List;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.BaseFragment;
import br.com.apt.application.BaseGsonMeuAdm;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.model.NotificationHandle;
import br.com.apt.ui.MyAdm.Occurrences;
import br.com.apt.util.Logger;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomEditTextLight;
import br.com.apt.widget.CustomTextViewLight;
import br.com.apt.widget.SendSuccessDialog;

/**
 * Created by adminbs on 8/4/17.
 */

public class SacDetailsFragment extends BaseFragment implements Volley.Callback {

    private Occurrences occurrence;
    private ListView listView;
    private List<History> historyList;

    private CustomTextViewLight opening;
    private CustomTextViewLight subject;
    private CustomTextViewLight theme;
    private CustomTextViewLight title;
    private CustomTextViewLight showMore;
    private CustomTextViewLight status;

    private CustomEditTextLight message;
    private LinearLayout baseComment;

    private ImageButton sendComment;

    public static SacDetailsFragment newInstance() {
        return new SacDetailsFragment();
    }

    public void setOccurrence(Occurrences occurrence) {
        this.occurrence = occurrence;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sac_details, container, false);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if (view != null) {
            listView = (ListView) view.findViewById(R.id.listView);

            opening = (CustomTextViewLight) view.findViewById(R.id.opening);
            subject = (CustomTextViewLight) view.findViewById(R.id.subject);
            theme = (CustomTextViewLight) view.findViewById(R.id.theme);
            status = (CustomTextViewLight) view.findViewById(R.id.status);
            title = (CustomTextViewLight) view.findViewById(R.id.title);
            showMore = (CustomTextViewLight) view.findViewById(R.id.showMore);
            message = (CustomEditTextLight) view.findViewById(R.id.message);
            sendComment = (ImageButton) view.findViewById(R.id.sendComment);

            baseComment = (LinearLayout) view.findViewById(R.id.baseComment);

            opening.setText(
                    new StringBuffer(Util.dateFormatted(occurrence.getCreatedAt()))
                            .append(" às ")
                            .append(Util.timeFormatted(occurrence.getCreatedAt()))
            );

            title.setText(occurrence.getTitle());
            showMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showMore();
                }
            });

            Occurrences.Subject subject = occurrence.getSubject();
            if (subject != null) {
                this.subject.setText(subject.getName());
            }
            Occurrences.Theme theme = occurrence.getTheme();
            if (theme != null) {
                this.theme.setText(theme.getName());
            }

            String status = occurrence.getStatus();

            if(!TextUtils.isEmpty(status)) {
                if(status.contains("Andamento")){
                    this.status.setText("ANDAMENTO");
                    this.status.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.border_radius_full_green));
                    this.status.setTextColor(ContextCompat.getColor(getContext(), R.color.sac_andamento_text_color));
                }else if(status.contains("Finalizado")){
                    this.status.setText("FINALIZADO");
                    this.status.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.border_radius_blue));
                    this.status.setTextColor(ContextCompat.getColor(getContext(), R.color.sac_andamento_text_color));
                }else{
                    this.status.setText("ABERTO");
                    this.status.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.border_radius_yellow));
                    this.status.setTextColor(ContextCompat.getColor(getContext(), R.color.sac_andamento_text_color));
                }
            }

            sendComment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sendComment();
                }
            });

            getComments();
            readSac();
        }
    }

    private void readSac() {
        Volley volley = new Volley(new Volley.Callback() {
            @Override
            public void result(String result, int type) {
                if(result != null){
                    NotificationHandle.SAC = NotificationHandle.SAC-1;
                }
            }

            @Override
            public String uri() {
                return String.format(URI.ACKNOWLEDGED, occurrence.getId());
            }
        });
        volley.isMeuADM(true);
        volley.request(Request.Method.POST, "");
    }

    private void showMore() {
        DialogSacDetails.show(getFragmentManager(), occurrence);
    }

    public void getComments() {
        Volley volley = new Volley(this);
        volley.isMeuADM(true);
        volley.showDialog(getActivity());
        volley.request();
    }

    private void sendComment() {
        if(TextUtils.isEmpty(message.getText())){
            SendSuccessDialog.show(getString(R.string.error_comment), null, getFragmentManager(), null, SendSuccessDialog.TYPE_ERROR);
            return;
        }

        sendComment.setVisibility(View.GONE);
        SacComment comment = new SacComment(message.getText().toString(), true, true);
        message.setText("");

        Volley volley = new Volley(new Volley.Callback() {
            @Override
            public void result(String result, int type) {
                try{
                    BaseGsonMeuAdm baseGsonMeuAdm = App.getGson().fromJson(result, BaseGsonMeuAdm.class);
                    if(baseGsonMeuAdm.getCode() == Volley.STATUS_OK){
                        getComments();
                        sendComment.setVisibility(View.VISIBLE);
                    }else{
                        SendSuccessDialog.show(baseGsonMeuAdm.getMessage(), null, getFragmentManager(), new SendSuccessDialog.Actions() {
                            @Override
                            public void back() {
                                getActivity().finish();
                                sendComment.setVisibility(View.VISIBLE);
                            }
                        }, SendSuccessDialog.TYPE_ERROR);
                    }
                }catch (Exception e){
                    SendSuccessDialog.show(getString(R.string.error_generic), null, getFragmentManager(), null, SendSuccessDialog.TYPE_DYNAMIC_ERROR);
                }
            }

            @Override
            public String uri() {
                return String.format(URI.OCCURRENCES_COMMENT, occurrence.getId());
            }
        });
        volley.isMeuADM(true);
        volley.request(Request.Method.PUT, comment.toJson());
    }

    @Override
    public void result(String result, int type) {
        try{
            HistoryRequest historyRequest = App.getGson().fromJson(result, HistoryRequest.class);
            historyList = historyRequest.getObject();
            listView.setAdapter(new HistoryAdapter(getContext(), historyList));
        }catch (Exception e){
            SendSuccessDialog.show(getString(R.string.error_generic), null, getFragmentManager(), new SendSuccessDialog.Actions() {
                @Override
                public void back() {
                    getActivity().finish();
                }
            }, SendSuccessDialog.TYPE_ERROR);
        }
    }

    @Override
    public String uri() {
        return String.format(URI.OCCURRENCES_HISTORY, occurrence.getId());
    }

}
