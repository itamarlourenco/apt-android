package br.com.apt.ui.myinfo.myinfofragment.resident;

import java.util.List;

import br.com.apt.application.BaseGson;
import br.com.apt.ui.myapt.myaptFragments.notice.Notice;
import br.com.apt.ui.myinfo.myinfofragment.NewUser;

/**
 * Created by adminbs on 11/3/16.
 */

public class ResidentRequest extends BaseGson {
    public List<NewUser> Object;

    public List<NewUser> getObject() {
        return Object;
    }
}

