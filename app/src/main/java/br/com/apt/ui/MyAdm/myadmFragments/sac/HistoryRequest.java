package br.com.apt.ui.MyAdm.myadmFragments.sac;

import java.util.List;

import br.com.apt.application.BaseGsonMeuAdm;
import br.com.apt.ui.MyAdm.myadmFragments.Billet.Quotas;
import br.com.apt.ui.MyAdm.myadmFragments.sac.occurrence.OccurrenceSubject;

/**
 * Created by adminbs on 8/4/17.
 */

class HistoryRequest extends BaseGsonMeuAdm {
    private List<History> object;

    public List<History> getObject() {
        return object;
    }

    public void setObject(List<History> object) {
        this.object = object;
    }
}
