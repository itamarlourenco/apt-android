package br.com.apt.ui.myCondominium.myCondominiumFragments.EdictsAndMinutes;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.Request;
import com.github.ybq.android.spinkit.SpinKitView;

import java.util.ArrayList;
import java.util.List;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.OpenURL;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.model.user.User;
import br.com.apt.ui.lobby.lobbyFragments.deliveries.DeliveriesListFragment;
import br.com.apt.ui.lobby.lobbyFragments.deliveries.DeliveriesListViewAdapter;
import br.com.apt.ui.lobby.lobbyFragments.deliveries.DeliveryActivity;
import br.com.apt.ui.myCondominium.OpenPDF;
import br.com.apt.ui.myCondominium.myCondominiumFragments.Attachment;
import br.com.apt.ui.myCondominium.myCondominiumFragments.infoCondominium.Condominium;
import br.com.apt.ui.myCondominium.myCondominiumFragments.infoCondominium.CondominiumRequest;
import br.com.apt.widget.PagerItemFragment;
import br.com.apt.widget.SendSuccessDialog;

/**
 * Created by adminbs on 9/3/17.
 */

public class EdictsAndMinutesFragment extends PagerItemFragment {
    private User user = getUser();

    private SpinKitView loader;
    private ListView listView;
    private List<Attachment> attachments = new ArrayList<>();

    @Override
    protected String getPagerTitle() {
        return App.getContext().getString(R.string.edicts_ans_minutes);
    }

    public void callback(final String url) {
        Volley volley = new Volley(new Volley.Callback() {
            @Override
            public void result(String result, int type) {
                try{
                    EdictsAndMinutesRequest internalRules =  App.getGson().fromJson(result, EdictsAndMinutesRequest.class);
                    if (internalRules != null) {
                        List<Attachment> attachments = internalRules.getObject();
                        EdictsAndMinutesFragment.this.attachments.addAll(attachments);
                        listView.setAdapter(new EdictsAndMinutesAdapter(getContext(), EdictsAndMinutesFragment.this.attachments));
                    }
                }catch (Exception e){
                    SendSuccessDialog.show(getString(R.string.error_generic), e.getMessage(), getFragmentManager(), new SendSuccessDialog.Actions() {
                        @Override
                        public void back() {
                        }
                    }, SendSuccessDialog.TYPE_ERROR);
                }
            }

            @Override
            public String uri() {
                return String.format(url, user.getCondominiumId());
            }
        });
        volley.isMeuADM(true);
        volley.showDialog(getActivity());
        volley.request();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if (view != null) {
            loader = (SpinKitView) view.findViewById(R.id.loader);
            listView = (ListView) view.findViewById(R.id.listView);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Attachment attachment = attachments.get(position);
                    startActivity(OpenURL.newIntent(getContext(), "https://drive.google.com/viewerng/viewer?embedded=true&url="+ attachment.getUrl()));
                }
            });
        }
        callback(URI.MINUTE_URL);
        callback(URI.EDITAL_URL);
        loader.setVisibility(View.INVISIBLE);
    }

    @Override
    protected int getPagerIcon() {
        return R.drawable.ico_menu_editais;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.edicts_minutes_fragment, container, false);
    }
}