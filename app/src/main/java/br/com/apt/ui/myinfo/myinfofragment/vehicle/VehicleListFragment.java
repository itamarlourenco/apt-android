package br.com.apt.ui.myinfo.myinfofragment.vehicle;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.Request;
import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.github.ybq.android.spinkit.SpinKitView;

import java.util.HashMap;
import java.util.List;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.ui.ExceptionWithoutResult;
import br.com.apt.ui.myinfo.myinfofragment.FragmentMyInfoAddImage;
import br.com.apt.ui.myinfo.myinfofragment.pet.Pet;
import br.com.apt.util.Util;
import br.com.apt.widget.PagerItemFragment;

/**
 * Created by fabriciooliveira on 9/28/16.
 */
public class VehicleListFragment extends FragmentMyInfoAddImage implements Volley.Callback {

    private SwipeMenuListView listView;
    private List<Vehicle> vehiclesList;
    private SpinKitView loader;

    public static PagerItemFragment newInstance(){
        return new VehicleListFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_vehicle, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if (view != null) {
            loader = (SpinKitView) view.findViewById(R.id.loader);
            listView = (SwipeMenuListView) view.findViewById(R.id.listView);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    startActivity(VehicleAddActivity.newIntent(getContext(), vehiclesList.get(position)));
                }
            });

            listView.setMenuCreator(new SwipeMenuCreator() {
                @Override
                public void create(SwipeMenu menu) {
                    SwipeMenuItem openItem = new SwipeMenuItem(getContext());
                    openItem.setBackground(new ColorDrawable(Color.rgb(0xF9, 0x3F, 0x25)));
                    openItem.setWidth(120);
                    openItem.setTitle("X");
                    openItem.setTitleSize(18);
                    openItem.setTitleColor(Color.BLACK);
                    menu.addMenuItem(openItem);
                }
            });

            listView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                    switch (index) {
                        case 0:
                            Vehicle vehicle = vehiclesList.get(position);
                            removeItem(vehicle.getId(), new CallbackDelete() {
                                @Override
                                public void delete() {
                                    getVehicles();
                                }
                            });
                            break;
                    }
                    return false;
                }
            });

            view.findViewById(R.id.newVehicle).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(VehicleAddActivity.newIntent(getContext()));
                    getActivity().overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
                }
            });
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getVehicles();
    }

    public void getVehicles() {
        Volley volley = new Volley(this);
        volley.request(Request.Method.GET, null);
    }

    @Override
    public void onActivityResultFragment(int requestCode, int resultCode, Intent data) {

    }

    @Override
    public void result(String result, int type) {
        View view = getView();
        try{
            Util.hidePlaceholder(view);
            Util.changeVisibility(loader, View.GONE, true);
            Util.changeVisibility(listView, View.VISIBLE, true);
            VehicleRequest vehicleRequest = App.getGson().fromJson(result, VehicleRequest.class);
            vehiclesList = vehicleRequest.getObject();
            listView.setAdapter(new VehicleAdapter(getContext(), vehiclesList));
            if(vehiclesList == null){
                throw new ExceptionWithoutResult(getString(R.string.message_without_vehicle), R.drawable.img_veiculos_no_have);
            }
        }catch (ExceptionWithoutResult exceptionWithoutResult){
            Util.showPlaceholder(view, getActivity(), exceptionWithoutResult.getDrawableId(), exceptionWithoutResult.getMessage());
        }catch (Exception exception){
            Util.handlePlaceholderError(view, getActivity());
        }
    }

    @Override
    public String uri() {
        return URI.VEHICLE;
    }

    @Override
    protected String getPagerTitle() {
        return App.getContext().getString(R.string.vehicle);
    }

    @Override
    protected int getPagerIcon() {
        return R.drawable.ico_veiculos;
    }

    @Override
    protected HashMap<String, String> save() {
        return null;
    }

    @Override
    protected String getUri() {
        return URI.VEHICLE;
    }

    @Override
    protected int getTypeRequest() {
        return Request.Method.POST;
    }

    @Override
    protected void saved() {
    }

    @Override
    protected void saved(String json) {

    }

    @Override
    protected void setConfigPhotos(Uri mCurrentPhotoUri, String mCurrentPhotoPath) {
    }

    @Override
    public Uri getCurrentPhotoUri() {
        return null;
    }

    @Override
    public String getCurrentPhotoPath() {
        return null;
    }
}
