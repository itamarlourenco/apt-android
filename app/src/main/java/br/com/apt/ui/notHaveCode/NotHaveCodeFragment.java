package br.com.apt.ui.notHaveCode;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;

import java.lang.reflect.Method;

import br.com.apt.BuildConfig;
import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.BaseFragment;
import br.com.apt.application.BaseGson;
import br.com.apt.application.SendRequest;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.ui.reservations.Installations;
import br.com.apt.util.Logger;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomEditText;

public class NotHaveCodeFragment extends BaseFragment implements View.OnClickListener{

    private CustomEditText editTextName;
    private CustomEditText editTextEmail;
    private CustomEditText editTextZipcode;
    private CustomEditText editTextNumberBuilding;

    public static NotHaveCodeFragment newInstance() {
        return new NotHaveCodeFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.not_have_code_fragment, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if(view != null){
            view.findViewById(R.id.back).setOnClickListener(this);
            view.findViewById(R.id.submit).setOnClickListener(this);

            editTextName = (CustomEditText) view.findViewById(R.id.name);
            editTextEmail = (CustomEditText) view.findViewById(R.id.email);
            editTextZipcode = (CustomEditText) view.findViewById(R.id.zipcode);
            editTextNumberBuilding = (CustomEditText) view.findViewById(R.id.numberBuilding);

            if(BuildConfig.DEBUG){
                editTextName.setText("Itamar");
                editTextEmail.setText("itamar.developer@gmail.com");
                editTextZipcode.setText("03451000");
                editTextNumberBuilding.setText("19");
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.back:
                closeActivity();
                break;

            case R.id.submit:
                submitNewBuilding();
                break;
        }
    }

    private void closeActivity() {
        getActivity().finish();
    }

    private void submitNewBuilding(){
        Volley volley = new Volley(new Volley.Callback() {
            @Override
            public void result(String result, int type) {
                if(result != null){
                    BaseGson baseGson = App.getGson().fromJson(result, BaseGson.class);
                    if(baseGson != null){
                        Util.showDialog(getActivity(), baseGson.getMessage(), new DialogInterface.OnClickListener(){
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getActivity().finish();
                            }
                        });
                    }
                }
            }

            @Override
            public String uri() {
                return URI.CONTACT;
            }
        });

        ContactSend contactSend = new ContactSend();
        contactSend.setName(editTextName.getText().toString());
        contactSend.setEmail(editTextEmail.getText().toString());
        contactSend.setNumber(editTextNumberBuilding.getText().toString());
        contactSend.setZipcode(editTextZipcode.getText().toString());

        volley.showDialog(getActivity());
        volley.request(Request.Method.POST, contactSend.toJson());
    }

    public class ContactSend extends SendRequest{
        private String name;
        private String zipcode;
        private String number;
        private String email;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getZipcode() {
            return zipcode;
        }

        public void setZipcode(String zipcode) {
            this.zipcode = zipcode;
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }
    }
}
