package br.com.apt.ui.consumption;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import br.com.apt.application.ModelObject;

/**
 * Created by itamarlourenco on 16/01/16.
 */
public class Consumption implements ModelObject {
    public static final String TYPE_WATER = "Água";
    public static final String TYPE_GAS = "Gás";
    public static final String TYPE_ENERGY = "Luz";


    @SerializedName("consumoTotal")
    private float consumptionTotal;

    @SerializedName("consumoUsuario")
    private float consumptionUser;

    @SerializedName("porcentagem")
    private float percent;

    @SerializedName("dataTotal")
    private String currentConsumptionTotal;

    @SerializedName("dataUsuario")
    private String currentConsumptionUser;

    public Consumption(){

    }

    public float getConsumptionTotal() {
        return consumptionTotal;
    }

    public void setConsumptionTotal(float consumptionTotal) {
        this.consumptionTotal = consumptionTotal;
    }

    public float getConsumptionUser() {
        return consumptionUser;
    }

    public void setConsumptionUser(float consumptionUser) {
        this.consumptionUser = consumptionUser;
    }

    public float getPercent() {
        return percent;
    }

    public void setPercent(float percent) {
        this.percent = percent;
    }

    public String getCurrentConsumptionTotal() {
        return currentConsumptionTotal;
    }

    public void setCurrentConsumptionTotal(String currentConsumptionTotal) {
        this.currentConsumptionTotal = currentConsumptionTotal;
    }

    public String getCurrentConsumptionUser() {
        return currentConsumptionUser;
    }

    public void setCurrentConsumptionUser(String currentConsumptionUser) {
        this.currentConsumptionUser = currentConsumptionUser;
    }

    public Consumption(float consumptionTotal, float consumptionUser, float percent, String currentConsumptionTotal, String currentConsumptionUser) {
        this.consumptionTotal = consumptionTotal;
        this.consumptionUser = consumptionUser;
        this.percent = percent;
        this.currentConsumptionTotal = currentConsumptionTotal;
        this.currentConsumptionUser = currentConsumptionUser;
    }

    protected Consumption(Parcel in) {
        consumptionTotal = in.readFloat();
        consumptionUser = in.readFloat();
        percent = in.readFloat();
        currentConsumptionUser = in.readString();
        currentConsumptionTotal = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeFloat(consumptionTotal);
        dest.writeFloat(consumptionUser);
        dest.writeFloat(percent);
        dest.writeString(currentConsumptionUser);
        dest.writeString(currentConsumptionTotal);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Consumption> CREATOR = new Parcelable.Creator<Consumption>() {
        @Override
        public Consumption createFromParcel(Parcel in) {
            return new Consumption(in);
        }

        @Override
        public Consumption[] newArray(int size) {
            return new Consumption[size];
        }
    };
}