package br.com.apt.ui.reservations;

import android.content.Context;
import android.media.Image;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;

import br.com.apt.R;
import br.com.apt.ui.MyAdm.myadmFragments.Billet.BilletDetailAdapter;
import br.com.apt.ui.MyAdm.myadmFragments.Billet.BilletDetails;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomTextViewLight;

/**
 * Created by adminbs on 9/7/17.
 */

public class TimeListAdapter extends BaseAdapter {

    private Context context;
    private List<Times> timesList;

    public TimeListAdapter(Context context, List<Times> timesList) {
        this.context = context;
        this.timesList = timesList;
    }

    @Override
    public int getCount() {
        return timesList == null ? 0 : timesList.size();
    }

    @Override
    public Times getItem(int position) {
        return timesList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        TimeListAdapter.ViewHolder holder;

        if(convertView == null){
            view = LayoutInflater.from(context).inflate(R.layout.row_timer, parent, false);
            holder = new TimeListAdapter.ViewHolder(view);
            view.setTag(holder);
        }else{
            view = convertView;
            holder = (TimeListAdapter.ViewHolder) view.getTag();
        }

        bindList(holder, position);
        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private void bindList(TimeListAdapter.ViewHolder holder, int position) {
        Times item = getItem(position);
        holder.textView.setText(item.getTime());
        holder.radio.setChecked(item.isSelected());

//        if(item.isSelected()){
//            holder.viewDetail.setVisibility(View.VISIBLE);
//            holder.textView.setTextColor(ContextCompat.getColor(context, R.color.faded_blue));
//            holder.baseBackground.setBackground(ContextCompat.getDrawable(context, R.drawable.button_menu_list_dark));
//        }else{
//            holder.viewDetail.setVisibility(View.GONE);
//            holder.textView.setTextColor(ContextCompat.getColor(context, R.color.time_list_color));
//            holder.baseBackground.setBackground(ContextCompat.getDrawable(context, R.drawable.button_menu_list_dark_selected));
//        }
//
//        if(item.isFirst()){
//            holder.imageFirst.setVisibility(View.VISIBLE);
//            holder.viewDetailFirst.setVisibility(View.VISIBLE);
//            holder.viewDetail.setVisibility(View.GONE);
//        }else{
//            holder.imageFirst.setVisibility(View.GONE);
//            holder.viewDetailFirst.setVisibility(View.GONE);
//        }
//
//        if(item.isLast()){
//            holder.imageLast.setVisibility(View.VISIBLE);
//            holder.viewDetailLast.setVisibility(View.VISIBLE);
//            holder.viewDetail.setVisibility(View.GONE);
//        }else{
//            holder.imageLast.setVisibility(View.GONE);
//            holder.viewDetailLast.setVisibility(View.GONE);
//        }
//
        if(item.isDisabled()){
            holder.radio.setEnabled(false);
            holder.textView.setTextColor(ContextCompat.getColor(context, R.color.disabled_time));
            holder.label.setVisibility(View.VISIBLE);
        }else{
            holder.radio.setEnabled(true);
            holder.textView.setTextColor(ContextCompat.getColor(context, R.color.time_list_color));
            holder.label.setVisibility(View.GONE);
        }

    }

    public class ViewHolder{
        private CustomTextViewLight textView;
        private CustomTextViewLight label;
        private RadioButton radio;
        private View viewDetail;
        private View viewDetailLast;
        private View viewDetailFirst;
        private ImageView imageLast;
        private ImageView imageFirst;
        private RelativeLayout baseBackground;

        public ViewHolder(View view) {
            textView = (CustomTextViewLight) view.findViewById(R.id.textView);
            label = (CustomTextViewLight) view.findViewById(R.id.label);
            radio = (RadioButton) view.findViewById(R.id.radio);
            imageLast = (ImageView) view.findViewById(R.id.imageLast);
            imageFirst = (ImageView) view.findViewById(R.id.imageFirst);
            viewDetail = view.findViewById(R.id.viewDetail);
            viewDetailLast = view.findViewById(R.id.viewDetailLast);
            viewDetailFirst = view.findViewById(R.id.viewDetailFirst);
            baseBackground = (RelativeLayout) view.findViewById(R.id.baseBackground);
        }
    }
}
