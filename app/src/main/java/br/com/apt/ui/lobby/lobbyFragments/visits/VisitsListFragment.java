package br.com.apt.ui.lobby.lobbyFragments.visits;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.Request;
import com.github.ybq.android.spinkit.SpinKitView;

import java.util.List;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.BaseFragment;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.model.user.User;
import br.com.apt.ui.ExceptionWithoutResult;
import br.com.apt.ui.lobby.lobbyFragments.deliveries.DeliveriesFragment;
import br.com.apt.ui.lobby.lobbyFragments.deliveries.DeliveriesListFragment;
import br.com.apt.ui.lobby.lobbyFragments.deliveries.DeliveriesListViewAdapter;
import br.com.apt.ui.lobby.lobbyFragments.deliveries.Delivery;
import br.com.apt.ui.lobby.lobbyFragments.deliveries.DeliveryFragment;
import br.com.apt.ui.lobby.lobbyFragments.deliveries.DeliveryRequest;
import br.com.apt.util.Logger;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomButton;
import br.com.apt.widget.PagerItemFragment;

/**
 * Created by itamarlourenco on 09/07/17.
 */

public class VisitsListFragment extends BaseFragment implements Volley.Callback {

    private VisitsFragment parent;
    private SpinKitView loader;
    private ListView listView;
    private List<Visit> visits;
    private CustomButton scheduleVisitorButton;
    private VisitsScheduleFragment visitsScheduleFragment = VisitsScheduleFragment.newInstance();
    private static final int REQUEST_VISITS = 0;

    public void setParent(VisitsFragment parent) {
        this.parent = parent;
    }

    public static VisitsListFragment newInstance(VisitsFragment parent) {
        VisitsListFragment visitsListFragment = new VisitsListFragment();
        visitsListFragment.setParent(parent);
        return visitsListFragment;
    }

    @Override
    public String uri() {
        return URI.VISITS;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list_visits, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if (view != null) {
            loader = (SpinKitView) view.findViewById(R.id.loader);
            scheduleVisitorButton = (CustomButton) view.findViewById(R.id.scheduleVisitorButton);
            listView = (ListView) view.findViewById(R.id.listView);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if(VisitsListFragment.this.parent != null){
                        startActivity(VisitsDetailsActivity.newIntent(getContext(), visits.get(position)));
                    }
                }
            });
            scheduleVisitorButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(VisitsScheduleActivity.newIntent(getContext()));
                    getActivity().overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
                }
            });

            requestVisits();
        }
    }

    private void requestVisits() {
        User user = getUser();
        if (user != null) {
            Volley volley = new Volley(this);
            VisitSend visitSend = new VisitSend();
            visitSend.setBuildId(user.getBuildId());
            visitSend.setUserId(String.valueOf(user.getId()));
            volley.request(Request.Method.POST, visitSend.toJson(), VisitsListFragment.REQUEST_VISITS);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        requestVisits();
    }

    @Override
    public void result(String result, int type) {
        View view = getView();
        try{
            Util.changeVisibility(loader, View.GONE, true);
            Util.changeVisibility(listView, View.VISIBLE, true);
            VisitRequest visitRequest = App.getGson().fromJson(result, VisitRequest.class);
            visits = visitRequest.getObject();

            if(visits == null){
                throw new ExceptionWithoutResult(getString(R.string.message_without_visits), R.drawable.img_visita_no_have);
            }
            listView.setAdapter(new VisitsListViewAdapter(getContext(), visits));
        }catch (ExceptionWithoutResult exceptionWithoutResult){
            Util.showPlaceholder(view, getActivity(), exceptionWithoutResult.getDrawableId(), exceptionWithoutResult.getMessage());
        }catch (Exception exception){
            Util.handlePlaceholderError(view, getActivity());
        }
    }

    public void onActivityResultFragment(int requestCode, int resultCode, Intent data) {
        visitsScheduleFragment.onActivityResultFragment(requestCode, resultCode, data);
    }
}
