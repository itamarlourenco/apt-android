package br.com.apt.ui.MyAdm.myadmFragments.sac.occurrence;

import java.util.List;

import br.com.apt.application.BaseGsonMeuAdm;

/**
 * Created by adminbs on 8/4/17.
 */

class OccurrenceThemeRequest extends BaseGsonMeuAdm{
    private List<OccurrenceSubject> object;

    public List<OccurrenceSubject> getObject() {
        return object;
    }

    public void setObject(List<OccurrenceSubject> object) {
        this.object = object;
    }
}
