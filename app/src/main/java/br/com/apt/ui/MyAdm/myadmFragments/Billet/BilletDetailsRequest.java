package br.com.apt.ui.MyAdm.myadmFragments.Billet;

import br.com.apt.application.BaseGsonMeuAdm;

/**
 * Created by adminbs on 8/1/17.
 */

public class BilletDetailsRequest extends BaseGsonMeuAdm {
    private BilletDetails object;

    public BilletDetails getObject() {
        return object;
    }

    public void setObject(BilletDetails object) {
        this.object = object;
    }
}