package br.com.apt.ui.MyAdm;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import br.com.apt.application.BaseActivity;
import br.com.apt.ui.reservations.CalendarFragment;

public class MyAdmActivity extends BaseActivity {

    private Fragment myAdmFragment;
    public static Intent newIntent(Context context) {
        return new Intent(context, MyAdmActivity.class);
    }

    public static Intent newIntent(Context context, int position) {
        return intentWithPosition(new Intent(context, MyAdmActivity.class), position);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public Fragment getFragment(){
        if(this.myAdmFragment == null){
            this.myAdmFragment =  MyAdmFragment.newInstance(getPositionFragment());
        }
        return this.myAdmFragment;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        MyAdmFragment myAptFragment = (MyAdmFragment) getFragment();
        myAptFragment.onActivityResultFragment(requestCode, resultCode, data);
    }
}