package br.com.apt.ui.lobby.lobbyFragments.visits;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;

import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.apt.R;
import br.com.apt.application.Volley;
import br.com.apt.ui.lobby.lobbyFragments.deliveries.DeliveriesListViewAdapter;
import br.com.apt.ui.lobby.lobbyFragments.deliveries.Delivery;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomTextViewLight;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by itamarlourenco on 09/07/17.
 */

public class VisitsListViewAdapter extends BaseAdapter {

    private Context context;
    private List<Visit> visits;

    public static int WAITING_VISITS = 2;
    public static int IS_FREE = 1;
    public static int NOT_FREE = 0;

    public  VisitsListViewAdapter(Context context, List<Visit> visits) {
        this.context = context;
        this.visits = visits;
    }

    @Override
    public int getCount() {
        return visits.size();
    }

    @Override
    public Visit getItem(int position) {
        return visits.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        VisitsListViewAdapter.ViewHolder holder;

        if(convertView == null){
            view = LayoutInflater.from(context).inflate(R.layout.row_visits, parent, false);
            holder = new VisitsListViewAdapter.ViewHolder(view);
            view.setTag(holder);
        }else{
            view = convertView;
            holder = (VisitsListViewAdapter.ViewHolder) view.getTag();
        }

        bindList(holder, position);
        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private void bindList(VisitsListViewAdapter.ViewHolder holder, int position) {
        Visit item = getItem(position);

        if(position % 2 == 0){
            holder.baseBackground.setBackground(ContextCompat.getDrawable(context, R.drawable.button_menu_list_dark));
        }else{
            holder.baseBackground.setBackground(ContextCompat.getDrawable(context, R.drawable.button_menu_list_light));
        }

        Picasso.with(context).load(Volley.getUrlByImage(item.getImage())).placeholder(R.drawable.placeholder_camera).into(holder.image);

        holder.title.setText(item.getName());
        holder.description.setText("Entrada " +
                String.format(context.getText(R.string.hours_and_time).toString(), Util.dateFormatted(item.getDate()), Util.timeFormatted(item.getDate(), "HH:mm"))
        );


        if(item.isFree() == VisitsListViewAdapter.WAITING_VISITS){
            holder.label.setText(R.string.waiting_visits);
        }

        if(item.isScheduled()){
            holder.label.setText(R.string.scheduled);
        }

        if(item.isFree() == VisitsListViewAdapter.IS_FREE){
            holder.label.setText(R.string.free);
        }

        if(item.isFree() == VisitsListViewAdapter.NOT_FREE){
            holder.label.setText(R.string.not_free);
        }

    }

    public class ViewHolder{
        private RelativeLayout baseBackground;
        private CircleImageView image;
        private CustomTextViewLight title;
        private CustomTextViewLight description;
        private CustomTextViewLight label;

        public ViewHolder(View view) {
            baseBackground = (RelativeLayout) view.findViewById(R.id.baseBackground);
            image = (CircleImageView) view.findViewById(R.id.image);
            title = (CustomTextViewLight) view.findViewById(R.id.title);
            description = (CustomTextViewLight) view.findViewById(R.id.description);
            label = (CustomTextViewLight) view.findViewById(R.id.label);
        }
    }
}
