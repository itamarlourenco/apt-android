package br.com.apt.ui.myCondominium.myCondominiumFragments.regulationAndReforms;

import br.com.apt.application.BaseGsonMeuAdm;
import br.com.apt.ui.myCondominium.myCondominiumFragments.Attachment;

/**
 * Created by adminbs on 8/5/17.
 */

class InternalRulesRequest extends BaseGsonMeuAdm {
   private Attachment object;

    public Attachment getObject() {
        return object;
    }

    public void setObject(Attachment object) {
        this.object = object;
    }
}
