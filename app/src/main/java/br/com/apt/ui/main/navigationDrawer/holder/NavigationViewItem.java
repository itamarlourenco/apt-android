package br.com.apt.ui.main.navigationDrawer.holder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.com.apt.R;

/**
 * Created by itamarlourenco on 02/01/16.
 */
public class NavigationViewItem extends RecyclerView.ViewHolder {
    public NavigationViewItem(View itemView) {
        super(itemView);
    }

    public static View getView(Context context, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.navigation_view_item, parent, false);
    }
}
