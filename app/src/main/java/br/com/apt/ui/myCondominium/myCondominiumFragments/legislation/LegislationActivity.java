package br.com.apt.ui.myCondominium.myCondominiumFragments.legislation;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.webkit.WebView;

import br.com.apt.R;

public class LegislationActivity extends AppCompatActivity {

    private WebView mWebView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_legislation);
        Intent intent = getIntent();
        setTitle(intent.getStringExtra("title"));
        mWebView = (WebView) findViewById(R.id.mWebView);
        mWebView.loadUrl(intent.getStringExtra("url"));
        mWebView.getSettings().setJavaScriptEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
