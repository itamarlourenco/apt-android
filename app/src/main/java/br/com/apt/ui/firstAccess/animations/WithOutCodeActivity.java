package br.com.apt.ui.firstAccess.animations;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.VideoView;

import br.com.apt.R;
import br.com.apt.application.BaseActivity;
import br.com.apt.application.FullScreenBaseActivity;
import br.com.apt.util.Logger;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomTextView;

/**
 * Created by adminbs on 9/13/16.
 */
public class WithOutCodeActivity extends FullScreenBaseActivity {

    private VideoView video;
    private Handler handler = new Handler();
    private boolean stop = true;
    private CustomTextView videoLetterString;
    private ImageView closeVideo;
    private RelativeLayout baseVideo;

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        baseVideo = (RelativeLayout) findViewById(R.id.baseVideo);
        startVideo();
    }

    private void startVideo() {
        final String pathVideo = "android.resource://" + getPackageName() + "/" + R.raw.papper_all;

        video = (VideoView) findViewById(R.id.video);
        video.setVideoURI(Uri.parse(pathVideo));
        video.setMediaController(null);
        video.setVisibility(View.VISIBLE);

        video.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                video.start();
                updateProgressBar();
            }
        });

        videoLetterString = (CustomTextView) findViewById(R.id.videoLetterString);
        closeVideo = (ImageView) findViewById(R.id.closeVideo);
        closeVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stop = false;
                video.start();
                new CountDownTimer(1000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {}

                    @Override
                    public void onFinish() {
                        stopHandler();
                        finish();
                    }
                }.start();
            }
        });
    }

    private void stopHandler() {
        handler.removeCallbacks(updateTimeTask);
    }

    @Override
    protected int getIdLayoutActivity() {
        return R.layout.without_code_animate;
    }


    public static Intent newIntent(Context context){
        return new Intent(context, WithOutCodeActivity.class);
    }

    @Override
    protected boolean checkLogin() {
        return false;
    }

    public void updateProgressBar() {
        handler.postDelayed(updateTimeTask, 100);
    }

    private Runnable updateTimeTask = new Runnable() {
        public void run() {
            int videoCurrent = video.getCurrentPosition();
            if((videoCurrent >= 1000 && videoCurrent <= 1500)){
                Util.changeVisibility(videoLetterString, View.VISIBLE, true);
                Util.changeVisibility(closeVideo, View.VISIBLE, true);
            }

            if((videoCurrent >= 2800 && videoCurrent <= 3100) && stop){
                video.pause();
                return;
            }
            handler.postDelayed(this, 100);
        }
    };

}
