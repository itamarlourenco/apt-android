package br.com.apt.ui.about;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import br.com.apt.R;
import br.com.apt.application.BaseActivity;
import br.com.apt.application.Config;
import br.com.apt.ui.setting.SettingFragment;

/**
 * Created by adminbs on 5/16/16.
 */
public class AboutActivity extends BaseActivity {

    public static Intent newIntent(Context context) {
        return new Intent(context, AboutActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WebView webView = (WebView) findViewById(R.id.webView);
        final ProgressBar loader = (ProgressBar) findViewById(R.id.loader);
        webView.loadUrl(Config.About.URL);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.setWebViewClient(new WebViewClient() {
            public void onPageFinished(WebView view, String url) {
                loader.setVisibility(View.GONE);
            }
        });
    }

    @Override
    protected int getTitleToolbarColor() {
        return R.color.about_color;
    }

    @Override
    protected int getIdLayoutActivity() {
        return R.layout.about_activity;
    }
}