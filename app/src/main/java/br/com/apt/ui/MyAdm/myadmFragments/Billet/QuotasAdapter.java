package br.com.apt.ui.MyAdm.myadmFragments.Billet;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;

import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.apt.R;
import br.com.apt.application.Volley;
import br.com.apt.ui.lobby.lobbyFragments.deliveries.DeliveriesListViewAdapter;
import br.com.apt.ui.lobby.lobbyFragments.deliveries.Delivery;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomTextViewLight;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by adminbs on 8/1/17.
 */

class QuotasAdapter extends BaseAdapter {
    private Context context;
    private List<Quotas.Receipt> receipts;

    public QuotasAdapter(Context context, List<Quotas.Receipt> receipts) {
        this.context = context;
        this.receipts = receipts;
    }

    @Override
    public int getCount() {
        return receipts != null ? receipts.size() : 0;
    }

    @Override
    public Quotas.Receipt getItem(int position) {
        return receipts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        QuotasAdapter.ViewHolder holder;

        if(convertView == null){
            view = LayoutInflater.from(context).inflate(R.layout.row_quotas, parent, false);
            holder = new QuotasAdapter.ViewHolder(view);
            view.setTag(holder);
        }else{
            view = convertView;
            holder = (QuotasAdapter.ViewHolder) view.getTag();
        }

        bindList(holder, position);
        return view;
    }

    private void bindList(QuotasAdapter.ViewHolder holder, int position) {
        Quotas.Receipt item = getItem(position);

        holder.receipts.setText(item.getReceipt());
        holder.limit.setText(Util.dateFormatted(item.getLimit()));
        holder.dateLimit.setText(Util.dateFormatted(item.getLimit()));
        holder.totalValue.setText(context.getString(R.string.money) + " " + item.getTotal());

        StringBuilder historyString = new StringBuilder();
        for(Quotas.History history: item.getHistory()){
            historyString.append(history.getItem());
            historyString.append("\n");
        }

        StringBuilder historyValueString = new StringBuilder();
        for(Quotas.History history: item.getHistory()){
            historyValueString.append(context.getString(R.string.money));
            historyValueString.append(" ");
            historyValueString.append(history.getTotal());
            historyValueString.append("\n");
        }

        holder.history.setText(historyString);
        holder.historyValue.setText(historyValueString);
    }

    public class ViewHolder{
        private CustomTextViewLight receipts;
        private CustomTextViewLight history;
        private CustomTextViewLight historyValue;
        private CustomTextViewLight limit;
        private CustomTextViewLight dateLimit;
        private CustomTextViewLight totalValue;

        public ViewHolder(View view) {
            receipts = (CustomTextViewLight) view.findViewById(R.id.receipts);
            history = (CustomTextViewLight) view.findViewById(R.id.history);
            historyValue = (CustomTextViewLight) view.findViewById(R.id.historyValue);
            limit = (CustomTextViewLight) view.findViewById(R.id.limit);
            dateLimit = (CustomTextViewLight) view.findViewById(R.id.dateLimit);
            totalValue = (CustomTextViewLight) view.findViewById(R.id.totalValue);
        }
    }
}
