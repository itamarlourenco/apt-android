package br.com.apt.ui.trip;

import android.content.Context;
import android.content.SharedPreferences;

import br.com.apt.application.App;

/**
 * Created by adminbs on 1/20/16.
 */
public class TripPreference {
    private static final String SHARED_PREFERENCE_NAME = "TRIP_NAME_PREFERENCE";

    private static final String ACTIVATE_MODE_TRIP = "ACTIVATE_MODE_TRIP";
    private static final String VISIT = "TRIP_CHECK_IN";
    private static final String DELIVERY = "TRIP_NOTICE";
    private static final String BILLBOARD = "TRIP_BILLBOARD";
    private static final String MESSAGE = "TRIP_MESSAGE";
    private static final String POLL = "TRIP_POLL";

    private static SharedPreferences sharedPrefs;

    public TripPreference() {
        sharedPrefs = getSharedPreference();
    }

    public void setActivateModeTrip(boolean isActiveModeTrip){
        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putBoolean(ACTIVATE_MODE_TRIP, isActiveModeTrip);
        editor.apply();
    }

    public static boolean isActivateMode() {
        SharedPreferences sharedPrefs = TripPreference.getSharedPreference();
        if (sharedPrefs != null) {
            return sharedPrefs.getBoolean(ACTIVATE_MODE_TRIP, false);
        }
        return false;
    }

    public void setCheckIn(boolean checkIn){
        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putBoolean(VISIT, checkIn);
        editor.apply();
    }

    public static boolean isVisits() {
        SharedPreferences sharedPrefs = TripPreference.getSharedPreference();
        if (sharedPrefs != null) {
            return sharedPrefs.getBoolean(VISIT, false);
        }
        return false;
    }

    public void setNotice(boolean notice){
        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putBoolean(DELIVERY, notice);
        editor.apply();
    }

    public static boolean isDelivery() {
        SharedPreferences sharedPrefs = TripPreference.getSharedPreference();
        if (sharedPrefs != null) {
            return sharedPrefs.getBoolean(DELIVERY, false);
        }
        return false;
    }

    public void setBillboard(boolean billboard){
        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putBoolean(BILLBOARD, billboard);
        editor.apply();
    }

    public static boolean isBillboard() {
        SharedPreferences sharedPrefs = TripPreference.getSharedPreference();
        if (sharedPrefs != null) {
            return sharedPrefs.getBoolean(BILLBOARD, false);
        }
        return false;
    }

    public void setMessage(boolean message){
        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putBoolean(MESSAGE, message);
        editor.apply();
    }

    public static boolean isNotice() {
        SharedPreferences sharedPrefs = TripPreference.getSharedPreference();
        if (sharedPrefs != null) {
            return sharedPrefs.getBoolean(MESSAGE, false);
        }
        return false;
    }

    public void setPoll(boolean poll){
        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putBoolean(POLL, poll);
        editor.apply();
    }

    public static boolean isPoll() {
        SharedPreferences sharedPrefs = TripPreference.getSharedPreference();
        if (sharedPrefs != null) {
            return sharedPrefs.getBoolean(POLL, false);
        }
        return false;
    }

    public static SharedPreferences getSharedPreference() {
        return App.getContext().getSharedPreferences(SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE);
    }
}
