package br.com.apt.ui.MyAdm.myadmFragments.sac;

import br.com.apt.application.SendRequest;

/**
 * Created by adminbs on 8/5/17.
 */

class SacComment extends SendRequest {
    private String comment;
    private boolean view_syndic;
    private boolean view_resident;

    public SacComment(String comment, boolean view_syndic, boolean view_resident) {
        this.comment = comment;
        this.view_syndic = view_syndic;
        this.view_resident = view_resident;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isView_syndic() {
        return view_syndic;
    }

    public void setView_syndic(boolean view_syndic) {
        this.view_syndic = view_syndic;
    }

    public boolean isView_resident() {
        return view_resident;
    }

    public void setView_resident(boolean view_resident) {
        this.view_resident = view_resident;
    }
}
