package br.com.apt.ui.myinfo.myinfofragment;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

import br.com.apt.application.ModelObject;
import br.com.apt.ui.myinfo.myinfofragment.resident.TypeUser;

/**
 * Created by adminbs on 11/3/16.
 */
public class NewUser implements ModelObject {

    public static final String TYPE_MALE = "MALE";
    public static final String TYPE_FEMALE = "FEMALE";

    private int id;
    private int actived;
    private int apartment_id;
    private String name;
    private String username;
    private String apns;
    private String auth_key;
    private String birthday;
    private String cpf;
    private Date created_at;
    private String document;
    private String gender;
    private String image;
    private String ipad_access;
    private String mobile;
    private int owner;
    private String password;
    private String phone;
    private transient String type;
    @SerializedName("type")
    private TypeUser typeObject;
    private String oldPassword;
    private int resident;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getActived() {
        return actived;
    }

    public void setActived(int actived) {
        this.actived = actived;
    }

    public int getApartment_id() {
        return apartment_id;
    }

    public void setApartment_id(int apartment_id) {
        this.apartment_id = apartment_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getApns() {
        return apns;
    }

    public void setApns(String apns) {
        this.apns = apns;
    }

    public String getAuth_key() {
        return auth_key;
    }

    public void setAuth_key(String auth_key) {
        this.auth_key = auth_key;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getIpad_access() {
        return ipad_access;
    }

    public void setIpad_access(String ipad_access) {
        this.ipad_access = ipad_access;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public int getOwner() {
        return owner;
    }

    public void setOwner(int owner) {
        this.owner = owner;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public TypeUser getTypeObject() {
        return typeObject;
    }

    public void setTypeObject(TypeUser typeObject) {
        this.typeObject = typeObject;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public int getResident() {
        return resident;
    }

    public void setResident(int resident) {
        this.resident = resident;
    }


    public NewUser() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeInt(this.actived);
        dest.writeInt(this.apartment_id);
        dest.writeString(this.name);
        dest.writeString(this.username);
        dest.writeString(this.apns);
        dest.writeString(this.auth_key);
        dest.writeString(this.birthday);
        dest.writeString(this.cpf);
        dest.writeLong(this.created_at != null ? this.created_at.getTime() : -1);
        dest.writeString(this.document);
        dest.writeString(this.gender);
        dest.writeString(this.image);
        dest.writeString(this.ipad_access);
        dest.writeString(this.mobile);
        dest.writeInt(this.owner);
        dest.writeString(this.password);
        dest.writeString(this.phone);
        dest.writeParcelable(this.typeObject, flags);
        dest.writeString(this.oldPassword);
        dest.writeInt(this.resident);
    }

    protected NewUser(Parcel in) {
        this.id = in.readInt();
        this.actived = in.readInt();
        this.apartment_id = in.readInt();
        this.name = in.readString();
        this.username = in.readString();
        this.apns = in.readString();
        this.auth_key = in.readString();
        this.birthday = in.readString();
        this.cpf = in.readString();
        long tmpCreated_at = in.readLong();
        this.created_at = tmpCreated_at == -1 ? null : new Date(tmpCreated_at);
        this.document = in.readString();
        this.gender = in.readString();
        this.image = in.readString();
        this.ipad_access = in.readString();
        this.mobile = in.readString();
        this.owner = in.readInt();
        this.password = in.readString();
        this.phone = in.readString();
        this.typeObject = in.readParcelable(TypeUser.class.getClassLoader());
        this.oldPassword = in.readString();
        this.resident = in.readInt();
    }

    public static final Creator<NewUser> CREATOR = new Creator<NewUser>() {
        @Override
        public NewUser createFromParcel(Parcel source) {
            return new NewUser(source);
        }

        @Override
        public NewUser[] newArray(int size) {
            return new NewUser[size];
        }
    };
}