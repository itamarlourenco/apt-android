package br.com.apt.ui.MyAdm.myadmFragments.register;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by adminbs on 7/31/17.
 */

public class Register implements Parcelable {
    private Document document;
    private String name;
    private String mobile;
    private String email;
    private Addressdetail addressdetail;
    private AttachmentAddress attachmentAddress;

    public Register(){

    }

    public Register(Document document, String name, String mobile, String email, Addressdetail addressdetail, AttachmentAddress attachmentAddress) {
        this.document = document;
        this.name = name;
        this.mobile = mobile;
        this.email = email;
        this.addressdetail = addressdetail;
        this.attachmentAddress = attachmentAddress;
    }

    public static class Document implements Parcelable {
        private String type;
        private String number;

        public Document(){

        }

        public Document(String type, String number) {
            this.type = type;
            this.number = number;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        protected Document(Parcel in) {
            type = in.readString();
            number = in.readString();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(type);
            dest.writeString(number);
        }

        @SuppressWarnings("unused")
        public static final Parcelable.Creator<Document> CREATOR = new Parcelable.Creator<Document>() {
            @Override
            public Document createFromParcel(Parcel in) {
                return new Document(in);
            }

            @Override
            public Document[] newArray(int size) {
                return new Document[size];
            }
        };
    }

    public static class AttachmentAddress implements Parcelable {
        private String zipCode;
        private String address;
        private String complement;
        private String number;
        private String neighborhood;
        private String state;
        private String city;

        public AttachmentAddress(String zipCode, String address, String complement, String number, String neighborhood, String state, String city) {
            this.zipCode = zipCode;
            this.address = address;
            this.complement = complement;
            this.number = number;
            this.neighborhood = neighborhood;
            this.state = state;
            this.city = city;
        }

        public String getZipCode() {
            return zipCode;
        }

        public void setZipCode(String zipCode) {
            this.zipCode = zipCode;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getComplement() {
            return complement;
        }

        public void setComplement(String complement) {
            this.complement = complement;
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public String getNeighborhood() {
            return neighborhood;
        }

        public void setNeighborhood(String neighborhood) {
            this.neighborhood = neighborhood;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        protected AttachmentAddress(Parcel in) {
            zipCode = in.readString();
            address = in.readString();
            complement = in.readString();
            number = in.readString();
            neighborhood = in.readString();
            state = in.readString();
            city = in.readString();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(zipCode);
            dest.writeString(address);
            dest.writeString(complement);
            dest.writeString(number);
            dest.writeString(neighborhood);
            dest.writeString(state);
            dest.writeString(city);
        }

        @SuppressWarnings("unused")
        public static final Parcelable.Creator<AttachmentAddress> CREATOR = new Parcelable.Creator<AttachmentAddress>() {
            @Override
            public AttachmentAddress createFromParcel(Parcel in) {
                return new AttachmentAddress(in);
            }

            @Override
            public AttachmentAddress[] newArray(int size) {
                return new AttachmentAddress[size];
            }
        };
    }

    public static class Addressdetail implements Parcelable {
        private String zipCode;
        private String neighborhood;
        private String street;
        private String number;
        private String city;
        private String state;
        private String country;
        private String complement;

        public Addressdetail(){

        }

        public Addressdetail(String zipCode, String neighborhood, String street, String number, String city, String state, String country, String complement) {
            this.zipCode = zipCode;
            this.neighborhood = neighborhood;
            this.street = street;
            this.number = number;
            this.city = city;
            this.state = state;
            this.country = country;
            this.complement = complement;
        }

        public String getZipCode() {
            return zipCode;
        }

        public void setZipCode(String zipCode) {
            this.zipCode = zipCode;
        }

        public String getNeighborhood() {
            return neighborhood;
        }

        public void setNeighborhood(String neighborhood) {
            this.neighborhood = neighborhood;
        }

        public String getStreet() {
            return street;
        }

        public void setStreet(String street) {
            this.street = street;
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getComplement() {
            return complement;
        }

        public void setComplement(String complement) {
            this.complement = complement;
        }

        protected Addressdetail(Parcel in) {
            zipCode = in.readString();
            neighborhood = in.readString();
            street = in.readString();
            number = in.readString();
            city = in.readString();
            state = in.readString();
            country = in.readString();
            complement = in.readString();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(zipCode);
            dest.writeString(neighborhood);
            dest.writeString(street);
            dest.writeString(number);
            dest.writeString(city);
            dest.writeString(state);
            dest.writeString(country);
            dest.writeString(complement);
        }

        @SuppressWarnings("unused")
        public static final Parcelable.Creator<Addressdetail> CREATOR = new Parcelable.Creator<Addressdetail>() {
            @Override
            public Addressdetail createFromParcel(Parcel in) {
                return new Addressdetail(in);
            }

            @Override
            public Addressdetail[] newArray(int size) {
                return new Addressdetail[size];
            }
        };
    }

    public Document getDocument() {
        return document;
    }

    public void setDocument(Document document) {
        this.document = document;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Addressdetail getAddressdetail() {
        return addressdetail;
    }

    public AttachmentAddress getAttachmentAddress() {
        return attachmentAddress;
    }

    public void setAddressdetail(Addressdetail addressdetail) {
        this.addressdetail = addressdetail;
    }



    protected Register(Parcel in) {
        document = (Document) in.readValue(Document.class.getClassLoader());
        name = in.readString();
        mobile = in.readString();
        email = in.readString();
        addressdetail = (Addressdetail) in.readValue(Addressdetail.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(document);
        dest.writeString(name);
        dest.writeString(mobile);
        dest.writeString(email);
        dest.writeValue(addressdetail);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Register> CREATOR = new Parcelable.Creator<Register>() {
        @Override
        public Register createFromParcel(Parcel in) {
            return new Register(in);
        }

        @Override
        public Register[] newArray(int size) {
            return new Register[size];
        }
    };

    public String handleObjectByBoby(){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Informações Pessoais \n");

        Document document = getDocument();
        if(document != null){
            stringBuilder.append(document.getType());
            stringBuilder.append(": ");
            stringBuilder.append(document.getNumber());
        }


        stringBuilder.append("\n\nAos cuidados: ");
        stringBuilder.append(getName());
        stringBuilder.append("\nCelular: ");
        stringBuilder.append(getMobile());
        stringBuilder.append("\nE-mail: ");
        stringBuilder.append(getEmail());
        stringBuilder.append("\n \nDetalhes do endereço: ");
        Addressdetail addressdetail = getAddressdetail();
        if(addressdetail != null){
            stringBuilder.append("\nCEP: ");
            stringBuilder.append(addressdetail.getZipCode());
            stringBuilder.append("\nBairro: ");
            stringBuilder.append(addressdetail.getNeighborhood());
            stringBuilder.append("\nEndereço: ");
            stringBuilder.append(addressdetail.getStreet());
            stringBuilder.append("\nNúmero: ");
            stringBuilder.append(addressdetail.getStreet());
            stringBuilder.append("\nCidade: ");
            stringBuilder.append(addressdetail.getCity());
            stringBuilder.append("\nEstado: ");
            stringBuilder.append(addressdetail.getState());
            stringBuilder.append("\nPaís: ");
            stringBuilder.append(addressdetail.getCountry());
            stringBuilder.append("\nComplemento: ");
            stringBuilder.append(addressdetail.getComplement());
        }

        AttachmentAddress attachmentAddress = getAttachmentAddress();
        if(attachmentAddress != null){
            stringBuilder.append("\n \n \nEndereço para boleto: ");

            stringBuilder.append("\nCEP: ");
            stringBuilder.append(attachmentAddress.getZipCode());
            stringBuilder.append("\nBairro: ");
            stringBuilder.append(attachmentAddress.getNeighborhood());
            stringBuilder.append("\nEndereço: ");
            stringBuilder.append(attachmentAddress.getAddress());
            stringBuilder.append("\nNúmero: ");
            stringBuilder.append(attachmentAddress.getNumber());
            stringBuilder.append("\nCidade: ");
            stringBuilder.append(attachmentAddress.getCity());
            stringBuilder.append("\nEstado: ");
            stringBuilder.append(attachmentAddress.getState());
            stringBuilder.append("\nComplemento: ");
            stringBuilder.append(attachmentAddress.getComplement());
        }



        return stringBuilder.toString();
    }
}