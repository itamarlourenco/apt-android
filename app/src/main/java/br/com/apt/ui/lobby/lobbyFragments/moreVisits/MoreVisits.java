package br.com.apt.ui.lobby.lobbyFragments.moreVisits;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

import br.com.apt.application.ModelObject;
import br.com.apt.model.user.User;
import br.com.apt.ui.myapt.myaptFragments.build.Apartment;
import br.com.apt.ui.myinfo.myinfofragment.NewUser;

/**
 * Created by adminbs on 11/10/16.
 */
public class MoreVisits implements ModelObject {

    public static final String STATUS_RELEASED = "RELEASED";
    public static final String STATUS_NOT_RELEASED = "NOT_RELEASED";
    public static final String STATUS_WAITING = "WAITING";

    private int id;
    private Apartment apartment;
    private int apartment_id;
    private String arrival_date;
    private String arrival_time;
    private Date created_at;
    private String image;
    private String name;
    private String status;
    private int user_id;
    private NewUser user;
    private String released;

    public String getReleased() {
        return released;
    }

    public void setReleased(String released) {
        this.released = released;
    }

    public String getArrival_time() {
        return arrival_time;
    }

    public String getArrival_date() {
        return arrival_date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Apartment getApartment() {
        return apartment;
    }

    public void setApartment(Apartment apartment) {
        this.apartment = apartment;
    }

    public int getApartment_id() {
        return apartment_id;
    }

    public void setApartment_id(int apartment_id) {
        this.apartment_id = apartment_id;
    }

    public String getArrivalDate() {
        return arrival_date;
    }

    public void setArrival_date(String arrival_date) {
        this.arrival_date = arrival_date;
    }

    public String getArrivalTime() {
        return arrival_time;
    }

    public void setArrival_time(String arrival_time) {
        this.arrival_time = arrival_time;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public NewUser getUser() {
        return user;
    }

    public void setUser(NewUser user) {
        this.user = user;
    }

    protected MoreVisits(Parcel in) {
        id = in.readInt();
        apartment = (Apartment) in.readValue(Apartment.class.getClassLoader());
        apartment_id = in.readInt();
        arrival_date = in.readString();
        arrival_time = in.readString();
        long tmpCreated_at = in.readLong();
        created_at = tmpCreated_at != -1 ? new Date(tmpCreated_at) : null;
        image = in.readString();
        name = in.readString();
        status = in.readString();
        user_id = in.readInt();
        user = (NewUser) in.readValue(NewUser.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeValue(apartment);
        dest.writeInt(apartment_id);
        dest.writeString(arrival_date);
        dest.writeString(arrival_time);
        dest.writeLong(created_at != null ? created_at.getTime() : -1L);
        dest.writeString(image);
        dest.writeString(name);
        dest.writeString(status);
        dest.writeInt(user_id);
        dest.writeValue(user);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<MoreVisits> CREATOR = new Parcelable.Creator<MoreVisits>() {
        @Override
        public MoreVisits createFromParcel(Parcel in) {
            return new MoreVisits(in);
        }

        @Override
        public MoreVisits[] newArray(int size) {
            return new MoreVisits[size];
        }
    };
}