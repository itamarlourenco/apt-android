package br.com.apt.ui.tickets.tickets;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.github.ybq.android.spinkit.SpinKitView;

import java.util.List;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.BaseFragment;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.ui.ExceptionWithoutResult;
import br.com.apt.util.Util;

@Deprecated
public class TicketsListFragment extends BaseFragment implements Volley.Callback {

    private List<Ticket> tickets;
    private ListView listView;
    private SpinKitView loader;


    public static TicketsListFragment newInstance() {
        return new TicketsListFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list_tickets, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        requestTickets();
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if(view != null){
            loader = (SpinKitView) view.findViewById(R.id.loader);
            listView = (ListView) view.findViewById(R.id.listView);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    startActivity(TicketItemActivity.newIntent(getContext(), tickets.get(position)));
                }
            });
            view.findViewById(R.id.newTicket).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(TicketAddActivity.newIntent(getContext()));
                    getActivity().overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
                }
            });
        }
    }

    public void requestTickets(){
        Volley volley = new Volley(this);
        volley.request();
    }

    @Override
    public void result(String result, int type) {
        View view = getView();
        try{
            Util.hidePlaceholder(view);
            Util.changeVisibility(loader, View.GONE, true);
            Util.changeVisibility(listView, View.VISIBLE, true);
            TicketsRequest ticketsRequest = App.getGson().fromJson(result, TicketsRequest.class);
            tickets = ticketsRequest.getObject();
            listView.setAdapter(new TicketsAdapter(getContext(), tickets));
            if(tickets == null){
                throw new ExceptionWithoutResult(getString(R.string.message_without_ticket), R.drawable.img_notificacoes_no_itens);
            }
        }catch (ExceptionWithoutResult exceptionWithoutResult){
            Util.showPlaceholder(view, getActivity(), exceptionWithoutResult.getDrawableId(), exceptionWithoutResult.getMessage());
        }catch (Exception exception){
            Util.handlePlaceholderError(view, getActivity());
        }
    }

    @Override
    public String uri() {
        return URI.TICKETS;
    }
}