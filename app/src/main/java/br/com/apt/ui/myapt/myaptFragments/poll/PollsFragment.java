package br.com.apt.ui.myapt.myaptFragments.poll;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.android.volley.Request;
import com.github.ybq.android.spinkit.SpinKitView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.BaseGson;
import br.com.apt.application.SendRequest;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.application.ZoomImageView;
import br.com.apt.model.user.User;
import br.com.apt.model.user.UserMeuadm;
import br.com.apt.ui.ExceptionWithoutResult;
import br.com.apt.ui.lobby.lobbyFragments.visits.VisitRequest;
import br.com.apt.ui.lobby.lobbyFragments.visits.VisitsListViewAdapter;
import br.com.apt.ui.myapt.MyAptFragment;
import br.com.apt.ui.tickets.tickets.TicketAddActivity;
import br.com.apt.util.Logger;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomTextView;
import br.com.apt.widget.CustomTextViewBold;
import br.com.apt.widget.CustomTextViewLight;
import br.com.apt.widget.PagerItemFragment;

/**
 * Created by adminbs on 1/12/16.
 */
public class PollsFragment extends PagerItemFragment implements Volley.Callback{
    private ListView listView;
    private List<Poll> pollList = new ArrayList<>();
    private SpinKitView loader;
    private CustomTextViewLight messagePollEmpty;

    @Override
    public String uri() {
        User user = getUser();
        return String.format(URI.POLLS_SEARCH, user.getCompanyKey(), user.getCondominiumId(), user.getApto());
    }

    @Override
    protected int getIndicator() {
        return App.getNotificationSharedPreferences().getCountPoll();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_polls, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if(view != null){
            listView = (ListView) view.findViewById(R.id.listView);
            loader = (SpinKitView) view.findViewById(R.id.loader);
            messagePollEmpty = (CustomTextViewLight) view.findViewById(R.id.messagePollEmpty);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    startActivity(PollItemActivity.newIntent(getContext(), pollList.get(position)));
                }
            });
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        requestPolls();
    }

    public void requestPolls(){
        final User user = getUser();
        if(user != null){
            Volley volley = new Volley(this);
            volley.isPoll(true);
            volley.request(Request.Method.GET, null);
        }
    }

    @Override
    protected String getPagerTitle() {
        return App.getContext().getString(R.string.polls);
    }

    @Override
    protected int getPagerIcon() {
        return R.drawable.ico_menu_votacoes ;
    }

    @Override
    public void result(String result, int type) {
        View view = getView();
        try{
            Util.changeVisibility(loader, View.GONE, true);
            Util.changeVisibility(listView, View.VISIBLE, true);
            PollRequest pollRequest = App.getGson().fromJson(result, PollRequest.class);
            pollList = pollRequest.getObject();

            if(pollList.size() == 0)
                messagePollEmpty.setVisibility(View.VISIBLE);

            if(pollList == null){
                throw new ExceptionWithoutResult(getString(R.string.message_without_poll), R.drawable.img_votacoes_no_itens);
            }
            listView.setAdapter(new PollAdapter(getContext(), pollList));
        }catch (ExceptionWithoutResult exceptionWithoutResult){
            Util.showPlaceholder(view, getActivity(), exceptionWithoutResult.getDrawableId(), exceptionWithoutResult.getMessage());
        }catch (Exception exception){
            Util.handlePlaceholderError(view, getActivity());
        }
    }
}
