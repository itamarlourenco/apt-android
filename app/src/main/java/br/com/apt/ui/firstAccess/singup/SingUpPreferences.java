package br.com.apt.ui.firstAccess.singup;

import android.content.Context;
import android.content.SharedPreferences;

import br.com.apt.application.App;

/**
 * Created by adminbs on 9/15/16.
 */
public class SingUpPreferences {
    private static String SHARED_PREFERENCE_NAME = "SingUpPreferences";
    private static String PREFERENCE_SING_UP_NAME = "name";
    private static String PREFERENCE_SING_UP_EMAIL = "email";
    private static String PREFERENCE_SING_UP_PHONE = "phone";
    private static String PREFERENCE_SING_UP_CPF = "cpf";

    private static SharedPreferences sharedPrefs;

    public SingUpPreferences() {
        sharedPrefs = App.getContext().getSharedPreferences(SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE);
    }

    public void setName(String name) {
        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putString(PREFERENCE_SING_UP_NAME, name);
        editor.apply();
    }

    public String getName() {
        return sharedPrefs != null ? sharedPrefs.getString(PREFERENCE_SING_UP_NAME, "") : "";
    }




    public void setEmail(String email) {
        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putString(PREFERENCE_SING_UP_EMAIL, email);
        editor.apply();
    }

    public String getEmail() {
        return sharedPrefs != null ? sharedPrefs.getString(PREFERENCE_SING_UP_EMAIL, "") : "";
    }



    public void setPhone(String phone) {
        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putString(PREFERENCE_SING_UP_PHONE, phone);
        editor.apply();
    }

    public String getPhone() {
        return sharedPrefs != null ? sharedPrefs.getString(PREFERENCE_SING_UP_PHONE, "") : "";
    }



    public void setCpf(String cpf) {
        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putString(PREFERENCE_SING_UP_EMAIL, cpf);
        editor.apply();
    }

    public String getCpf() {
        return sharedPrefs != null ? sharedPrefs.getString(PREFERENCE_SING_UP_CPF, "") : "";
    }
}
