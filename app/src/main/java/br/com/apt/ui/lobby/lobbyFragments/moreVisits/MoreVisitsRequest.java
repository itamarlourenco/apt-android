package br.com.apt.ui.lobby.lobbyFragments.moreVisits;

import android.os.Parcel;

import java.util.Date;
import java.util.List;

import br.com.apt.application.BaseGson;
import br.com.apt.application.ModelObject;
import br.com.apt.ui.myapt.myaptFragments.build.Apartment;
import br.com.apt.ui.myinfo.myinfofragment.NewUser;

/**
 * Created by adminbs on 11/10/16.
 */
public class MoreVisitsRequest extends BaseGson {
    public List<MoreVisits> Object;

    public List<MoreVisits> getObject() {
        return Object;
    }
}

