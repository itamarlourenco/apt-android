package br.com.apt.ui.myinfo.myinfofragment;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;

import com.android.volley.Request;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.BaseGson;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.model.user.User;
import br.com.apt.model.user.UserData;
import br.com.apt.model.user.UserRequest;
import br.com.apt.util.Logger;
import br.com.apt.util.Mask;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomButton;
import br.com.apt.widget.CustomEditText;
import br.com.apt.widget.PagerItemFragment;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by fabricio.bezerra on 22/09/2016.
 */
public class UserFragment extends FragmentMyInfoAddImage implements View.OnClickListener {
    public static ImageView profileImageUser;
    private static Uri mCurrentPhotoUriUser;
    private static String mCurrentPhotoPathUser;

    private CustomEditText name;
    private CustomEditText email;
    private CustomEditText birthDate;
    private CustomEditText cpf;
    private CustomEditText rg;
    private CustomEditText phone;
    private CustomEditText mobile;

    private CustomEditText oldPassword;
    private CustomEditText newPassword;
    private CustomEditText confirmPassword;

    private Spinner genders;

    private CustomButton buttonSaveUser;

    private String gender = "MALE";
    private NewUser newUser;

    public static PagerItemFragment newInstance(){
        return new UserFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_user, container, false);
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if(view != null){
            profileImageUser = (ImageView) view.findViewById(R.id.profileImage);
            profileImageUser.setOnClickListener(this);

            name = (CustomEditText) view.findViewById(R.id.name);
            email = (CustomEditText) view.findViewById(R.id.email);
            birthDate = (CustomEditText) view.findViewById(R.id.birthDate);
            cpf = (CustomEditText) view.findViewById(R.id.cpf);
            cpf.addTextChangedListener(Mask.insert("###.###.###-##", cpf));
            rg = (CustomEditText) view.findViewById(R.id.rg);
            phone = (CustomEditText) view.findViewById(R.id.phone);
            phone.addTextChangedListener(Mask.insert("(##)#####-####", phone));
            mobile = (CustomEditText) view.findViewById(R.id.mobile);
            mobile.addTextChangedListener(Mask.insert("(##)#####-####", mobile));
            genders = (Spinner) view.findViewById(R.id.genders);

            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(), R.array.genders, R.layout.spinner_gender);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            genders.setAdapter(adapter);
            genders.setSelection(0);

            oldPassword = (CustomEditText) view.findViewById(R.id.oldPassword);
            newPassword = (CustomEditText) view.findViewById(R.id.newPassword);
            confirmPassword = (CustomEditText) view.findViewById(R.id.confirmPassword);

            buttonSaveUser = (CustomButton) view.findViewById(R.id.buttonSaveUser);
            buttonSaveUser.setOnClickListener(this);

            genders.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if(position == 0){
                        gender = NewUser.TYPE_MALE;
                    }else{
                        gender = NewUser.TYPE_FEMALE;
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            Volley volley = new Volley(new Volley.Callback() {
                @Override
                public void result(String result, int type) {
                    if(result != null){
                        NewUserRequest newUserRequest = App.getGson().fromJson(result, NewUserRequest.class);
                        newUser = newUserRequest.getObject();
                        if(newUser != null){
                            name.setText(newUser.getName());
                            email.setText(newUser.getUsername());
                            birthDate.setText(newUser.getBirthday());

                            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                            try {
                                Date date = format.parse(newUser.getBirthday());
                                SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
                                String datetime = dateformat.format(date);
                                birthDate.setText(datetime);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                            cpf.setText(newUser.getCpf());
                            rg.setText(newUser.getDocument());
                            phone.setText(newUser.getPhone());
                            mobile.setText(newUser.getMobile());

                            Picasso.with(getContext()).load(Volley.getUrlByImage(newUser.getImage())).placeholder(R.drawable.default_camera).into(profileImageUser);
                            if(newUser.getGender() != null){
                                gender = newUser.getGender();
                            }


                            if(NewUser.TYPE_MALE.equals(newUser.getGender())){
                                genders.setSelection(0);
                            }else{
                                genders.setSelection(1);
                            }
                        }

                    }

                    birthDate.addTextChangedListener(Mask.insert("##/##/####", birthDate));
                }

                @Override
                public String uri() {
                    return URI.USER;
                }
            });
            volley.showDialog(getActivity());
            volley.request(Request.Method.GET, null);
        }
    }

    @Override
    protected String getPagerTitle() {
        return App.getContext().getString(R.string.user);
    }

    @Override
    protected int getPagerIcon() {
        return R.drawable.ico_meus_dados;
    }

    @Override
    protected void setConfigPhotos(Uri mCurrentPhotoUri, String mCurrentPhotoPath) {
        mCurrentPhotoUriUser = mCurrentPhotoUri;
        mCurrentPhotoPathUser = mCurrentPhotoPath;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.profileImage:
                this.pickPictureOrPickFromGallery();
                break;
            case R.id.buttonSaveUser:
                saveUser();
                break;
        }
    }

    @Override
    protected HashMap<String, String> save() {
        if (name == null){
            return new HashMap<>();
        }
        if(TextUtils.isEmpty(name.getText())){
            Util.showDialog(getActivity(), R.string.enter_with_name);
            return null;
        }

        if(TextUtils.isEmpty(email.getText())){
            Util.showDialog(getActivity(), R.string.enter_with_email);
            return null;
        }


        if(!TextUtils.isEmpty(oldPassword.getText()) || !TextUtils.isEmpty(confirmPassword.getText())){
            if(!TextUtils.equals(newPassword.getText(), confirmPassword.getText())){
                Util.showDialog(getActivity(), R.string.my_info_confirm_new_password);
                return null;
            }

            if(TextUtils.isEmpty(oldPassword.getText())){
                Util.showDialog(getActivity(), R.string.pleaseEnterWithOldPassword);
                return null;
            }
        }

        String clearCpf = Util.clearCpf(cpf.getText().toString());

        if(!Util.ValidaCPF.isCPF(clearCpf)){
            Util.showDialog(getActivity(), R.string.validateCpf);
            return null;
        }


        String dateString = "";
        if(!TextUtils.isEmpty(birthDate.getText())){
            dateString = Util.dateUsToBr(birthDate.getText().toString());
        }

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("name", name.getText().toString());
        hashMap.put("username", email.getText().toString());
        hashMap.put("gender", gender);
        hashMap.put("oldPassword", oldPassword.getText().toString());
        hashMap.put("password", newPassword.getText().toString());
        hashMap.put("birthday", dateString);
        hashMap.put("phone", phone.getText().toString());
        hashMap.put("mobile", mobile.getText().toString());
        hashMap.put("document", rg.getText().toString());
        hashMap.put("cpf", cpf.getText().toString());

        return hashMap;
    }

    @Override
    protected String getUri() {
        return URI.USER;
    }

    @Override
    protected int getTypeRequest() {
        return Request.Method.POST;
    }

    @Override
    protected void saved() {}

    @Override
    protected void saved(String json) {
        UserRequest userRequest = App.getGson().fromJson(json,UserRequest.class);

        User user = getUser();

        user.setImage(userRequest.getObject().getImage());

        UserData.saveUser(user);
    }

    private void saveUser() {
        this.sendSave();
    }

    @Override
    public void onActivityResultFragment(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK && profileImageUser != null && mCurrentPhotoUriUser != null) {
            profileImageUser.setVisibility(View.VISIBLE);
            profileImageUser.setImageBitmap(decodeFile(new File(mCurrentPhotoUriUser.getPath())));
            return;
        }

        if(requestCode == REQUEST_IMAGE_FROM_GALLERY && resultCode == Activity.RESULT_OK && profileImageUser != null) {
            try {
                profileImageUser.setImageBitmap(decodeUri(App.getContext(), data.getData(), 150));
                mCurrentPhotoPathUser = createImageFile().getAbsolutePath();
                mCurrentPhotoUriUser = data.getData();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public Uri getCurrentPhotoUri() {
        return mCurrentPhotoUriUser;
    }

    @Override
    public String getCurrentPhotoPath() {
        return mCurrentPhotoPathUser;
    }
}
