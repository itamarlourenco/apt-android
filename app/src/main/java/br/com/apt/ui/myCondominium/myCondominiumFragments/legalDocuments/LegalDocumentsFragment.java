package br.com.apt.ui.myCondominium.myCondominiumFragments.legalDocuments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ListView;

import com.github.ybq.android.spinkit.SpinKitView;

import java.util.List;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.model.user.User;
import br.com.apt.ui.myCondominium.OpenPDF;
import br.com.apt.ui.myCondominium.myCondominiumFragments.AttachmentRequest;
import br.com.apt.ui.myCondominium.myCondominiumFragments.Attachments;
import br.com.apt.ui.myCondominium.myCondominiumFragments.AttachmentsRequest;
import br.com.apt.util.Util;
import br.com.apt.widget.PagerItemFragment;
import br.com.apt.widget.SendSuccessDialog;

/**
 * Created by adminbs on 8/5/17.
 */

public class LegalDocumentsFragment extends PagerItemFragment implements Volley.Callback {

    private User user = getUser();
    private ListView listView;
    private SpinKitView loader;
    private List<Attachments> attachmentsList;

    @Override
    protected String getPagerTitle() {
        return App.getContext().getString(R.string.legal_documents);
    }

    @Override
    protected int getPagerIcon() {
        return R.drawable.ico_documents;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_documents, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if(view != null){
            listView = (ListView) view.findViewById(R.id.listView);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    startActivity(OpenPDF.newIntent(getContext(), attachmentsList.get(position).getUrl()));
                }
            });
            loader = (SpinKitView) view.findViewById(R.id.loader);
        }

        Volley volley = new Volley(this);
        volley.isMeuADM(true);
        volley.request();
    }

    @Override
    public void result(String result, int type) {
        try{
            Util.changeVisibility(loader, View.GONE, true);
            Util.changeVisibility(listView, View.VISIBLE, true);

            AttachmentsRequest attachmentsRequest = App.getGson().fromJson(result, AttachmentsRequest.class);
            attachmentsList = attachmentsRequest.getObject();
            listView.setAdapter(new LegalDocumentsAdapter(getContext(), attachmentsList));
        }catch (Exception e){
            SendSuccessDialog.show(getString(R.string.error_generic), null, getFragmentManager(), new SendSuccessDialog.Actions() {
                @Override
                public void back() {
                    getActivity().finish();
                }
            }, SendSuccessDialog.TYPE_ERROR);
        }
    }

    @Override
    public String uri() {
        return String.format(URI.DOCUMENTS, user.getCondominiumId());
    }
}
