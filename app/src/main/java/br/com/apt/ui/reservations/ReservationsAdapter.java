package br.com.apt.ui.reservations;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.Volley;
import br.com.apt.ui.lobby.lobbyFragments.deliveries.DeliveriesListViewAdapter;
import br.com.apt.ui.lobby.lobbyFragments.deliveries.Delivery;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomTextViewLight;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by adminbs on 9/10/17.
 */

public class ReservationsAdapter extends BaseAdapter {

    private Context context;
    private List<Reservations> reservationsList;

    public ReservationsAdapter(Context context, List<Reservations> reservationsList) {
        this.context = context;
        this.reservationsList = reservationsList;
    }

    @Override
    public int getCount() {
        return reservationsList == null ? 0 : reservationsList.size();
    }

    @Override
    public Reservations getItem(int position) {
        return reservationsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ReservationsAdapter.ViewHolder holder;

        if(convertView == null){
            view = LayoutInflater.from(context).inflate(R.layout.row_reservations, parent, false);
            holder = new ReservationsAdapter.ViewHolder(view);
            view.setTag(holder);
        }else{
            view = convertView;
            holder = (ReservationsAdapter.ViewHolder) view.getTag();
        }

        bindList(holder, position);
        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private void bindList(ReservationsAdapter.ViewHolder holder, int position) {
        Reservations item = getItem(position);

        if(position % 2 == 0){
            holder.deliveryBaseBackground.setBackground(ContextCompat.getDrawable(context, R.drawable.button_menu_list_dark));
        }else{
            holder.deliveryBaseBackground.setBackground(ContextCompat.getDrawable(context, R.drawable.button_menu_list_light));
        }

        if(item.isRead()){
            holder.read.setVisibility(View.VISIBLE);
        }else{
            holder.read.setVisibility(View.GONE);
        }

        Installations installation = item.getInstallation();
        if (installation != null) {
            holder.reservationTitle.setText(installation.getName());

            try {
                SimpleDateFormat simpleDateFormat = App.getSimpleDateFormat();
                Date parseStart = simpleDateFormat.parse(item.getStart());
                Date parseEnd = simpleDateFormat.parse(item.getEnd());

                String dateStart = new SimpleDateFormat("dd/MM/yyyy", Locale.US).format(parseStart);
                String timeStart = new SimpleDateFormat("HH:mm", Locale.US).format(parseStart);
                String timeEnd = new SimpleDateFormat("HH:mm", Locale.US).format(parseEnd);

                holder.reservationDescription.setText(dateStart + " das " + timeStart + " às " + timeEnd);

                Picasso.with(context).load(Installations.handleUrlIcon(installation.getIcon())).into(holder.icon);


                if(TextUtils.equals(item.getApproved(), Reservations.TYPE_CREATED)){
                    holder.reservationLabel.setText("AGUARDANDO\nCONFIRMAÇÃO");
                    holder.reservationLabel.setBackground(ContextCompat.getDrawable(context, R.drawable.border_radius_yellow));
                    holder.reservationLabel.setTextColor(ContextCompat.getColor(context, android.R.color.black));
                }

                if(TextUtils.equals(item.getApproved(), Reservations.TYPE_APPROVED)){
                    holder.reservationLabel.setText("CONFIRMADO");
                    holder.reservationLabel.setBackground(ContextCompat.getDrawable(context, R.drawable.border_radius_blue));
                    holder.reservationLabel.setTextColor(ContextCompat.getColor(context, R.color.confirmed_color_reservation));
                }

                if(TextUtils.equals(item.getApproved(), Reservations.TYPE_NOT_APPROVED)){
                    holder.reservationLabel.setText("RECUSADA");
                    holder.reservationLabel.setBackground(ContextCompat.getDrawable(context, R.drawable.border_radius_red_reservations));
                    holder.reservationLabel.setTextColor(ContextCompat.getColor(context, android.R.color.white));
                }


                Date currentDate = Calendar.getInstance().getTime();
                if(currentDate.compareTo(parseEnd) > 0){
                    holder.reservationLabel.setText("JÁ UTLIZADA");
                    holder.reservationLabel.setBackground(ContextCompat.getDrawable(context, R.drawable.border_radius_black));
                    holder.reservationLabel.setTextColor(ContextCompat.getColor(context, R.color.valid_reservation));
                }


            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }

    public class ViewHolder{
        private RelativeLayout deliveryBaseBackground;
        private ImageView icon;
        private CustomTextViewLight reservationTitle;
        private CustomTextViewLight reservationDescription;
        private CustomTextViewLight reservationLabel;
        private View read;

        public ViewHolder(View view) {
            deliveryBaseBackground = (RelativeLayout) view.findViewById(R.id.deliveryBaseBackground);
            icon = (ImageView) view.findViewById(R.id.icon);
            read = (View) view.findViewById(R.id.read);
            reservationTitle = (CustomTextViewLight) view.findViewById(R.id.deliveryTitle);
            reservationDescription = (CustomTextViewLight) view.findViewById(R.id.deliveryDescription);
            reservationLabel = (CustomTextViewLight) view.findViewById(R.id.deliveryLabel);
        }
    }
}
