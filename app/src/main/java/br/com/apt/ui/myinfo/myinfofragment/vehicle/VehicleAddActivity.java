package br.com.apt.ui.myinfo.myinfofragment.vehicle;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import br.com.apt.R;
import br.com.apt.application.BaseActivity;
import br.com.apt.ui.reservations.CalendarFragment;

/**
 * Created by adminbs on 7/20/17.
 */

public class VehicleAddActivity extends BaseActivity {
    public static final String EXTRA_VEHICLE = "EXTRA_VEHICLE";
    private VehicleAddFragment vehicleAddFragment = VehicleAddFragment.newInstance();

    public static Intent newIntent(Context context) {
        return new Intent(context, VehicleAddActivity.class);
    }

    public static Intent newIntent(Context context, Vehicle vehicle) {
        Intent intent = new Intent(context, VehicleAddActivity.class);
        intent.putExtra(EXTRA_VEHICLE, vehicle);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public Fragment getFragment(){
        Intent intent = getIntent();
        if(intent != null){
            Vehicle vehicle = intent.getParcelableExtra(VehicleAddActivity.EXTRA_VEHICLE);
            vehicleAddFragment.setVehicle(vehicle);
        }
        return vehicleAddFragment;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        vehicleAddFragment.onActivityResultFragment(requestCode, resultCode, data);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_down, R.anim.slide_out_down);
    }
}
