package br.com.apt.ui.myinfo.myinfofragment.pet;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

import br.com.apt.ui.myinfo.myinfofragment.AutoComplete;


/**
 * Created by adminbs on 11/7/16.
 */
public class PetType extends AutoComplete {
    private Date created_at;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    protected PetType(Parcel in) {
        id = in.readInt();
        name = in.readString();
        long tmpCreated_at = in.readLong();
        created_at = tmpCreated_at != -1 ? new Date(tmpCreated_at) : null;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeLong(created_at != null ? created_at.getTime() : -1L);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<PetType> CREATOR = new Parcelable.Creator<PetType>() {
        @Override
        public PetType createFromParcel(Parcel in) {
            return new PetType(in);
        }

        @Override
        public PetType[] newArray(int size) {
            return new PetType[size];
        }
    };
}