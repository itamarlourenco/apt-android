package br.com.apt.ui.MyAdm.myadmFragments.administrator;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import br.com.apt.R;
import br.com.apt.application.BaseActivity;
import br.com.apt.ui.reservations.CalendarFragment;


public class AdministratorKnowMoreActivity extends BaseActivity {

    private AdministratorKnowMoreFragment administratorKnowMoreFragment = AdministratorKnowMoreFragment.newInstance();
    private static final String EXTRA_KNOW_MORE = "EXTRA_KNOW_MORE";

    public static Intent newIntent(Context context, CompanyData companyData) {
        Intent intent = new Intent(context, AdministratorKnowMoreActivity.class);
        intent.putExtra(EXTRA_KNOW_MORE, companyData);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public Fragment getFragment(){
        Intent intent = getIntent();
        if(intent != null){
            CompanyData companyData = intent.getParcelableExtra(AdministratorKnowMoreActivity.EXTRA_KNOW_MORE);
            administratorKnowMoreFragment.setCompanyData(companyData);
        }
        return administratorKnowMoreFragment;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_down, R.anim.slide_out_down);
    }
}
