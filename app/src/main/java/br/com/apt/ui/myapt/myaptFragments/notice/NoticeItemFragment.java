package br.com.apt.ui.myapt.myaptFragments.notice;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.android.volley.Request;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import br.com.apt.R;
import br.com.apt.application.BaseFragment;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.application.ZoomImageView;
import br.com.apt.ui.tickets.tickets.Ticket;
import br.com.apt.util.Logger;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomButton;
import br.com.apt.widget.CustomTextViewLight;


public class NoticeItemFragment extends BaseFragment{

    private Notice notice;

    public void setNotice(Notice notice) {
        this.notice = notice;
    }

    public static NoticeItemFragment newInstance(Notice notice) {
        NoticeItemFragment ticketItemFragment = new NoticeItemFragment();
        ticketItemFragment.setNotice(notice);
        return ticketItemFragment;
    }

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_item_notices, container, false);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if(view != null){
            ZoomImageView image = (ZoomImageView) view.findViewById(R.id.image);
            CustomTextViewLight title = (CustomTextViewLight) view.findViewById(R.id.title);
            CustomTextViewLight message = (CustomTextViewLight) view.findViewById(R.id.message);
            CustomTextViewLight created = (CustomTextViewLight) view.findViewById(R.id.created);
            CustomTextViewLight by = (CustomTextViewLight) view.findViewById(R.id.by);
            CustomButton close = (CustomButton) view.findViewById(R.id.close);
            RelativeLayout baseImage = (RelativeLayout) view.findViewById(R.id.baseImage);


            String urlImage = notice.getImage();
            if(TextUtils.isEmpty(urlImage)){
                baseImage.setVisibility(View.GONE);
            }else{
                Picasso.with(getContext()).load(Volley.getUrlByImage(notice.getImage())).placeholder(R.drawable.placeholder_camera).into(image);
            }

            title.setText(notice.getTitle());
            message.setText(notice.getText());
            created.setText(
                    String.format(getText(R.string.hours_and_time).toString(), Util.dateFormatted(notice.getCreatedAt()), Util.timeFormatted(notice.getCreatedAt(), "HH:mm"))
            );
            by.setText(notice.getUserName());

            readThisNotice();

            close.setOnClickListener(new OnClickListener(){
                @Override
                public void onClick(View v) {
                    getActivity().finish();
                }
            });
        }
    }

    public void readThisNotice(){
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("notices_id", notice.getId());
            Volley volley = new Volley(new Volley.Callback() {
                @Override
                public void result(String result, int type) {
                    if (result != null) {
                        //Logger.t(result);
                    }
                }

                @Override
                public String uri() {
                    return URI.USERS_HAS_NOTICES;
                }
            });
            volley.request(Request.Method.PUT, jsonObject.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}