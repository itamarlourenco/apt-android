package br.com.apt.ui.MyAdm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.model.NotificationHandle;
import br.com.apt.model.OccurrencesNotRead;
import br.com.apt.model.OccurrencesNotReadSend;
import br.com.apt.ui.MyAdm.myadmFragments.Billet.BilletFragment;
import br.com.apt.ui.MyAdm.myadmFragments.SelectTime;
import br.com.apt.ui.MyAdm.myadmFragments.administrator.AdministratorFragment;
import br.com.apt.ui.MyAdm.myadmFragments.register.RegisterFragment;
import br.com.apt.ui.MyAdm.myadmFragments.sac.SacFragment;
import br.com.apt.ui.MyAdm.myadmFragments.term.TermFragment;
import br.com.apt.ui.myapt.myaptFragments.notice.NoticesFragment;
import br.com.apt.widget.PagerFragment;
import br.com.apt.widget.PagerItemFragment;


public class MyAdmFragment extends PagerFragment implements Observer {

    private ArrayList<PagerItemFragment> pagerItems = null;

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            NotificationHandle.SAC = intent.getIntExtra(OccurrencesNotRead.OCCURRENCES_NOT_READ_BROADCAST_TOTAL, 0);
            updateIndicator();
        }
    };

    public static MyAdmFragment newInstance() {
        return new MyAdmFragment();
    }

    public static PagerFragment newInstance(int position) {
        return instanceWithPosition(new MyAdmFragment(), position);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        LocalBroadcastManager.getInstance(getContext()).registerReceiver(receiver, new IntentFilter(OccurrencesNotRead.OCCURRENCES_NOT_READ_BROADCAST));
    }

    @Override
    public ArrayList<PagerItemFragment> getPagerItems() {
        if(pagerItems == null) {
            pagerItems = new ArrayList<>();
            pagerItems.add(new AdministratorFragment());
            pagerItems.add(new RegisterFragment());
            pagerItems.add(new SacFragment());
            pagerItems.add(new BilletFragment());
            pagerItems.add(new TermFragment());
            pagerItems.add((new SelectTime()).setIsClass(SelectTime.CASH_FLOW));
            pagerItems.add((new SelectTime()).setIsClass(SelectTime.ACCOUNT));
        }
        return pagerItems;
    }


    @Override
    public void onResume() {
        super.onResume();
        if(br.com.apt.ui.MyAdm.myadmFragments.sac.billet.BilletFragment.isGoToBillet()){
            br.com.apt.ui.MyAdm.myadmFragments.sac.billet.BilletFragment.setGoToBillet(false);
            pager.setCurrentItem(3);
        }

        updateIndicator();
    }

    @Override
    protected int getDrawableSelected() {
        return R.drawable.background_tab_selected_my_apt;
    }

    @Override
    public void update(Observable observable, Object data) {

        int pos = this.getPositionFragment();
        switch (pos){
            case 0:
                ((NoticesFragment)getPagerItems().get(pos)).onCounterUpdate();
                break;
            case 1:
                //((TicketsFragment)getPagerItems().get(pos)).onCounterUpdate();
                break;
            case 2:
                //((PollsFragment)getPagerItems().get(pos)).onCounterUpdate();
                break;
            case 3:
                break;
            case 4:
                break;
        }

        this.updateIndicator();
    }

    public void onActivityResultFragment(int requestCode, int resultCode, Intent data) {

    }

    /**
     * Callback interface for responding to Notifications Counter
     */
    public interface CounterUpdateListener {

        /**
         * This method will be invoked when the app gets a notification
         * it should update the content of the page in display
         * maybe update counters?
         */
        public void onCounterUpdate();
    }

    @Override
    public void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(receiver);
    }
}
