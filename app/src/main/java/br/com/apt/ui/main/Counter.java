package br.com.apt.ui.main;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import br.com.apt.application.BaseGson;
import br.com.apt.application.ModelObject;

/**
 * Created by adminbs on 1/21/16.
 */
public class Counter implements ModelObject {

    private MyApt myapt;
    private Reception reception;
    private Reservation reservation;
    private int notificationCount = 0;

    public MyApt getMyapt() {
        return myapt;
    }

    public void setMyapt(MyApt myapt) {
        this.myapt = myapt;
    }

    public Reception getReception() {
        return reception;
    }

    public void setReception(Reception reception) {
        this.reception = reception;
    }

    public int getNotificationCount() {
        return notificationCount;
    }

    public void setNotificationCount(int notificationCount) {
        this.notificationCount = notificationCount;
    }

    public Reservation getReservation() {
        return reservation;
    }

    public void setReservation(Reservation reservation) {
        this.reservation = reservation;
    }

    public static class Reservation implements ModelObject{
        private int reservation;

        public Reservation(int reservation) {
            this.reservation = reservation;
        }

        public int getReservation() {
            return reservation;
        }

        public void setReservation(int reservation) {
            this.reservation = reservation;
        }


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.reservation);
        }

        public Reservation() {
        }

        protected Reservation(Parcel in) {
            this.reservation = in.readInt();
        }

        public final Creator<Reservation> CREATOR = new Creator<Reservation>() {
            @Override
            public Reservation createFromParcel(Parcel source) {
                return new Reservation(source);
            }

            @Override
            public Reservation[] newArray(int size) {
                return new Reservation[size];
            }
        };
    }

    public class Reception implements ModelObject{
        private int deliveries;
        private int visits;

        public int getDeliveries() {
            return deliveries;
        }

        public void setDeliveries(int deliveries) {
            this.deliveries = deliveries;
        }

        public int getVisits() {
            return visits;
        }

        public void setVisits(int visits) {
            this.visits = visits;
        }


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.deliveries);
            dest.writeInt(this.visits);
        }

        public Reception() {
        }

        protected Reception(Parcel in) {
            this.deliveries = in.readInt();
            this.visits = in.readInt();
        }

        public final Creator<Reception> CREATOR = new Creator<Reception>() {
            @Override
            public Reception createFromParcel(Parcel source) {
                return new Reception(source);
            }

            @Override
            public Reception[] newArray(int size) {
                return new Reception[size];
            }
        };
    }

    public class MyApt implements ModelObject{
        private int notices;
        private int polls;

        public int getNotices() {
            return notices;
        }

        public void setNotices(int notices) {
            this.notices = notices;
        }

        public int getPolls() {
            return polls;
        }

        public void setPolls(int polls) {
            this.polls = polls;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.notices);
            dest.writeInt(this.polls);
        }

        public MyApt() {
        }

        protected MyApt(Parcel in) {
            this.notices = in.readInt();
            this.polls = in.readInt();
        }

        public final Creator<MyApt> CREATOR = new Creator<MyApt>() {
            @Override
            public MyApt createFromParcel(Parcel source) {
                return new MyApt(source);
            }

            @Override
            public MyApt[] newArray(int size) {
                return new MyApt[size];
            }
        };
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.myapt, flags);
        dest.writeParcelable(this.reception, flags);
        dest.writeInt(this.notificationCount);
    }

    public Counter() {
    }

    protected Counter(Parcel in) {
        this.myapt = in.readParcelable(MyApt.class.getClassLoader());
        this.reception = in.readParcelable(Reception.class.getClassLoader());
        this.notificationCount = in.readInt();
    }

    public static final Creator<Counter> CREATOR = new Creator<Counter>() {
        @Override
        public Counter createFromParcel(Parcel source) {
            return new Counter(source);
        }

        @Override
        public Counter[] newArray(int size) {
            return new Counter[size];
        }
    };
}