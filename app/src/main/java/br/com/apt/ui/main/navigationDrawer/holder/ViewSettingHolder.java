package br.com.apt.ui.main.navigationDrawer.holder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import br.com.apt.R;
import br.com.apt.ui.main.navigationDrawer.NavigationDrawerMenuAdapter;

public class ViewSettingHolder  extends RecyclerView.ViewHolder implements View.OnClickListener {
    public ImageView closeDrawable;
    private NavigationDrawerMenuAdapter.OnClickItemMenu onClickItemMenu;


    public ViewSettingHolder(View itemView, NavigationDrawerMenuAdapter.OnClickItemMenu clickItemMenu) {
        super(itemView);
        onClickItemMenu = clickItemMenu;
        closeDrawable = (ImageView) itemView.findViewById(R.id.closeDrawer);
        closeDrawable.setOnClickListener(this);
    }

    public static View getView(Context context, ViewGroup viewGroup){
        return LayoutInflater.from(context).inflate(R.layout.view_setting_holder, viewGroup, false);
    }

    @Override
    public void onClick(View v) {
        onClickItemMenu.clickClose(v, getAdapterPosition());
    }
}
