package br.com.apt.ui.MyAdm.myadmFragments.sac.occurrence;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.BaseFragment;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.widget.SendSuccessDialog;

/**
 * Created by adminbs on 8/4/17.
 */

public abstract class BaseOccurrence extends BaseFragment implements Volley.Callback {

    private OccurrenceThemeRequest occurrenceTheme;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getSubject();
    }

    public void getSubject(){
        Volley volley = new Volley(this);
        volley.isMeuADM(true);
        volley.showDialog(getActivity());
        volley.request();
    }

    protected abstract String themeUiID();

    protected void callBackOccurrenceTheme(List<OccurrenceSubject> occurrenceTheme){
        if (getSpinner() != null) {
            ArrayAdapter<OccurrenceSubject> adapter = new ArrayAdapter<>(getContext(), R.layout.simple_spinner_dropdown_item, occurrenceTheme);
            getSpinner().setAdapter(adapter);
            getSpinner().setSelection(0);
        }
    }

    @Override
    public void result(String result, int type) {
        try{
            occurrenceTheme = App.getGson().fromJson(result, OccurrenceThemeRequest.class);
            ArrayList<OccurrenceSubject> object = new ArrayList<>();
            object.add(new OccurrenceSubject("", getString(R.string.select_subject_occurrence)));

            for(OccurrenceSubject occurrenceSubject: occurrenceTheme.getObject()){
                object.add(occurrenceSubject);
            }
            callBackOccurrenceTheme(object);
        }catch (Exception e){
            e.printStackTrace();
            SendSuccessDialog.show(getString(R.string.error_generic), null, getFragmentManager(), new SendSuccessDialog.Actions() {
                @Override
                public void back() {
                    getActivity().finish();
                }
            }, SendSuccessDialog.TYPE_ERROR);
        }
    }

    @Override
    public String uri() {
        return String.format(URI.OCCURRENCES_SUBJECTS, themeUiID());
    }

    public Spinner getSpinner(){
        return null;
    }
}
