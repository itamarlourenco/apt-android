package br.com.apt.ui.myinfo.myinfofragment.resident;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.android.volley.Request;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.BaseGson;
import br.com.apt.application.PicturesHelper;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.ui.myinfo.AutoCompleteDialog;
import br.com.apt.ui.myinfo.myinfofragment.AutoComplete;
import br.com.apt.ui.myinfo.myinfofragment.FragmentMyInfoAdd;
import br.com.apt.ui.myinfo.myinfofragment.NewUser;
import br.com.apt.util.Mask;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomButton;
import br.com.apt.widget.CustomEditText;
import br.com.apt.widget.SendSuccessDialog;
import siclo.com.ezphotopicker.api.EZPhotoPick;
import siclo.com.ezphotopicker.api.EZPhotoPickStorage;

/**
 * Created by adminbs on 12/29/17.
 */

public class ResidentAddFragment extends FragmentMyInfoAdd implements Volley.Callback {
    private NewUser resident = new NewUser();
    private List<AutoComplete> residentType;
    private long residentTypeId = 0;
    public Bitmap bitmap = null;

    private CustomEditText name;
    private CustomEditText type;
    private CustomEditText birth;
    private CustomEditText cpf;
    private CustomButton sendResident;
    private RelativeLayout canvasAlterImage;
    private Spinner genders;
    private static ImageView image;
    private Date originalDateSelected = new Date();
    private String genderType = NewUser.TYPE_MALE;

    public void setResident(NewUser resident) {
        this.resident = resident;
    }

    public static ResidentAddFragment newInstance(){
        return new ResidentAddFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_add_resident, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final View view = getView();
        if(view != null) {
            name = (CustomEditText) view.findViewById(R.id.name);
            type = (CustomEditText) view.findViewById(R.id.type);
            birth = (CustomEditText) view.findViewById(R.id.birth);
            cpf = (CustomEditText) view.findViewById(R.id.cpf);
            image = (ImageView) view.findViewById(R.id.image);
            genders = (Spinner) view.findViewById(R.id.genders);
            sendResident = (CustomButton) view.findViewById(R.id.sendResident);
            cpf.addTextChangedListener(Mask.insert("###.###.###-##", cpf));
            canvasAlterImage = (RelativeLayout) view.findViewById(R.id.canvasAlterImage);

            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(), R.array.genders, R.layout.spinner_gender);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            genders.setAdapter(adapter);
            genders.setSelection(0);

            genders.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if(position == 0){
                        genderType = NewUser.TYPE_MALE;
                    }else{
                        genderType = NewUser.TYPE_FEMALE;
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });


            sendResident.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    sendResidentObject();
                }
            });

            type.setOnFocusChangeListener(new View.OnFocusChangeListener(){
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if(hasFocus){
                        openDialogResidentTyoe();
                    }
                }
            });
            type.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openDialogResidentTyoe();
                }
            });


            birth.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showTimePickerDialog(birth);
                }
            });
            birth.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if(hasFocus){
                        showTimePickerDialog(birth);
                    }
                }
            });

            canvasAlterImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alterImage();
                }
            });

            handleResidentByEdit();
        }else{
            SendSuccessDialog.show(getString(R.string.error_generic), null, getFragmentManager(), null, SendSuccessDialog.TYPE_ERROR);
        }
    }

    private void handleResidentByEdit() {
        if(resident != null && resident.getId() > 0){
            name.setText(resident.getName());
            TypeUser typeObject = resident.getTypeObject();
            if(typeObject != null){
                type.setText(typeObject.getName());
                residentTypeId = typeObject.getId();
            }
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            try {
                Date date = format.parse(resident.getBirthday());
                SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
                String datetime = dateformat.format(date);
                birth.setText(datetime);
            } catch (Exception e) {
                e.printStackTrace();
            }
            cpf.setText(resident.getCpf());
            Picasso.with(getContext()).load(Volley.getUrlByImage(resident.getImage())).placeholder(R.drawable.placeholder_camera).into(new Target() {
                @Override
                public void onBitmapLoaded (final Bitmap bitmap, Picasso.LoadedFrom from) {
                    ResidentAddFragment.this.bitmap = bitmap;
                    image.setImageBitmap(bitmap);
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {}

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {}
            });

        }
    }


    private void alterImage(){
        CharSequence[] options = {getString(R.string.takePhoto), getString(R.string.chooseGallery)};
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity());

        builder.setTitle("").setItems(options, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if(which == 0) {
                    PicturesHelper.Camera(getActivity());
                } else if(which == 1) {
                    PicturesHelper.Gallery(getActivity());
                }else{
                    image.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.placeholder_camera));
                }
            }
        });
        builder.create().show();
    }

    @Override
    public void onResume() {
        super.onResume();
        getResidentType();
    }


    private void openDialogResidentTyoe() {
        AutoCompleteDialog.show(getFragmentManager(), residentType, new AutoCompleteDialog.Click() {
            @Override
            public void click(AutoComplete autoComplete) {
                residentTypeId = autoComplete.getId();
                type.setText(autoComplete.getName());
            }
        }, getString(R.string.choose_type));
    }


    public void showTimePickerDialog(final CustomEditText editText){
        Calendar calendar = Calendar.getInstance();
        final int year = calendar.get(Calendar.YEAR);
        final int month = calendar.get(Calendar.MONTH);
        final int day = calendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                editText.setText(
                        String.format("%s/%s/%s", ((dayOfMonth < 10 ? "0" : "") + String.valueOf(dayOfMonth)), (((monthOfYear+1) < 10 ? "0" : "") + String.valueOf((monthOfYear+1))), String.valueOf(year))
                );

                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                calendar.set(Calendar.MONTH, monthOfYear);
                originalDateSelected = calendar.getTime();
            }
        }, year, month, day); //Yes 24 hour time
        datePickerDialog.setTitle(getString(R.string.titleTimePickerDialog));
        datePickerDialog.show();
    }


    private void getResidentType(){
        Volley volley = new Volley(new Volley.Callback() {
            @Override
            public void result(String result, int type) {
                if(result != null){
                    ResidentTypeRequest residentTypeRequest = App.getGson().fromJson(result, ResidentTypeRequest.class);
                    if(residentTypeRequest != null){
                        residentType = residentTypeRequest.getObject();
                    }
                }
            }
            @Override
            public String uri() {
                return URI.RESIDENTS_TYPE;
            }
        });
        volley.showDialog(getActivity());
        volley.request(Request.Method.GET, null);
    }

    public void onActivityResultFragment(int requestCode, int resultCode, Intent data) {
        if(requestCode == EZPhotoPick.PHOTO_PICK_REQUEST_CODE && ResidentAddFragment.image != null){
            try {
                EZPhotoPickStorage ezPhotoPickStorage = new EZPhotoPickStorage(getActivity());
                bitmap = ezPhotoPickStorage.loadLatestStoredPhotoBitmap();

                ResidentAddFragment.image.setImageBitmap(bitmap);
                ResidentAddFragment.image.setVisibility(View.VISIBLE);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private void sendResidentObject(){
        try{
            if(TextUtils.isEmpty(name.getText())){
                throw new Exception(getString(R.string.error_name_resident));
            }

            if(TextUtils.isEmpty(birth.getText())){
                throw new Exception(getString(R.string.error_birth));
            }

            if(residentTypeId <= 0){
                throw new Exception(getString(R.string.error_select_type_resident));
            }

            HashMap<String, String> hashMap = new HashMap<>();
            if(resident != null && resident.getId() > 0){
                hashMap.put("id", String.valueOf(resident.getId()));
            }
            hashMap.put("name", name.getText().toString());
            hashMap.put("residents_type_id", String.valueOf(residentTypeId));
            hashMap.put("gender", genderType);
            hashMap.put("birthday", Util.dateFormatted(originalDateSelected, "yyyy-MM-dd"));
            hashMap.put("cpf", cpf.getText().toString());

            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            if(bitmap != null) {
                bitmap.compress(Bitmap.CompressFormat.PNG, 80, byteArrayOutputStream);
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);
                hashMap.put("image", encoded);
            } else {
                hashMap.put("image", "");
            }

            Volley volley = new Volley(this);
            volley.showDialog(getActivity());
            volley.request(Request.Method.POST, App.getGson().toJson(hashMap));
        } catch (Exception e) {
            SendSuccessDialog.show(e.getMessage(), null, getFragmentManager(), null, SendSuccessDialog.TYPE_ERROR);
        }
    }

    @Override
    protected String getPagerTitle() {
        return null;
    }

    @Override
    protected int getPagerIcon() {
        return 0;
    }

    @Override
    protected HashMap<String, String> save() {
        return null;
    }

    @Override
    protected String getUri() {
        return URI.RESIDENTS;
    }

    @Override
    protected int getTypeRequest() {
        return 0;
    }

    @Override
    protected void saved() {

    }

    @Override
    protected void saved(String json) {

    }

    @Override
    public void result(String result, int type) {
        if(result != null){
            BaseGson baseGson = App.getGson().fromJson(result, BaseGson.class);
            if(baseGson.getStatus() == Volley.STATUS_OK){
                SendSuccessDialog.show(getString((resident != null && resident.getId() > 0) ? R.string.success_resident_edit :R.string.success_resident), null, getFragmentManager(), new SendSuccessDialog.Actions(){
                    @Override
                    public void back() {
                        getActivity().finish();
                    }
                });
            }else{
                SendSuccessDialog.show(getString(R.string.error_generic), null, getFragmentManager(), null, SendSuccessDialog.TYPE_ERROR);
            }
        }
    }

    @Override
    public String uri() {
        return URI.RESIDENTS;
    }
}
