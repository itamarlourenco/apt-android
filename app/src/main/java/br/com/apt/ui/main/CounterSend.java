package br.com.apt.ui.main;

import com.google.gson.annotations.SerializedName;

import br.com.apt.application.SendRequest;

/**
 * Created by adminbs on 1/21/16.
 */
public class CounterSend extends SendRequest {
    @SerializedName("edificioId")
    private String buildId;

    @SerializedName("id")
    private String userId;

    public String getBuildId() {
        return buildId;
    }

    public void setBuildId(String buildId) {
        this.buildId = buildId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
