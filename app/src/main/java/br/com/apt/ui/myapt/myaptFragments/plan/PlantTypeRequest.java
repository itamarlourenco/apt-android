package br.com.apt.ui.myapt.myaptFragments.plan;

import java.util.List;

import br.com.apt.application.BaseGson;
import br.com.apt.application.BaseGsonMeuAdm;
import br.com.apt.ui.myinfo.myinfofragment.AutoComplete;

/**
 * Created by adminbs on 2/23/18.
 */

public class PlantTypeRequest extends BaseGsonMeuAdm {
    public List<AutoComplete> object;

    public List<AutoComplete> getObject() {
        return object;
    }
}

