package br.com.apt.ui.lobby.lobbyFragments.deliveries;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;

import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.apt.R;
import br.com.apt.application.Volley;
import br.com.apt.util.Logger;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomTextViewLight;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by adminbs on 7/8/17.
 */

public class DeliveriesListViewAdapter extends BaseAdapter {

    private Context context;
    private List<Delivery> deliveries;

    public  DeliveriesListViewAdapter(Context context, List<Delivery> deliveries) {
        this.context = context;
        this.deliveries = deliveries;
    }

    @Override
    public int getCount() {
        return deliveries.size();
    }

    @Override
    public Delivery getItem(int position) {
        return deliveries.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;

        if(convertView == null){
            view = LayoutInflater.from(context).inflate(R.layout.row_deliveries, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        }else{
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }

        bindList(holder, position);
        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private void bindList(ViewHolder holder, int position) {
        Delivery item = getItem(position);

        if(position % 2 == 0){
            holder.deliveryBaseBackground.setBackground(ContextCompat.getDrawable(context, R.drawable.button_menu_list_dark));
        }else{
            holder.deliveryBaseBackground.setBackground(ContextCompat.getDrawable(context, R.drawable.button_menu_list_light));
        }

        Picasso.with(context).load(Volley.getUrlByImage(item.getImage())).placeholder(R.drawable.placeholder_camera).into(holder.deliveryImage);

        holder.deliveryLabel.setText(item.isWithdrawn() ? R.string.withdrawal : R.string.waiting_visits);


        holder.deliveryTitle.setText(item.getNameUser());
        holder.deliveryDescription.setText(
                String.format(context.getText(R.string.description_delivery).toString(), Util.dateFormatted(item.getDate()), Util.timeFormatted(item.getDate()))
        );
    }

    public class ViewHolder{
        private RelativeLayout deliveryBaseBackground;
        private CircleImageView deliveryImage;
        private CustomTextViewLight deliveryTitle;
        private CustomTextViewLight deliveryDescription;
        private CustomTextViewLight deliveryLabel;

        public ViewHolder(View view) {
            deliveryBaseBackground = (RelativeLayout) view.findViewById(R.id.deliveryBaseBackground);
            deliveryImage = (CircleImageView) view.findViewById(R.id.deliveryImage);
            deliveryTitle = (CustomTextViewLight) view.findViewById(R.id.deliveryTitle);
            deliveryDescription = (CustomTextViewLight) view.findViewById(R.id.deliveryDescription);
            deliveryLabel = (CustomTextViewLight) view.findViewById(R.id.deliveryLabel);
        }
    }
}
