package br.com.apt.ui.MyAdm;

import android.os.Parcel;
import android.os.Parcelable;

import br.com.apt.application.SendRequest;

/**
 * Created by adminbs on 7/31/17.
 */

public class OccurrencesSend extends SendRequest implements Parcelable {
    public static final String SUBJECT_UUID_REGISTER = "8b3ae6b0-e1aa-452e-996a-8976dc4ee81e";
    public static final String SUBJECT_UUID_PROPOSAL = "5eced66c-9571-4e5a-b6de-3692482b997e";
    public static final String SUBJECT_UUID_DOUBLE = "c92d90c0-6a6c-4fb2-8460-b342f2e2e054";
    public static final String SUBJECT_UUID_OTHERS = "6add9f3f-78b8-40e2-a591-8ad64686e24e";

    private String title;
    private String body;
    private long condominium_key;
    private String subject_uuid;
    private String unit_key;
    private Occurrences.Attachments attachments;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public long getCondominium_key() {
        return condominium_key;
    }

    public void setCondominium_key(long condominium_key) {
        this.condominium_key = condominium_key;
    }

    public String getSubject_uuid() {
        return subject_uuid;
    }

    public void setSubject_uuid(String subject_uuid) {
        this.subject_uuid = subject_uuid;
    }

    public String getUnit_key() {
        return unit_key;
    }

    public void setUnit_key(String unit_key) {
        this.unit_key = unit_key;
    }


    public OccurrencesSend(String title, String body, long condominium_key, String subject_uuid, String unit_key, Occurrences.Attachments attachments) {
        this.title = title;
        this.body = body;
        this.condominium_key = condominium_key;
        this.subject_uuid = subject_uuid;
        this.unit_key = unit_key;
        this.attachments = attachments;
    }

    public OccurrencesSend(String title, String body, long condominium_key, String subject_uuid, String unit_key) {
        this.title = title;
        this.body = body;
        this.condominium_key = condominium_key;
        this.subject_uuid = subject_uuid;
        this.unit_key = unit_key;
    }

    protected OccurrencesSend(Parcel in) {
        title = in.readString();
        body = in.readString();
        condominium_key = in.readLong();
        subject_uuid = in.readString();
        unit_key = in.readString();
        attachments = (Occurrences.Attachments) in.readValue(Occurrences.Attachments.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(body);
        dest.writeLong(condominium_key);
        dest.writeString(subject_uuid);
        dest.writeString(unit_key);
        dest.writeValue(attachments);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<OccurrencesSend> CREATOR = new Parcelable.Creator<OccurrencesSend>() {
        @Override
        public OccurrencesSend createFromParcel(Parcel in) {
            return new OccurrencesSend(in);
        }

        @Override
        public OccurrencesSend[] newArray(int size) {
            return new OccurrencesSend[size];
        }
    };
}