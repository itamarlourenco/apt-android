package br.com.apt.ui.myCondominium.myCondominiumFragments.EdictsAndMinutes;

/**
 * Created by adminbs on 9/3/17.
 */

public class EdictsAndMinutes {
    public static final String TYPE_NOTICE = "notice";
    public static final String TYPE_MINUTES = "minute";

    private String name;
    private String type;
    private String url;

    public EdictsAndMinutes(String name, String type, String url) {
        this.name = name;
        this.type = type;
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
