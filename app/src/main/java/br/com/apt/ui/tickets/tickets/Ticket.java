package br.com.apt.ui.tickets.tickets;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

import br.com.apt.R;
import br.com.apt.application.ModelObject;

/**
 * Created by adminbs on 5/11/16.
 */
public class Ticket implements ModelObject {
    public static final String TODO = "TODO";
    public static final String PROGRESS = "PROGRESS";
    public static final String DONE = "DONE";

    private int id;

    @SerializedName("apartment_id")
    private int apartmentId;

    private String title;
    private String description;

    @SerializedName("image")
    private String imageString;
    private String status;

    @SerializedName("apartment_identification")
    private String apartmentIdentification;

    private Date created_at;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getApartmentId() {
        return apartmentId;
    }

    public void setApartmentId(int apartmentId) {
        this.apartmentId = apartmentId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageString() {
        return imageString;
    }

    public void setImageString(String imageString) {
        this.imageString = imageString;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreatedAt() {
        return created_at;
    }

    public void setCreatedAt(Date created_at) {
        this.created_at = created_at;
    }

    public String getApartmentIdentification() {
        return apartmentIdentification;
    }

    public void setApartmentIdentification(String apartmentIdentification) {
        this.apartmentIdentification = apartmentIdentification;
    }

    protected Ticket(Parcel in) {
        id = in.readInt();
        apartmentId = in.readInt();
        title = in.readString();
        description = in.readString();
        imageString = in.readString();
        status = in.readString();
        apartmentIdentification = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(apartmentId);
        dest.writeString(title);
        dest.writeString(description);
        dest.writeString(imageString);
        dest.writeString(status);
        dest.writeString(apartmentIdentification);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Ticket> CREATOR = new Parcelable.Creator<Ticket>() {
        @Override
        public Ticket createFromParcel(Parcel in) {
            return new Ticket(in);
        }

        @Override
        public Ticket[] newArray(int size) {
            return new Ticket[size];
        }
    };

    public static int translateStatus(String status){
        switch (status){
            case Ticket.TODO:
                return R.string.todo;

            case Ticket.PROGRESS:
                return R.string.progress;

            case Ticket.DONE:
                return R.string.done;
        }

        return R.string.todo;
    }

}