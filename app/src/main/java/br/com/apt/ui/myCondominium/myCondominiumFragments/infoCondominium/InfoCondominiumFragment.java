package br.com.apt.ui.myCondominium.myCondominiumFragments.infoCondominium;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.model.user.User;
import br.com.apt.widget.CustomTextViewLight;
import br.com.apt.widget.PagerItemFragment;

/**
 * Created by adminbs on 9/3/17.
 */

public class InfoCondominiumFragment extends PagerItemFragment implements Volley.Callback {

    private CustomTextViewLight name;
    private CustomTextViewLight address;
    private CustomTextViewLight phone;
    private CustomTextViewLight block;
    private CustomTextViewLight units;
    private CustomTextViewLight document;
    private CustomTextViewLight building_area;
    private CustomTextViewLight construction_year;
    private CustomTextViewLight area_land;
    private CustomTextViewLight year_implementation;


    @Override
    protected String getPagerTitle() {
        return App.getContext().getString(R.string.info_condominium);
    }

    @Override
    protected int getPagerIcon() {
        return R.drawable.ico_menu_info_cond_selected;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_info_condominium, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Volley volley = new Volley(this);
        volley.showDialog(getActivity());
        volley.isMeuADM(true);
        volley.request();

        View view = getView();
        if(view != null){
            name = (CustomTextViewLight) view.findViewById(R.id.name);
            address = (CustomTextViewLight) view.findViewById(R.id.address);
            phone = (CustomTextViewLight) view.findViewById(R.id.phone);
            block = (CustomTextViewLight) view.findViewById(R.id.block);
            units = (CustomTextViewLight) view.findViewById(R.id.units);
            document = (CustomTextViewLight) view.findViewById(R.id.document);
            building_area = (CustomTextViewLight) view.findViewById(R.id.building_area);
            construction_year = (CustomTextViewLight) view.findViewById(R.id.construction_year);
            area_land = (CustomTextViewLight) view.findViewById(R.id.area_land);
            year_implementation = (CustomTextViewLight) view.findViewById(R.id.year_implementation);
        }
    }

    @Override
    public void result(String result, int type) {
        try {
            CondominiumRequest condominiumRequest = App.getGson().fromJson(result, CondominiumRequest.class);
            Condominium condominium = condominiumRequest.getObject();

            if(condominium != null) {
                name.setText(condominium.getName());
                address.setText(condominium.getAddress());
                phone.setText(condominium.getManager_phone());
                document.setText(condominium.getDocument());
                units.setText(String.valueOf(condominium.getApartment_quantity()));
                block.setText(String.valueOf(condominium.getBlocks().size()));
                building_area.setText("---");
                construction_year.setText("---");
                area_land.setText("---");
                year_implementation.setText("---");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public String uri() {
        User user = getUser();
        if(user != null){
            return String.format(URI.CONDOMINIUMS, user.getCondominiumId());
        }
        return "";
    }
}
