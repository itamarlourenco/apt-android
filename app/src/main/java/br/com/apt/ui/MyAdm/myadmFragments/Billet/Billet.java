package br.com.apt.ui.MyAdm.myadmFragments.Billet;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by adminbs on 7/31/17.
 */

public class Billet implements Parcelable {
    private Stats stats;
    private List<Items> items;

    public Stats getStats() {
        return stats;
    }

    public void setStats(Stats stats) {
        this.stats = stats;
    }

    public List<Items> getItems() {
        return items;
    }

    public void setItems(List<Items> items) {
        this.items = items;
    }

    public static class Stats implements Parcelable {
        private String totalBillet;
        private String totalAmount;

        public String getTotalBillet() {
            return totalBillet;
        }

        public void setTotalBillet(String totalBillet) {
            this.totalBillet = totalBillet;
        }

        public String getTotalAmount() {
            return totalAmount;
        }

        public void setTotalAmount(String totalAmount) {
            this.totalAmount = totalAmount;
        }

        protected Stats(Parcel in) {
            totalBillet = in.readString();
            totalAmount = in.readString();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(totalBillet);
            dest.writeString(totalAmount);
        }

        @SuppressWarnings("unused")
        public static final Parcelable.Creator<Stats> CREATOR = new Parcelable.Creator<Stats>() {
            @Override
            public Stats createFromParcel(Parcel in) {
                return new Stats(in);
            }

            @Override
            public Stats[] newArray(int size) {
                return new Stats[size];
            }
        };
    }

    public static class Items implements Parcelable {
        private long condominio;
        private String bloco;
        private String unidade;
        private String emissao;
        private String recibo;
        private Date vencto;
        private Float valor_recibo;
        private String nro_bancario;
        private String carteira;
        private String codigo_barra;
        private String linha_digitavel;
        private String conta_bancaria;
        private String agrupados;

        public long getCondominio() {
            return condominio;
        }

        public void setCondominio(long condominio) {
            this.condominio = condominio;
        }

        public String getBloco() {
            return bloco;
        }

        public void setBloco(String bloco) {
            this.bloco = bloco;
        }

        public String getUnidade() {
            return unidade;
        }

        public void setUnidade(String unidade) {
            this.unidade = unidade;
        }

        public String getEmissao() {
            return emissao;
        }

        public void setEmissao(String emissao) {
            this.emissao = emissao;
        }

        public String getRecibo() {
            return recibo;
        }

        public void setRecibo(String recibo) {
            this.recibo = recibo;
        }

        public Date getVencto() {
            return vencto;
        }

        public void setVencto(Date vencto) {
            this.vencto = vencto;
        }

        public Float getValor_recibo() {
            return valor_recibo;
        }

        public void setValor_recibo(Float valor_recibo) {
            this.valor_recibo = valor_recibo;
        }

        public String getNro_bancario() {
            return nro_bancario;
        }

        public void setNro_bancario(String nro_bancario) {
            this.nro_bancario = nro_bancario;
        }

        public String getCarteira() {
            return carteira;
        }

        public void setCarteira(String carteira) {
            this.carteira = carteira;
        }

        public String getCodigo_barra() {
            return codigo_barra;
        }

        public void setCodigo_barra(String codigo_barra) {
            this.codigo_barra = codigo_barra;
        }

        public String getLinha_digitavel() {
            return linha_digitavel;
        }

        public void setLinha_digitavel(String linha_digitavel) {
            this.linha_digitavel = linha_digitavel;
        }

        public String getConta_bancaria() {
            return conta_bancaria;
        }

        public void setConta_bancaria(String conta_bancaria) {
            this.conta_bancaria = conta_bancaria;
        }

        public String getAgrupados() {
            return agrupados;
        }

        public void setAgrupados(String agrupados) {
            this.agrupados = agrupados;
        }

        protected Items(Parcel in) {
            condominio = in.readLong();
            bloco = in.readString();
            unidade = in.readString();
            emissao = in.readString();
            recibo = in.readString();
            long tmpVencto = in.readLong();
            vencto = tmpVencto != -1 ? new Date(tmpVencto) : null;
            valor_recibo = in.readByte() == 0x00 ? null : in.readFloat();
            nro_bancario = in.readString();
            carteira = in.readString();
            codigo_barra = in.readString();
            linha_digitavel = in.readString();
            conta_bancaria = in.readString();
            agrupados = in.readString();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeLong(condominio);
            dest.writeString(bloco);
            dest.writeString(unidade);
            dest.writeString(emissao);
            dest.writeString(recibo);
            dest.writeLong(vencto != null ? vencto.getTime() : -1L);
            if (valor_recibo == null) {
                dest.writeByte((byte) (0x00));
            } else {
                dest.writeByte((byte) (0x01));
                dest.writeFloat(valor_recibo);
            }
            dest.writeString(nro_bancario);
            dest.writeString(carteira);
            dest.writeString(codigo_barra);
            dest.writeString(linha_digitavel);
            dest.writeString(conta_bancaria);
            dest.writeString(agrupados);
        }

        @SuppressWarnings("unused")
        public static final Parcelable.Creator<Items> CREATOR = new Parcelable.Creator<Items>() {
            @Override
            public Items createFromParcel(Parcel in) {
                return new Items(in);
            }

            @Override
            public Items[] newArray(int size) {
                return new Items[size];
            }
        };
    }

    protected Billet(Parcel in) {
        stats = (Stats) in.readValue(Stats.class.getClassLoader());
        if (in.readByte() == 0x01) {
            items = new ArrayList<Items>();
            in.readList(items, Items.class.getClassLoader());
        } else {
            items = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(stats);
        if (items == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(items);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Billet> CREATOR = new Parcelable.Creator<Billet>() {
        @Override
        public Billet createFromParcel(Parcel in) {
            return new Billet(in);
        }

        @Override
        public Billet[] newArray(int size) {
            return new Billet[size];
        }
    };
}