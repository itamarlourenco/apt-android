package br.com.apt.ui.myapt.myaptFragments.poll;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;

import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;

import br.com.apt.R;
import br.com.apt.application.Volley;
import br.com.apt.ui.lobby.lobbyFragments.visits.Visit;
import br.com.apt.ui.lobby.lobbyFragments.visits.VisitsListViewAdapter;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomTextViewLight;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by adminbs on 7/14/17.
 */

public class PollAdapter extends BaseAdapter {

    private Context context;
    private List<Poll> polls;

    public PollAdapter(Context context, List<Poll> poll) {
        this.context = context;
        this.polls = poll;
    }

    @Override
    public int getCount() {
        return polls.size();
    }

    @Override
    public Poll getItem(int position) {
        return polls.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        PollAdapter.ViewHolder holder;

        if(convertView == null){
            view = LayoutInflater.from(context).inflate(R.layout.row_poll, parent, false);
            holder = new PollAdapter.ViewHolder(view);
            view.setTag(holder);
        }else{
            view = convertView;
            holder = (PollAdapter.ViewHolder) view.getTag();
        }

        bindList(holder, position);
        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private void bindList(PollAdapter.ViewHolder holder, int position) {
        Poll item = getItem(position);

        if(position % 2 == 0){
            holder.baseBackground.setBackground(ContextCompat.getDrawable(context, R.drawable.button_menu_list_dark));
        }else{
            holder.baseBackground.setBackground(ContextCompat.getDrawable(context, R.drawable.button_menu_list_light));
        }

        Picasso.with(context).load(Volley.getUrlByImage(item.getImageUrl())).placeholder(R.drawable.placeholder_camera).into(holder.image);
        holder.title.setText(item.getTitle());
        holder.description.setText(
                String.format(context.getText(R.string.hours_and_time).toString(), Util.dateFormatted(item.getStartAt()), Util.timeFormatted(item.getStartAt(), "HH:mm"))
        );

        String[] date = DateFormat.getDateTimeInstance().format(item.getEndAt()).split(" ");
        if(date.length > 0){
            long differenceDay = Util.getDifferenceDay(new Date(), item.getEndAt());
            if(differenceDay <= 0){
                holder.label.setText(R.string.finishPoll);
                holder.label.setTextColor(ContextCompat.getColor(context, R.color.pale_olive_green));
                holder.label.setBackground(ContextCompat.getDrawable(context, R.drawable.border_radius_green));
            }else{
                holder.label.setText(R.string.not_voted);
                holder.label.setTextColor(ContextCompat.getColor(context, R.color.faded_grey));
                holder.label.setBackground(ContextCompat.getDrawable(context, R.drawable.border_radius_grey_poll));
            }

            if(item.isVoted()){
                holder.label.setText(R.string.voted);
                holder.label.setTextColor(ContextCompat.getColor(context, R.color.faded_blue));
                holder.label.setBackground(ContextCompat.getDrawable(context, R.drawable.border_radius_blue_poll));
            }
        }
    }

    public class ViewHolder{
        private RelativeLayout baseBackground;
        private CircleImageView image;
        private CustomTextViewLight title;
        private CustomTextViewLight description;
        private CustomTextViewLight label;

        public ViewHolder(View view) {
            baseBackground = (RelativeLayout) view.findViewById(R.id.baseBackground);
            image = (CircleImageView) view.findViewById(R.id.image);
            title = (CustomTextViewLight) view.findViewById(R.id.title);
            description = (CustomTextViewLight) view.findViewById(R.id.description);
            label = (CustomTextViewLight) view.findViewById(R.id.label);
        }
    }
}
