package br.com.apt.ui.myinfo.myinfofragment;

import br.com.apt.application.BaseGson;
import br.com.apt.model.user.User;

/**
 * Created by adminbs on 11/3/16.
 */
public class NewUserRequest extends BaseGson {
    private NewUser Object;

    public NewUser getObject() {
        return Object;
    }

    public void setObject(NewUser object) {
        this.Object = object;
    }
}
