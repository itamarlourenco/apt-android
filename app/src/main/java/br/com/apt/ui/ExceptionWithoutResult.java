package br.com.apt.ui;

/**
 * Created by adminbs on 7/8/17.
 */

public class ExceptionWithoutResult extends Exception {

    private int drawableId;

    public ExceptionWithoutResult(String detailMessage, int drawableId) {
        super(detailMessage);
        this.drawableId = drawableId;
    }

    public int getDrawableId() {
        return drawableId;
    }
}
