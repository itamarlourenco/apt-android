package br.com.apt.ui.myapt.myaptFragments.notice;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

import br.com.apt.application.ModelObject;

public class Notice implements ModelObject {
    private long id;

    @SerializedName("created_at")
    private Date createdAt;

    @SerializedName("userId")
    private String userId;

    @SerializedName("titulo")
    private String title;

    @SerializedName("texto")
    private String text;

    @SerializedName("blocoId")
    private long blockId;

    @SerializedName("nomeUsuario")
    private String userName;

    private boolean read;

    @SerializedName("image")
    private String image;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public long getBlockId() {
        return blockId;
    }

    public void setBlockId(long blockId) {
        this.blockId = blockId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public boolean isRead() {
        return read;
    }

    public void setRead(boolean read) {
        this.read = read;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    protected Notice(Parcel in) {
        id = in.readLong();
        long tmpCreatedAt = in.readLong();
        createdAt = tmpCreatedAt != -1 ? new Date(tmpCreatedAt) : null;
        userId = in.readString();
        title = in.readString();
        text = in.readString();
        blockId = in.readLong();
        userName = in.readString();
        read = in.readByte() != 0x00;
        image = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeLong(createdAt != null ? createdAt.getTime() : -1L);
        dest.writeString(userId);
        dest.writeString(title);
        dest.writeString(text);
        dest.writeLong(blockId);
        dest.writeString(userName);
        dest.writeByte((byte) (read ? 0x01 : 0x00));
        dest.writeString(image);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Notice> CREATOR = new Parcelable.Creator<Notice>() {
        @Override
        public Notice createFromParcel(Parcel in) {
            return new Notice(in);
        }

        @Override
        public Notice[] newArray(int size) {
            return new Notice[size];
        }
    };
}