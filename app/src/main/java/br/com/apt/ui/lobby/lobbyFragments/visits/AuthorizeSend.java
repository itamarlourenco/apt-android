package br.com.apt.ui.lobby.lobbyFragments.visits;

import com.google.gson.annotations.SerializedName;

import br.com.apt.application.BaseGson;
import br.com.apt.application.SendRequest;

/**
 * Created by itamarlourenco on 16/01/16.
 */
public class AuthorizeSend extends SendRequest {

    @SerializedName("visits_id")
    private long visitId;

    @SerializedName("apartment_id")
    private String apartmentId;
    private boolean released;


    public long getVisitId() {
        return visitId;
    }

    public void setVisitId(long visitId) {
        this.visitId = visitId;
    }

    public String getApartmentId() {
        return apartmentId;
    }

    public void setApartmentId(String apartmentId) {
        this.apartmentId = apartmentId;
    }

    public boolean isReleased() {
        return released;
    }

    public void setReleased(boolean released) {
        this.released = released;
    }
}
