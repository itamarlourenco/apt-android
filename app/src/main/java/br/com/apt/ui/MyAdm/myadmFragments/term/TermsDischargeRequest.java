package br.com.apt.ui.MyAdm.myadmFragments.term;

import br.com.apt.application.BaseGsonMeuAdm;

/**
 * Created by adminbs on 8/2/17.
 */

class TermsDischargeRequest extends BaseGsonMeuAdm {
    private String object;

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }
}