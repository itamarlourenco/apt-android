package br.com.apt.ui.myinfo.myinfofragment.pet;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

import br.com.apt.R;
import br.com.apt.application.ModelObject;

/**
 * Created by fabricio.bezerra on 25/09/2016.
 */
public class Pet implements ModelObject {

    public static final String SIZE_LARGE = "LARGE";
    public static final String SIZE_MEDIUM = "MEDIUM";
    public static final String SIZE_SMALL = "SMALL";

    private int id;
    private String name;
    private String image;
    private String size;
    private int apartment_id;
    private Date created_at;
    private String color;
    private int pet_breed_id;
    private int pet_type_id;
    private Breed breed;
    private Type type;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public int getApartment_id() {
        return apartment_id;
    }

    public void setApartment_id(int apartment_id) {
        this.apartment_id = apartment_id;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getPet_breed_id() {
        return pet_breed_id;
    }

    public void setPet_breed_id(int pet_breed_id) {
        this.pet_breed_id = pet_breed_id;
    }

    public int getPet_type_id() {
        return pet_type_id;
    }

    public void setPet_type_id(int pet_type_id) {
        this.pet_type_id = pet_type_id;
    }

    public Breed getBreed() {
        return breed;
    }

    public void setBreed(Breed breed) {
        this.breed = breed;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public static class Breed implements Parcelable {
        private int id;
        private String name;
        private int pet_type_id;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getPetTypeId() {
            return pet_type_id;
        }

        public void setPet_type_id(int pet_type_id) {
            this.pet_type_id = pet_type_id;
        }

        protected Breed(Parcel in) {
            id = in.readInt();
            name = in.readString();
            pet_type_id = in.readInt();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(id);
            dest.writeString(name);
            dest.writeInt(pet_type_id);
        }

        @SuppressWarnings("unused")
        public static final Parcelable.Creator<Breed> CREATOR = new Parcelable.Creator<Breed>() {
            @Override
            public Breed createFromParcel(Parcel in) {
                return new Breed(in);
            }

            @Override
            public Breed[] newArray(int size) {
                return new Breed[size];
            }
        };
    }

    public static class Type implements Parcelable {
        private int id;
        private String name;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        protected Type(Parcel in) {
            id = in.readInt();
            name = in.readString();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(id);
            dest.writeString(name);
        }

        @SuppressWarnings("unused")
        public static final Parcelable.Creator<Type> CREATOR = new Parcelable.Creator<Type>() {
            @Override
            public Type createFromParcel(Parcel in) {
                return new Type(in);
            }

            @Override
            public Type[] newArray(int size) {
                return new Type[size];
            }
        };
    }

    protected Pet(Parcel in) {
        id = in.readInt();
        name = in.readString();
        image = in.readString();
        size = in.readString();
        apartment_id = in.readInt();
        long tmpCreated_at = in.readLong();
        created_at = tmpCreated_at != -1 ? new Date(tmpCreated_at) : null;
        color = in.readString();
        pet_breed_id = in.readInt();
        pet_type_id = in.readInt();
        breed = (Breed) in.readValue(Breed.class.getClassLoader());
        type = (Type) in.readValue(Type.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(image);
        dest.writeString(size);
        dest.writeInt(apartment_id);
        dest.writeLong(created_at != null ? created_at.getTime() : -1L);
        dest.writeString(color);
        dest.writeInt(pet_breed_id);
        dest.writeInt(pet_type_id);
        dest.writeValue(breed);
        dest.writeValue(type);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Pet> CREATOR = new Parcelable.Creator<Pet>() {
        @Override
        public Pet createFromParcel(Parcel in) {
            return new Pet(in);
        }

        @Override
        public Pet[] newArray(int size) {
            return new Pet[size];
        }
    };
}