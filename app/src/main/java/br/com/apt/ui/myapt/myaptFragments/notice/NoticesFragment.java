package br.com.apt.ui.myapt.myaptFragments.notice;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.github.ybq.android.spinkit.SpinKitView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import javax.crypto.Cipher;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.application.notification.NotificationSharedPreferences;
import br.com.apt.model.NotificationHandle;
import br.com.apt.model.UsersHasNotices;
import br.com.apt.application.ZoomImageView;
import br.com.apt.model.user.User;
import br.com.apt.model.user.UserData;
import br.com.apt.ui.ExceptionWithoutResult;
import br.com.apt.ui.lobby.lobbyFragments.visits.VisitRequest;
import br.com.apt.ui.lobby.lobbyFragments.visits.VisitsListViewAdapter;
import br.com.apt.ui.myapt.MyAptFragment;
import br.com.apt.util.Logger;
import br.com.apt.util.CustomImage;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomTextView;
import br.com.apt.widget.PagerFragment;
import br.com.apt.widget.PagerItemFragment;

public class NoticesFragment extends PagerItemFragment implements Volley.Callback, MyAptFragment.CounterUpdateListener {
    private List<Notice> noticesList = new ArrayList<>();
    private ListView listView;
    private SpinKitView loader;
    private NoticesAdapter noticesAdapter;

    @Override
    public String uri() {
        return URI.NOTICES;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_notices, container, false);
    }

    @Override
    protected int getIndicator() {
        return NotificationSharedPreferences.getCountNotices();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if(view != null){
            loader = (SpinKitView) view.findViewById(R.id.loader);
            listView = (ListView) view.findViewById(R.id.listView);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Notice notice = noticesList.get(position);
                    notice.setRead(true);
                    startActivity(NoticesActivity.newIntent(getContext(), notice));
                    noticesAdapter.notifyDataSetChanged();
                }
            });
            requestNotices();
        }
    }

    public void requestNotices(){
        Volley volley = new Volley(this);
        volley.request();
    }

    @Override
    public void result(String result, int type) {
        View view = getView();
        try{
            Util.changeVisibility(loader, View.GONE, true);
            Util.changeVisibility(listView, View.VISIBLE, true);
            NoticeRequest noticeRequest = App.getGson().fromJson(result, NoticeRequest.class);
            noticesList = noticeRequest.getObject();
            if(noticesList == null){
                throw new ExceptionWithoutResult(getString(R.string.message_without_notice), R.drawable.img_avisos_no_itens);
            }
            noticesAdapter = new NoticesAdapter(getContext(), noticesList);
            listView.setAdapter(noticesAdapter);
        }catch (ExceptionWithoutResult exceptionWithoutResult){
            Util.showPlaceholder(view, getActivity(), exceptionWithoutResult.getDrawableId(), exceptionWithoutResult.getMessage());
        }catch (Exception exception){
            Util.handlePlaceholderError(view, getActivity());
        }
    }

    @Override
    protected String getPagerTitle() {
        return App.getContext().getString(R.string.notices);
    }

    @Override
    protected int getPagerIcon() {
        return R.drawable.ico_menu_avisos;
    }

    @Override
    public void onCounterUpdate() {
        User user = getUser();
        if(user != null){
            Volley volley = new Volley(this);
            volley.request();
        }
    }


}
