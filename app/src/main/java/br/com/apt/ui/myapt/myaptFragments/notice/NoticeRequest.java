package br.com.apt.ui.myapt.myaptFragments.notice;

import java.util.List;

import br.com.apt.application.BaseGson;
import br.com.apt.ui.lobby.lobbyFragments.safety.Safe;

/**
 * Created by itamarlourenco on 17/01/16.
 */
public class NoticeRequest extends BaseGson {
    public List<Notice> Object;

    public List<Notice> getObject() {
        return Object;
    }
}

