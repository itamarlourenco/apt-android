package br.com.apt.ui.myapt.myaptFragments.chat;

import android.os.Parcel;
import android.os.Parcelable;

import br.com.apt.application.ModelObject;

/**
 * Created by adminbs on 1/12/16.
 */
public class Chat implements ModelObject {
    private String from;
    private String apto;
    private String block;
    private String imageUrl;

    public Chat(String from, String apto, String block, String imageUrl) {
        this.from = from;
        this.apto = apto;
        this.block = block;
        this.imageUrl = imageUrl;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getApto() {
        return apto;
    }

    public void setApto(String apto) {
        this.apto = apto;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    protected Chat(Parcel in) {
        from = in.readString();
        apto = in.readString();
        block = in.readString();
        imageUrl = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(from);
        dest.writeString(apto);
        dest.writeString(block);
        dest.writeString(imageUrl);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Chat> CREATOR = new Parcelable.Creator<Chat>() {
        @Override
        public Chat createFromParcel(Parcel in) {
            return new Chat(in);
        }

        @Override
        public Chat[] newArray(int size) {
            return new Chat[size];
        }
    };
}