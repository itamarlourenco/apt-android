package br.com.apt.ui.trip;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import br.com.apt.R;
import br.com.apt.application.BaseActivity;
import br.com.apt.ui.reservations.CalendarFragment;

public class TripActivity extends BaseActivity {

    public static Intent newIntent(Context context) {
        return new Intent(context, TripActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public Fragment getFragment(){
        return TripFragment.newInstance();
    }

    @Override
    protected int getTitleToolbarColor() {
        return R.color.trip_color;
    }

}