package br.com.apt.ui.MyAdm.myadmFragments.Billet;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;

import br.com.apt.R;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomTextViewLight;

/**
 * Created by adminbs on 7/31/17.
 */

public class BilletAdapter extends BaseAdapter {

    private Context context;
    private Billet billet;

    public BilletAdapter(Context context, Billet billet) {
        this.context = context;
        this.billet = billet;
    }

    @Override
    public int getCount() {
        return billet != null && billet.getItems() != null ? billet.getItems().size() : 0;
    }

    @Override
    public Billet.Items getItem(int position) {
        return billet.getItems().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        BilletAdapter.ViewHolder holder;

        if(convertView == null){
            view = LayoutInflater.from(context).inflate(R.layout.row_billet, parent, false);
            holder = new BilletAdapter.ViewHolder(view);
            view.setTag(holder);
        }else{
            view = convertView;
            holder = (BilletAdapter.ViewHolder) view.getTag();
        }

        bindList(holder, position);
        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private void bindList(BilletAdapter.ViewHolder holder, int position) {
        Billet.Items item = getItem(position);

        if(position % 2 == 0){
            holder.deliveryBaseBackground.setBackground(ContextCompat.getDrawable(context, R.drawable.button_menu_list_dark));
        }else{
            holder.deliveryBaseBackground.setBackground(ContextCompat.getDrawable(context, R.drawable.button_menu_list_light));
        }


        holder.receipt.setText(String.format(context.getString(R.string.receipt), item.getRecibo()));
        holder.receiptValue.setText(context.getString(R.string.money) + " " + item.getValor_recibo());
        holder.limit.setText(Util.dateFormatted(item.getVencto()));
        holder.endDate.setText(Util.dateFormatted(item.getVencto()));

    }

    public class ViewHolder{
        private RelativeLayout deliveryBaseBackground;
        private CustomTextViewLight receipt;
        private CustomTextViewLight receiptValue;
        private CustomTextViewLight limit;
        private CustomTextViewLight endDate;

        public ViewHolder(View view) {
            deliveryBaseBackground = (RelativeLayout) view.findViewById(R.id.deliveryBaseBackground);
            receipt = (CustomTextViewLight) view.findViewById(R.id.receipt);
            receiptValue = (CustomTextViewLight) view.findViewById(R.id.receiptValue);
            limit = (CustomTextViewLight) view.findViewById(R.id.limit);
            endDate = (CustomTextViewLight) view.findViewById(R.id.endDate);;
        }
    }
}
