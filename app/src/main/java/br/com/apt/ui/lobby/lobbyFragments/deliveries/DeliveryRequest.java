package br.com.apt.ui.lobby.lobbyFragments.deliveries;

import java.util.List;

import br.com.apt.application.BaseGson;

/**
 * Created by adminbs on 1/15/16.
 */
public class DeliveryRequest extends BaseGson {
    public List<Delivery> Object;

    public List<Delivery> getObject() {
        return Object;
    }

    public void setObject(List<Delivery> object) {
        Object = object;
    }
}
