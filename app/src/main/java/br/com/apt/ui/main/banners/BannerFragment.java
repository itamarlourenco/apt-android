package br.com.apt.ui.main.banners;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.List;

import br.com.apt.R;
import br.com.apt.application.BaseFragment;
import br.com.apt.model.build.Build;
import br.com.apt.model.user.User;
import br.com.apt.model.user.UserData;
import br.com.apt.ui.main.Sensors;
import br.com.apt.ui.main.SensorsRequest;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomTextView;
import br.com.apt.widget.CustomTextViewLight;


public class BannerFragment extends BaseFragment{

    private static final String ARGS_POSITION = "POSITION_FRAGMENT";
    private static final String ARGS_SENSORS = "ARGS_SENSORS";

    public static BannerFragment newIntent(int position, SensorsRequest sensorsRequest) {
        BannerFragment bannerFragment = new BannerFragment();
        Bundle args = new Bundle();
        args.putInt(ARGS_POSITION, position);
        args.putParcelable(ARGS_SENSORS, sensorsRequest);
        bannerFragment.setArguments(args);
        return bannerFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Bundle bundle = this.getArguments();
        int position = bundle.getInt(ARGS_POSITION, 0);

        SensorsRequest sensorsRequest = bundle.getParcelable(ARGS_SENSORS);
        ViewGroup viewGroup = null;

        if(sensorsRequest != null){
            List<Sensors> sensors = sensorsRequest.getObject();
                if(sensors != null && sensors.size() > 0) {
                    Sensors sensor;
                    if(position == 0){
                        viewGroup = (ViewGroup) inflater.inflate(R.layout.banner_building_fragment, container, false);
                        handleBannerBuilding(viewGroup);
                    }else{
                        viewGroup = (ViewGroup) inflater.inflate(R.layout.banner_numeric_fragment, container, false);
                        try{
                            sensor = sensors.get(position - 1);
                            String description = handleSensorDescription(sensor);

                            if(description.toLowerCase().contains("Umidade".toLowerCase())){
                                handleBannerNumeric(viewGroup, getValueTemp(sensor), description, R.drawable.ic_banner_moisture);
                            }else if(description.toLowerCase().contains("Piscina".toLowerCase())){
                                handleBannerNumeric(viewGroup, getValueTemp(sensor), description, R.drawable.ic_banner_piscina);
                            }else if(description.toLowerCase().contains("Temp. Externa".toLowerCase())){
                                handleBannerNumeric(viewGroup, getValueTemp(sensor), description, R.drawable.ic_banner_temp);
                            }else{
                                String valueAir = getValueAir(sensor);

                                if (valueAir.toLowerCase().contains("ruim")) {
                                    handleBannerNumeric(viewGroup, getString(R.string.bad).toUpperCase(), handleSensorDescription(sensor), R.drawable.ic_banner_bad_air);
                                } else if (valueAir.toLowerCase().contains("regular")) {
                                    handleBannerNumeric(viewGroup, getString(R.string.regular).toUpperCase(), handleSensorDescription(sensor), R.drawable.ic_banner_regular_air);
                                } if (valueAir.toLowerCase().contains("boa")) {
                                    handleBannerNumeric(viewGroup, getString(R.string.good).toUpperCase(), handleSensorDescription(sensor), R.drawable.ic_banner_good_air);
                                }
                            }
                        }catch (IndexOutOfBoundsException e){}
                    }
                }
        }
        return viewGroup;
    }

    private String getValueAir(Sensors sensors) {
        if(sensors != null){
            return sensors.getValue();
        }
        return null;
    }

    private String handleSensorDescription(Sensors sensors) {
        return sensors != null ? sensors.getType().getName() : "";
    }

    private String getValueTemp(Sensors sensors) {
        return getValue(sensors, "ºC");
    }

    private String getValueMoisture(Sensors sensors) {
        return getValue(sensors, "%");
    }

    private String getValuePoll(Sensors sensors) {
        return getValue(sensors, "ºC");
    }

    private String getValue(Sensors sensors, String ind){
        if(sensors != null && sensors.getValue() != null){
            return sensors.getValue();
        }
        return "";
    }

    private void handleBannerNumeric(ViewGroup viewGroup, String numeric, String sensorName, int drawableLeft) {
        CustomTextViewLight numericView = (CustomTextViewLight) viewGroup.findViewById(R.id.numeric);
        CustomTextViewLight numericDesc = (CustomTextViewLight) viewGroup.findViewById(R.id.numericDesc);
        ImageView imageView = (ImageView) viewGroup.findViewById(R.id.imageView);

        numericView.setText(numeric);
        numericDesc.setText(sensorName.toUpperCase());
        imageView.setImageDrawable(ContextCompat.getDrawable(getContext(), drawableLeft));
    }

    private void handleBannerBuilding(ViewGroup viewGroup) {
        CustomTextViewLight buildingName = (CustomTextViewLight) viewGroup.findViewById(R.id.buildingName);
        CustomTextViewLight address = (CustomTextViewLight) viewGroup.findViewById(R.id.address);

        User user = UserData.getUser();
        if(user != null){
            Build build = user.getBuild();

            if(build != null && !TextUtils.isEmpty(build.getName()) && !TextUtils.isEmpty(build.getLocation())){
                buildingName.setText(build.getName().toUpperCase());
                address.setText(build.getLocation().toUpperCase());
            }
        }
    }
}