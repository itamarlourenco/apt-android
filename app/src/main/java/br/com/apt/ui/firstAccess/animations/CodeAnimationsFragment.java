package br.com.apt.ui.firstAccess.animations;

/**
 * Created by adminbs on 9/13/16.
 */

import android.graphics.Paint;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.BaseFragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;

import org.w3c.dom.Text;

import br.com.apt.R;
import br.com.apt.application.BaseFragment;
import br.com.apt.application.BaseGson;
import br.com.apt.application.SendRequest;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.ui.firstAccess.singup.CheckApartmentActivity;
import br.com.apt.ui.firstAccess.singup.SingUpActivity;
import br.com.apt.util.Logger;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomButton;
import br.com.apt.widget.CustomEditText;
import br.com.apt.widget.CustomTextView;

/**
 * Created by adminbs on 9/12/16.
 */
public class CodeAnimationsFragment extends BaseFragment implements View.OnClickListener{

    private CustomEditText code_01;
    private CustomEditText code_02;
    private CustomEditText code_03;
    private CustomEditText code_04;
    private CustomEditText code_05;
    private CustomEditText code_06;


    private static CodeAnimationsListener codeAnimationsListener;

    public static CodeAnimationsFragment newInstance(CodeAnimationsListener codeAnimationsListener){
        CodeAnimationsFragment.codeAnimationsListener = codeAnimationsListener;
        return new CodeAnimationsFragment();
    }



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.code_animate, container, false);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if(view != null){
            CustomTextView findCode = (CustomTextView) view.findViewById(R.id.findCode);
            if(App.isApt()){
                findCode.setVisibility(View.VISIBLE);
            }
            findCode.setPaintFlags(findCode.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

            CustomButton notHaveCode = (CustomButton) view.findViewById(R.id.notHaveCode);
            notHaveCode.setOnClickListener(this);
            findCode.setOnClickListener(this);

            code_01 = (CustomEditText) view.findViewById(R.id.code_01);
            code_02 = (CustomEditText) view.findViewById(R.id.code_02);
            code_03 = (CustomEditText) view.findViewById(R.id.code_03);
            code_04 = (CustomEditText) view.findViewById(R.id.code_04);
            code_05 = (CustomEditText) view.findViewById(R.id.code_05);
            code_06 = (CustomEditText) view.findViewById(R.id.code_06);


            code_01.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if(count > 0){
                        code_02.requestFocus();
                        doCode();
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            code_01.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if(hasFocus){
                        code_01.setText("");
                    }
                }
            });


            code_02.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if(keyCode == KeyEvent.KEYCODE_DEL){
                        code_01.requestFocus();
                    }
                    return false;
                }
            });
            code_02.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if(count > 0){
                        code_03.requestFocus();
                        doCode();
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            code_02.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if(hasFocus){
                        code_02.setText("");
                    }
                }
            });

            code_03.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if(keyCode == KeyEvent.KEYCODE_DEL){
                        code_02.requestFocus();
                    }
                    return false;
                }
            });
            code_03.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if(count > 0){
                        code_04.requestFocus();
                        doCode();
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            code_03.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if(hasFocus){
                        code_03.setText("");
                    }
                }
            });

            code_04.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if(keyCode == KeyEvent.KEYCODE_DEL){
                        code_03.requestFocus();
                    }
                    return false;
                }
            });
            code_04.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if(count > 0){
                        code_05.requestFocus();
                        doCode();
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            code_04.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if(hasFocus){
                        code_04.setText("");
                    }
                }
            });

            code_05.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if(keyCode == KeyEvent.KEYCODE_DEL){
                        code_04.requestFocus();
                    }
                    return false;
                }
            });
            code_05.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if(count > 0){
                        code_06.requestFocus();
                        doCode();
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            code_05.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if(hasFocus){
                        code_05.setText("");
                    }
                }
            });

            code_06.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if(keyCode == KeyEvent.KEYCODE_DEL){
                        code_05.requestFocus();
                    }
                    return false;
                }
            });
            code_06.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if(count > 0){
                        doCode();
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            code_06.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if(hasFocus){
                        code_06.setText("");
                    }
                }
            });

        }

        if(App.isDeviceHml()){
            code_01.setText("0");
            code_02.setText("0");
            code_03.setText("0");
            code_04.setText("0");
            code_05.setText("0");
            code_06.setText("3");
        }
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.findCode){
            startActivity(WithOutCodeActivity.newIntent(getActivity()));
            AnimationFragment.loop = true;
        }else if(v.getId() == R.id.notHaveCode){
            codeAnimationsListener.notHaveCode(v);
        }
    }

    public void doCode(){
        if(!TextUtils.isEmpty(code_01.getText()) && !TextUtils.isEmpty(code_02.getText())
                && !TextUtils.isEmpty(code_03.getText()) && !TextUtils.isEmpty(code_04.getText())
                && !TextUtils.isEmpty(code_05.getText()) && !TextUtils.isEmpty(code_06.getText())){
            final String code = code_01.getText().toString() + code_02.getText().toString() + code_03.getText().toString() + code_04.getText().toString() + code_05.getText().toString() + code_06.getText().toString();

            Volley volley = new Volley(new Volley.Callback() {
                @Override
                public void result(String result, int type) {
                    if(result != null){
                        CodeRequest baseGson = App.getGson().fromJson(result, CodeRequest.class);
                        if(baseGson.getStatus() == Volley.STATUS_OK){
                            Code requestCode = baseGson.getObject();
                            startActivity(CheckApartmentActivity.newIntent(getContext(), requestCode));
                            getActivity().finish();
                        }else{
                            Util.showDialog(getActivity(), baseGson.getMessage());
                        }
                    }
                }

                @Override
                public String uri() {
                    return URI.CODE;
                }
            });
            CodeSend codeSend = new CodeSend();
            codeSend.setCode(code);
            volley.showDialog(getActivity());
            volley.request(Request.Method.POST, codeSend.toJson());
        }
    }


    public interface CodeAnimationsListener{
        void notHaveCode(View view);
    }

    public class CodeSend extends SendRequest {
        private String code;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }
    }

    public class CodeRequest extends BaseGson{
        public Code Object;

        public Code getObject() {
            return Object;
        }

        public void setObject(Code object) {
            this.Object = object;
        }
    }

    public static class Code implements Parcelable {
        private String blocoId;
        private String blockKey;
        private String bloco;
        private String unitKey;
        private String condominiumKey;
        private String condominium;
        private String companyKey;
        private String apto;
        private String id;

        public Code(String blocoId, String blockKey, String bloco, String unitKey, String condominiumKey, String condominium, String companyKey, String apto, String id) {
            this.blocoId = blocoId;
            this.blockKey = blockKey;
            this.bloco = bloco;
            this.condominiumKey = condominiumKey;
            this.condominium = condominium;
            this.companyKey = companyKey;
            this.unitKey = unitKey;
            this.apto = apto;
            this.id = id;
        }

        protected Code(Parcel in) {
            blocoId = in.readString();
            blockKey = in.readString();
            bloco = in.readString();
            condominiumKey = in.readString();
            condominium = in.readString();
            companyKey = in.readString();
            unitKey = in.readString();
            apto = in.readString();
            id = in.readString();
        }

        public String getBlocoId() {
            return blocoId;
        }

        public void setBlocoId(String blocoId) {
            this.blocoId = blocoId;
        }

        public String getBlockKey() {
            return blockKey;
        }

        public void setBlockKey(String blockKey) {
            this.blockKey = blockKey;
        }

        public String getBloco() {
            return bloco;
        }

        public void setBloco(String bloco) {
            this.bloco = bloco;
        }

        public String getCondominiumKey() {
            return condominiumKey;
        }

        public void setCondominiumKey(String condominiumKey) {
            this.condominiumKey = condominiumKey;
        }

        public String getCondominium() {
            return condominium;
        }

        public void setCondominium(String condominium) {
            this.condominium = condominium;
        }

        public String getCompanyKey() {
            return companyKey;
        }

        public void setCompanyKey(String companyKey) {
            this.companyKey = companyKey;
        }

        public String getApto() {
            return apto;
        }

        public void setApto(String apto) {
            this.apto = apto;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUnitKey() {
            return unitKey;
        }

        public void setUnitKey(String unitKey) {
            this.unitKey = unitKey;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(blocoId);
            dest.writeString(blockKey);
            dest.writeString(bloco);
            dest.writeString(condominiumKey);
            dest.writeString(condominium);
            dest.writeString(companyKey);
            dest.writeString(unitKey);
            dest.writeString(apto);
            dest.writeString(id);
        }

        @SuppressWarnings("unused")
        public static final Parcelable.Creator<Code> CREATOR = new Parcelable.Creator<Code>() {
            @Override
            public Code createFromParcel(Parcel in) {
                return new Code(in);
            }

            @Override
            public Code[] newArray(int size) {
                return new Code[size];
            }
        };
    }
}