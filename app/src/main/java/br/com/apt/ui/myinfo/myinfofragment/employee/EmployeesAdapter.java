package br.com.apt.ui.myinfo.myinfofragment.employee;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;

import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.apt.R;
import br.com.apt.application.Volley;
import br.com.apt.widget.CustomTextViewLight;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by adminbs on 7/19/17.
 */

public class EmployeesAdapter extends BaseAdapter {
    private Context context;
    private List<Employees> employeesList;

    public EmployeesAdapter(Context context, List<Employees> employeesList) {
        this.context = context;
        this.employeesList = employeesList;
    }

    @Override
    public int getCount() {
        return employeesList != null ? employeesList.size() : 0;
    }

    @Override
    public Employees getItem(int position) {
        return employeesList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        EmployeesAdapter.ViewHolder holder;
        if(convertView == null){
            view = LayoutInflater.from(context).inflate(R.layout.adapter_employees, parent, false);
            holder = new EmployeesAdapter.ViewHolder(view);
            view.setTag(holder);
        }else{
            view = convertView;
            holder = (EmployeesAdapter.ViewHolder) view.getTag();
        }

        bindList(holder, position);
        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private void bindList(EmployeesAdapter.ViewHolder holder, int position) {
        if(position % 2 == 0){
            holder.baseBackground.setBackground(ContextCompat.getDrawable(context, R.drawable.button_menu_list_dark));
        }else{
            holder.baseBackground.setBackground(ContextCompat.getDrawable(context, R.drawable.button_menu_list_light));
        }

        Employees employees = getItem(position);
        if(employees != null){
            Picasso.with(context).load(Volley.getUrlByImage(employees.getImage())).placeholder(R.drawable.placeholder_camera).into(holder.image);
            holder.title.setText(employees.getName());
            Employees.Type type = employees.getType();
            if(type != null){
                holder.description.setText(type.getName());
            }
        }
    }

    public class ViewHolder{
        private RelativeLayout baseBackground;
        private CircleImageView image;
        private CustomTextViewLight title;
        private CustomTextViewLight description;
        private CustomTextViewLight label;

        public ViewHolder(View view) {
            baseBackground = (RelativeLayout) view.findViewById(R.id.baseBackground);
            image = (CircleImageView) view.findViewById(R.id.deliveryImage);
            title = (CustomTextViewLight) view.findViewById(R.id.deliveryTitle);
            description = (CustomTextViewLight) view.findViewById(R.id.deliveryDescription);
            label = (CustomTextViewLight) view.findViewById(R.id.deliveryLabel);
        }
    }
}
