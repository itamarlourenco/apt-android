package br.com.apt.ui.myinfo.myinfofragment.pet;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import br.com.apt.R;
import br.com.apt.application.BaseActivity;
import br.com.apt.ui.reservations.CalendarFragment;
import br.com.apt.ui.tickets.tickets.TicketAddActivity;

/**
 * Created by adminbs on 7/17/17.
 */

public class MyPetActivity extends BaseActivity {

    private MyPetListFragment myPetAddFragment = MyPetListFragment.newInstance();

    public static Intent newIntent(Context context) {
        return new Intent(context, TicketAddActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public Fragment getFragment(){
        return myPetAddFragment;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        myPetAddFragment.onActivityResultFragment(requestCode, resultCode, data);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_down, R.anim.slide_out_down);
    }
}





