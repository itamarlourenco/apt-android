package br.com.apt.ui.trip;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import br.com.apt.application.SendRequest;

/**
 * Created by adminbs on 5/18/16.
 */
class Travel extends SendRequest implements Parcelable {
    private long id;

    @SerializedName("user_id")
    private long userId;

    private Boolean travel;
    private Boolean registry_entries;
    private Boolean warning_correspondence;
    private Boolean billboard;
    private Boolean tickets;
    private Boolean poll;

    public Travel() {

    }

    public class TravelRequest extends SendRequest{
        private Travel Object;

        public Travel getObject() {
            return Object;
        }

        public void setObject(Travel object) {
            Object = object;
        }
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public Boolean getTravel() {
        return travel;
    }

    public void setTravel(Boolean travel) {
        this.travel = travel;
    }

    public Boolean getRegistryEntries() {
        return registry_entries;
    }

    public void setRegistryEntries(Boolean registry_entries) {
        this.registry_entries = registry_entries;
    }

    public Boolean getWarningCorrespondence() {
        return warning_correspondence;
    }

    public void setWarningCorrespondence(Boolean warning_correspondence) {
        this.warning_correspondence = warning_correspondence;
    }

    public Boolean getBillboard() {
        return billboard;
    }

    public void setBillboard(Boolean billboard) {
        this.billboard = billboard;
    }

    public Boolean getTickets() {
        return tickets;
    }

    public void setTickets(Boolean tickets) {
        this.tickets = tickets;
    }

    public Boolean getPoll() {
        return poll;
    }

    public void setPoll(Boolean poll) {
        this.poll = poll;
    }

    protected Travel(Parcel in) {
        id = in.readLong();
        userId = in.readLong();
        byte travelVal = in.readByte();
        travel = travelVal == 0x02 ? null : travelVal != 0x00;
        byte registry_entriesVal = in.readByte();
        registry_entries = registry_entriesVal == 0x02 ? null : registry_entriesVal != 0x00;
        byte warning_correspondenceVal = in.readByte();
        warning_correspondence = warning_correspondenceVal == 0x02 ? null : warning_correspondenceVal != 0x00;
        byte billboardVal = in.readByte();
        billboard = billboardVal == 0x02 ? null : billboardVal != 0x00;
        byte ticketsVal = in.readByte();
        tickets = ticketsVal == 0x02 ? null : ticketsVal != 0x00;
        byte pollVal = in.readByte();
        poll = pollVal == 0x02 ? null : pollVal != 0x00;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeLong(userId);
        if (travel == null) {
            dest.writeByte((byte) (0x02));
        } else {
            dest.writeByte((byte) (travel ? 0x01 : 0x00));
        }
        if (registry_entries == null) {
            dest.writeByte((byte) (0x02));
        } else {
            dest.writeByte((byte) (registry_entries ? 0x01 : 0x00));
        }
        if (warning_correspondence == null) {
            dest.writeByte((byte) (0x02));
        } else {
            dest.writeByte((byte) (warning_correspondence ? 0x01 : 0x00));
        }
        if (billboard == null) {
            dest.writeByte((byte) (0x02));
        } else {
            dest.writeByte((byte) (billboard ? 0x01 : 0x00));
        }
        if (tickets == null) {
            dest.writeByte((byte) (0x02));
        } else {
            dest.writeByte((byte) (tickets ? 0x01 : 0x00));
        }
        if (poll == null) {
            dest.writeByte((byte) (0x02));
        } else {
            dest.writeByte((byte) (poll ? 0x01 : 0x00));
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Travel> CREATOR = new Parcelable.Creator<Travel>() {
        @Override
        public Travel createFromParcel(Parcel in) {
            return new Travel(in);
        }

        @Override
        public Travel[] newArray(int size) {
            return new Travel[size];
        }
    };
}