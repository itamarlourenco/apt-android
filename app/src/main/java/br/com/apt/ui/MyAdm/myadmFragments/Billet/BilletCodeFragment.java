package br.com.apt.ui.MyAdm.myadmFragments.Billet;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.android.volley.Request;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.BaseFragment;
import br.com.apt.application.BaseGsonMeuAdm;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomTextViewLight;
import br.com.apt.widget.SendSuccessDialog;

/**
 * Created by adminbs on 7/21/17.
 */

public class BilletCodeFragment extends BaseFragment implements Volley.Callback{

    private Billet.Items billet;

    private CustomTextViewLight limitOriginal;
    private CustomTextViewLight dateLimit;
    private CustomTextViewLight valueCondominium;
    private CustomTextViewLight barCode;
    private ListView listView;

    private BilletDetails billetDetails;

    public static Fragment newInstance(Billet.Items item) {
        BilletCodeFragment billetCodeFragment = new BilletCodeFragment();
        billetCodeFragment.setBillet(item);
        return billetCodeFragment;
    }

    public void setBillet(Billet.Items billet) {
        this.billet = billet;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_billits_code, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if(view != null){
            limitOriginal = (CustomTextViewLight) view.findViewById(R.id.limitOriginal);
            dateLimit = (CustomTextViewLight) view.findViewById(R.id.dateLimit);
            valueCondominium = (CustomTextViewLight) view.findViewById(R.id.valueCondominium);
            barCode = (CustomTextViewLight) view.findViewById(R.id.barCode);
            listView = (ListView) view.findViewById(R.id.listView);
            view.findViewById(R.id.copyCode).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    copyAndRedirect();
                }
            });

            view.findViewById(R.id.sendBillet).setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    sendBillet();
                }
            });

            Volley volley = new Volley(this);
            volley.isMeuADM(true);
            volley.showDialog(getActivity());
            volley.request();
        }
    }

    private void sendBillet() {
        Volley volley = new Volley(new Volley.Callback() {
            @Override
            public void result(String result, int type) {
                try{
                    if(result != null){
                        BaseGsonMeuAdm baseGsonMeuAdm = App.getGson().fromJson(result, BaseGsonMeuAdm.class);
                        SendSuccessDialog.show(baseGsonMeuAdm.getMessage(), null, getFragmentManager(), new SendSuccessDialog.Actions() {
                            @Override
                            public void back() {
                                getActivity().finish();
                            }
                        });
                    }
                }catch (Exception e){
                    SendSuccessDialog.show(getString(R.string.error_generic), null, getFragmentManager(), new SendSuccessDialog.Actions() {
                        @Override
                        public void back() {
                            getActivity().finish();
                        }
                    }, SendSuccessDialog.TYPE_ERROR);
                }
            }

            @Override
            public String uri() {
                return String.format(URI.MAIL_BILLET, billet.getRecibo());
            }
        });
        volley.showDialog(getActivity());
        volley.isMeuADM(true);
        volley.request(Request.Method.POST, null);
    }

    @Override
    public void result(String result, int type) {
        try{
            BilletDetailsRequest billetDetailsRequest = App.getGson().fromJson(result, BilletDetailsRequest.class);
            billetDetails = billetDetailsRequest.getObject();
            if(billetDetails != null){
                limitOriginal.setText(Util.dateFormatted(billetDetails.getLimit()));
                dateLimit.setText(Util.dateFormatted(billetDetails.getDead_line()));
                valueCondominium.setText(getString(R.string.money) + " " + billetDetails.getAmount());
                barCode.setText(billetDetails.getPay_line());

                listView.setAdapter(new BilletDetailAdapter(getContext(), billetDetails.getItems()));
                Util.justifyListViewHeightBasedOnChildren(listView);
            }

        }catch (Exception e){
            SendSuccessDialog.show(getString(R.string.error_generic), null, getFragmentManager(), new SendSuccessDialog.Actions() {
                @Override
                public void back() {
                    getActivity().finish();
                }
            }, SendSuccessDialog.TYPE_ERROR);
        }
    }

    @Override
    public String uri() {
        return String.format(URI.BILLET_DETAIL, billet.getRecibo());
    }

    public void copyAndRedirect(){
        ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Activity.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("Pay_line", billetDetails.getPay_line());
        clipboard.setPrimaryClip(clip);

        SendSuccessDialog.show(getString(R.string.procedure), getString(R.string.procedure_bank), getFragmentManager(), new SendSuccessDialog.Actions() {
            @Override
            public void back() {
                startActivity(BankActivity.newIntent(getContext(), billetDetails));
            }
        });
    }
}
