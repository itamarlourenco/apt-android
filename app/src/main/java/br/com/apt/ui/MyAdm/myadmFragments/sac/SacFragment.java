package br.com.apt.ui.MyAdm.myadmFragments.sac;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.Request;
import com.github.ybq.android.spinkit.SpinKitView;

import java.util.List;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.model.NotificationHandle;
import br.com.apt.model.user.User;
import br.com.apt.ui.ExceptionWithoutResult;
import br.com.apt.ui.MyAdm.Occurrences;
import br.com.apt.ui.MyAdm.OccurrencesRequest;
import br.com.apt.util.Logger;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomButton;

/**
 * Created by adminbs on 7/21/17.
 */

public class SacFragment extends br.com.apt.widget.PagerItemFragment implements Volley.Callback {

    private SpinKitView loader;
    private ListView listView;
    private List<Occurrences> occurrences;
    private boolean isNotification = false;
    private CustomButton newSolicitation;

    public void isNotification(boolean isNotification) {
        this.isNotification = isNotification;
    }


    @Override
    protected String getPagerTitle() {
        return App.getContext().getString(R.string.sac);
    }

    @Override
    protected int getPagerIcon() {
        return R.drawable.ico_menu_votacoes;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sac, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if(view != null){
            loader = (SpinKitView) view.findViewById(R.id.loader);
            listView = (ListView) view.findViewById(R.id.listView);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    startActivity(SacDetailsActivity.newIntent(getContext(), occurrences.get(position), true));
                }
            });

            newSolicitation = (CustomButton) view.findViewById(R.id.newSolicitation);
            newSolicitation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(SolicitationAddActivity.newIntent(getContext()));
                    getActivity().overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
                }
            });
        }
        getSacs();
        handleViewIfIsNotification();
    }

    private void handleViewIfIsNotification() {
        if(isNotification){
            newSolicitation.setText(R.string.new_ticket);
            newSolicitation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(OthersOccurrencesActivity.newIntent(getActivity()));
                    getActivity().overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
                }
            });
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getSacs();
    }

    private void getSacs(){
        User user = getUser();
        if (user != null) {
            Volley volley = new Volley(this);
            volley.isMeuADM(true);
            volley.request(Request.Method.GET, null);
        }
    }

    @Override
    public void result(String result, int type) {
        View view = getView();
        try{
            Util.hidePlaceholder(view);
            Util.changeVisibility(loader, View.GONE, true);
            Util.changeVisibility(listView, View.VISIBLE, true);
            OccurrencesRequest occurrencesRequest = App.getGson().fromJson(result, OccurrencesRequest.class);
            occurrences = occurrencesRequest.getObject();

            if(occurrences == null || occurrences.size() <= 0){
                if(isNotification){
                    throw new ExceptionWithoutResult(getString(R.string.message_without_ticket), R.drawable.error);
                }
                throw new ExceptionWithoutResult(getString(R.string.message_without_sac), R.drawable.error);
            }
            listView.setAdapter(new SacAdapter(getContext(), occurrences));
        }catch (ExceptionWithoutResult exceptionWithoutResult){
            Util.showPlaceholder(view, getActivity(), exceptionWithoutResult.getDrawableId(), exceptionWithoutResult.getMessage());
        }catch (Exception exception){
            Util.handlePlaceholderError(view, getActivity());
        }
    }

    @Override
    public String uri() {
        return URI.OCCURRENCES_ORDER_PROTOCOL;
    }

    @Override
    protected int getIndicator() {
        return NotificationHandle.SAC;
    }
}
