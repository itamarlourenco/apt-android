package br.com.apt.ui.myinfo.myinfofragment.employee;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

import br.com.apt.application.ModelObject;

/**
 * Created by fabricio.bezerra on 27/09/2016.
 */
public class Employees implements ModelObject, Parcelable {

    public static final String TODO = "TODO";
    public static final String PROGRESS = "PROGRESS";
    public static final String DONE = "DONE";

    private int id;
    private int apartment_id;
    private String cpf;
    private Date created_at;
    private String document;
    private int domestics_type_id;
    private String end_time;
    private String gender;
    private String image;
    private String name;
    private String start_time;
    private Type type;
    private String work_days;

    public static String getTODO() {
        return TODO;
    }

    public static String getPROGRESS() {
        return PROGRESS;
    }

    public static String getDONE() {
        return DONE;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getApartment_id() {
        return apartment_id;
    }

    public void setApartment_id(int apartment_id) {
        this.apartment_id = apartment_id;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public int getDomestics_type_id() {
        return domestics_type_id;
    }

    public void setDomestics_type_id(int domestics_type_id) {
        this.domestics_type_id = domestics_type_id;
    }

    public String getEndTime() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStartTime() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getWorkDays() {
        return work_days;
    }

    public void setWork_days(String work_days) {
        this.work_days = work_days;
    }

    public static class Type implements Parcelable {
        private int id;
        private String name;
        private Date created_at;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Date getCreated_at() {
            return created_at;
        }

        public void setCreated_at(Date created_at) {
            this.created_at = created_at;
        }

        protected Type(Parcel in) {
            id = in.readInt();
            name = in.readString();
            long tmpCreated_at = in.readLong();
            created_at = tmpCreated_at != -1 ? new Date(tmpCreated_at) : null;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(id);
            dest.writeString(name);
            dest.writeLong(created_at != null ? created_at.getTime() : -1L);
        }

        @SuppressWarnings("unused")
        public static final Parcelable.Creator<Type> CREATOR = new Parcelable.Creator<Type>() {
            @Override
            public Type createFromParcel(Parcel in) {
                return new Type(in);
            }

            @Override
            public Type[] newArray(int size) {
                return new Type[size];
            }
        };
    }


    protected Employees(Parcel in) {
        id = in.readInt();
        apartment_id = in.readInt();
        cpf = in.readString();
        long tmpCreated_at = in.readLong();
        created_at = tmpCreated_at != -1 ? new Date(tmpCreated_at) : null;
        document = in.readString();
        domestics_type_id = in.readInt();
        end_time = in.readString();
        gender = in.readString();
        image = in.readString();
        name = in.readString();
        start_time = in.readString();
        type = (Type) in.readValue(Type.class.getClassLoader());
        work_days = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(apartment_id);
        dest.writeString(cpf);
        dest.writeLong(created_at != null ? created_at.getTime() : -1L);
        dest.writeString(document);
        dest.writeInt(domestics_type_id);
        dest.writeString(end_time);
        dest.writeString(gender);
        dest.writeString(image);
        dest.writeString(name);
        dest.writeString(start_time);
        dest.writeValue(type);
        dest.writeString(work_days);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Employees> CREATOR = new Parcelable.Creator<Employees>() {
        @Override
        public Employees createFromParcel(Parcel in) {
            return new Employees(in);
        }

        @Override
        public Employees[] newArray(int size) {
            return new Employees[size];
        }
    };
}