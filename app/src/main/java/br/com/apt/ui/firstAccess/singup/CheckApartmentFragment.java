package br.com.apt.ui.firstAccess.singup;

import android.annotation.TargetApi;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.VideoView;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.BaseActivity;
import br.com.apt.application.BaseFragment;
import br.com.apt.ui.firstAccess.animations.BaseFragmentActivity;
import br.com.apt.ui.firstAccess.animations.CodeAnimationsFragment;
import br.com.apt.ui.firstAccess.animations.LoginAnimationFragment;
import br.com.apt.widget.CustomEditText;
import br.com.apt.widget.CustomTextView;
import br.com.apt.widget.CustomTextViewLight;

/**
 * Created by adminbs on 9/16/16.
 */
public class CheckApartmentFragment extends BaseFragment {

    private VideoView videoView;
    private RelativeLayout baseVideoAnimations;

    public static Fragment newInstance() {
        return new CheckApartmentFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.check_apartment_fragment, container, false);
    }


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if(view != null){

            Intent intent = getActivity().getIntent();
            Bundle bundle = intent.getExtras();
            final CodeAnimationsFragment.Code code = bundle.getParcelable(CheckApartmentActivity.EXTRA_CODE);


            if(code != null){
                ((CustomTextViewLight) view.findViewById(R.id.checkApto)).setText(
                        String.format(getString(R.string.checkApto), code.getApto(), code.getBloco(), code.getCondominium())
                );

                (view.findViewById(R.id.no)).setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v) {
                        startActivity(BaseFragmentActivity.newIntent(getActivity(),true));
                        getActivity().finish();
                    }
                });

                (view.findViewById(R.id.yes)).setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v) {
                        startActivity(SingUpActivity.newIntent(getActivity(), code));
                        getActivity().finish();
                    }
                });
            }else{
                getActivity().finish();
            }


        }
    }
}
