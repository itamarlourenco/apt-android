package br.com.apt.ui.firstAccess.singup;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import br.com.apt.application.FullScreenBaseActivity;
import br.com.apt.ui.firstAccess.animations.CodeAnimationsFragment;
import br.com.apt.ui.reservations.CalendarFragment;

public class SingUpActivity extends FullScreenBaseActivity {

    public static final String EXTRA_CODE = "EXTRA_CODE";

    public static Intent newIntent(Context context, CodeAnimationsFragment.Code code) {
        Intent intent = new Intent(context, SingUpActivity.class);
        intent.putExtra(SingUpActivity.EXTRA_CODE, code);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public Fragment getFragment(){
        return SingUpFragment.newInstance();
    }

    @Override
    protected boolean checkLogin() {
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        SingUpFragment singUpFragment = (SingUpFragment) getFragment();
        singUpFragment.onActivityResultFragment(requestCode, resultCode, data);
    }

}