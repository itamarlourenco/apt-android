package br.com.apt.ui.trip;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;

import com.android.volley.Request;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.BaseFragment;
import br.com.apt.application.SendRequest;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.util.Logger;
import br.com.apt.widget.CustomEditText;
import br.com.apt.widget.CustomTextView;

public class TripFragment extends BaseFragment implements CompoundButton.OnCheckedChangeListener {

    private CustomTextView customTextViewTravel;
    private Switch switchTravel;

    private CustomTextView customTextViewWarningCorrespondence;
    private Switch switchWarningCorrespondence;

    private CustomTextView customTextViewRegistryEntries;
    private Switch switchRegistryEntries;

    private CustomTextView customTextViewBillboard;
    private Switch switchBillboard;

    private CustomTextView customTextViewTickets;
    private Switch switchTickets;

    private CustomTextView customTextViewPoll;
    private Switch switchPoll;

    private LinearLayout canvas;

    private static Boolean isFirstTime = false;


    public static TripFragment newInstance() {
        return new TripFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_trip, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if(view != null){
            customTextViewTravel = (CustomTextView) view.findViewById(R.id.customTextViewTravel);
            switchTravel = (Switch) view.findViewById(R.id.switchTravel);
            switchTravel.setOnCheckedChangeListener(this);


            customTextViewWarningCorrespondence = (CustomTextView) view.findViewById(R.id.customTextViewWarningCorrespondence);
            switchWarningCorrespondence = (Switch) view.findViewById(R.id.switchWarningCorrespondence);
            switchWarningCorrespondence.setOnCheckedChangeListener(this);

            customTextViewRegistryEntries = (CustomTextView) view.findViewById(R.id.customTextViewRegistryEntries);
            switchRegistryEntries = (Switch) view.findViewById(R.id.switchRegistryEntries);
            switchRegistryEntries.setOnCheckedChangeListener(this);

            customTextViewBillboard = (CustomTextView) view.findViewById(R.id.customTextViewBillboard);
            switchBillboard = (Switch) view.findViewById(R.id.switchBillboard);
            switchBillboard.setOnCheckedChangeListener(this);

            customTextViewTickets = (CustomTextView) view.findViewById(R.id.customTextViewTickets);
            switchTickets = (Switch) view.findViewById(R.id.switchTickets);
            switchTickets.setOnCheckedChangeListener(this);

            customTextViewPoll = (CustomTextView) view.findViewById(R.id.customTextViewPoll);
            switchPoll = (Switch) view.findViewById(R.id.switchPoll);
            switchPoll.setOnCheckedChangeListener(this);

            canvas = (LinearLayout) view.findViewById(R.id.canvas);

            Volley volley = new Volley(new Volley.Callback() {
                @Override
                public void result(String result, int type) {
                    if(result != null){
                        Travel.TravelRequest travelRequest = App.getGson().fromJson(result, Travel.TravelRequest.class);
                        if(travelRequest != null){
                            Travel travel = travelRequest.getObject();
                            if(travel != null){
                                switchTravel.setChecked(travel.getTravel());
                                switchWarningCorrespondence.setChecked(travel.getWarningCorrespondence());
                                switchRegistryEntries.setChecked(travel.getRegistryEntries());
                                switchBillboard.setChecked(travel.getBillboard());
                                switchTickets.setChecked(travel.getTickets());
                                switchPoll.setChecked(travel.getPoll());
                            }
                        }
                    }
                }

                @Override
                public String uri() {
                    return URI.TRAVEL;
                }
            });
            volley.showDialog(getActivity());
            volley.request();

            setAllSwitch(switchTravel.isChecked());
        }
    }


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()){
            case R.id.switchTravel:
                setAllSwitch(switchTravel.isChecked());
                break;
        }

        if(TripFragment.isFirstTime){
            sendToServer();
        }else{
            TripFragment.isFirstTime = true;
        }
    }

    private void setAllSwitch(boolean check){

        switchWarningCorrespondence.setEnabled(check);
        switchRegistryEntries.setEnabled(check);
        switchBillboard.setEnabled(check);
        switchTickets.setEnabled(check);
        switchPoll.setEnabled(check);

        if(!check){
            switchPoll.setChecked(check);
            switchTickets.setChecked(check);
            switchBillboard.setChecked(check);
            switchRegistryEntries.setChecked(check);
            switchWarningCorrespondence.setChecked(check);
        }


        setAllTitle(check);
    }

    private void setAllTitle(boolean enabled){
        if(!enabled){
            canvas.setBackgroundResource(R.color.background_trip_selected);

            customTextViewTravel.setTextColor(ContextCompat.getColor(getContext(), R.color.tripFirstElement));
            customTextViewWarningCorrespondence.setTextColor(ContextCompat.getColor(getContext(), R.color.colorDisabledTrip));
            customTextViewRegistryEntries.setTextColor(ContextCompat.getColor(getContext(), R.color.colorDisabledTrip));
            customTextViewBillboard.setTextColor(ContextCompat.getColor(getContext(), R.color.colorDisabledTrip));
            customTextViewTickets.setTextColor(ContextCompat.getColor(getContext(), R.color.colorDisabledTrip));
            customTextViewPoll.setTextColor(ContextCompat.getColor(getContext(), R.color.colorDisabledTrip));
        }else{
            canvas.setBackgroundResource(R.color.trip_color);

            customTextViewTravel.setTextColor(ContextCompat.getColor(getContext(), R.color.tripFirstElement));
            customTextViewWarningCorrespondence.setTextColor(ContextCompat.getColor(getContext(), R.color.tripTitleColor));
            customTextViewRegistryEntries.setTextColor(ContextCompat.getColor(getContext(), R.color.tripTitleColor));
            customTextViewBillboard.setTextColor(ContextCompat.getColor(getContext(), R.color.tripTitleColor));
            customTextViewTickets.setTextColor(ContextCompat.getColor(getContext(), R.color.tripTitleColor));
            customTextViewPoll.setTextColor(ContextCompat.getColor(getContext(), R.color.tripTitleColor));
        }
    }

    private void sendToServer() {
        Volley volley = new Volley(new Volley.Callback() {
            @Override
            public void result(String result, int type) {
                Logger.d(result);
            }

            @Override
            public String uri() {
                return URI.TRAVEL;
            }
        });

        Travel travel = new Travel();
        travel.setTravel(switchTravel.isChecked());
        travel.setWarningCorrespondence(switchWarningCorrespondence.isChecked());
        travel.setRegistryEntries(switchRegistryEntries.isChecked());
        travel.setBillboard(switchBillboard.isChecked());
        travel.setTickets(switchTickets.isChecked());
        travel.setPoll(switchPoll.isChecked());

        volley.request(Request.Method.POST, travel.toJson());
    }

}
