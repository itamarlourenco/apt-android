package br.com.apt.ui.trip;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.util.Util;

/**
 * Created by adminbs on 1/20/16.
 */
public enum TripItem {
    ACTIVE_MODE_TRIP {
        @Override
        public boolean isCheck() {
            return TripPreference.isActivateMode();
        }

        @Override
        public String toString() {
            return Util.getItemString(App.getContext(), R.array.trip, 0);
        }
    },
    CHECK_IN {
        @Override
        public boolean isCheck() {
            return TripPreference.isVisits();
        }

        @Override
        public String toString() {
            return Util.getItemString(App.getContext(), R.array.trip, 1);
        }
    },
    NOTICE {
        @Override
        public boolean isCheck() {
            return TripPreference.isDelivery();
        }

        @Override
        public String toString() {
            return Util.getItemString(App.getContext(), R.array.trip, 2);
        }
    },
    BILLBOARD {
        @Override
        public boolean isCheck() {
            return TripPreference.isBillboard();
        }

        @Override
        public String toString() {
            return Util.getItemString(App.getContext(), R.array.trip, 3);
        }
    },
    MESSAGE {
        @Override
        public boolean isCheck() {
            return TripPreference.isNotice();
        }

        @Override
        public String toString() {
            return Util.getItemString(App.getContext(), R.array.trip, 4);
        }
    },
    POLL {
        @Override
        public boolean isCheck() {
            return TripPreference.isPoll();
        }

        @Override
        public String toString() {
            return Util.getItemString(App.getContext(), R.array.trip, 5);
        }
    };

    public abstract boolean isCheck();
}
