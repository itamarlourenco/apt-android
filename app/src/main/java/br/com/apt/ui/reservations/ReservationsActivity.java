package br.com.apt.ui.reservations;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import br.com.apt.application.BaseActivity;

/**
 * Created by andre on 5/15/17.
 */

public class ReservationsActivity extends BaseActivity {

    public static Intent newIntent(Context context) {
        return new Intent(context, ReservationsActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public Fragment getFragment() {
        return ReservationsFragment.newInstance();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}