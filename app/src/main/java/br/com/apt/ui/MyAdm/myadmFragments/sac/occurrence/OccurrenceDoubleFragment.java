package br.com.apt.ui.MyAdm.myadmFragments.sac.occurrence;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;


import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.event.ProgressEvent;
import com.amazonaws.event.ProgressListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.android.volley.Request;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.BaseFragment;
import br.com.apt.application.BaseGsonMeuAdm;
import br.com.apt.application.Config;
import br.com.apt.application.PicturesHelper;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.model.user.User;
import br.com.apt.model.user.UserData;
import br.com.apt.ui.MyAdm.OccurrenceRequest;
import br.com.apt.ui.MyAdm.Occurrences;
import br.com.apt.ui.MyAdm.OccurrencesSend;
import br.com.apt.ui.tickets.tickets.TicketsAddFragment;
import br.com.apt.util.Logger;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomEditText;
import br.com.apt.widget.SendSuccessDialog;
import dmax.dialog.SpotsDialog;
import siclo.com.ezphotopicker.api.EZPhotoPick;
import siclo.com.ezphotopicker.api.EZPhotoPickStorage;

/**
 * Created by adminbs on 8/2/17.
 */

public class OccurrenceDoubleFragment extends BaseFragment implements Volley.Callback{

    private CustomEditText message;

    public static Bitmap bitmapPhoto;
    public static Bitmap bitmapReceipt;
    public static ImageView photo;
    public static ImageView receipt;


    private boolean isPhoto = true;

    private String namePhoto = Util.generateName("billet");
    private String nameReceipt = Util.generateName("receipt");
    private String pathPhoto = Config.Amazon.handleLocation(namePhoto);
    private String pathReceipt = Config.Amazon.handleLocation(nameReceipt);

    private SpotsDialog dialog;
    private User user = UserData.getUser();


    public static OccurrenceDoubleFragment newInstance(){
        return new OccurrenceDoubleFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.occurrence_double_play, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if (view != null) {
            dialog = new SpotsDialog(getActivity(), R.style.AlertDialogCustom);
            photo = (ImageView) view.findViewById(R.id.photo);
            message = (CustomEditText) view.findViewById(R.id.message);
            photo.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    isPhoto = true;
                    alterImage();
                }
            });
            receipt = (ImageView) view.findViewById(R.id.receipt);
            receipt.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    isPhoto = false;
                    alterImage();
                }
            });

            view.findViewById(R.id.sendSac).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    handlePhotos();
                }
            });
        }
    }

    private void handlePhotos() {
        if(TextUtils.isEmpty(message.getText())){
            SendSuccessDialog.show(getString(R.string.error_add_photo_message), null, getFragmentManager());
            return;
        }

        if(bitmapPhoto == null){
            SendSuccessDialog.show(getString(R.string.error_add_photo_billet), null, getFragmentManager());
            return;
        }

        if(bitmapReceipt == null){
            SendSuccessDialog.show(getString(R.string.error_add_photo_receipt), null, getFragmentManager());
            return;
        }

        if(dialog != null && !dialog.isShowing()){
            dialog.show();
        }

        final File finalFileBillet = Util.createImageFile();
        if(bitmapPhoto != null && finalFileBillet != null){
            Util.sendBitmapToFile(finalFileBillet, bitmapPhoto);
        }

        final File finalFileReceipt = Util.createImageFile();
        if(finalFileReceipt != null && finalFileBillet != null){
            Util.sendBitmapToFile(finalFileReceipt, bitmapReceipt);
        }

        new AsyncTask<Void, Void, Void>(){
            @Override
            protected Void doInBackground(Void... params) {
                AmazonS3Client s3Client = new AmazonS3Client(new BasicAWSCredentials(Config.Amazon.ACCESS_KEY, Config.Amazon.PUBLIC_KEY));

                PutObjectRequest requestBillet = new PutObjectRequest(Config.Amazon.BUCKET, String.format(Config.Amazon.PATH, namePhoto), finalFileBillet);
                s3Client.putObject(requestBillet);

                PutObjectRequest requestReceipt = new PutObjectRequest(Config.Amazon.BUCKET, String.format(Config.Amazon.PATH, nameReceipt), finalFileReceipt);
                s3Client.putObject(requestReceipt);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                initVolley();
            }
        }.execute();
    }

    public void initVolley(){
        List<Occurrences.Attachments> attachmentsList = new ArrayList<>();
        attachmentsList.add(new Occurrences.Attachments(namePhoto + ".jpg", namePhoto, pathPhoto));
        attachmentsList.add(new Occurrences.Attachments(nameReceipt + ".jpg", nameReceipt, pathReceipt));

        OccurrenceDouble occurrenceDouble = new OccurrenceDouble(
                OccurrencesSend.SUBJECT_UUID_DOUBLE,
                getString(R.string.title_double_pay),
                message.getText().toString(),
                2,
                user.getCondominiumId(),
                user.getBlockId(),
                user.getApto(),
                attachmentsList
        );

        Volley volley = new Volley(this);
        volley.isMeuADM(true);
        volley.request(Request.Method.POST, occurrenceDouble.toJson());
    }

    public void alterImage(){
        CharSequence[] options = {getString(R.string.takePhoto), getString(R.string.chooseGallery)};
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity());
        builder.setTitle("").setItems(options, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if(which == 0) {
                            PicturesHelper.Camera(getActivity());
                        } else if(which == 1) {
                            PicturesHelper.Gallery(getActivity());
                        }
                    }
                });
        builder.create().show();
    }

    public void onActivityResultFragment(int requestCode, int resultCode, Intent data) {
        if(requestCode == EZPhotoPick.PHOTO_PICK_REQUEST_CODE && resultCode != 0){
            try {
                EZPhotoPickStorage ezPhotoPickStorage = new EZPhotoPickStorage(getActivity());
                Bitmap bitmap = ezPhotoPickStorage.loadLatestStoredPhotoBitmap();

                if(isPhoto){
                    OccurrenceDoubleFragment.bitmapPhoto = bitmap;
                    OccurrenceDoubleFragment.photo.setImageBitmap(bitmap);
                    OccurrenceDoubleFragment.photo.setScaleType(ImageView.ScaleType.CENTER_CROP);
                }else{
                    OccurrenceDoubleFragment.bitmapReceipt = bitmap;
                    OccurrenceDoubleFragment.receipt.setImageBitmap(bitmap);
                    OccurrenceDoubleFragment.receipt.setScaleType(ImageView.ScaleType.CENTER_CROP);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void result(String result, int type) {
        try{
            dialog.dismiss();
            BaseGsonMeuAdm baseGsonMeuAdm = App.getGson().fromJson(result, BaseGsonMeuAdm.class);
            SendSuccessDialog.show(baseGsonMeuAdm.getMessage(), null, getFragmentManager(), new SendSuccessDialog.Actions() {
                @Override
                public void back() {
                    getActivity().finish();
                }
            }, SendSuccessDialog.TYPE_DIALOG);

        }catch (Exception e){
            SendSuccessDialog.show(getString(R.string.error_generic), null, getFragmentManager(), new SendSuccessDialog.Actions() {
                @Override
                public void back() {
                    getActivity().finish();
                }
            }, SendSuccessDialog.TYPE_ERROR);
        }
    }

    @Override
    public String uri() {
        return URI.OCCURRENCES;
    }
}
