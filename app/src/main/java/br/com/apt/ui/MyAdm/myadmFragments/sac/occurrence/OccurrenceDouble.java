package br.com.apt.ui.MyAdm.myadmFragments.sac.occurrence;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

import br.com.apt.application.SendRequest;
import br.com.apt.ui.MyAdm.Occurrences;

/**
 * Created by adminbs on 8/3/17.
 */

public class OccurrenceDouble extends SendRequest implements Parcelable {
    private String subject_uuid;
    private String title;
    private String body;
    private int priority;
    private String condominium_key;
    private String block_key;
    private String unit_key;
    private List<Occurrences.Attachments> attachments;

    public String getSubject_uuid() {
        return subject_uuid;
    }

    public void setSubject_uuid(String subject_uuid) {
        this.subject_uuid = subject_uuid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public String getCondominium_key() {
        return condominium_key;
    }

    public void setCondominium_key(String condominium_key) {
        this.condominium_key = condominium_key;
    }

    public String getBlock_key() {
        return block_key;
    }

    public void setBlock_key(String block_key) {
        this.block_key = block_key;
    }

    public String getUnit_key() {
        return unit_key;
    }

    public void setUnit_key(String unit_key) {
        this.unit_key = unit_key;
    }

    public List<Occurrences.Attachments> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<Occurrences.Attachments> attachments) {
        this.attachments = attachments;
    }

    public OccurrenceDouble(String subject_uuid, String title, String body, int priority, String condominium_key, String block_key, String unit_key, List<Occurrences.Attachments> attachments) {
        this.subject_uuid = subject_uuid;
        this.title = title;
        this.body = body;
        this.priority = priority;
        this.condominium_key = condominium_key;
        this.block_key = block_key;
        this.unit_key = unit_key;
        this.attachments = attachments;
    }

    protected OccurrenceDouble(Parcel in) {
        subject_uuid = in.readString();
        title = in.readString();
        body = in.readString();
        priority = in.readInt();
        condominium_key = in.readString();
        block_key = in.readString();
        unit_key = in.readString();
        if (in.readByte() == 0x01) {
            attachments = new ArrayList<Occurrences.Attachments>();
            in.readList(attachments, Occurrences.Attachments.class.getClassLoader());
        } else {
            attachments = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(subject_uuid);
        dest.writeString(title);
        dest.writeString(body);
        dest.writeInt(priority);
        dest.writeString(condominium_key);
        dest.writeString(block_key);
        dest.writeString(unit_key);
        if (attachments == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(attachments);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<OccurrenceDouble> CREATOR = new Parcelable.Creator<OccurrenceDouble>() {
        @Override
        public OccurrenceDouble createFromParcel(Parcel in) {
            return new OccurrenceDouble(in);
        }

        @Override
        public OccurrenceDouble[] newArray(int size) {
            return new OccurrenceDouble[size];
        }
    };
}