package br.com.apt.ui.lobby;

import android.content.Intent;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.ui.lobby.lobbyFragments.deliveries.DeliveriesFragment;
import br.com.apt.ui.lobby.lobbyFragments.moreVisits.MoreVisitsFragment;
import br.com.apt.ui.lobby.lobbyFragments.safety.SafetyFragment;
import br.com.apt.ui.lobby.lobbyFragments.visits.VisitsFragment;
import br.com.apt.ui.myinfo.myinfofragment.FragmentMyInfoAdd;
import br.com.apt.widget.PagerFragment;
import br.com.apt.widget.PagerItemFragment;

public class LobbyFragment extends PagerFragment {

    private VisitsFragment visitsFragment = VisitsFragment.newInstance();
    private ArrayList<PagerItemFragment> pagerItems = null;


    public static PagerFragment newInstance(int position) {
        return instanceWithPosition(new LobbyFragment(), position);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public ArrayList<PagerItemFragment> getPagerItems() {
        if(pagerItems == null) {
            pagerItems = new ArrayList<>();
            pagerItems.add(DeliveriesFragment.newInstance());
            pagerItems.add(visitsFragment);
            pagerItems.add(new SafetyFragment());
            pagerItems.add(new br.com.apt.ui.lobby.lobbyFragments.phone.PhoneFragment());
        }
        return pagerItems;
    }

    @Override
    protected int getDrawableSelected() {
        return R.drawable.background_tab_selected_lobby;
    }

    public void update() {
        //Add
        int pos = this.getPositionFragment();
        switch (pos){
            case 0:
                //((DeliveriesFragment)getPagerItems().get(pos)).onCounterUpdate();
                break;
            case 1:
                //((VisitsFragment)getPagerItems().get(pos)).onCounterUpdate();
                break;
            case 2:
                ((MoreVisitsFragment)getPagerItems().get(pos)).onCounterUpdate();
                break;
            case 3:
                break;
            case 4:
                break;
        }

        this.updateIndicator();
    }

    public void onActivityResultFragment(int requestCode, int resultCode, Intent data) {
        visitsFragment.onActivityResultFragment(requestCode, resultCode, data);
    }

    /**
     * Callback interface for responding to Notifications Counter
     */
    public interface CounterUpdateListener {

        /**
         * This method will be invoked when the app gets a notification
         * it should update the content of the page in display
         * maybe update counters?
         */
        public void onCounterUpdate();
    }

}
