package br.com.apt.ui.myapt.myaptFragments.poll;

import com.google.gson.annotations.SerializedName;

import br.com.apt.application.SendRequest;

/**
 * Created by adminbs on 1/18/16.
 */
public class VoteSend extends SendRequest{
    @SerializedName("poll_id")
    private long pollId;

    @SerializedName("favor")
    private boolean favor;

    public long getPollId() {
        return pollId;
    }

    public void setPollId(long pollId) {
        this.pollId = pollId;
    }

    public boolean isFavor() {
        return favor;
    }

    public void setFavor(boolean favor) {
        this.favor = favor;
    }
}
