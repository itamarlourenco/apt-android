package br.com.apt.ui.MyAdm.myadmFragments.sac;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import br.com.apt.R;
import br.com.apt.application.BaseActivity;
import br.com.apt.ui.reservations.CalendarFragment;

/**
 * Created by adminbs on 8/7/17.
 */

public class OthersOccurrencesActivity extends BaseActivity {

    private static final String IS_NOTIFICATION = "IS_NOTIFICATION";

    public OthersOccurrencesFragments othersOccurrencesFragments = OthersOccurrencesFragments.newInstance(true);

    public static Intent newIntent(Context context) {
        return newIntent(context, false);
    }

    public static Intent newIntent(Context context, boolean isNotification) {
        if(isNotification){
            Intent intent = new Intent(context, OthersOccurrencesActivity.class);
            intent.putExtra(IS_NOTIFICATION, true);
            return intent;
        }
        return new Intent(context, OthersOccurrencesActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        if(intent != null){
            setTitle(R.string.new_ticket);
        }
    }

    public Fragment getFragment(){
        Intent intent = getIntent();
        if(intent != null){
            boolean isNotification = intent.getBooleanExtra(IS_NOTIFICATION, false);
            othersOccurrencesFragments.isNotification(isNotification);
        }

        return othersOccurrencesFragments;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        othersOccurrencesFragments.onActivityResultFragment(requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_down, R.anim.slide_out_down);
    }
}
