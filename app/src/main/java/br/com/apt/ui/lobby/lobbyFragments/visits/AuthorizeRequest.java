package br.com.apt.ui.lobby.lobbyFragments.visits;

import java.util.List;

import br.com.apt.application.BaseGson;

/**
 * Created by itamarlourenco on 16/01/16.
 */
public class AuthorizeRequest extends BaseGson {
    private List<Visit> Object;

    public List<Visit> getObject() {
        return Object;
    }
}
