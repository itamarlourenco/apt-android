package br.com.apt.ui.reservations;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.github.ybq.android.spinkit.SpinKitView;

import java.util.List;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.BaseFragment;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.model.user.User;
import br.com.apt.util.Logger;
import br.com.apt.util.Util;

/**
 * Created by adminbs on 9/6/17.
 */

public class ReservationsFragment extends BaseFragment implements Volley.Callback {

    private SpinKitView loader;
    private ListView listView;
    private LinearLayout canvas;
    private List<Reservations> reservationsList;

    public static ReservationsFragment newInstance(){
        return new ReservationsFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_reservations, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if (view != null) {
            loader = (SpinKitView) view.findViewById(R.id.loader);
            listView = (ListView) view.findViewById(R.id.listView);
            canvas = (LinearLayout) view.findViewById(R.id.canvas);
            view.findViewById(R.id.newReservation).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openNewReservation();
                }
            });
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    //DialogReservationDetail.show(ReservationsFragment.this, getFragmentManager(), reservationsList.get(position));
                    startActivity(ReservationsDetailActivity.newIntent(getContext(), reservationsList.get(position)));
                }
            });
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getReservations();
    }

    private void openNewReservation() {
        startActivity(InstallationsActivity.newIntent(getActivity()));
        getActivity().overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
    }

    private void showPlaceholder(){
        Util.changeVisibility(loader, View.GONE, true);
        Util.changeVisibility(canvas, View.VISIBLE, true);
        canvas.removeAllViews();
        canvas.addView(LayoutInflater.from(getContext()).inflate(R.layout.reservation_placeholder, canvas, false));
    }

    public void getReservations() {
        Volley volley = new Volley(this);
        volley.isReservation(true);
        volley.request();
    }

    @Override
    public void result(String result, int type) {
        try{
            reservationsList = null;
            ReservationsRequest reservationsRequest = App.getGson().fromJson(result, ReservationsRequest.class);
            reservationsList = reservationsRequest.getObject();
            if(reservationsList.size() <= 0){
                throw new Exception();
            }
            Util.changeVisibility(loader, View.GONE, true);
            Util.changeVisibility(canvas, View.VISIBLE, true);
            listView.setAdapter(new ReservationsAdapter(getContext(), reservationsList));
        }catch(Exception e){
            showPlaceholder();
        }
    }

    @Override
    public String uri() {
        User user = getUser();
        return String.format(URI.RESERVATION_FILTER, user.getBlockId(), user.getCompanyKey(), user.getCondominiumId(), user.getApto());
    }
}

