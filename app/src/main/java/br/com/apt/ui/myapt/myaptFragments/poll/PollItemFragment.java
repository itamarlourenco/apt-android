package br.com.apt.ui.myapt.myaptFragments.poll;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.android.volley.Request;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.BaseFragment;
import br.com.apt.application.BaseGson;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.application.ZoomImageView;
import br.com.apt.model.user.User;
import br.com.apt.ui.tickets.tickets.Ticket;
import br.com.apt.ui.tickets.tickets.TicketItemFragment;
import br.com.apt.util.Logger;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomButton;
import br.com.apt.widget.CustomTextViewLight;
import br.com.apt.widget.SendSuccessDialog;

/**
 * Created by itamarlourenco on 15/07/17.
 */

public class PollItemFragment extends BaseFragment {

    private Poll poll;

    public void setPoll(Poll poll) {
        this.poll = poll;
    }

    public static PollItemFragment newInstance(Poll poll) {
        PollItemFragment pollItemFragment = new PollItemFragment();
        pollItemFragment.setPoll(poll);
        return pollItemFragment;
    }

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_item_poll, container, false);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if(view != null){
            ZoomImageView zoomImageView = (ZoomImageView) view.findViewById(R.id.image);
            CustomTextViewLight title = (CustomTextViewLight) view.findViewById(R.id.title);
            CustomTextViewLight description = (CustomTextViewLight) view.findViewById(R.id.description);
            CustomTextViewLight createdAt = (CustomTextViewLight) view.findViewById(R.id.createdAt);
            CustomTextViewLight validate = (CustomTextViewLight) view.findViewById(R.id.validate);
            LinearLayout baseOptions = (LinearLayout) view.findViewById(R.id.baseOptions);

            Picasso.with(getContext()).load(Volley.getUrlByImage(poll.getImageUrl())).placeholder(R.drawable.placeholder_camera).into(zoomImageView);
            title.setText(poll.getTitle());
            description.setText(poll.getDescription());
            createdAt.setText(String.format(getString(R.string.created_poll), Util.dateFormatted(poll.getStartAt())));

            String[] date = DateFormat.getDateTimeInstance().format(poll.getEndAt()).split(" ");
            if(date.length > 0){
                long differenceDay = Util.getDifferenceDay(new Date(), poll.getEndAt());
                validate.setText(String.format(getString(differenceDay <= 0 ? R.string.created_finish : R.string.created_aitved), Util.dateFormatted(poll.getEndAt())));
                validate.setTextColor(ContextCompat.getColor(getContext(), differenceDay <= 0 ? R.color.pale_olive_green : R.color.faded_blue));
                handleOptions(poll.getOptions(), baseOptions, (differenceDay <= 0), poll.isVoted());
            }

            view.findViewById(R.id.close).setOnClickListener(new OnClickListener(){
                @Override
                public void onClick(View v) {
                    getActivity().finish();
                }
            });
        }
    }

    private void handleOptions(List<Poll.Options> options, LinearLayout baseOptions, final boolean close, final boolean voted) {
        for(final Poll.Options option: options){
            View view = LayoutInflater.from(getContext()).inflate(R.layout.option_poll_layout, baseOptions, false);
            CustomButton button = (CustomButton) view.findViewById(R.id.button);
            CustomButton buttonVoted = (CustomButton) view.findViewById(R.id.buttonVoted);
            CustomTextViewLight customTextViewLight = (CustomTextViewLight) view.findViewById(R.id.percentFavor);
            button.setEnabled(!(close || voted));
            button.setText(option.getValue());

            if(close){
                customTextViewLight.setText(String.valueOf((int) option.getPercentage()) + "%");
            }

            if(poll.getUser_voted() != null && option.getId() == poll.getUser_voted().getOption().getId()) {
                button.setVisibility(View.GONE);
                buttonVoted.setVisibility(View.VISIBLE);
                buttonVoted.setText(option.getValue());
            }

            button.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    SendSuccessDialog.show(getString(R.string.you_are_sure), String.format(getString(R.string.ask_vote), option.getValue()), getFragmentManager(), new SendSuccessDialog.Actions() {
                        @Override
                        public void back() {
                            vote(option);
                        }
                    }, SendSuccessDialog.TYPE_DYNAMIC);
                }
            });
            baseOptions.addView(view);
        }
    }

    private void vote(final Poll.Options option) {
        Volley volley = new Volley(new Volley.Callback() {
            @Override
            public void result(String result, int type) {
                if(result != null){
                    SendSuccessDialog.show(getString(R.string.vote_success), getString(R.string.vote_description), getFragmentManager(), new SendSuccessDialog.Actions() {
                        @Override
                        public void back() {
                            getActivity().finish();
                        }
                    }, SendSuccessDialog.TYPE_DIALOG);
                }
            }

            @Override
            public String uri() {
                return URI.ANSWERS;
            }
        });

        JSONObject jsonObject = new JSONObject();
        try {
            User user = getUser();

            jsonObject.put("option_id", option.getId());
            jsonObject.put("unit_key", user.getApto());
            jsonObject.put("custom_answer", user.getEmail());
            jsonObject.put("image_etag", "");
            jsonObject.put("image_url", "");
            jsonObject.put("user_name", user.getName());
            jsonObject.put("user_image", user.getImage());

            volley.isPoll(true);
            volley.showDialog(getActivity());
            volley.request(Request.Method.POST, jsonObject.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}