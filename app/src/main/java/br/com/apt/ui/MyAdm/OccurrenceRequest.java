package br.com.apt.ui.MyAdm;

import br.com.apt.application.BaseGsonMeuAdm;

/**
 * Created by adminbs on 7/31/17.
 */

public class OccurrenceRequest extends BaseGsonMeuAdm {
    private OccurrencesSend object;

    public OccurrencesSend getObject() {
        return object;
    }

    public void setObject(OccurrencesSend object) {
        this.object = object;
    }
}