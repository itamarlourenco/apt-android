package br.com.apt.ui.myinfo.myinfofragment.pet;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.android.volley.Request;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.BaseGson;
import br.com.apt.application.PicturesHelper;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.ui.myinfo.AutoCompleteDialog;
import br.com.apt.ui.myinfo.myinfofragment.AutoComplete;
import br.com.apt.ui.myinfo.myinfofragment.FragmentMyInfoAdd;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomButton;
import br.com.apt.widget.CustomEditText;
import br.com.apt.widget.SendSuccessDialog;
import siclo.com.ezphotopicker.api.EZPhotoPick;
import siclo.com.ezphotopicker.api.EZPhotoPickStorage;

/**
 * Created by fabricio.bezerra on 22/09/2016.
 */
public class MyPetAddFragment extends FragmentMyInfoAdd implements Volley.Callback {

    private Bitmap bitmap = null;
    private RelativeLayout canvasAlterImage;
    private static ImageView image;
    private List<AutoComplete> petTypes;
    private List<AutoComplete> petBreeds;
    private CustomEditText name;
    private CustomEditText type;
    private CustomEditText breed;
    private CustomEditText color;
    private CustomEditText size;
    private CustomButton sendPet;

    private int petTypeId = 0;
    private int petBreedId = 0;
    private int petSizeId = 0;

    private Pet petByEdd;

    public void setPetByAdd(Pet pet) {
        this.petByEdd = pet;
    }

    public static MyPetAddFragment newInstance(){
        return new MyPetAddFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_add_my_pet, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final View view = getView();
        if(view != null) {
            name = (CustomEditText) view.findViewById(R.id.name);
            type = (CustomEditText) view.findViewById(R.id.type);
            breed = (CustomEditText) view.findViewById(R.id.breed);
            color = (CustomEditText) view.findViewById(R.id.color);
            size = (CustomEditText) view.findViewById(R.id.size);

            canvasAlterImage = (RelativeLayout) view.findViewById(R.id.canvasAlterImage);
            image = (ImageView) view.findViewById(R.id.image);
            canvasAlterImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alterImage();
                }
            });


            sendPet = (CustomButton) view.findViewById(R.id.sendPet);
            sendPet.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sendPet();
                }
            });

            type.setOnFocusChangeListener(new OnFocusChangeListener(){
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if(hasFocus) {
                        openDialogPetsTypes();
                    }
                }
            });
            type.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openDialogPetsTypes();
                }
            });

            breed.setOnFocusChangeListener(new OnFocusChangeListener(){
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if(hasFocus){
                        openDialogPetsBreeds();
                    }
                }
            });
            breed.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openDialogPetsBreeds();
                }
            });

            size.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openDialogSize();
                }
            });
            size.setOnFocusChangeListener(new OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if(hasFocus){
                        openDialogSize();
                    }
                }
            });

            willBeEdited();
        }else{
            SendSuccessDialog.show(getString(R.string.error_generic), null, getFragmentManager(), null, SendSuccessDialog.TYPE_ERROR);
        }
    }

    private void willBeEdited() {
        if(petByEdd != null){
            name.setText(petByEdd.getName());
            type.setText(petByEdd.getType().getName());
            petTypeId = petByEdd.getPet_type_id();
            breed.setText(petByEdd.getBreed().getName());
            petBreedId = petByEdd.getPet_breed_id();
            size.setText(petByEdd.getType().getName());
            petTypeId = petByEdd.getPet_type_id();
            color.setText(petByEdd.getColor());
            Picasso.with(getContext()).load(Volley.getUrlByImage(petByEdd.getImage())).placeholder(R.drawable.placeholder_camera).into(image);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getPetsType();
    }

    private void alterImage(){
        CharSequence[] options = {getString(R.string.takePhoto), getString(R.string.chooseGallery)};
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity());

        builder.setTitle("")
                .setItems(options, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if(which == 0) {
                            PicturesHelper.Camera(getActivity());
                        } else if(which == 1) {
                            PicturesHelper.Gallery(getActivity());
                        }else{
                            image.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.placeholder_camera));
                        }
                    }
                });
        builder.create().show();
    }

    private void sendPet(){
        try{
            if(TextUtils.isEmpty(name.getText())){
                throw new Exception(getString(R.string.error_name));
            }

            if(TextUtils.isEmpty(color.getText())){
                throw new Exception(getString(R.string.error_color_pet));
            }

            if(petTypeId <= 0){
                throw new Exception(getString(R.string.error_type_pet));
            }

            if(petBreedId <= 0){
                throw new Exception(getString(R.string.error_breed_pet));
            }

            HashMap<String, String> hashMap = new HashMap<>();
            if(petByEdd != null && petByEdd.getId() > 0){
                hashMap.put("id", String.valueOf(petByEdd.getId()));
            }
            hashMap.put("name", name.getText().toString());
            hashMap.put("pet_type_id", String.valueOf(petTypeId));
            hashMap.put("pet_breed_id", String.valueOf(petBreedId));
            hashMap.put("color", color.getText().toString());

            String sizeString = "";
            if(petSizeId == 0){
                sizeString = Pet.SIZE_SMALL;
            }else if (petSizeId == 1){
                sizeString = Pet.SIZE_MEDIUM;
            }else if (petSizeId == 2){
                sizeString = Pet.SIZE_LARGE;
            }
            hashMap.put("size", sizeString);

            File finalFile = Util.createImageFile();
            if(bitmap != null && finalFile != null){
                Util.sendBitmapToFile(finalFile, bitmap);
            }

            Volley volley = new Volley(this);
            volley.showDialog(getActivity());
            volley.request(Request.Method.POST, hashMap, 0, finalFile, "image");
        } catch (Exception e) {
            SendSuccessDialog.show(e.getMessage(), null, getFragmentManager(), null, SendSuccessDialog.TYPE_ERROR);
        }
    }


    @Override
    public void result(String result, int type) {
        if(result != null){
            BaseGson baseGson = App.getGson().fromJson(result, BaseGson.class);
            if(baseGson.getStatus() == Volley.STATUS_OK){
                SendSuccessDialog.show(getString(R.string.success_my_pet), null, getFragmentManager(), new SendSuccessDialog.Actions(){
                    @Override
                    public void back() {
                        getActivity().finish();
                    }
                });
            }else{
                SendSuccessDialog.show(getString(R.string.error_generic), null, getFragmentManager(), null, SendSuccessDialog.TYPE_ERROR);
            }
        }
    }

    @Override
    public String uri() {
        return URI.PETS;
    }

    private void openDialogPetsTypes() {
        AutoCompleteDialog.show(getFragmentManager(), petTypes, new AutoCompleteDialog.Click() {
            @Override
            public void click(AutoComplete autoComplete) {
                petTypeId = autoComplete.getId();
                type.setText(autoComplete.getName());
                getBreeds();
            }
        }, getString(R.string.choose_type));
    }

    private void openDialogPetsBreeds() {
        AutoCompleteDialog.show(getFragmentManager(), petBreeds, new AutoCompleteDialog.Click() {
            @Override
            public void click(AutoComplete autoComplete) {
                petBreedId = autoComplete.getId();
                breed.setText(autoComplete.getName());
            }
        }, getString(R.string.choose_breed));
    }

    public void onActivityResultFragment(int requestCode, int resultCode, Intent data) {
        if(requestCode == EZPhotoPick.PHOTO_PICK_REQUEST_CODE && resultCode != 0 && MyPetAddFragment.image != null){
            try {
                EZPhotoPickStorage ezPhotoPickStorage = new EZPhotoPickStorage(getActivity());
                bitmap = ezPhotoPickStorage.loadLatestStoredPhotoBitmap();

                MyPetAddFragment.image.setImageBitmap(bitmap);
                MyPetAddFragment.image.setVisibility(View.VISIBLE);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void getPetsType(){
        Volley volley = new Volley(new Volley.Callback() {
            @Override
            public void result(String result, int type) {
                if(result != null){
                    PetTypeRequest petTypeRequest = App.getGson().fromJson(result, PetTypeRequest.class);
                    if(petTypeRequest != null){
                        petTypes = petTypeRequest.getObject();
                        getBreeds();
                    }
                }
            }
            @Override
            public String uri() {
                return URI.PETS_TYPE;
            }
        });
        volley.showDialog(getActivity());
        volley.request(Request.Method.GET, null);
    }

    public void getBreeds(){
        Volley volley = new Volley(new Volley.Callback() {
            @Override
            public void result(String result, int type) {
                if(result != null){
                    PetBreedRequest petBreedRequest = App.getGson().fromJson(result, PetBreedRequest.class);
                    if(petBreedRequest != null){
                        petBreeds = petBreedRequest.getObject();
                    }
                }
            }
            @Override
            public String uri() {
                return URI.PETS_BREED;
            }
        });
        volley.showDialog(getActivity());
        try{
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("type", petTypeId);
            volley.request(Request.Method.POST, jsonObject.toString());
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void openDialogSize() {
        List<AutoComplete> autoComplete = new ArrayList<>();
        autoComplete.add(new AutoComplete(0, getString(R.string.size_small)));
        autoComplete.add(new AutoComplete(1, getString(R.string.size_medium)));
        autoComplete.add(new AutoComplete(2, getString(R.string.size_large)));

        AutoCompleteDialog.show(getFragmentManager(), autoComplete, new AutoCompleteDialog.Click() {
            @Override
            public void click(AutoComplete autoComplete) {
                petSizeId = autoComplete.getId();
                size.setText(autoComplete.getName());
            }
        });
    }


    @Override
    protected String getPagerTitle() {
        return null;
    }

    @Override
    protected int getPagerIcon() {
        return 0;
    }

    @Override
    protected HashMap<String, String> save() {
        return null;
    }

    @Override
    protected String getUri() {
        return null;
    }

    @Override
    protected int getTypeRequest() {
        return 0;
    }

    @Override
    protected void saved() {}

    @Override
    protected void saved(String json) {}
}
