package br.com.apt.ui.myapt.myaptFragments.plan;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.apt.R;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomTextViewLight;

/**
 * Created by adminbs on 2/23/18.
 */

public class PlantAdapter extends BaseAdapter {

    private Context context;
    private List<Plant> plants;

    public PlantAdapter(Context context, List<Plant> plants) {
        this.context = context;
        this.plants = plants;
    }

    @Override
    public int getCount() {
        return plants != null ? plants.size() : 0;
    }

    @Override
    public Plant getItem(int position) {
        return plants.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        PlantAdapter.ViewHolder holder;

        if(convertView == null){
            view = LayoutInflater.from(context).inflate(R.layout.row_plant, parent, false);
            holder = new PlantAdapter.ViewHolder(view);
            view.setTag(holder);
        }else{
            view = convertView;
            holder = (PlantAdapter.ViewHolder) view.getTag();
        }

        bindList(holder, position);
        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private void bindList(final PlantAdapter.ViewHolder holder, final int position) {
        if(position % 2 == 0){
            holder.baseBackground.setBackground(ContextCompat.getDrawable(context, R.drawable.button_menu_list_dark));
        }else{
            holder.baseBackground.setBackground(ContextCompat.getDrawable(context, R.drawable.button_menu_list_light));
        }

        Plant plant = getItem(position);
        Picasso.with(context).load(plant.getImage()).placeholder(R.drawable.placeholder_camera).into(holder.image);

        Plant.PlantType plantType = plant.getPlantType();
        if(plantType != null) holder.title.setText(plantType.getTitle());
        holder.description.setText(String.format(context.getString(R.string.add), Util.dateFormatted(plant.getUpdatedAt())));
    }

    public class ViewHolder{
        private ImageView image;
        private CustomTextViewLight title;
        private CustomTextViewLight description;
        private RelativeLayout baseBackground;

        public ViewHolder(View view) {
            image = (ImageView) view.findViewById(R.id.image);
            title = (CustomTextViewLight) view.findViewById(R.id.title);
            description = (CustomTextViewLight) view.findViewById(R.id.description);
            baseBackground = (RelativeLayout) view.findViewById(R.id.baseBackground);
        }
    }
}
