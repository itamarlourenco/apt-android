package br.com.apt.ui.setting;

import java.util.ArrayList;

import br.com.apt.R;
import br.com.apt.ui.setting.settingFragment.password.PasswordFragment;
import br.com.apt.ui.setting.settingFragment.UserSettingFragment;
import br.com.apt.widget.PagerFragment;
import br.com.apt.widget.PagerItemFragment;

public class SettingFragment extends PagerFragment {
    public static SettingFragment newInstance() {
        return new SettingFragment();
    }


    @Override
    public ArrayList<PagerItemFragment> getPagerItems() {
        ArrayList<PagerItemFragment> pagerItems = new ArrayList<>();
        pagerItems.add(new UserSettingFragment());
        //pagerItems.add(new AptoFragment());
        pagerItems.add(new PasswordFragment());

        return pagerItems;
    }

    @Override
    protected int getDrawableSelected() {
        return R.drawable.background_tab_selected_setting;
    }
}
