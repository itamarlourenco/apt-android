package br.com.apt.ui.myCondominium;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import br.com.apt.application.BaseActivity;
import br.com.apt.ui.reservations.CalendarFragment;

/**
 * Created by adminbs on 8/6/17.
 */

public class OpenPDF extends BaseActivity {

    private static final String EXTRA_URL = "EXTRA_URL";

    public static Intent newIntent(Context context, String url) {
        Intent intent = new Intent(context, OpenPDF.class);
        intent.putExtra(EXTRA_URL, url);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public Fragment getFragment(){
        Intent intent = getIntent();
        if (intent != null) {
            OpenPDFFragment openPDFFragment = OpenPDFFragment.newInstance();
            openPDFFragment.setUrl(intent.getStringExtra(OpenPDF.EXTRA_URL));

            return openPDFFragment;
        }

        return null;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}