package br.com.apt.ui.MyAdm.myadmFragments.Billet;

import br.com.apt.application.BaseGsonMeuAdm;
import br.com.apt.ui.MyAdm.OccurrencesSend;

/**
 * Created by adminbs on 7/31/17.
 */

public class BilletRequest extends BaseGsonMeuAdm {
    private Billet object;

    public Billet getObject() {
        return object;
    }

    public void setObject(Billet object) {
        this.object = object;
    }
}