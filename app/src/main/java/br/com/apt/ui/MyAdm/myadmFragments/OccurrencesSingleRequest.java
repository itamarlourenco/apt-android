package br.com.apt.ui.MyAdm.myadmFragments;

import java.util.List;

import br.com.apt.application.BaseGsonMeuAdm;
import br.com.apt.ui.MyAdm.Occurrences;
import br.com.apt.ui.MyAdm.OccurrencesSingle;

/**
 * Created by adminbs on 7/31/17.
 */

public class OccurrencesSingleRequest extends BaseGsonMeuAdm {
    private OccurrencesSingle object;

    public OccurrencesSingle getObject() {
        return object;
    }

    public void setObject(OccurrencesSingle object) {
        this.object = object;
    }
}