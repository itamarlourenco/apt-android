package br.com.apt.ui.lobby.lobbyFragments.visits;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import br.com.apt.R;
import br.com.apt.application.BaseActivity;
import br.com.apt.ui.reservations.CalendarFragment;

/**
 * Created by adminbs on 7/13/17.
 */

public class VisitsScheduleActivity extends BaseActivity {
    public static Intent newIntent(Context context) {
        return new Intent(context, VisitsScheduleActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public Fragment getFragment(){
        return VisitsScheduleFragment.newInstance();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        VisitsScheduleFragment visitsScheduleFragment = (VisitsScheduleFragment) getFragment();
        visitsScheduleFragment.onActivityResultFragment(requestCode, resultCode, data);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_down, R.anim.slide_out_down);
    }

}
