package br.com.apt.ui.myCondominium;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.ui.myCondominium.myCondominiumFragments.EdictsAndMinutes.EdictsAndMinutesFragment;
import br.com.apt.ui.myCondominium.myCondominiumFragments.infoCondominium.InfoCondominiumFragment;
import br.com.apt.ui.myCondominium.myCondominiumFragments.legalDocuments.LegalDocumentsFragment;
import br.com.apt.ui.myCondominium.myCondominiumFragments.legislation.LegislationFragment;
import br.com.apt.ui.myCondominium.myCondominiumFragments.regulationAndReforms.RegulationAndReformsFragment;
import br.com.apt.ui.myCondominium.myCondominiumFragments.convention.ConventionFragment;
import br.com.apt.ui.myapt.myaptFragments.notice.NoticesFragment;
import br.com.apt.widget.PagerFragment;
import br.com.apt.widget.PagerItemFragment;

/**
 * Created by adminbs on 8/5/17.
 */

public class MyCondominiumFragment extends PagerFragment implements Observer {

    private ArrayList<PagerItemFragment> pagerItems = null;


    public static MyCondominiumFragment newInstance() {
        return new MyCondominiumFragment();
    }

    public static PagerFragment newInstance(int position) {
        return instanceWithPosition(new MyCondominiumFragment(), position);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        App.getNotificationSharedPreferences().addObserver(this);
    }

    @Override
    protected int getDrawableSelected() {
        return R.drawable.background_tab_selected_my_apt;
    }

    @Override
    public ArrayList<PagerItemFragment> getPagerItems() {
        if(pagerItems == null) {
            pagerItems = new ArrayList<>();

            if(App.isAdbens()) {
                pagerItems.add(new NoticesFragment());
            }
            pagerItems.add(new RegulationAndReformsFragment());
            pagerItems.add(new ConventionFragment());
            pagerItems.add(new LegislationFragment());
            if(App.isAdbens()) {
                pagerItems.add(new LegalDocumentsFragment());
            }
            pagerItems.add(new InfoCondominiumFragment());
            pagerItems.add(new EdictsAndMinutesFragment());

        }
        return pagerItems;
    }

    @Override
    public void update(Observable observable, Object data) {

    }
}
