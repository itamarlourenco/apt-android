package br.com.apt.ui.firstAccess.animations;

import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.android.volley.Request;
import com.google.gson.annotations.SerializedName;

import br.com.apt.BuildConfig;
import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.BaseFragment;
import br.com.apt.application.BaseGson;
import br.com.apt.application.SendRequest;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.util.Logger;
import br.com.apt.util.Mask;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomButton;
import br.com.apt.widget.CustomEditText;
import br.com.apt.widget.CustomTextView;

/**
 * Created by adminbs on 9/14/16.
 */
public class NotHaveCodeAnimationFragment extends BaseFragment implements View.OnClickListener {

    private ImageButton buttonKey;
    private ImageButton buttonTool;
    private ImageButton buttonPen;

    private CustomEditText name;
    private CustomEditText email;
    private CustomEditText phone;
    private CustomEditText zipCode;

    private String typeString = "";

    private CustomButton sendContact;

    public static NotHaveCodeListener notHaveCodeListener;

    public static NotHaveCodeAnimationFragment newInstance(NotHaveCodeListener notHaveCodeListener){
        NotHaveCodeAnimationFragment.notHaveCodeListener = notHaveCodeListener;
        return new NotHaveCodeAnimationFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.not_have_code, container, false);
    }

    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if (view != null) {
            (view.findViewById(R.id.back)).setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    NotHaveCodeAnimationFragment.notHaveCodeListener.backToCode(v);
                }
            });

            buttonKey = (ImageButton) view.findViewById(R.id.buttonKey);
            buttonTool = (ImageButton) view.findViewById(R.id.buttonTool);
            buttonPen = (ImageButton) view.findViewById(R.id.buttonPen);

            sendContact = (CustomButton) view.findViewById(R.id.sendContact);

            buttonKey.setOnClickListener(this);
            buttonTool.setOnClickListener(this);
            buttonPen.setOnClickListener(this);
            sendContact.setOnClickListener(this);

            name = (CustomEditText) view.findViewById(R.id.name);
            email = (CustomEditText) view.findViewById(R.id.email);
            phone = (CustomEditText) view.findViewById(R.id.phone);
            zipCode = (CustomEditText) view.findViewById(R.id.zipcode);

            if(BuildConfig.DEBUG){
                name.setText("Itamar");
                email.setText("itamar.developer@gmail.com");
                phone.setText("(11) 9755004464");
            }


            phone.addTextChangedListener(Mask.insert("(##)#####-####", phone));
            zipCode.addTextChangedListener(Mask.insert("#####-###", zipCode));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonKey:
                disabledAllEnabledThis(buttonKey);
                break;
            case R.id.buttonTool:
                disabledAllEnabledThis(buttonTool);
                break;
            case R.id.buttonPen:
                disabledAllEnabledThis(buttonPen);
                break;
            case R.id.sendContact:
                validateForm();
                break;
        }
    }

    private void validateForm() {
        if(TextUtils.isEmpty(name.getText())){
            Util.showDialog(getActivity(), R.string.enter_with_name);
            return;
        }

        if(TextUtils.isEmpty(email.getText())){
            Util.showDialog(getActivity(), R.string.enter_with_email);
            return;
        }

        if(TextUtils.isEmpty(phone.getText())){
            Util.showDialog(getActivity(), R.string.enter_with_phone);
            return;
        }
        if(TextUtils.isEmpty(typeString) && !App.isAdbens()){
            Util.showDialog(getActivity(), R.string.enter_with_type);
            return;
        }

        if(TextUtils.isEmpty(zipCode.getText())){
            Util.showDialog(getActivity(), R.string.enter_with_zipcode);
            return;
        }


        checkZipCode();
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void disabledAllEnabledThis(ImageButton selected) {
        buttonKey.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.background_button_not_have_code));
        buttonKey.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.icon_key_white));

        buttonTool.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.background_button_not_have_code));
        buttonTool.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.icon_tool_white));

        buttonPen.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.background_button_not_have_code));
        buttonPen.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.icon_pen_white));

        selected.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.background_button_not_have_code_selected));
        if(selected == buttonKey){
            buttonKey.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.icon_key_blue));
            typeString = getString(R.string.resident);
        }else if(selected == buttonTool){
            buttonTool.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.icon_tool_blue));
            typeString = getString(R.string.caretaker);
        }else if(selected == buttonPen){
            buttonPen.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.icon_pen_blue));
            typeString = getString(R.string.syndic);
        }
    }


    public interface NotHaveCodeListener{
        void backToCode(View view);
    }



    private void checkZipCode(){
        String cepURL = zipCode.getText().toString().replace("-","");
        Volley volley = new Volley(new Volley.Callback() {
            @Override
            public void result(String result, int type) {
                if(result != null){
                    ZipCode zipcode = App.getGson().fromJson(result, ZipCode.class);
                    System.out.println(zipcode.result);

                    if(zipcode.result.equals("0")){
                        //Error
                        Util.showDialog(getActivity(), R.string.fix_zipcode);
                        return;
                    }else{
                        submitNewContact();
                    }

                }
            }
            @Override
            public String uri() {

                return "cep";
            }
        },new String[]{"?cep="+cepURL});
        volley.showDialog(getActivity());

        volley.request(Request.Method.GET,null);
    }

    private void submitNewContact(){
        Volley volley = new Volley(new Volley.Callback() {
            @Override
            public void result(String result, int type) {
                Util.showDialog(getActivity(), getString(R.string.contact_send), new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getActivity().finish();
                    }
                });
            }

            @Override
            public String uri() {
                return URI.CONTACT;
            }
        });

        ContactSend contactSend = new ContactSend();
        contactSend.setName(name.getText().toString());
        contactSend.setEmail(email.getText().toString());
        contactSend.setNumber(phone.getText().toString());
        contactSend.setZipcode(zipCode.getText().toString());
        contactSend.setType(typeString);

        volley.showDialog(getActivity());
        volley.request(Request.Method.POST, contactSend.toJson());
    }

    public class ContactSend extends SendRequest {
        private String name;
        private String zipcode;
        private String number;
        private String email;
        private String type;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getZipcode() {
            return zipcode;
        }

        public void setZipcode(String zipcode) {
            this.zipcode = zipcode;
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }
    }
    public class ZipCode extends SendRequest {
        @SerializedName("resultado")
        private String result;

        public String getResult() {
            return result;
        }

        public void setResult(String result) {
            this.result = result;
        }
    }
}
