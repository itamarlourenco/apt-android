package br.com.apt.ui.MyAdm.myadmFragments.Billet;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by adminbs on 8/1/17.
 */

public class BilletDetails implements Parcelable {
    private String block;
    private String unit;
    private String amount;
    private Date limit;
    private Date dead_line;
    private String pay_line;
    private List<Banks> banks;
    private List<Items> items;


    public static class Banks implements Parcelable {
        private String name;
        private String url;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        protected Banks(Parcel in) {
            name = in.readString();
            url = in.readString();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(name);
            dest.writeString(url);
        }

        @SuppressWarnings("unused")
        public static final Parcelable.Creator<Banks> CREATOR = new Parcelable.Creator<Banks>() {
            @Override
            public Banks createFromParcel(Parcel in) {
                return new Banks(in);
            }

            @Override
            public Banks[] newArray(int size) {
                return new Banks[size];
            }
        };
    }

    public static class Items implements Parcelable {
        private String history;
        private String amount;

        public String getHistory() {
            return history;
        }

        public void setHistory(String history) {
            this.history = history;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        protected Items(Parcel in) {
            history = in.readString();
            amount = in.readString();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(history);
            dest.writeString(amount);
        }

        @SuppressWarnings("unused")
        public static final Parcelable.Creator<Items> CREATOR = new Parcelable.Creator<Items>() {
            @Override
            public Items createFromParcel(Parcel in) {
                return new Items(in);
            }

            @Override
            public Items[] newArray(int size) {
                return new Items[size];
            }
        };
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public Date getLimit() {
        return limit;
    }

    public void setLimit(Date limit) {
        this.limit = limit;
    }

    public Date getDead_line() {
        return dead_line;
    }

    public void setDead_line(Date dead_line) {
        this.dead_line = dead_line;
    }

    public String getPay_line() {
        return pay_line;
    }

    public void setPay_line(String pay_line) {
        this.pay_line = pay_line;
    }

    public List<Banks> getBanks() {
        return banks;
    }

    public void setBanks(List<Banks> banks) {
        this.banks = banks;
    }

    public List<Items> getItems() {
        return items;
    }

    public void setItems(List<Items> items) {
        this.items = items;
    }

    protected BilletDetails(Parcel in) {
        block = in.readString();
        unit = in.readString();
        amount = in.readString();
        long tmpLimit = in.readLong();
        limit = tmpLimit != -1 ? new Date(tmpLimit) : null;
        long tmpDead_line = in.readLong();
        dead_line = tmpDead_line != -1 ? new Date(tmpDead_line) : null;
        pay_line = in.readString();
        if (in.readByte() == 0x01) {
            banks = new ArrayList<>();
            in.readList(banks, Banks.class.getClassLoader());
        } else {
            banks = null;
        }
        if (in.readByte() == 0x01) {
            items = new ArrayList<>();
            in.readList(items, Items.class.getClassLoader());
        } else {
            items = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(block);
        dest.writeString(unit);
        dest.writeString(amount);
        dest.writeLong(limit != null ? limit.getTime() : -1L);
        dest.writeLong(dead_line != null ? dead_line.getTime() : -1L);
        dest.writeString(pay_line);
        if (banks == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(banks);
        }
        if (items == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(items);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<BilletDetails> CREATOR = new Parcelable.Creator<BilletDetails>() {
        @Override
        public BilletDetails createFromParcel(Parcel in) {
            return new BilletDetails(in);
        }

        @Override
        public BilletDetails[] newArray(int size) {
            return new BilletDetails[size];
        }
    };
}