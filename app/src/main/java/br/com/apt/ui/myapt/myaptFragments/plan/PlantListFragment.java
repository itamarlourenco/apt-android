package br.com.apt.ui.myapt.myaptFragments.plan;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.github.ybq.android.spinkit.SpinKitView;

import java.util.List;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomButton;
import br.com.apt.widget.PagerItemFragment;

/**
 * Created by adminbs on 1/12/16.
 */
public class PlantListFragment extends PagerItemFragment implements View.OnClickListener {
    private List<Plant> plants;
    private ListView listView;
    private LinearLayout placeholder;
    private SpinKitView loader;
    private CustomButton newPlant;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_plan, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if(view == null) return;
        listView = (ListView) view.findViewById(R.id.listView);
        placeholder = (LinearLayout) view.findViewById(R.id.placeholder);
        loader = (SpinKitView) view.findViewById(R.id.loader);
        newPlant = (CustomButton) view.findViewById(R.id.newPlant);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startActivity(PlantViewActivity.newIntent(getContext(), plants.get(position)));
            }
        });

        newPlant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(PlantAddActivity.newIntent(getContext()));
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        getPlants();
    }

    private void getPlants() {
        Volley volley = new Volley(new Volley.Callback() {
            @Override
            public void result(String result, int type) {
                if(TextUtils.isEmpty(result)) {
                    Util.changeVisibility(placeholder, View.VISIBLE, true);
                    return;
                }

                PlantRequest plantRequest = App.getGson().fromJson(result, PlantRequest.class);
                plants = plantRequest.getObject();
                loader.setVisibility(View.GONE);
                if(plants != null && plants.size() > 0) {
                    Util.changeVisibility(listView, View.VISIBLE, true);
                    Util.changeVisibility(placeholder, View.GONE, true);
                    listView.setAdapter(new PlantAdapter(getContext(), plants));
                }else{
                    Util.changeVisibility(listView, View.GONE, true);
                    Util.changeVisibility(placeholder, View.VISIBLE, true);
                }
            }

            @Override
            public String uri() {
                return URI.PLANT;
            }
        });
        volley.isMeuADM(true);
        volley.request();
    }

    @Override
    protected String getPagerTitle() {
        return App.getContext().getString(R.string.plan);
    }

    @Override
    protected int getPagerIcon() {
        return R.drawable.ico_menu_planta;
    }

    @Override
    public void onClick(View v) {
    }

    public void onActivityResultFragment(int requestCode, int resultCode, Intent data) {

    }

}
