package br.com.apt.ui.myapt.myaptFragments.poll;

import com.google.gson.annotations.SerializedName;

import br.com.apt.application.SendRequest;

/**
 * Created by itamarlourenco on 17/01/16.
 */
public class PollSend extends SendRequest {
    @SerializedName("edificioID")
    private String buildId;

    private long id;

    @SerializedName("blocoId")
    private String blockId;

    public String getBuildId() {
        return buildId;
    }

    public void setBuildId(String buildId) {
        this.buildId = buildId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getBlockId() {
        return blockId;
    }

    public void setBlockId(String blockId) {
        this.blockId = blockId;
    }
}
