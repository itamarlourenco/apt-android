package br.com.apt.ui.consumption.consumptionsFragments;

import android.os.Bundle;
import android.support.percent.PercentLayoutHelper;
import android.support.percent.PercentRelativeLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.ui.consumption.Consumption;
import br.com.apt.ui.consumption.ConsumptionRequest;
import br.com.apt.util.Logger;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomTextView;
import br.com.apt.widget.PagerItemFragment;

public class WaterFragment extends PagerItemFragment {

    private static final String ARGS_CONSUMPTION = "ARGS_CONSUMPTION";

    public static PagerItemFragment newInstance(ConsumptionRequest consumptionRequest){
        WaterFragment waterFragment = new WaterFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARGS_CONSUMPTION, consumptionRequest);
        waterFragment.setArguments(args);
        return waterFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_consumption_water, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Bundle bundle = this.getArguments();
        ConsumptionRequest consumptionRequest = bundle.getParcelable(ARGS_CONSUMPTION);

        View view = getView();
        if (view != null && consumptionRequest != null) {
            CustomTextView selectDate = (CustomTextView) view.findViewById(R.id.selectDate);

            String[] months = App.getContext().getResources().getStringArray(R.array.months_uppercase);

            Calendar cal = Calendar.getInstance();
            selectDate.setText(months[cal.get(Calendar.MONTH)] +"/"+ cal.get(Calendar.YEAR));

            ConsumptionRequest.AllConsumption allConsumption = consumptionRequest.getObject();
            if(allConsumption != null){
                Consumption water = allConsumption.getWater();
                if(water != null){
                    TextView percentTextView = (TextView) getView().findViewById(R.id.percentTextView);
                    TextView consumptionTotal = (TextView) getView().findViewById(R.id.consumptionTotal);
                    TextView consumptionTotalApto = (TextView) getView().findViewById(R.id.consumptionTotalApto);

                    percentTextView.setText(String.valueOf(Math.round(water.getPercent())) + "%");
                    consumptionTotal.setText(getString(R.string.consumption_total) + " " + Math.round(water.getConsumptionTotal()) + "m³");
                    consumptionTotalApto.setText(getString(R.string.consumption_apartment) + " " + Math.round(water.getConsumptionUser()) + "m³");

                    View percentView = view.findViewById(R.id.percentView);
                    PercentRelativeLayout.LayoutParams params = (PercentRelativeLayout.LayoutParams) percentView.getLayoutParams();
                    PercentLayoutHelper.PercentLayoutInfo info = params.getPercentLayoutInfo();
                    info.heightPercent = water.getPercent() / 100;
                    percentView.requestLayout();
                }
            }
        }
    }



    @Override
    protected String getPagerTitle() {
        return App.getContext().getString(R.string.water);
    }

    @Override
    protected int getPagerIcon() {
        return R.drawable.ic_water;
    }
}
