package br.com.apt.ui.myCondominium.myCondominiumFragments.legislation;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import java.util.List;

import br.com.apt.R;
import br.com.apt.ui.myCondominium.myCondominiumFragments.Attachments;
import br.com.apt.widget.CustomTextViewLight;

/**
 * Created by meuapt on 07/03/18.
 */

public class LegislationAdapter extends BaseAdapter {

    private Context context;
    private List<Attachments> attachmentsList;

    public LegislationAdapter(Context context, List<Attachments> attachmentList) {
        this.context = context;
        this.attachmentsList = attachmentList;
    }

    @Override
    public int getCount() {
        return attachmentsList != null ? attachmentsList.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        return attachmentsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        LegislationAdapter.ViewHolder holder;

        if(convertView == null){
            view = LayoutInflater.from(context).inflate(R.layout.row_documents, parent, false);
            holder = new LegislationAdapter.ViewHolder(view);
            view.setTag(holder);
        }else{
            view = convertView;
            holder = (LegislationAdapter.ViewHolder) view.getTag();
        }

        bindList(holder, position);
        return view;
    }

    private void bindList(LegislationAdapter.ViewHolder holder, int position) {
        Attachments item = (Attachments) getItem(position);
        holder.item.setText(item.getTitle());
    }

    public class ViewHolder{
        private CustomTextViewLight item;

        public ViewHolder(View view) {
            item = (CustomTextViewLight) view.findViewById(R.id.item);
        }
    }
}
