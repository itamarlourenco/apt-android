package br.com.apt.ui.myapt.myaptFragments.plan;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import br.com.apt.R;
import br.com.apt.application.BaseActivity;

public class PlantViewActivity extends BaseActivity {

    public static final String EXTRA_PLANT = "EXTRA_PLANT";
    private PlantViewFragment plantViewFragment = PlantViewFragment.newInstance();

    public static Intent newIntent(Context context, Plant plant) {
        Intent intent = new Intent(context, PlantViewActivity.class);
        intent.putExtra(EXTRA_PLANT, plant);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public Fragment getFragment(){
        Intent intent = getIntent();
        if(intent != null){
            Plant plant = intent.getParcelableExtra(PlantAddActivity.EXTRA_PLANT);
            plantViewFragment.setPlant(plant);
        }

        return plantViewFragment;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        plantViewFragment.onActivityResultFragment(requestCode, resultCode, data);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_down, R.anim.slide_out_down);
    }
}
