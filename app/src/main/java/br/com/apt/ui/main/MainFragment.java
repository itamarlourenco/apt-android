package br.com.apt.ui.main;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;

import java.util.List;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.BaseActivity;
import br.com.apt.application.BaseFragment;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.application.notification.NotificationSharedPreferences;
import br.com.apt.application.services.NotificationService;
import br.com.apt.model.user.User;
import br.com.apt.model.user.UserData;
import br.com.apt.model.user.UserMeuadm;
import br.com.apt.ui.main.banners.BannerFragment;
import br.com.apt.ui.main.menu.MainMenuFragment;
import br.com.apt.ui.myCondominium.myCondominiumFragments.Attachments;
import br.com.apt.widget.CustomTextView;
import br.com.apt.widget.ViewPagerCustom;

public class MainFragment extends BaseFragment implements Volley.Callback{

    private ViewPagerCustom viewPager;
    private SensorsRequest sensorsRequest;

    @Override
    public String uri() {
        return URI.SENSOR;
    }

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.main_fragment, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        NotificationService.startService(getContext());
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        handleMenu();
        User user = getUser();
        if(user != null){
            ((BaseActivity) getActivity()).showSelectUnits(true);
            Volley volley = new Volley(this);
            volley.showDialog(getActivity());
            volley.request(Request.Method.POST, null);
        }
    }

    @Override
    public void result(String result, int type) {
        View view = getView();
        if(view != null){
            viewPager = (ViewPagerCustom) view.findViewById(R.id.bannerPager);
            sensorsRequest = App.getGson().fromJson(result, SensorsRequest.class);
            if(sensorsRequest != null){
                viewPager.setAdapter(new BannerPager(getActivity().getSupportFragmentManager()), view);
            }
        }
    }

    public class BannerPager extends FragmentPagerAdapter {

        public final int NUM_PAGES = (sensorsRequest != null && sensorsRequest.getObject() != null ? sensorsRequest.getObject().size() + 1 : 5);

        public BannerPager(FragmentManager fm) {
            super(fm);

        }

        @Override
        public Fragment getItem(int position) {
            return BannerFragment.newIntent(position, sensorsRequest);
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }
    }

    private void handleMenu() {
        MainMenuFragment mainMenuFragment = MainMenuFragment.newInstance();
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.mainMenu, mainMenuFragment);
        fragmentTransaction.commit();
    }
}
