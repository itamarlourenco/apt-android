package br.com.apt.ui.MyAdm.myadmFragments.sac;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Spinner;

import com.android.volley.Request;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.BaseGsonMeuAdm;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.model.user.User;
import br.com.apt.ui.MyAdm.OccurrencesSend;
import br.com.apt.ui.MyAdm.myadmFragments.sac.occurrence.BaseOccurrence;
import br.com.apt.ui.MyAdm.myadmFragments.sac.occurrence.OccurrenceSubject;
import br.com.apt.ui.myCondominium.MyCondominiumActivity;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomEditText;
import br.com.apt.widget.CustomTextViewLight;
import br.com.apt.widget.SendSuccessDialog;

/**
 * Created by adminbs on 8/3/17.
 */

public class MakeSacFragment extends BaseOccurrence{

    private Spinner subject;
    private CustomEditText message;
    private ImageView iconImage;
    private CustomTextViewLight nameTextView;

    private User user = getUser();
    private int icon;
    private String themeUiID;
    private String name;

    public void setName(String name) {
        this.name = name;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public void setThemeUiID(String themeUiID) {
        this.themeUiID = themeUiID;
    }

    public static MakeSacFragment newInstance(){
        return new MakeSacFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_make_sac, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if(view != null){
            subject = (Spinner) view.findViewById(R.id.subject);
            message = (CustomEditText) view.findViewById(R.id.message);
            iconImage = (ImageView) view.findViewById(R.id.icon);
            iconImage.setImageDrawable(ContextCompat.getDrawable(getContext(), icon));
            nameTextView = (CustomTextViewLight) view.findViewById(R.id.name);
            nameTextView.setText(name);

            view.findViewById(R.id.sendSac).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sendSac();
                }
            });

            subject.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    OccurrenceSubject occurrenceSubject = (OccurrenceSubject) subject.getSelectedItem();
                    if(occurrenceSubject.getName().equals("Atas")){
                        getActivity().finish();
                        startActivity(MyCondominiumActivity.newIntent(getContext()));
                    }

                    if(occurrenceSubject.getName().equals("Editais")){
                        getActivity().finish();
                        startActivity(MyCondominiumActivity.newIntent(getContext()));
                    }

                    if(occurrenceSubject.getName().equals("Procuração")){
                        getActivity().finish();
                        startActivity(MyCondominiumActivity.newIntent(getContext()));
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
    }

    private void sendSac() {
        try{
            if(TextUtils.isEmpty(message.getText())){
                throw new Exception(getString(R.string.message_occurrence));
            }

            final OccurrenceSubject occurrenceSubject = (OccurrenceSubject) subject.getSelectedItem();

            if(TextUtils.isEmpty(occurrenceSubject.getId())){
                throw new Exception(getString(R.string.sac_type_message));
            }

            OccurrencesSend occurrencesSend = new OccurrencesSend(
                    String.format("%s - %s", Util.capitalizeString(name), occurrenceSubject.getName()),
                    message.getText().toString(),
                    Integer.parseInt(user.getCondominiumId()),
                    occurrenceSubject.getId(),
                    user.getApto()
            );

            Volley volley = new Volley(new Volley.Callback() {
                @Override
                public void result(String result, int type) {
                    try{
                        BaseGsonMeuAdm baseGson = App.getGson().fromJson(result, BaseGsonMeuAdm.class);
                        SendSuccessDialog.show(baseGson.getMessage(), null, getFragmentManager(), new SendSuccessDialog.Actions() {
                            @Override
                            public void back() {
                                getActivity().finish();
                            }
                        });
                    }catch (Exception e){
                        e.printStackTrace();
                        SendSuccessDialog.show(getString(R.string.error_generic), null, getFragmentManager(), new SendSuccessDialog.Actions() {
                            @Override
                            public void back() {
                                getActivity().finish();
                            }
                        }, SendSuccessDialog.TYPE_ERROR);
                    }
                }

                @Override
                public String uri() {
                    return URI.OCCURRENCES;
                }
            });
            volley.isMeuADM(true);
            volley.showDialog(getActivity());
            volley.request(Request.Method.POST, occurrencesSend.toJson());
        }catch (Exception e){
            SendSuccessDialog.show(e.getMessage(), null, getFragmentManager(), null, SendSuccessDialog.TYPE_ERROR);
        }
    }

    @Override
    protected String themeUiID() {
        return themeUiID;
    }

    @Override
    public Spinner getSpinner() {
        return subject;
    }
}
