package br.com.apt.ui.myapt.myaptFragments.poll;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import br.com.apt.application.BaseActivity;
import br.com.apt.ui.reservations.CalendarFragment;

/**
 * Created by itamarlourenco on 15/07/17.
 */

public class PollItemActivity extends BaseActivity {

    private static final String EXTRA_POLL = "EXTRA_POLL";

    public static Intent newIntent(Context context, Poll poll) {
        Intent intent = new Intent(context, PollItemActivity.class);
        intent.putExtra(EXTRA_POLL, poll);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public Fragment getFragment(){
        Intent intent = getIntent();
        if(intent != null){
            Poll poll =  intent.getParcelableExtra(EXTRA_POLL);
            return PollItemFragment.newInstance(poll);
        }
        return null;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}