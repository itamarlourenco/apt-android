package br.com.apt.ui.lobby.lobbyFragments.deliveries;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.Request;
import com.github.ybq.android.spinkit.SpinKitView;

import java.util.List;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.BaseFragment;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.model.user.User;
import br.com.apt.ui.ExceptionWithoutResult;
import br.com.apt.util.Util;

/**
 * Created by itamarlourenco on 09/07/17.
 */

public class DeliveriesListFragment extends BaseFragment implements Volley.Callback {

    private DeliveriesFragment parent;
    private SpinKitView loader;
    private ListView listView;
    private List<Delivery> deliveries;

    public void setParent(DeliveriesFragment parent) {
        this.parent = parent;
    }

    public static DeliveriesListFragment newInstance(DeliveriesFragment parent) {
        DeliveriesListFragment deliveriesListFragment = new DeliveriesListFragment();
        deliveriesListFragment.setParent(parent);
        return deliveriesListFragment;
    }

    @Override
    public String uri() {
        return URI.DELIVERIES;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list_deliveries, container, false);
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if (view != null) {
            loader = (SpinKitView) view.findViewById(R.id.loader);
            listView = (ListView) view.findViewById(R.id.listView);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if(DeliveriesListFragment.this.parent != null){
                        startActivity(DeliveryActivity.newIntent(getContext(), deliveries.get(position)));
                    }
                }
            });

            User user = getUser();
            if (user != null) {
                Volley volley = new Volley(this);
                volley.request(Request.Method.GET, null);
            }
        }
    }

    @Override
    public void result(String result, int type) {
        View view = getView();
        try{
            Util.hidePlaceholder(view);
            Util.changeVisibility(loader, View.GONE, true);
            Util.changeVisibility(listView, View.VISIBLE, true);
            DeliveryRequest deliveryRequest = App.getGson().fromJson(result, DeliveryRequest.class);
            deliveries = deliveryRequest.getObject();

            if(deliveries == null){
                throw new ExceptionWithoutResult(getString(R.string.message_without_message), R.drawable.img_entregas_no_have);
            }
            listView.setAdapter(new DeliveriesListViewAdapter(getContext(), deliveries));
        }catch (ExceptionWithoutResult exceptionWithoutResult){
            Util.showPlaceholder(view, getActivity(), exceptionWithoutResult.getDrawableId(), exceptionWithoutResult.getMessage());
        }catch (Exception exception){
            Util.handlePlaceholderError(view, getActivity());
        }
    }

}
