package br.com.apt.ui.reservations;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;
import java.util.List;

import br.com.apt.application.BaseGsonMeuAdm;
import br.com.apt.application.SendRequest;
import br.com.apt.ui.MyAdm.myadmFragments.Billet.Billet;

/**
 * Created by adminbs on 9/10/17.
 */

public class ReservationsRequest extends BaseGsonMeuAdm {
    private List<Reservations> object;

    public List<Reservations> getObject() {
        return object;
    }

    public void setObject(List<Reservations> object) {
        this.object = object;
    }
}