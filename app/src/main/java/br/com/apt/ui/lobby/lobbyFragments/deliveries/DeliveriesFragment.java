package br.com.apt.ui.lobby.lobbyFragments.deliveries;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.BaseFragment;
import br.com.apt.application.notification.NotificationSharedPreferences;
import br.com.apt.widget.PagerFragment;
import br.com.apt.widget.PagerItemFragment;

public class DeliveriesFragment extends PagerItemFragment {

    public static DeliveriesFragment newInstance(){
        return new DeliveriesFragment();
    }

    @Override
    protected String getPagerTitle() {
        return App.getContext().getString(R.string.derivery);
    }

    @Override
    protected int getPagerIcon() {
        return R.drawable.ico_entregas;
    }

    @Override
    protected int getIndicator() {
        return NotificationSharedPreferences.getCountDelivery();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_deliveries, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if(view != null){
            DeliveriesFragment.handleDeliveriesFragments(DeliveriesListFragment.newInstance(this), getActivity());
        }
    }


    public static void handleDeliveriesFragments(BaseFragment baseFragment, FragmentActivity activity){
        DeliveriesFragment.handleDeliveriesFragments(R.id.fragmentContainerAnimationDelivery, baseFragment, activity);
    }
}
