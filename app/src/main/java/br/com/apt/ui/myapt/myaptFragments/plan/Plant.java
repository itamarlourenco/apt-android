package br.com.apt.ui.myapt.myaptFragments.plan;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;
import java.util.List;

import br.com.apt.application.BaseGson;

/**
 * Created by adminbs on 2/23/18.
 */

public class Plant extends BaseGson implements Parcelable {
    private String uuid;
    private String company_key;
    private String condominium_key;
    private int building_id;
    private String block_key;
    private String area;
    private String plant_type_uuid;
    private String plant_type_title;
    private PlantType plant_type;
    private String image;
    private Date created_at;
    private Date updated_at;
    private List<Units> units;

    public static final String AREA = "UNIT";


    public Plant(String condominium_key, int building_id, String block_key, String area, String plant_type_uuid, String image, List<Units> units) {
        this.condominium_key = condominium_key;
        this.building_id = building_id;
        this.block_key = block_key;
        this.area = area;
        this.plant_type_uuid = plant_type_uuid;
        this.image = image;
        this.units = units;
    }

    public static class Units implements Parcelable {
        private int apartment_id;
        private String unit_key;

        public Units(int apartment_id, String unit_key) {
            this.apartment_id = apartment_id;
            this.unit_key = unit_key;
        }

        public int getApartment_id() {
            return apartment_id;
        }

        public void setApartment_id(int apartment_id) {
            this.apartment_id = apartment_id;
        }

        public String getUnit_key() {
            return unit_key;
        }

        public void setUnit_key(String unit_key) {
            this.unit_key = unit_key;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.apartment_id);
            dest.writeString(this.unit_key);
        }

        public Units() {
        }

        protected Units(Parcel in) {
            this.apartment_id = in.readInt();
            this.unit_key = in.readString();
        }

        public static final Creator<Units> CREATOR = new Creator<Units>() {
            @Override
            public Units createFromParcel(Parcel source) {
                return new Units(source);
            }

            @Override
            public Units[] newArray(int size) {
                return new Units[size];
            }
        };
    }


    public static class PlantType implements Parcelable {
        private String id;
        private String uuid;
        private String title;
        private Date created_at;
        private Date updated_at;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUuid() {
            return uuid;
        }

        public void setUuid(String uuid) {
            this.uuid = uuid;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public Date getCreated_at() {
            return created_at;
        }

        public void setCreated_at(Date created_at) {
            this.created_at = created_at;
        }

        public Date getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(Date updated_at) {
            this.updated_at = updated_at;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.id);
            dest.writeString(this.uuid);
            dest.writeString(this.title);
            dest.writeLong(this.created_at != null ? this.created_at.getTime() : -1);
            dest.writeLong(this.updated_at != null ? this.updated_at.getTime() : -1);
        }

        public PlantType() {
        }

        protected PlantType(Parcel in) {
            this.id = in.readString();
            this.uuid = in.readString();
            this.title = in.readString();
            long tmpCreated_at = in.readLong();
            this.created_at = tmpCreated_at == -1 ? null : new Date(tmpCreated_at);
            long tmpUpdated_at = in.readLong();
            this.updated_at = tmpUpdated_at == -1 ? null : new Date(tmpUpdated_at);
        }

        public static final Creator<PlantType> CREATOR = new Creator<PlantType>() {
            @Override
            public PlantType createFromParcel(Parcel source) {
                return new PlantType(source);
            }

            @Override
            public PlantType[] newArray(int size) {
                return new PlantType[size];
            }
        };
    }

    public String getCondominium_key() {
        return condominium_key;
    }

    public void setCondominium_key(String condominium_key) {
        this.condominium_key = condominium_key;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getCompany_key() {
        return company_key;
    }

    public void setCompany_key(String company_key) {
        this.company_key = company_key;
    }

    public int getBuilding_id() {
        return building_id;
    }

    public void setBuilding_id(int building_id) {
        this.building_id = building_id;
    }

    public String getBlock_key() {
        return block_key;
    }

    public void setBlock_key(String block_key) {
        this.block_key = block_key;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getPlant_type_uuid() {
        return plant_type_uuid;
    }

    public void setPlant_type_uuid(String plant_type_uuid) {
        this.plant_type_uuid = plant_type_uuid;
    }

    public String getPlant_type_title() {
        return plant_type_title;
    }

    public void setPlant_type_title(String plant_type_title) {
        this.plant_type_title = plant_type_title;
    }

    public PlantType getPlantType() {
        return plant_type;
    }

    public void setPlant_type(PlantType plant_type) {
        this.plant_type = plant_type;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public Date getUpdatedAt() {
        return updated_at;
    }

    public void setUpdated_at(Date updated_at) {
        this.updated_at = updated_at;
    }

    public Plant() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.uuid);
        dest.writeString(this.company_key);
        dest.writeString(this.condominium_key);
        dest.writeInt(this.building_id);
        dest.writeString(this.block_key);
        dest.writeString(this.area);
        dest.writeString(this.plant_type_uuid);
        dest.writeString(this.plant_type_title);
        dest.writeParcelable(this.plant_type, flags);
        dest.writeString(this.image);
        dest.writeLong(this.created_at != null ? this.created_at.getTime() : -1);
        dest.writeLong(this.updated_at != null ? this.updated_at.getTime() : -1);
        dest.writeTypedList(this.units);
    }

    protected Plant(Parcel in) {
        this.uuid = in.readString();
        this.company_key = in.readString();
        this.condominium_key = in.readString();
        this.building_id = in.readInt();
        this.block_key = in.readString();
        this.area = in.readString();
        this.plant_type_uuid = in.readString();
        this.plant_type_title = in.readString();
        this.plant_type = in.readParcelable(PlantType.class.getClassLoader());
        this.image = in.readString();
        long tmpCreated_at = in.readLong();
        this.created_at = tmpCreated_at == -1 ? null : new Date(tmpCreated_at);
        long tmpUpdated_at = in.readLong();
        this.updated_at = tmpUpdated_at == -1 ? null : new Date(tmpUpdated_at);
        this.units = in.createTypedArrayList(Units.CREATOR);
    }

    public static final Creator<Plant> CREATOR = new Creator<Plant>() {
        @Override
        public Plant createFromParcel(Parcel source) {
            return new Plant(source);
        }

        @Override
        public Plant[] newArray(int size) {
            return new Plant[size];
        }
    };
}
