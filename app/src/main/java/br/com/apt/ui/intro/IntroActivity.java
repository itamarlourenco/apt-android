package br.com.apt.ui.intro;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import com.viewpagerindicator.CirclePageIndicator;

import br.com.apt.R;
import br.com.apt.application.BaseActivity;
import br.com.apt.ui.login.LoginActivity;

public class IntroActivity extends BaseActivity implements ViewPager.OnPageChangeListener {

    public static Intent newIntent(Context context) {
        return new Intent(context, IntroActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.intro_activity);

        ViewPager viewPager = (ViewPager) findViewById(R.id.viewPager);
        viewPager.setAdapter(new IntroPager(getSupportFragmentManager()));
        CirclePageIndicator bannerPageIndicator = (CirclePageIndicator) findViewById(R.id.bannerPageIndicator);
        bannerPageIndicator.setViewPager(viewPager);
        viewPager.addOnPageChangeListener(this);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
        if (position == (IntroPager.NUM_PAGES - 1)) {
            startActivity(LoginActivity.newIntent(this));
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            finish();
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    public class IntroPager extends FragmentPagerAdapter {

        public static final int NUM_PAGES = 4 + 1; //+1 pull to LoginActivity

        public IntroPager(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return IntroFragment.newIntent(position);
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }
    }

    @Override
    public boolean showToolbar() {
        return false;
    }

    @Override
    protected boolean checkLogin() {
        return false;
    }
}
