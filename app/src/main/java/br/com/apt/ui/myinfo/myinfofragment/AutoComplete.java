package br.com.apt.ui.myinfo.myinfofragment;

import android.annotation.SuppressLint;
import android.os.Parcel;
import android.text.TextUtils;

import br.com.apt.application.ModelObject;

/**
 * Created by adminbs on 11/7/16.
 */
@SuppressLint("ParcelCreator")
public class AutoComplete implements ModelObject {

    protected int id;
    protected String name;
    protected String marca;
    protected String ano;
    protected int codigo_marca = 0;
    protected int tipo;
    protected int codigo_modelo = 0;
    protected String modelo;
    protected int id_ano;
    protected String uuid;
    protected String title;


    public AutoComplete() {

    }

    public AutoComplete(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getCodigoMarca() {
        return codigo_marca;
    }

    public void setCodigoMarca(int codigo_marca) {
        this.codigo_marca = codigo_marca;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }



    public int getId() {
        if(id_ano > 0){
            return id_ano;
        }
        if(codigo_modelo > 0){
            return codigo_modelo;
        }
        if(codigo_marca > 0){
            return codigo_marca;
        }
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        if(TextUtils.isEmpty(name) && !TextUtils.isEmpty(marca)){
            return marca;
        }
        if(TextUtils.isEmpty(name) && !TextUtils.isEmpty(modelo)){
            return modelo;
        }

        if(TextUtils.isEmpty(name) && !TextUtils.isEmpty(ano)){
            return ano;
        }

        if(TextUtils.isEmpty(name) && TextUtils.isEmpty(ano) && !TextUtils.isEmpty(title)){
            return title;
        }

        return name;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }
}
