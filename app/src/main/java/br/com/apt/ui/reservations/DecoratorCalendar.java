package br.com.apt.ui.reservations;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.IntDef;
import android.support.v4.content.ContextCompat;
import android.text.style.ForegroundColorSpan;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;

import java.util.Calendar;
import java.util.Collection;

import br.com.apt.R;
import br.com.apt.application.App;

/**
 * Created by adminbs on 9/7/17.
 */

public class DecoratorCalendar {
    public static class ClickDecoratorDay implements DayViewDecorator {
        private final Drawable drawable;

        public ClickDecoratorDay(Context context) {
            drawable = ContextCompat.getDrawable(context, R.drawable.day_selector);
        }

        @Override
        public boolean shouldDecorate(CalendarDay day) {
            return true;
        }

        @Override
        public void decorate(DayViewFacade view) {
            view.setSelectionDrawable(drawable);
        }
    }

    public static class ClickDecoratorTextviewColor implements DayViewDecorator {
        private CalendarDay day;

        public ClickDecoratorTextviewColor(CalendarDay day) {
            this.day = day;
        }

        @Override
        public boolean shouldDecorate(CalendarDay day) {
            return this.day.equals(day);
        }

        @Override
        public void decorate(DayViewFacade view) {
            view.addSpan(new ForegroundColorSpan(ContextCompat.getColor(App.getContext(), android.R.color.black)));
        }
    }

    public static class AllDecoratorDays implements DayViewDecorator {
        private Context context;

        public AllDecoratorDays(Context context){
            this.context = context;
        }


        @Override
        public boolean shouldDecorate(CalendarDay day) {
            return true;
        }

        @Override
        public void decorate(DayViewFacade view) {
            view.addSpan(new ForegroundColorSpan(ContextCompat.getColor(context, R.color.calendar_day_text_color)));
            view.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.select_circle));
        }
    }

    public static class DisabledDays implements DayViewDecorator {
        private Context context;
        private Installations installations;

        public DisabledDays(Context context, Installations installations){
            this.context = context;
            this.installations = installations;
        }

        @Override
        public boolean shouldDecorate(CalendarDay day) {
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DAY_OF_MONTH, installations.getMinimum_day_reservation());
            Calendar dayCalendar = day.getCalendar();

            Calendar calendar2 = Calendar.getInstance();
            calendar2.add(Calendar.DAY_OF_MONTH, installations.getMaximum_day_reservation());
            Calendar dayCalendar2 = day.getCalendar();

            Calendar calendarWeekEnd = Calendar.getInstance();
            calendarWeekEnd.setTime(day.getDate());
            String weekDay = String.valueOf(calendarWeekEnd.get(Calendar.DAY_OF_WEEK) - 1);



            if((calendar.compareTo(dayCalendar) > 0 && installations.getMinimum_day_reservation() > 0)
                    || (calendar2.compareTo(dayCalendar2) < 0 && installations.getMaximum_day_reservation() > 0)
                    || !installations.getWork_days().contains(weekDay)){

                return true;
            }
            return calendar.compareTo(dayCalendar) > 0;
        }

        @Override
        public void decorate(DayViewFacade view) {
            view.addSpan(new ForegroundColorSpan(ContextCompat.getColor(context, R.color.previous_disabled)));
            view.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.select_circle_disabled));
        }
    }

}
