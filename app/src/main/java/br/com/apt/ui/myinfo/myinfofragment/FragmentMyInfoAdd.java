package br.com.apt.ui.myinfo.myinfofragment;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.view.View;

import com.android.volley.Request;
import com.davemorrissey.labs.subscaleview.ImageSource;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.BaseGson;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.util.Logger;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomTextView;
import br.com.apt.widget.PagerItemFragment;
import br.com.apt.widget.SendSuccessDialog;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by adminbs on 11/3/16.
 */
public abstract class FragmentMyInfoAdd extends PagerItemFragment {
    public static final int REQUEST_IMAGE_CAPTURE = 3;
    public static final int REQUEST_IMAGE_FROM_GALLERY = 4;

    @Override
    protected abstract String getPagerTitle();
    @Override
    protected abstract int getPagerIcon();

    public void pickPictureOrPickFromGallery() {
        CharSequence[] options = {getString(R.string.takePictures), getString(R.string.chooseGallery)};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("")
                .setItems(options, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        if(which == 0) {
                            takePicture();
                        } else {
                            pickPictureFromGallery();
                        }
                    }
                });
        builder.create().show();
    }

    protected void pickPictureFromGallery() {
        Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
        getIntent.setType("image/*");

        Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickIntent.setType("image/*");

        Intent chooserIntent = Intent.createChooser(getIntent, getString(R.string.choosePhoto));
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[] {pickIntent});

        getActivity().startActivityForResult(chooserIntent, REQUEST_IMAGE_FROM_GALLERY);
    }

    protected void takePicture() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = createImageFile();
                setConfigPhotos(Uri.fromFile(photoFile), photoFile.getAbsolutePath());

            } catch (IOException ex) {
                Logger.t("Ocorreu um erro, por favor tente novamente." + ex.getMessage());
            }
            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                getActivity().startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    public static Bitmap decodeUri(Context c, Uri uri, final int requiredSize) throws FileNotFoundException {
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(c.getContentResolver().openInputStream(uri), null, o);

        int width_tmp = o.outWidth
                , height_tmp = o.outHeight;
        int scale = 1;

        while(true) {
            if(width_tmp / 2 < requiredSize || height_tmp / 2 < requiredSize)
                break;
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        return BitmapFactory.decodeStream(c.getContentResolver().openInputStream(uri), null, o2);
    }

    public Bitmap decodeFile(File f){
        try {
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            FileInputStream stream1=new FileInputStream(f);
            BitmapFactory.decodeStream(stream1,null,o);
            stream1.close();

            final int REQUIRED_SIZE=150;
            int width_tmp=o.outWidth, height_tmp=o.outHeight;
            int scale=1;
            while(true){
                if(width_tmp/2<REQUIRED_SIZE || height_tmp/2<REQUIRED_SIZE)  break;
                width_tmp/=2;
                height_tmp/=2;
                scale*=2;
            }

            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize=scale;
            FileInputStream stream2=new FileInputStream(f);
            Bitmap bitmap=BitmapFactory.decodeStream(stream2, null, o2);
            stream2.close();
            return bitmap;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    protected void setConfigPhotos(Uri mCurrentPhotoUri, String mCurrentPhotoPath){

    }

    protected File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        return File.createTempFile(imageFileName, ".jpg", storageDir);
    }

    public void onActivityResultFragment(int requestCode, int resultCode, Intent data) {
    }

    protected abstract HashMap<String, String> save();

    public Uri getCurrentPhotoUri(){
        return null;
    }

    public String getCurrentPhotoPath(){
        return null;
    }

    protected void sendSave(){

        HashMap<String, String> save = save();
        if(save != null){

            File finalFile = null;
            try {
                if(getCurrentPhotoUri() != null){
                    finalFile = new File("");
                    InputStream photoInputStream = App.getContext().getContentResolver().openInputStream(getCurrentPhotoUri());
                    finalFile = Util.resizeFile(getCurrentPhotoPath(), photoInputStream);
                }else{
                    finalFile = createImageFile();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            Volley volley = new Volley(new Volley.Callback() {
                @Override
                public void result(String result, int type) {
                    if(result != null){
                        BaseGson baseGson = App.getGson().fromJson(result, BaseGson.class);
                        if(getActivity() != null) {
                            Util.showDialog(getActivity(), baseGson.getMessage());
                        }
                        if(baseGson.getStatus() == Volley.STATUS_OK){
                            saved();
                            saved(result);
                        }
                    }
                }

                @Override
                public String uri() {
                    return getUri();
                }
            });
            volley.showDialog(getActivity());
            volley.request(getTypeRequest(), save, 0, finalFile, "image");
        }
    }

    public void removeItem(final int id, final FragmentMyInfoAdd.CallbackDelete callbackDelete){
        removeItem(id, callbackDelete, null);
    }

    public void removeItem(final int id, final FragmentMyInfoAdd.CallbackDelete callbackDelete, String message){
        if(message == null){
            message = getString(R.string.ask_remove);
        }

        SendSuccessDialog.show(message, null, getFragmentManager(), new SendSuccessDialog.Actions() {
            @Override
            public void back() {
                Volley volley = new Volley(new Volley.Callback() {
                    @Override
                    public void result(String result, int type) {
                        if(result != null){
                            BaseGson baseGson = App.getGson().fromJson(result, BaseGson.class);
                            SendSuccessDialog.show(baseGson.getMessage(), null, getFragmentManager());
                            callbackDelete.delete();
                        }
                    }
                    @Override
                    public String uri() {
                        return getUri();
                    }
                });
                volley.showDialog(getActivity());
                JSONObject removeId = new JSONObject();
                try {
                    removeId.put("id", id);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                volley.request(Request.Method.DELETE, removeId.toString());
            }
        }, SendSuccessDialog.TYPE_DYNAMIC);


    }


    protected abstract String getUri();
    protected abstract int getTypeRequest();
    protected abstract void saved();
    protected abstract void saved(String json);

    public interface CallbackDelete{
        void delete();
    }

}
