package br.com.apt.ui.myinfo.myinfofragment.resident;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;

import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.apt.R;
import br.com.apt.application.Volley;
import br.com.apt.ui.myinfo.myinfofragment.NewUser;
import br.com.apt.ui.tickets.tickets.Ticket;
import br.com.apt.ui.tickets.tickets.TicketsAdapter;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomTextViewLight;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by adminbs on 7/17/17.
 */

public class ResidentAdapter extends BaseAdapter {

    private Context context;
    private List<NewUser> residents;

    public ResidentAdapter(Context context, List<NewUser> residents) {
        this.context = context;
        this.residents = residents;
    }

    @Override
    public int getCount() {
        return residents != null ? residents.size() : 0;
    }

    @Override
    public NewUser getItem(int position) {
        return residents.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ResidentAdapter.ViewHolder holder;
        if(convertView == null){
            view = LayoutInflater.from(context).inflate(R.layout.adapter_residents, parent, false);
            holder = new ResidentAdapter.ViewHolder(view);
            view.setTag(holder);
        }else{
            view = convertView;
            holder = (ResidentAdapter.ViewHolder) view.getTag();
        }

        bindList(holder, position);
        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private void bindList(ResidentAdapter.ViewHolder holder, int position) {
        if(position % 2 == 0){
            holder.baseBackground.setBackground(ContextCompat.getDrawable(context, R.drawable.button_menu_list_dark));
        }else{
            holder.baseBackground.setBackground(ContextCompat.getDrawable(context, R.drawable.button_menu_list_light));
        }

        NewUser newUser = getItem(position);
        if(newUser != null){
            Picasso.with(context).load(Volley.getUrlByImage(newUser.getImage())).placeholder(R.drawable.placeholder_camera).into(holder.image);

            holder.title.setText(newUser.getName());

            holder.description.setText(
                    String.format(context.getText(R.string.description_delivery).toString(), Util.dateFormatted(newUser.getCreated_at()), Util.timeFormatted(newUser.getCreated_at()))
            );
        }
    }

    public class ViewHolder{
        private RelativeLayout baseBackground;
        private CircleImageView image;
        private CustomTextViewLight title;
        private CustomTextViewLight description;
        private CustomTextViewLight label;

        public ViewHolder(View view) {
            baseBackground = (RelativeLayout) view.findViewById(R.id.baseBackground);
            image = (CircleImageView) view.findViewById(R.id.deliveryImage);
            title = (CustomTextViewLight) view.findViewById(R.id.deliveryTitle);
            description = (CustomTextViewLight) view.findViewById(R.id.deliveryDescription);
            label = (CustomTextViewLight) view.findViewById(R.id.deliveryLabel);
        }
    }
}
