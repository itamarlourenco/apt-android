package br.com.apt.ui.lobby.lobbyFragments.safety;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.android.volley.Request;
import com.github.ybq.android.spinkit.SpinKitView;
import com.google.android.gms.common.api.Releasable;

import java.util.ArrayList;
import java.util.List;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.BaseGson;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.model.user.User;
import br.com.apt.ui.ExceptionWithoutResult;
import br.com.apt.ui.lobby.lobbyFragments.visits.VisitRequest;
import br.com.apt.ui.lobby.lobbyFragments.visits.VisitsListViewAdapter;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomTextView;
import br.com.apt.widget.PagerItemFragment;
import br.com.apt.widget.ViewPagerCustom;

public class SafetyFragment extends PagerItemFragment implements Volley.Callback {

    private SpinKitView loader;
    private List<Safe> safeList = new ArrayList<>();
    private RelativeLayout canvasSafety;
    private ViewPagerCustom viewPager;

    @Override
    public String uri() {
        return URI.CAMERAS;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_safety, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if(view != null){
            loader = (SpinKitView) view.findViewById(R.id.loader);
            canvasSafety = (RelativeLayout) view.findViewById(R.id.canvasSafety);
            viewPager = (ViewPagerCustom) view.findViewById(R.id.viewPager);
        }

        User user = getUser();
        if(user != null){
            Volley volley = new Volley(this, new String[]{
                    String.valueOf(user.getBuildId())
            });
            volley.request(Request.Method.POST, null);
        }

    }

    @Override
    protected String getPagerTitle() {
        return App.getContext().getString(R.string.safety);
    }

    @Override
    protected int getPagerIcon() {
        return R.drawable.ico_seguranca;
    }

    @Override
    public void result(String result, int type) {
        View view = getView();
        try{
            Util.hidePlaceholder(view);
            Util.changeVisibility(loader, View.GONE, true);
            Util.changeVisibility(canvasSafety, View.VISIBLE, true);

            SafeRequest safeRequest = App.getGson().fromJson(result, SafeRequest.class);
            safeList = safeRequest.getObject();

            if(safeList == null){
                throw new ExceptionWithoutResult(getString(R.string.emptySecurityTitle), R.drawable.seguranca);
            }
            viewPager.setAdapter(new SafetyAdapter(getChildFragmentManager()), view);
        }catch (ExceptionWithoutResult exceptionWithoutResult){
            Util.showPlaceholder(view, getActivity(), exceptionWithoutResult.getDrawableId(), exceptionWithoutResult.getMessage());
        }catch (Exception exception){
            Util.handlePlaceholderError(view, getActivity());
        }
    }


    private class SafetyAdapter extends FragmentPagerAdapter {

        public SafetyAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return SafeFragment.newInstance(safeList.get(position));
        }

        @Override
        public int getCount() {
            return safeList == null ? 0 : safeList.size();
        }

    }
}
