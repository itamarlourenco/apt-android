package br.com.apt.ui.lobby.lobbyFragments.deliveries;

import com.google.gson.annotations.SerializedName;

import br.com.apt.application.SendRequest;

/**
 * Created by adminbs on 1/15/16.
 */
public class DeliverySend extends SendRequest {
    @SerializedName("edificioId")
    public String buildId;

    @SerializedName("id")
    public String userId;

    public String getBuildId() {
        return buildId;
    }

    public void setBuildId(String buildId) {
        this.buildId = buildId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}