package br.com.apt.ui.firstAccess.animations;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.android.volley.Request;

import br.com.apt.BuildConfig;
import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.BaseFragment;
import br.com.apt.application.BaseGson;
import br.com.apt.application.BaseGsonMeuAdm;
import br.com.apt.application.SendRequest;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.model.user.UserRequest;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomButton;
import br.com.apt.widget.CustomEditText;
import br.com.apt.widget.CustomTextView;

/**
 * Created by adminbs on 9/12/16.
 */
public class ForgetAnimationFragment extends BaseFragment implements View.OnClickListener, Volley.Callback {

    public static ForgetAnimationListener forgetAnimationListener;

    private CustomEditText email;
    private CustomButton btnSend;
    private LinearLayout baseFeedBack;
    private LinearLayout baseForgetPassword;

    public static ForgetAnimationFragment newInstance(ForgetAnimationListener forgetAnimationListener){
        ForgetAnimationFragment.forgetAnimationListener =  forgetAnimationListener;
        return new ForgetAnimationFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.forget_animate, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if(view != null){

            baseFeedBack = (LinearLayout) view.findViewById(R.id.baseFeedBack);
            baseForgetPassword = (LinearLayout) view.findViewById(R.id.baseForgetPassword);


            (view.findViewById(R.id.btnBack)).setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    forgetAnimationListener.back(v);
                }
            });
            email = (CustomEditText) view.findViewById(R.id.email);

            if(App.isDeviceHml()){
                email.setText("fatimabarreto2014@gmail.com");
            }

            btnSend = (CustomButton) view.findViewById(R.id.btnSend);
            btnSend.setOnClickListener(this);


            (view.findViewById(R.id.backToLogin)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Util.changeVisibility(baseForgetPassword, View.VISIBLE, true);
                    Util.changeVisibility(baseFeedBack, View.GONE, true);
                }
            });
        }
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btnSend){
            recoverPassword();
        }
    }

    private void recoverPassword() {
        if(TextUtils.isEmpty(email.getText())){
            Util.showDialog(getActivity(), R.string.error_lost_passowrd);
            return;
        }

        LostPasswordAdm lostPassword = new LostPasswordAdm();
        lostPassword.setEmail(email.getText().toString());
        lostPassword.setIdCompany(getString(R.string.companyKey));

        Volley volley = new Volley(this);
        volley.showDialog(getActivity());
        volley.isMeuADM(true);
        volley.request(Request.Method.POST, lostPassword.toJson());
    }

    public interface ForgetAnimationListener{
        void back(View view);
    }

    @Override
    public String uri() {
        return URI.LOST_PASSWORD_MEUADM;
    }

    public class LostPassword extends SendRequest {
        public String username;

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }
    }

    public class LostPasswordAdm extends SendRequest {
        public String email;
        public String idCompany;

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getIdCompany() {
            return idCompany;
        }

        public void setIdCompany(String idCompany) {
            this.idCompany = idCompany;
        }
    }

    @Override
    public void result(String result, int type) {

        BaseGsonMeuAdm baseGson = App.getGson().fromJson(result, BaseGsonMeuAdm.class);
        if(baseGson != null && baseGson.getCode() == Volley.STATUS_OK) {
            Util.changeVisibility(baseForgetPassword, View.GONE, true);
            Util.changeVisibility(baseFeedBack, View.VISIBLE, true);
            CustomTextView feedBackPassoword = (CustomTextView) baseFeedBack.findViewById(R.id.feedBackPassoword);
            feedBackPassoword.setText(String.format(getString(R.string.feedBackPassoword), email.getText()));

            View view = getView();
            if(view != null){
                (view.findViewById(R.id.topMsg)).setVisibility(View.VISIBLE);
            }
        }else{
            Util.changeVisibility(baseForgetPassword, View.GONE, true);
            Util.changeVisibility(baseFeedBack, View.VISIBLE, true);
            CustomTextView feedBackPassoword = (CustomTextView) baseFeedBack.findViewById(R.id.feedBackPassoword);

            feedBackPassoword.setText(baseGson == null ? getString(R.string.error_not_response_lost_password) : baseGson.getMessage());

            View view = getView();
            if(view != null){
                (view.findViewById(R.id.topMsg)).setVisibility(View.INVISIBLE);
            }
        }


    }
}
