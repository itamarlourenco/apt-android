package br.com.apt.ui.myCondominium.myCondominiumFragments.EdictsAndMinutes;

import java.util.List;

import br.com.apt.application.BaseGsonMeuAdm;
import br.com.apt.ui.myCondominium.myCondominiumFragments.Attachment;

/**
 * Created by meuapt on 16/03/18.
 */

public class EdictsAndMinutesRequest extends BaseGsonMeuAdm {
    private List<Attachment> object;

    public List<Attachment> getObject() {
        return object;
    }

    public void setObject(List<Attachment> object) {
        this.object = object;
    }
}
