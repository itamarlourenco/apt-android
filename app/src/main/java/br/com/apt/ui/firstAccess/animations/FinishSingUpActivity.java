package br.com.apt.ui.firstAccess.animations;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import br.com.apt.application.FullScreenBaseActivity;
import br.com.apt.ui.reservations.CalendarFragment;

/**
 * Created by adminbs on 10/3/16.
 */
public class FinishSingUpActivity extends FullScreenBaseActivity {

    public static final String EXTRA_USERNAME = "EXTRA_USERNAME";
    public static final String EXTRA_PASSWORD = "EXTRA_PASSWORD";
    public static final String EXTRA_NAME = "EXTRA_NAME";
    public static final String EXTRA_APTO = "EXTRA_APTO";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static Intent newIntent(Context context, String username, String password, String name, String apto){
        Intent intent = new Intent(context, FinishSingUpActivity.class);
        intent.putExtra(FinishSingUpActivity.EXTRA_USERNAME, username);
        intent.putExtra(FinishSingUpActivity.EXTRA_PASSWORD, password);
        intent.putExtra(FinishSingUpActivity.EXTRA_NAME, name);
        intent.putExtra(FinishSingUpActivity.EXTRA_APTO, apto);

        return intent;
    }

    @Override
    protected boolean checkLogin() {
        return false;
    }

    @Override
    protected Fragment getFragment() {
        return FinishSingUpFragment.newInstance();
    }

}
