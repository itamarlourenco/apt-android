package br.com.apt.ui.lobby.lobbyFragments.moreVisits;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.android.volley.Request;
import com.squareup.picasso.Cache;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.BaseGson;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.ui.lobby.LobbyFragment;
import br.com.apt.ui.myinfo.myinfofragment.FragmentMyInfoAdd;
import br.com.apt.ui.myinfo.myinfofragment.FragmentMyInfoAddImage;
import br.com.apt.util.Logger;
import br.com.apt.util.Mask;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomButton;
import br.com.apt.widget.CustomEditText;
import br.com.apt.widget.CustomTextView;
import br.com.apt.widget.CustomTextViewBold;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by adminbs on 11/7/16.
 */
public class MoreVisitsFragment extends FragmentMyInfoAddImage implements View.OnClickListener, LobbyFragment.CounterUpdateListener  {

    public static CircleImageView profileImageVisits;
    private static Uri mCurrentPhotoUriVisits;
    private static String mCurrentPhotoPathVisits;

    private ListView listView;
    private MoreVisitsAdapter mAdapter;
    private List<MoreVisits> mMoreVisits;
    private LinearLayout progressBarFeedback;
    private LinearLayout firstVisitsScreen;
    private LinearLayout secondVisitsScreen;

    private CustomEditText name;
    private CustomEditText date;
    private CustomEditText time;
    private CustomTextView feedBack;


    private CustomButton buttonAddMoreVisits;
    private CustomButton buttonSaveMoreVisits;
    private CustomButton buttonCancel;
    private CustomButton buttonUpdateMoreVisits;


    public int moreVisitsId = 0;
    private RelativeLayout errorContainer;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_more_visits, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final View view = getView();
        if (view != null) {
            profileImageVisits = (CircleImageView) view.findViewById(R.id.profileImage);
            profileImageVisits.setOnClickListener(this);

            listView = (ListView) view.findViewById(R.id.listView);
            progressBarFeedback = (LinearLayout) view.findViewById(R.id.progressBarFeedback);
            firstVisitsScreen = (LinearLayout) view.findViewById(R.id.firstVisitsScreen);
            secondVisitsScreen = (LinearLayout) view.findViewById(R.id.secondVisitScreen);
            errorContainer = (RelativeLayout)view.findViewById(R.id.errorAddVisitsContainer);
            buttonSaveMoreVisits = (CustomButton) view.findViewById(R.id.buttonSaveMoreVisits);
            buttonSaveMoreVisits.setOnClickListener(this);

            buttonCancel = (CustomButton) view.findViewById(R.id.buttonCancel);
            buttonCancel.setOnClickListener(this);
            buttonCancel.setVisibility(View.GONE);

            buttonUpdateMoreVisits = (CustomButton) view.findViewById(R.id.buttonUpdateMoreVisits);
            buttonUpdateMoreVisits.setOnClickListener(this);
            buttonUpdateMoreVisits.setVisibility(View.GONE);

            name = (CustomEditText) view.findViewById(R.id.name);
            date = (CustomEditText) view.findViewById(R.id.date);
            date.addTextChangedListener(Mask.insert("##/##/####", date));

            time = (CustomEditText) view.findViewById(R.id.time);
            time.addTextChangedListener(Mask.insert("##:##", time));

            buttonAddMoreVisits = (CustomButton) view.findViewById(R.id.buttonAddMoreVisits);
            buttonAddMoreVisits.setOnClickListener(this);
            buttonAddMoreVisits.setVisibility(View.GONE);

            feedBack = (CustomTextView) view.findViewById(R.id.feedBack);

            view.findViewById(R.id.close).setOnClickListener(this);

            getMoreVisits();
        }
    }

    public void getMoreVisits(){
        Volley volley = new Volley(new Volley.Callback() {
            @Override
            public void result(String result, int type) {
                if(result != null){
                    MoreVisitsRequest moreVisitsRequest = App.getGson().fromJson(result, MoreVisitsRequest.class);
                    if(moreVisitsRequest != null){
                        mMoreVisits = moreVisitsRequest.getObject();
                        if(mMoreVisits != null && mMoreVisits.size() > 0) {
                            mAdapter = new MoreVisitsAdapter();
                            listView.setAdapter(mAdapter);
                            buttonAddMoreVisits.setVisibility(View.VISIBLE);
                            errorContainer.setVisibility(View.GONE);
                            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    final MoreVisits moreVisits = mMoreVisits.get(position);
                                    if (moreVisits != null) {
                                        MoreVisitsFragment.this.moreVisitsId = moreVisits.getId();

                                        Picasso.with(getContext()).load(Volley.getUrlByImage(moreVisits.getImage())).placeholder(R.drawable.default_camera).into(profileImageVisits);
                                        name.setText(moreVisits.getName());

                                        try {
                                            Date dateDate = new SimpleDateFormat("yyyy-MM-dd").parse(moreVisits.getArrivalDate());
                                            String formattedDate = new SimpleDateFormat("dd/MM/yyyy ").format(dateDate);
                                            date.setText(formattedDate);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                        time.setText(moreVisits.getArrivalTime());

                                        Util.changeVisibility(secondVisitsScreen, View.GONE, true);
                                        Util.changeVisibility(firstVisitsScreen, View.VISIBLE, true);

                                        buttonCancel.setVisibility(View.VISIBLE);
                                        buttonUpdateMoreVisits.setVisibility(View.VISIBLE);
                                        buttonSaveMoreVisits.setVisibility(View.GONE);
                                        buttonAddMoreVisits.setVisibility(View.GONE);

                                        if(moreVisits.getStatus().equals(MoreVisits.STATUS_RELEASED)){
                                            name.setEnabled(false);
                                            date.setEnabled(false);
                                            time.setEnabled(false);
                                            buttonCancel.setVisibility(View.GONE);
                                            buttonUpdateMoreVisits.setVisibility(View.GONE);
                                        }
                                    }

                                }
                            });
//                            Util.changeVisibility(progressBarFeedback, View.GONE, true);
                            progressBarFeedback.setVisibility(View.GONE);
                            Util.changeVisibility(secondVisitsScreen, View.VISIBLE, true);
                        }else{
//                            Util.changeVisibility(progressBarFeedback, View.GONE, true);
                            progressBarFeedback.setVisibility(View.GONE);
//                            Util.changeVisibility(feedBack, View.VISIBLE, true);
//                            Util.changeVisibility(secondVisitsScreen, View.VISIBLE, true);
//                            feedBack.setText(getString(R.string.no_more_visits));
                            Util.hideFeedBackFragment(getView(),R.id.errorAddVisitsContainer);
                            secondVisitsScreen.setVisibility(View.GONE);
                            buttonAddMoreVisits.setVisibility(View.VISIBLE);
                        }
                    }
                }
            }
            @Override
            public String uri() {
                return URI.MORE_VISITS;
            }
        });
        volley.request(Request.Method.GET, null);
    }

    @Override
    protected String getPagerTitle() {
        return App.getContext().getString(R.string.residents);
    }

    @Override
    protected int getPagerIcon() {
        return R.drawable.visits_plus;
    }

    @Override
    protected HashMap<String, String> save() {
        if(TextUtils.isEmpty(name.getText())){
            Util.showDialog(getActivity(), R.string.please_enter_with_name);
            return null;
        }
        if(TextUtils.isEmpty(date.getText())){
            Util.showDialog(getActivity(), R.string.enter_with_date);
            return null;
        }

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("name", name.getText().toString());
        String formattedDate = "";
        try {
            String dateString = date.getText().toString();

            DateFormat dateDate = new SimpleDateFormat("dd/MM/yyyy");
            dateDate.setLenient(false);
            dateDate.parse(dateString);

            formattedDate = new SimpleDateFormat("yyyy-MM-dd").format(new SimpleDateFormat("dd/MM/yyyy").parseObject(dateString));
            hashMap.put("arrival_date", formattedDate);

            if(TextUtils.isEmpty(dateString)){
                Util.showDialog(getActivity(), R.string.enter_with_date_correct);
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            Util.showDialog(getActivity(), R.string.enter_with_date_correct);
            return null;
        }

        String timeString = time.getText().toString();

        if(!timeString.isEmpty()){
            String dateHour = String.format("%s %s",formattedDate, timeString);

            int compareResult = Util.compareDateWithNow(formattedDate);

            switch (compareResult){
                case Util.DATE_EQUAL :
                    if(!Util.validateTime(timeString)){
                        Util.showDialog(getActivity(), R.string.enter_with_time_correct);
                        return null;
                    }
                    break;
                case Util.DATE_HIGHER:
                    if(!Util.validateTime(timeString)){
                        Util.showDialog(getActivity(), R.string.enter_with_time_correct);
                        return null;
                    }
                    break;
                case Util.DATE_LESSER:
                    Util.showDialog(getActivity(), R.string.enter_with_time_higher);
                    return null;
                default:
                    Util.showDialog(getActivity(), R.string.enter_with_time_correct);
                    return null;
            }
        }else{
            Util.showDialog(getActivity(), R.string.enter_with_time_correct);
            return null;
        }

        hashMap.put("arrival_time", timeString);

        if(moreVisitsId != 0){
            hashMap.put("id", String.valueOf(moreVisitsId));
        }

        buttonSaveMoreVisits.setEnabled(false);

        return hashMap;
    }

    @Override
    protected String getUri() {
        return URI.MORE_VISITS;
    }

    @Override
    protected int getTypeRequest() {
        return Request.Method.POST;
    }

    @Override
    protected void saved() {
        getMoreVisits();
        Util.changeVisibility(secondVisitsScreen, View.VISIBLE, true);
        Util.changeVisibility(firstVisitsScreen, View.GONE, true);
        buttonSaveMoreVisits.setEnabled(true);
        mCurrentPhotoPathVisits = null;
        mCurrentPhotoUriVisits = null;
    }

    @Override
    protected void saved(String json) {

    }

    @Override
    protected void setConfigPhotos(Uri mCurrentPhotoUri, String mCurrentPhotoPath) {
        mCurrentPhotoUriVisits = mCurrentPhotoUri;
        mCurrentPhotoPathVisits = mCurrentPhotoPath;
    }

    @Override
    public void onActivityResultFragment(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK && profileImageVisits != null && mCurrentPhotoUriVisits != null) {
            profileImageVisits.setVisibility(View.VISIBLE);
            profileImageVisits.setImageBitmap(decodeFile(new File(mCurrentPhotoUriVisits.getPath())));
            return;
        }

        if(requestCode == REQUEST_IMAGE_FROM_GALLERY && resultCode == Activity.RESULT_OK && profileImageVisits != null) {
            try {
                profileImageVisits.setImageBitmap(decodeUri(App.getContext(), data.getData(), 150));
                mCurrentPhotoPathVisits = createImageFile().getAbsolutePath();
                mCurrentPhotoUriVisits = data.getData();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public Uri getCurrentPhotoUri() {
        return mCurrentPhotoUriVisits;
    }

    @Override
    public String getCurrentPhotoPath() {
        return mCurrentPhotoPathVisits;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonAddMoreVisits:
                buttonSaveMoreVisits.setEnabled(true);
                Util.changeVisibility(secondVisitsScreen, View.GONE, true);
                Util.changeVisibility(firstVisitsScreen, View.VISIBLE, true);
                profileImageVisits.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.default_camera));
                addNewVisits();
                buttonAddMoreVisits.setVisibility(View.GONE);
                break;

            case R.id.close:
                Util.changeVisibility(secondVisitsScreen, View.VISIBLE, true);
                Util.changeVisibility(firstVisitsScreen, View.GONE, true);
                if(mMoreVisits == null || mMoreVisits.size() <= 0){
                    Util.changeVisibility(feedBack, View.VISIBLE, true);
                }
                buttonAddMoreVisits.setVisibility(View.VISIBLE);
                break;

            case R.id.profileImage:
                this.pickPictureOrPickFromGallery();
                break;

            case R.id.buttonCancel:
                cancelVisits();
                break;

            case R.id.buttonSaveMoreVisits:
                this.sendSave();
                break;

            case R.id.buttonUpdateMoreVisits:
                this.sendSave();
                break;

        }
    }

    private void addNewVisits() {
        name.setText("");
        date.setText("");
        time.setText("");

        name.setEnabled(true);
        date.setEnabled(true);
        time.setEnabled(true);

        buttonCancel.setVisibility(View.GONE);
        buttonSaveMoreVisits.setVisibility(View.VISIBLE);
        buttonUpdateMoreVisits.setVisibility(View.GONE);


        Util.changeVisibility(feedBack, View.GONE, true);
    }

    public void cancelVisits(){
        Util.showDialog(getActivity(), getString(R.string.ask_remove_more_visits), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Volley volley = new Volley(new Volley.Callback() {
                    @Override
                    public void result(String result, int type) {
                        if(result != null){
                            if(listView != null){
                                listView.setAdapter(null);
                            }
                            BaseGson baseGson = App.getGson().fromJson(result, BaseGson.class);
                            Util.showDialog(getActivity(), baseGson.getMessage());
                            getMoreVisits();
                            Util.changeVisibility(secondVisitsScreen, View.VISIBLE, true);
                            Util.changeVisibility(firstVisitsScreen, View.GONE, true);
                        }
                    }

                    @Override
                    public String uri() {
                        return URI.MORE_VISITS;
                    }
                });
                volley.showDialog(getActivity());

                JSONObject removeId = new JSONObject();
                try {
                    removeId.put("id", MoreVisitsFragment.this.moreVisitsId);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                volley.request(Request.Method.DELETE, removeId.toString());
            }
        });
    }

    @Override
    public void onCounterUpdate() {
        getMoreVisits();
    }

    public class MoreVisitsAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return mMoreVisits == null ? 0 : mMoreVisits.size();
        }

        @Override
        public MoreVisits getItem(int position) {
            return mMoreVisits.get(position);
        }

        @Override
        public long getItemId(int position) {
            return getItem(position).getId();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;
            if(convertView == null) {
                convertView = getActivity().getLayoutInflater().inflate(R.layout.adapter_more_visits, parent, false);

                viewHolder = new ViewHolder();
                viewHolder.name = (CustomTextView) convertView.findViewById(R.id.name);
                viewHolder.when = (CustomTextView) convertView.findViewById(R.id.when);
                viewHolder.status = (CustomTextViewBold) convertView.findViewById(R.id.status);
                viewHolder.image = (CircleImageView) convertView.findViewById(R.id.profile_image);
                viewHolder.remove = (LinearLayout) convertView.findViewById(R.id.remove);
                viewHolder.canvas = (LinearLayout) convertView.findViewById(R.id.canvas);

                convertView.setTag(viewHolder);
            }else{
                viewHolder = (ViewHolder) convertView.getTag();
            }

            final MoreVisits moreVisits = getItem(position);
            if(moreVisits != null){
                viewHolder.name.setText(moreVisits.getName());

                try {
                    Date dateDate = new SimpleDateFormat("yyyy-MM-dd").parse(moreVisits.getArrivalDate());
                    String formattedDate = new SimpleDateFormat("dd/MM/yyyy ").format(dateDate);
                    viewHolder.when.setText("Visita em "+ formattedDate);
                } catch (Exception e) {
                    e.printStackTrace();
                }


                if(MoreVisits.STATUS_RELEASED.equals(moreVisits.getStatus())){
                    viewHolder.status.setTextColor(ContextCompat.getColor(getContext(), R.color.lobby_color));
                    try {
                        Date dateDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(moreVisits.getReleased());
                        String formattedDate = new SimpleDateFormat("dd/MM HH:mm").format(dateDate);
                        viewHolder.status.setText("Liberado " + formattedDate + "h");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }else if(MoreVisits.STATUS_WAITING.equals(moreVisits.getStatus())){
                    viewHolder.status.setTextColor(ContextCompat.getColor(getContext(), R.color.consumption_color));
                    viewHolder.status.setText(getString(R.string.waiting_time));
                }

                Picasso.with(getContext()).load(Volley.getUrlByImage(moreVisits.getImage(),true)).placeholder(R.drawable.default_camera).into(viewHolder.image);
                viewHolder.remove.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        removeItem(moreVisits.getId(), new FragmentMyInfoAdd.CallbackDelete(){
                            @Override
                            public void delete() {
                                getMoreVisits();
                            }
                        },getString(R.string.ask_remove_more_visits));
                    }
                });


                if(position % 2 == 0){
                    viewHolder.canvas.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.list_view_line_black));
                }else{
                    viewHolder.canvas.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.list_view_line_black_light));
                }
            }

            return convertView;
        }

        public class ViewHolder{
            public CustomTextView name;
            public CustomTextView when;
            public CustomTextViewBold status;
            public CircleImageView image;
            public LinearLayout remove;
            public LinearLayout canvas;
        }
    }
}
