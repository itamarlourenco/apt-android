package br.com.apt.ui.myapt.myaptFragments.chat;

import com.google.gson.annotations.SerializedName;

import br.com.apt.application.SendRequest;

/**
 * Created by itamarlourenco on 24/01/16.
 */
public class MessagePushChat extends SendRequest {
    private String message;

    @SerializedName("blocoId")
    private String blockId;

    private String apto;

    @SerializedName("edificioId")
    private String buildId;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getBlockId() {
        return blockId;
    }

    public void setBlockId(String blockId) {
        this.blockId = blockId;
    }

    public String getApto() {
        return apto;
    }

    public void setApto(String apto) {
        this.apto = apto;
    }

    public String getBuildId() {
        return buildId;
    }

    public void setBuildId(String buildId) {
        this.buildId = buildId;
    }
}
