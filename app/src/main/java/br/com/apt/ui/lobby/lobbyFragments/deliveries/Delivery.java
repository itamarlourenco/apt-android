package br.com.apt.ui.lobby.lobbyFragments.deliveries;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import com.activeandroid.Model;
import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import br.com.apt.R;
import br.com.apt.application.ModelObject;
import br.com.apt.model.CustomModel;
import br.com.apt.util.Util;

/**
 * Created by itamarlourenco on 10/01/16.
 */
public class Delivery implements ModelObject {
    private long id;

    @SerializedName("edificioId")
    private String buildId;

    @SerializedName("usuarioId")
    private String userId;

    @SerializedName("data")
    private Date date;

    @SerializedName("obs")
    private String description;

    @SerializedName("imagem")
    private String image;

    @SerializedName("retirado")
    private boolean isWithdrawn;

    @SerializedName("dtaRetirado")
    private Date dateWithdrawn;

    @SerializedName("nomeUsuario")
    private String nameUser;

    @SerializedName("apto")
    private String apto;

    @SerializedName("nomeBloco")
    private String nameBlock;

    @SerializedName("usuarioRetirou")
    private String userWithDraw;

    @SerializedName("usuarioRetirouImagem")
    private String userWithdrawImage;


    public Delivery() {
    }

    public Delivery(long id, String buildId, String userId, Date date, String description, String image, boolean isWithdrawn, Date dateWithdrawn, String nameUser, String apto, String nameBlock) {
        this.id = id;
        this.buildId = buildId;
        this.userId = userId;
        this.date = date;
        this.description = description;
        this.image = image;
        this.isWithdrawn = isWithdrawn;
        this.dateWithdrawn = dateWithdrawn;
        this.nameUser = nameUser;
        this.apto = apto;
        this.nameBlock = nameBlock;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getBuildId() {
        return buildId;
    }

    public void setBuildId(String buildId) {
        this.buildId = buildId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isWithdrawn() {
        return isWithdrawn;
    }

    public void setIsWithdrawn(boolean isWithdrawn) {
        this.isWithdrawn = isWithdrawn;
    }

    public Date getDateWithdrawn() {
        return dateWithdrawn;
    }

    public void setDateWithdrawn(Date dateWithdrawn) {
        this.dateWithdrawn = dateWithdrawn;
    }

    public String getNameUser() {
        return nameUser;
    }

    public void setNameUser(String nameUser) {
        this.nameUser = nameUser;
    }

    public String getApto() {
        return apto;
    }

    public void setApto(String apto) {
        this.apto = apto;
    }

    public String getNameBlock() {
        return nameBlock;
    }

    public void setNameBlock(String nameBlock) {
        this.nameBlock = nameBlock;
    }

    public String getUserWithDraw() {
        return userWithDraw;
    }

    public void setUserWithDraw(String userWithDraw) {
        this.userWithDraw = userWithDraw;
    }

    public String getUserWithdrawImage() {
        return userWithdrawImage;
    }

    public void setUserWithdrawImage(String userWithdrawImage) {
        this.userWithdrawImage = userWithdrawImage;
    }

    public String getCompleteDate(Context context){
        return String.format(context.getText(R.string.description_delivery).toString(), Util.dateFormatted(getDate()), Util.timeFormatted(getDate()));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.buildId);
        dest.writeString(this.userId);
        dest.writeLong(this.date != null ? this.date.getTime() : -1);
        dest.writeString(this.description);
        dest.writeString(this.image);
        dest.writeByte(this.isWithdrawn ? (byte) 1 : (byte) 0);
        dest.writeLong(this.dateWithdrawn != null ? this.dateWithdrawn.getTime() : -1);
        dest.writeString(this.nameUser);
        dest.writeString(this.apto);
        dest.writeString(this.nameBlock);
        dest.writeString(this.userWithDraw);
        dest.writeString(this.userWithdrawImage);
    }

    protected Delivery(Parcel in) {
        this.id = in.readLong();
        this.buildId = in.readString();
        this.userId = in.readString();
        long tmpDate = in.readLong();
        this.date = tmpDate == -1 ? null : new Date(tmpDate);
        this.description = in.readString();
        this.image = in.readString();
        this.isWithdrawn = in.readByte() != 0;
        long tmpDateWithdrawn = in.readLong();
        this.dateWithdrawn = tmpDateWithdrawn == -1 ? null : new Date(tmpDateWithdrawn);
        this.nameUser = in.readString();
        this.apto = in.readString();
        this.nameBlock = in.readString();
        this.userWithDraw = in.readString();
        this.userWithdrawImage = in.readString();
    }

    public static final Creator<Delivery> CREATOR = new Creator<Delivery>() {
        @Override
        public Delivery createFromParcel(Parcel source) {
            return new Delivery(source);
        }

        @Override
        public Delivery[] newArray(int size) {
            return new Delivery[size];
        }
    };
}
