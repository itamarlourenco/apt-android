package br.com.apt.ui.myCondominium.myCondominiumFragments;

import java.util.List;

import br.com.apt.application.BaseGsonMeuAdm;

/**
 * Created by adminbs on 8/6/17.
 */

public class AttachmentsRequest extends BaseGsonMeuAdm {

    private List<Attachments> object;

    public List<Attachments> getObject() {
        return object;
    }

    public void setObject(List<Attachments> object) {
        this.object = object;
    }
}
