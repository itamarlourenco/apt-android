package br.com.apt.ui.myapt.myaptFragments.build;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;

import java.text.NumberFormat;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.BaseGson;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.model.user.User;
import br.com.apt.ui.MyAdm.MyAdmActivity;
import br.com.apt.ui.MyAdm.myadmFragments.sac.billet.BilletFragment;
import br.com.apt.ui.main.navigationDrawer.NavigationDrawerMenu;
import br.com.apt.ui.myapt.myaptFragments.notice.NoticeRequest;
import br.com.apt.ui.myapt.myaptFragments.poll.PollRequest;
import br.com.apt.util.Logger;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomTextView;
import br.com.apt.widget.PagerItemFragment;
import br.com.apt.widget.SendSuccessDialog;

/**
 * Created by adminbs on 1/12/16.
 */
public class BuildFragment extends PagerItemFragment implements View.OnClickListener, Volley.Callback {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_build, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        User user = getUser();

        if(view != null && user != null){
            view.findViewById(R.id.buttonReceipt).setOnClickListener(this);
        }
    }

    @Override
    protected String getPagerTitle() {
        return App.getContext().getString(R.string.build);
    }

    @Override
    protected int getPagerIcon() {
        return R.drawable.ico_menu_condominio;
    }

    @Override
    public void onClick(View v) {
        Volley volley = new Volley(new Volley.Callback() {
            @Override
            public void result(String result, int type) {
                if(result != null){
                    BaseGson baseGson = App.getGson().fromJson(result, BaseGson.class);
                    SendSuccessDialog.show(baseGson.getMessage(), null, getFragmentManager());
                }
            }

            @Override
            public String uri() {
                return URI.CONDOMINIUM;
            }
        });
        volley.showDialog(getActivity());
        volley.request(Request.Method.POST, null);
    }

    @Override
    protected void onSelected() {
        // TODO: verify condition to show dialog
//        if (NavigationDrawerMenu.ADMINISTRATOR.show()) {
//            SendSuccessDialog.show(getString(R.string.here), getString(R.string.billit_here_description), getFragmentManager(), new SendSuccessDialog.Actions() {
//                @Override
//                public void back() {
//                    BilletFragment.setGoToBillet(true);
//                    startActivity(MyAdmActivity.newIntent(getContext()));
//                    getActivity().finish();
//                }
//            }, SendSuccessDialog.TYPE_DYNAMIC);
//        }
    }

    @Override
    public void result(String result, int type) {
        View view = getView();
        if(result != null && view != null){
            ApartmentRequest apartmentRequest = App.getGson().fromJson(result, ApartmentRequest.class);
            if(apartmentRequest != null){
                //Apartment apartment = apartmentRequest.getObject();

//                CustomTextView price = (CustomTextView) view.findViewById(R.id.price);
//                float condominium = apartment.getCondominium();
//                NumberFormat format = NumberFormat.getCurrencyInstance();
//                price.setText(format.format(condominium));
//                (view.findViewById(R.id.loader)).setVisibility(View.GONE);

            }
        }
    }

    @Override
    public String uri() {
        return URI.PLAN;
    }
}
