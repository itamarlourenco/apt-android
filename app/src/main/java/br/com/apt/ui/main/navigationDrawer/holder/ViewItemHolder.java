package br.com.apt.ui.main.navigationDrawer.holder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import br.com.apt.R;
import br.com.apt.ui.main.navigationDrawer.NavigationDrawerMenuAdapter;
import br.com.apt.widget.CustomTextView;

/**
 * Created by itamarlourenco on 04/01/16.
 */
public class ViewItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public CustomTextView textView;
    public ImageView imageView;
    private NavigationDrawerMenuAdapter.OnClickItemMenu onClickItemMenu;

    public ViewItemHolder(View itemView, NavigationDrawerMenuAdapter.OnClickItemMenu clickItemMenu) {
        super(itemView);
        textView = (CustomTextView) itemView.findViewById(R.id.customTextView);
        imageView = (ImageView) itemView.findViewById(R.id.imageView);

        itemView.findViewById(R.id.baseItem).setOnClickListener(this);
        onClickItemMenu = clickItemMenu;
    }

    public static View getView(Context context, ViewGroup viewGroup){
        return LayoutInflater.from(context).inflate(R.layout.view_item_holder, viewGroup, false);
    }

    @Override
    public void onClick(View v) {
        onClickItemMenu.clickClose(v, getAdapterPosition());
    }
}
