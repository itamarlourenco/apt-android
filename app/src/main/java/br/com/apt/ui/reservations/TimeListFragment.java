package br.com.apt.ui.reservations;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.squareup.picasso.Picasso;

import org.joda.time.Interval;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.BaseFragment;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.model.user.User;
import br.com.apt.util.Logger;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomButton;
import br.com.apt.widget.CustomTextViewLight;

/**
 * Created by adminbs on 9/7/17.
 */
public class TimeListFragment extends BaseFragment {

    private ListView listView;
    private int firstClick = 0;
    private List<Times> timesList = new ArrayList<>();
    private TimeListAdapter timeListAdapter;
    private Installations installations;
    private CalendarDay date;
    private ImageView icon;
    private CustomTextViewLight installationName;
    private CustomTextViewLight dateLabel;
    private CustomTextViewLight labelCalendar;
    private CustomButton confirmSchedule;
    private TimeSelected timesSelected = new TimeSelected();

    public void setDate(CalendarDay date) {
        this.date = date;
    }

    public void setInstallations(Installations installations) {
        this.installations = installations;
    }

    public static TimeListFragment newInstance(){
        return new TimeListFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_time_list, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        makeArrayFraction();
        timeListAdapter = new TimeListAdapter(getContext(), timesList);
        View view = getView();
        if (view != null) {
            listView = (ListView) view.findViewById(R.id.listView);
            listView.setAdapter(timeListAdapter);

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if(checkDateIsEnabled(position)){
                        return;
                    }
                    returnToDefault();
                    firstClick = position;
                    handleFirstClick();
                    Util.changeVisibility(confirmSchedule, View.VISIBLE, true);
                }
            });

            icon = (ImageView) view.findViewById(R.id.icon);
            installationName = (CustomTextViewLight) view.findViewById(R.id.installation_name);
            dateLabel = (CustomTextViewLight) view.findViewById(R.id.dateLabel);
            labelCalendar = (CustomTextViewLight) view.findViewById(R.id.labelCalendar);
            confirmSchedule = (CustomButton) view.findViewById(R.id.confirmSchedule);

            Calendar calendar = date.getCalendar();
            dateLabel.setText(
                    String.format(
                            getString(R.string.reservation_label_day),
                            Util.getDayWeek(calendar),
                            String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)),
                            Util.getMonthName(calendar.get(Calendar.MONTH)),
                            String.valueOf(calendar.get(Calendar.YEAR))
                    )
            );
            labelCalendar.setText(String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)));
            if (installations != null) {
                installationName.setText(installations.getName());
                Picasso.with(getContext()).load(Installations.handleUrlIcon(installations.getIcon())).into(icon);
            }

            confirmSchedule.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    timesSelected.setStart(timesList.get(firstClick).getTime());
                    startActivity(TermsActivity.newIntent(getContext(), installations, date, timesSelected));
                    getActivity().overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
                }
            });

        }
    }

    private boolean checkIfRentalLockTime(int firstClick, int lastClick) {
        Times startTime = timesList.get(firstClick);
        Times endTime = timesList.get(lastClick);
        try{
            long dateDiff = Util.getDateDiff(startTime.getDate(), endTime.getDate(), TimeUnit.MILLISECONDS);

            String[] split = installations.getRental_lock_time().split(":");
            int secondsToMs = Integer.parseInt(split[2]) * 1000;
            int minutesToMs = Integer.parseInt(split[1]) * 60000;
            int hoursToMs = Integer.parseInt(split[0]) * 3600000;
            long installationsRentalLockTimeMilliseconds = (secondsToMs + minutesToMs + hoursToMs);

            if(dateDiff >= installationsRentalLockTimeMilliseconds){
                Toast.makeText(getContext(), getString(R.string.limit_rental_lock_time) + " " + (split[0]+"h"+split[1]) , Toast.LENGTH_LONG).show();
                return true;
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    private boolean checkDateIsEnabled(int position) {
        Times times = timesList.get(position);
        return times.isDisabled();
    }

    private void makeArrayFraction() {
        if(!installations.isAllDay()){
            DateFormat df = new SimpleDateFormat("HH:mm", Locale.US);
            Calendar cal = Calendar.getInstance();
            Calendar openingCalendar = handleOpeningCalendar();
            Calendar closedCalendar = handleClosedCalendar();
            cal.set(Calendar.HOUR_OF_DAY, 0);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            int startDate = cal.get(Calendar.DATE);

            if (installations.getFraction_rental_time().equals("00:00:00")) {
                installations.setFraction_rental_time("01:00:00");
            }

            if (installations != null) {
                String[] split = installations.getFraction_rental_time().split(":");
                if(split.length >= 2){
                    try{
                        int hours = Integer.parseInt(split[0]);
                        int minute = Integer.parseInt(split[1]);
                        int seconds = Integer.parseInt(split[2]);

                        while (cal.get(Calendar.DATE) == startDate) {
                            String time = df.format(cal.getTime());
                            Date date = cal.getTime();

                            if(hours > 0){
                                cal.add(Calendar.HOUR, hours);
                            }
                            if(minute > 0){
                                cal.add(Calendar.MINUTE, minute);
                            }
                            if(seconds > 0){
                                cal.add(Calendar.SECOND, seconds);
                            }

                            String lastTime = df.format(cal.getTime());

                            Times times = new Times(time + " - " + lastTime, date, false, false, false);
                            if(cal.getTimeInMillis() > openingCalendar.getTimeInMillis() && cal.getTimeInMillis() <= closedCalendar.getTimeInMillis()){
                                timesList.add(times);
                            }
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        }else{
            Calendar calendar = handleOpeningCalendar();
            Times startTime = new Times(installations.getOpening_time().substring(0, installations.getOpening_time().length() - 3) + " - " + installations.getClosed_time().substring(0, installations.getClosed_time().length() - 3), calendar.getTime(), false, false, false);
            timesList.add(startTime);
        }
        getReservations();
    }

    private Calendar handleOpeningCalendar() {
        Calendar openingCalendar = Calendar.getInstance();
        String[] opening = installations.getOpening_time().split(":");
        int openingHour = Integer.parseInt(opening[0]);
        int openingMinute = Integer.parseInt(opening[1]);
        int openingSecond = Integer.parseInt(opening[2]);

        openingCalendar.set(Calendar.HOUR_OF_DAY, openingHour);
        openingCalendar.set(Calendar.MINUTE, openingMinute);
        openingCalendar.set(Calendar.SECOND, openingSecond);

        return openingCalendar;
    }

    private Calendar handleClosedCalendar() {
        Calendar closedCalendar = Calendar.getInstance();
        String[] opening = installations.getClosed_time().split(":");
        int openingHour = Integer.parseInt(opening[0]);
        int openingMinute = Integer.parseInt(opening[1]);
        int openingSecond = Integer.parseInt(opening[2]);

        closedCalendar.set(Calendar.HOUR_OF_DAY, openingHour);
        closedCalendar.set(Calendar.MINUTE, openingMinute);
        closedCalendar.set(Calendar.SECOND, openingSecond);

        return closedCalendar;
    }

    private void handleFirstClick() {
        Times times = timesList.get(firstClick);
        times.setSelected(true);
        times.setFirst(true);
        timeListAdapter.notifyDataSetChanged();
    }

    private void make(int firstClick, int lastClick) {
        if(checkIfRentalLockTime(firstClick, lastClick)){
            Util.changeVisibility(confirmSchedule, View.GONE, true);
            returnToDefault();
            return;
        }

        for(int i = firstClick; i<=lastClick; i++){

            if(checkDateIsEnabled(i)){
                returnToDefault();
                Util.changeVisibility(confirmSchedule, View.GONE, true);
                return;
            }

            Times times = timesList.get(i);
            times.setSelected(true);

            if(i == firstClick){
                times.setFirst(true);
            }

            if(i == lastClick){
                times.setLast(true);
            }

            timeListAdapter.notifyDataSetChanged();
        }

        timesSelected.setStart(timesList.get(firstClick).getTime());
        timesSelected.setEnd(timesList.get(lastClick).getTime());
    }

    private void returnToDefault(){
        for(Times times: timesList){
            times.setFirst(false);
            times.setLast(false);
            times.setSelected(false);
        }
        timeListAdapter.notifyDataSetChanged();
    }


    public void getReservations() {
        Volley volley = new Volley(new Volley.Callback() {
            @Override
            public void result(String result, int type) {
                ReservationsRequest reservationsRequest = App.getGson().fromJson(result, ReservationsRequest.class);
                List<Reservations> reservationsList = reservationsRequest.getObject();
                if (reservationsList != null) {
                    for(Reservations reservations: reservationsList){
                        try {
                            disabledTimes(reservations);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public String uri() {
                User user = getUser();
                Calendar calendar = date.getCalendar();
                String date = calendar.get(Calendar.YEAR) + "-" + (calendar.get(Calendar.MONTH)+1) + "-" + calendar.get(Calendar.DAY_OF_MONTH);

                return String.format(URI.RESERVATION_BY_CONDOMINIUM, user.getCompanyKey(), user.getCondominiumId(), date, installations.getId());
            }
        });
        volley.isReservation(true);
        volley.request();
    }

    private void disabledTimes(Reservations reservations) throws Exception {
        Calendar start = Calendar.getInstance();
        start.setTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(reservations.getStart()));

        Calendar end = Calendar.getInstance();
        end.setTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(reservations.getEnd()));

        for(Times times: timesList){
            Calendar time = Calendar.getInstance();
            time.setTime(times.getDate());
            time.set(Calendar.DAY_OF_MONTH, start.get(Calendar.DAY_OF_MONTH));
            time.set(Calendar.MONTH, start.get(Calendar.MONTH));
            time.set(Calendar.YEAR, start.get(Calendar.YEAR));

            if(time.compareTo(start) >= 0 && time.compareTo(end) <= 0){
                times.setDisabled(true);
            }
        }
        timeListAdapter.notifyDataSetChanged();
    }
}

