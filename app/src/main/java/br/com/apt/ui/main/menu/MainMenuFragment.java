package br.com.apt.ui.main.menu;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.BaseFragment;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.application.notification.Notification;
import br.com.apt.application.notification.NotificationSharedPreferences;
import br.com.apt.application.services.PushNotificationLocalBroadcast;
import br.com.apt.model.NotificationHandle;
import br.com.apt.model.OccurrencesNotRead;
import br.com.apt.model.OccurrencesNotReadSend;
import br.com.apt.model.user.User;
import br.com.apt.ui.main.Counter;
import br.com.apt.ui.main.CounterRequest;
import br.com.apt.ui.main.CounterSend;
import br.com.apt.ui.main.DisabledFunction;
import br.com.apt.ui.main.MainActivity;
import br.com.apt.ui.main.navigationDrawer.NavigationDrawerMenu;
import br.com.apt.widget.CustomTextView;

public class MainMenuFragment extends BaseFragment implements View.OnClickListener{

    private int aplhaById;

    public static MainMenuFragment newInstance(){
        return new MainMenuFragment();
    }

    public static List<DisabledFunction> disabledFunctions;

    private View lobby;
    private View reservations;
    private View notification;
    private View myApt;
    private View condominium;
    private View administrator;

    private CustomTextView indicatorMyAdm;
    private CustomTextView indicatorReservation;
    private CustomTextView indicatorLobby;
    private CustomTextView indicatorNotification;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.main_menu_fragment, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if(view != null){
            lobby = view.findViewById(NavigationDrawerMenu.LOBBY.id());
            lobby.setOnClickListener(this);

            condominium = view.findViewById(NavigationDrawerMenu.CONDOMINIUM.id());
            condominium.setOnClickListener(this);

            reservations = view.findViewById(NavigationDrawerMenu.RESERVATIONS.id());
            reservations.setOnClickListener(this);

            notification = view.findViewById(NavigationDrawerMenu.TICKET.id());
            notification.setOnClickListener(this);

            myApt = view.findViewById(NavigationDrawerMenu.MYAPT.id());
            myApt.setOnClickListener(this);

            administrator = view.findViewById(NavigationDrawerMenu.ADMINISTRATOR.id());
            administrator.setOnClickListener(this);

            indicatorMyAdm = (CustomTextView) view.findViewById(R.id.indicatorMyApt);
            indicatorReservation = (CustomTextView) view.findViewById(R.id.indicatorReservation);
            indicatorLobby = (CustomTextView) view.findViewById(R.id.indicatorLobby);
            indicatorNotification = (CustomTextView) view.findViewById(R.id.indicatorNotification);

            disabledFunctions();

            PushNotificationLocalBroadcast.initLocalBroadcast(getContext(), new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    Counter counter = intent.getParcelableExtra(PushNotificationLocalBroadcast.NOTIFICATION_LOCAL_BROAD_CAST_COUNT);
                    if (counter != null) {
                        Counter.Reception reception = counter.getReception();
                        if (reception != null) {
                            int deliveries = reception.getDeliveries();
                            int visits = reception.getVisits();
                            (new NotificationSharedPreferences()).setCountDelivery(deliveries);
                            (new NotificationSharedPreferences()).setCountVisit(visits);
                            if((deliveries+visits) > 0){
                                indicatorLobby.setVisibility(View.VISIBLE);
                                indicatorLobby.setText(String.valueOf((deliveries+visits)));
                            }else{
                                indicatorLobby.setVisibility(View.GONE);
                            }
                        }

                        Counter.MyApt myapt = counter.getMyapt();
                        if (myapt != null) {
                            int notices = myapt.getNotices();
                            int polls = myapt.getPolls();
                            (new NotificationSharedPreferences()).setCountNotices(notices);
                            (new NotificationSharedPreferences()).setCountPoll(polls);
                            if((notices+polls) > 0){
                                indicatorMyAdm.setVisibility(View.VISIBLE);
                                indicatorMyAdm.setText(String.valueOf((notices+polls)));
                            }else{
                                indicatorMyAdm.setVisibility(View.GONE);
                            }
                        }

                        int totalNotoficaition = counter.getNotificationCount();
                        if(totalNotoficaition > 0){
                            indicatorNotification.setVisibility(View.VISIBLE);
                            indicatorNotification.setText(String.valueOf(totalNotoficaition));
                        }else{
                            indicatorNotification.setVisibility(View.GONE);
                        }

                        Counter.Reservation reservation = counter.getReservation();
                        if(reservation != null && reservation.getReservation() > 0){
                            indicatorReservation.setVisibility(View.VISIBLE);
                            indicatorReservation.setText(String.valueOf(reservation.getReservation()));
                        }else{
                            indicatorReservation.setVisibility(View.GONE);
                        }
                    }
                }
            });
        }
    }

    private void disabledFunctions() {
        Volley volley = new Volley(new Volley.Callback() {
            @Override
            public void result(String result, int type) {
                DisabledFunction.DisabledFunctionRequest disabledFunctionRequest = App.getGson().fromJson(result, DisabledFunction.DisabledFunctionRequest.class);
                if (disabledFunctionRequest != null) {
                    disabledFunctions = disabledFunctionRequest.getObject();
                    if (disabledFunctions != null && disabledFunctions.size() > 0) {
                        View view = getView();
                        for (DisabledFunction disabledFunction : disabledFunctions) {

//                            if (TextUtils.equals(disabledFunction.getFunction(), DisabledFunction.LOBBY)) {
//                                lobby.getBackground().setAlpha(35);
//                                setAplhaById(R.id.lobbyIcon, view);
//                                lobby.setEnabled(false);
//                            }
//
//                            if (TextUtils.equals(disabledFunction.getFunction(), DisabledFunction.RESERVATIONS)) {
//                                reservations.getBackground().setAlpha(35);
//                                setAplhaById(R.id.reservationIcon, view);
//                                reservations.setEnabled(false);
//                            }
//
//                            if (TextUtils.equals(disabledFunction.getFunction(), DisabledFunction.NOTIFICATION)) {
//                                notification.getBackground().setAlpha(35);
//                                setAplhaById(R.id.neighborhoodIcon, view);
//                                notification.setEnabled(false);
//                            }
//
//                            if (TextUtils.equals(disabledFunction.getFunction(), DisabledFunction.TRAVEL)) {
//                                administrator.getBackground().setAlpha(35);
//                                setAplhaById(R.id.tripIcon, view);
//                                administrator.setEnabled(false);
//                            }
//
//                            if (TextUtils.equals(disabledFunction.getFunction(), DisabledFunction.MY_APT)) {
//                                myApt.getBackground().setAlpha(35);
//                                setAplhaById(R.id.meuaptoIcon, view);
//                                myApt.setEnabled(false);
//                            }
                        }
                    }
                }
            }

            @Override
            public String uri() {
                return URI.DISABLED_FUNCTION;
            }
        });
        volley.showDialog(getActivity());
        volley.request(Request.Method.GET, null);
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void onClick(View v) {
        List<NavigationDrawerMenu> navigationDrawerMenuArrayList = App.getItems();
        for(NavigationDrawerMenu navigationDrawerMenu: navigationDrawerMenuArrayList){
            if(navigationDrawerMenu.id() > 0 && navigationDrawerMenu.id() == v.getId()){
                MainActivity.openActivity(getActivity(), navigationDrawerMenu);
            }
        }
    }

    public void setAplhaById(int aplhaById, View view) {
        if(view == null) return;

        CustomTextView customText = (CustomTextView) view.findViewById(aplhaById);
        if(customText != null){
            customText.setTextColor(Color.argb(35, 255, 255, 255));
            Drawable[] compundDrawables = customText.getCompoundDrawables();
            if(compundDrawables.length > 0){
                Drawable topCompoundDrawable = compundDrawables[1];
                if(topCompoundDrawable != null){
                    topCompoundDrawable.setAlpha(35);
                }
            }
        }
    }
}