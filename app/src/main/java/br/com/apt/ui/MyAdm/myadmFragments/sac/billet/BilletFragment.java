package br.com.apt.ui.MyAdm.myadmFragments.sac.billet;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.com.apt.R;
import br.com.apt.application.BaseFragment;
import br.com.apt.ui.MyAdm.myadmFragments.sac.OthersOccurrencesActivity;
import br.com.apt.ui.MyAdm.myadmFragments.sac.occurrence.OccurrenceDoublePayActivity;
import br.com.apt.ui.tickets.TicketActivity;
import br.com.apt.util.Logger;
import br.com.apt.widget.SendSuccessDialog;

/**
 * Created by adminbs on 7/31/17.
 */

public class BilletFragment extends BaseFragment {

    public static boolean goToBillet = false;
    public static BilletFragment newInstance() {
        return new BilletFragment();
    }

    public static void setGoToBillet(boolean goToBillet) {
        BilletFragment.goToBillet = goToBillet;
    }

    public static boolean isGoToBillet() {
        return goToBillet;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_billit, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if(view != null){
            view.findViewById(R.id.occurrencesBilletSecondBillet).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SendSuccessDialog.show(getString(R.string.here), getString(R.string.billit_here_description), getFragmentManager(), new SendSuccessDialog.Actions() {
                        @Override
                        public void back() {
                            getActivity().finish();
                            BilletFragment.setGoToBillet(true);
                        }
                    }, SendSuccessDialog.TYPE_DYNAMIC);
                }
            });

            view.findViewById(R.id.occurrencesBilletDuplicatePayment).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(OccurrenceDoublePayActivity.newIntent(getContext()));
                }
            });

            view.findViewById(R.id.occurrencesBilletDoubts).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(OthersOccurrencesActivity.newIntent(getActivity()));
                }
            });
        }
    }
}
