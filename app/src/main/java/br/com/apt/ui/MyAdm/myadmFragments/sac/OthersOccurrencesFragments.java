package br.com.apt.ui.MyAdm.myadmFragments.sac;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toolbar;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.android.volley.Request;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.BaseActivity;
import br.com.apt.application.BaseFragment;
import br.com.apt.application.BaseGsonMeuAdm;
import br.com.apt.application.Config;
import br.com.apt.application.PicturesHelper;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.model.user.User;
import br.com.apt.model.user.UserData;
import br.com.apt.ui.MyAdm.Occurrences;
import br.com.apt.ui.MyAdm.OccurrencesSend;
import br.com.apt.ui.MyAdm.myadmFragments.sac.occurrence.OccurrenceDouble;
import br.com.apt.ui.MyAdm.myadmFragments.sac.occurrence.OccurrenceDoubleFragment;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomButton;
import br.com.apt.widget.CustomEditText;
import br.com.apt.widget.SendSuccessDialog;
import dmax.dialog.SpotsDialog;
import siclo.com.ezphotopicker.api.EZPhotoPick;
import siclo.com.ezphotopicker.api.EZPhotoPickStorage;

/**
 * Created by adminbs on 8/7/17.
 */

public class OthersOccurrencesFragments extends BaseFragment implements Volley.Callback {

    public static ImageView photo;
    public static Bitmap bitmapPhoto;
    private String namePhoto = Util.generateName("notification");
    private String pathPhoto = Config.Amazon.handleLocation(namePhoto);
    private SpotsDialog dialog;
    private User user = UserData.getUser();
    private CustomEditText message;
    private CustomEditText title;
    private boolean isNotification = false;
    private CustomButton sendSac;

    public static OthersOccurrencesFragments newInstance(){
        return new OthersOccurrencesFragments();
    }

    public static OthersOccurrencesFragments newInstance(boolean isNotification){
        OthersOccurrencesFragments othersOccurrencesFragments = new OthersOccurrencesFragments();
        othersOccurrencesFragments.isNotification(isNotification);

        return othersOccurrencesFragments;
    }

    public void isNotification(boolean isNotification) {
        this.isNotification = isNotification;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.others_occurrence_fragments, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if(view != null){

            dialog = new SpotsDialog(getActivity(), R.style.AlertDialogCustom);
            OthersOccurrencesFragments.photo = (ImageView) view.findViewById(R.id.photo);
            message = (CustomEditText) view.findViewById(R.id.message);
            title = (CustomEditText) view.findViewById(R.id.title);
            photo.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    alterImage();
                }

            });

            sendSac = (CustomButton) view.findViewById(R.id.sendSac);
            sendSac.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    handlePhotos();
                }
            });

            handleIfIsNotification();
        }
    }

    private void handleIfIsNotification() {
        sendSac.setText(R.string.send_ticket);
    }

    private void handlePhotos() {
        if(TextUtils.isEmpty(title.getText())){
            SendSuccessDialog.show(getString(R.string.error_add_photo_title), null, getFragmentManager());
            return;
        }

        if(TextUtils.isEmpty(message.getText())){
            SendSuccessDialog.show(getString(R.string.error_add_photo_message), null, getFragmentManager());
            return;
        }

        if(dialog != null && !dialog.isShowing()){
            dialog.show();
        }

        final File finalFileBillet = Util.createImageFile();
        if(bitmapPhoto != null && finalFileBillet != null){
            Util.sendBitmapToFile(finalFileBillet, bitmapPhoto);
        }

        new AsyncTask<Void, Void, Void>(){
            @Override
            protected Void doInBackground(Void... params) {
                AmazonS3Client s3Client = new AmazonS3Client(new BasicAWSCredentials(Config.Amazon.ACCESS_KEY, Config.Amazon.PUBLIC_KEY));

                PutObjectRequest requestBillet = new PutObjectRequest(Config.Amazon.BUCKET, String.format(Config.Amazon.PATH, namePhoto), finalFileBillet);
                s3Client.putObject(requestBillet);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                initVolley();
            }
        }.execute();
    }

    public void initVolley(){
        List<Occurrences.Attachments> attachmentsList = new ArrayList<>();
        attachmentsList.add(new Occurrences.Attachments(namePhoto + ".jpg", namePhoto, pathPhoto));

        OccurrenceDouble occurrenceDouble = new OccurrenceDouble(
                OccurrencesSend.SUBJECT_UUID_OTHERS,
                title.getText().toString(),
                message.getText().toString(),
                2,
                user.getCondominiumId(),
                user.getBlockId(),
                user.getApto(),
                attachmentsList
        );

        Volley volley = new Volley(this);
        volley.isMeuADM(true);
        volley.request(Request.Method.POST, occurrenceDouble.toJson());
    }


    public void alterImage(){
        CharSequence[] options = {getString(R.string.takePhoto), getString(R.string.chooseGallery)};
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity());
        builder.setTitle("").setItems(options, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if(which == 0) {
                    PicturesHelper.Camera(getActivity());
                } else if(which == 1) {
                    PicturesHelper.Gallery(getActivity());
                }
            }
        });
        builder.create().show();
    }

    public void onActivityResultFragment(int requestCode, int resultCode, Intent data) {
        if(requestCode == EZPhotoPick.PHOTO_PICK_REQUEST_CODE && resultCode != 0){
            try {
                EZPhotoPickStorage ezPhotoPickStorage = new EZPhotoPickStorage(getActivity());
                Bitmap bitmap = ezPhotoPickStorage.loadLatestStoredPhotoBitmap();
                OthersOccurrencesFragments.bitmapPhoto = bitmap;
                OthersOccurrencesFragments.photo.setImageBitmap(bitmap);
                OthersOccurrencesFragments.photo.setScaleType(ImageView.ScaleType.CENTER_CROP);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void result(String result, int type) {
        try{
            dialog.dismiss();
            BaseGsonMeuAdm baseGsonMeuAdm = App.getGson().fromJson(result, BaseGsonMeuAdm.class);
            SendSuccessDialog.show(baseGsonMeuAdm.getMessage(), null, getFragmentManager(), new SendSuccessDialog.Actions() {
                @Override
                public void back() {
                    getActivity().finish();
                }
            }, baseGsonMeuAdm.getCode() == Volley.STATUS_OK ? SendSuccessDialog.TYPE_DIALOG : SendSuccessDialog.TYPE_ERROR);
        }catch (Exception e){
            SendSuccessDialog.show(getString(R.string.error_generic), null, getFragmentManager(), new SendSuccessDialog.Actions() {
                @Override
                public void back() {
                    getActivity().finish();
                }
            }, SendSuccessDialog.TYPE_ERROR);
        }
    }

    @Override
    public String uri() {
        return URI.OCCURRENCES;
    }
}
