package br.com.apt.ui.myinfo.myinfofragment.pet;

import android.os.Parcel;

import java.util.Date;
import java.util.List;

import br.com.apt.application.BaseGson;
import br.com.apt.ui.myinfo.myinfofragment.AutoComplete;


/**
 * Created by adminbs on 11/7/16.
 */
public class PetBreedRequest extends BaseGson {
    public List<AutoComplete> Object;

    public List<AutoComplete> getObject() {
        return Object;
    }
}

