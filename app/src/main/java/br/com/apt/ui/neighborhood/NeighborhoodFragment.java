package br.com.apt.ui.neighborhood;

import java.util.ArrayList;

import br.com.apt.R;
import br.com.apt.ui.neighborhood.neighborhoodFragment.DeliveryNeighborhoodFragment;
import br.com.apt.ui.neighborhood.neighborhoodFragment.LaundryFragment;
import br.com.apt.ui.neighborhood.neighborhoodFragment.MarketFragment;
import br.com.apt.ui.neighborhood.neighborhoodFragment.ShoestoreFragment;
import br.com.apt.widget.PagerFragment;
import br.com.apt.widget.PagerItemFragment;

public class NeighborhoodFragment extends PagerFragment {

    public static NeighborhoodFragment newInstance() {
        return new NeighborhoodFragment();
    }


    @Override
    public ArrayList<PagerItemFragment> getPagerItems() {
        ArrayList<PagerItemFragment> pagerItems = new ArrayList<>();
        pagerItems.add(new DeliveryNeighborhoodFragment());
        pagerItems.add(new MarketFragment());
        pagerItems.add(new LaundryFragment());
        pagerItems.add(new ShoestoreFragment());

        return pagerItems;
    }

    @Override
    protected int getDrawableSelected() {
        return R.drawable.background_tab_selected_neighborhood;
    }
}
