package br.com.apt.ui.myapt.myaptFragments.poll;

import java.util.List;

import br.com.apt.application.BaseGson;
import br.com.apt.application.BaseGsonMeuAdm;

/**
 * Created by itamarlourenco on 17/01/16.
 */
public class PollRequest extends BaseGsonMeuAdm {
    private List<Poll> object;

    public List<Poll> getObject() {
        return object;
    }

    public void setObject(List<Poll> object) {
        object = object;
    }
}
