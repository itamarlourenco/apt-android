package br.com.apt.ui.myCondominium.myCondominiumFragments.regulationAndReforms;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.model.user.User;
import br.com.apt.ui.myCondominium.OpenPDF;
import br.com.apt.ui.myCondominium.myCondominiumFragments.Attachment;
import br.com.apt.ui.myCondominium.myCondominiumFragments.Attachments;
import br.com.apt.ui.myCondominium.myCondominiumFragments.AttachmentsRequest;
import br.com.apt.widget.PagerItemFragment;
import br.com.apt.widget.SendSuccessDialog;

import static br.com.apt.R.layout.fragment_regulations_and_reforms;

/**
 * Created by adminbs on 8/5/17.
 */

public class RegulationAndReformsFragment extends PagerItemFragment {

    private User user = getUser();

    @Override
    protected String getPagerTitle() {
        return App.getContext().getString(R.string.admForm);
    }

    @Override
    protected int getPagerIcon() {
        return R.drawable.ico_meusdados_menu_moradores;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(fragment_regulations_and_reforms, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if (view != null) {
            view.findViewById(R.id.regulationInternal).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openRegulationInternal();
                }
            });
            view.findViewById(R.id.useOfCommonAreas).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    useOfCommonAreas();
                }
            });
        }
    }

    private void useOfCommonAreas() {
        Volley volley = new Volley(new Volley.Callback() {
            @Override
            public void result(String result, int type) {
                try{
                    AttachmentsRequest internalRulesFragmentRequest = App.getGson().fromJson(result, AttachmentsRequest.class);
                    final List<Attachments> internalRulesFragments = internalRulesFragmentRequest.getObject();
                    CharSequence[] listByDialog = new CharSequence[internalRulesFragments.size()];

                    for(int i = 0; i<internalRulesFragments.size(); i++){
                        listByDialog[i] = internalRulesFragments.get(i).getTitle().toUpperCase();
                    }
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                    builder.setTitle("")
                            .setItems(listByDialog, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                                    Attachments internalRulesFragment = internalRulesFragments.get(which);

                                    builder.setMessage(internalRulesFragment.getBody())
                                            .setCancelable(false)
                                            .setPositiveButton(getString(R.string.close), new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.cancel();
                                                }
                                            });

                                    AlertDialog alert = builder.create();
                                    alert.setTitle(internalRulesFragment.getTitle());
                                    alert.show();
                                }
                            });
                    builder.create().show();

                }catch (Exception e){
                    SendSuccessDialog.show(getString(R.string.error_generic), null, getFragmentManager(), null, SendSuccessDialog.TYPE_ERROR);
                }
            }

            @Override
            public String uri() {
                return String.format(URI.INTERNAL_RULES_FRAGMENTS, user.getCondominiumId());
            }
        });
        volley.isMeuADM(true);
        volley.showDialog(getActivity());
        volley.request();
    }

    private void openRegulationInternal() {
        Volley volley = new Volley(new Volley.Callback() {
            @Override
            public void result(String result, int type) {
                try{
                    InternalRulesRequest internalRules = App.getGson().fromJson(result, InternalRulesRequest.class);
                    if (internalRules != null) {
                        Attachment attachment = internalRules.getObject();
                        startActivity(OpenPDF.newIntent(getContext(), attachment.getUrl()));
                    }

                }catch (Exception e){
                    SendSuccessDialog.show(getString(R.string.error_generic), null, getFragmentManager(), new SendSuccessDialog.Actions() {
                        @Override
                        public void back() {
                        }
                    }, SendSuccessDialog.TYPE_ERROR);
                }

            }

            @Override
            public String uri() {
                return String.format(URI.INTERNAL_RULES, user.getCondominiumId());
            }
        });
        volley.isMeuADM(true);
        volley.showDialog(getActivity());
        volley.request();
    }
}
