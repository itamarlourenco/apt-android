package br.com.apt.ui.myinfo.myinfofragment.employee;

import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TimePicker;

import com.android.volley.Request;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.BaseFragment;
import br.com.apt.application.BaseGson;
import br.com.apt.application.PicturesHelper;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.ui.lobby.lobbyFragments.visits.VisitsScheduleFragment;
import br.com.apt.ui.myinfo.AutoCompleteDialog;
import br.com.apt.ui.myinfo.myinfofragment.AutoComplete;
import br.com.apt.ui.myinfo.myinfofragment.FragmentMyInfoAdd;
import br.com.apt.ui.myinfo.myinfofragment.NewUser;
import br.com.apt.ui.myinfo.myinfofragment.pet.MyPetAddFragment;
import br.com.apt.util.Logger;
import br.com.apt.util.Mask;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomButton;
import br.com.apt.widget.CustomEditText;
import br.com.apt.widget.CustomEditTextLight;
import br.com.apt.widget.CustomTextViewLight;
import br.com.apt.widget.SendSuccessDialog;
import siclo.com.ezphotopicker.api.EZPhotoPick;
import siclo.com.ezphotopicker.api.EZPhotoPickStorage;

import static br.com.apt.R.id.email;
import static br.com.apt.R.id.end;
import static br.com.apt.R.id.view;

/**
 * Created by adminbs on 7/19/17.
 */

public class EmployeesAddFragment extends BaseFragment implements View.OnClickListener, Volley.Callback {
    private Employees employees;
    private Bitmap bitmap = null;
    private static ImageView image;
    private Spinner genders;
    private CustomEditText name;
    private CustomEditText type;
    private CustomEditText rg;
    private CustomEditText cpf;
    private CustomEditText startTime;
    private CustomEditText endTime;
    private RelativeLayout canvasAlterImage;
    private int employeesTypeId = 0;
    private List<AutoComplete> employeesType;
    private String genderType = NewUser.TYPE_MALE;

    private CustomButton D;
    private CustomButton S;
    private CustomButton T;
    private CustomButton Q;
    private CustomButton Q5;
    private CustomButton S6;
    private CustomButton S7;

    public void setEmployees(Employees employees) {
        this.employees = employees;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_add_employees, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if(view != null){
            image =  (ImageView) view.findViewById(R.id.image);
            name = (CustomEditText) view.findViewById(R.id.name);
            type = (CustomEditText) view.findViewById(R.id.type);
            rg = (CustomEditText) view.findViewById(R.id.rg);
            cpf = (CustomEditText) view.findViewById(R.id.cpf);
            cpf.addTextChangedListener(Mask.insert("###.###.###-##", cpf));

            startTime = (CustomEditText) view.findViewById(R.id.startTime);
            endTime = (CustomEditText) view.findViewById(R.id.endTime);
            canvasAlterImage = (RelativeLayout) view.findViewById(R.id.canvasAlterImage);
            canvasAlterImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alterImage();
                }
            });

            D = (CustomButton) view.findViewById(R.id.D);
            D.setOnClickListener(this);
            S = (CustomButton) view.findViewById(R.id.S);
            S.setOnClickListener(this);
            T = (CustomButton) view.findViewById(R.id.T);
            T.setOnClickListener(this);
            Q = (CustomButton) view.findViewById(R.id.Q);
            Q.setOnClickListener(this);
            Q5 = (CustomButton) view.findViewById(R.id.Q5);
            Q5.setOnClickListener(this);
            S6 = (CustomButton) view.findViewById(R.id.S6);
            S6.setOnClickListener(this);
            S7 = (CustomButton) view.findViewById(R.id.S7);
            S7.setOnClickListener(this);

            genders = (Spinner) view.findViewById(R.id.genders);
            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(), R.array.genders, R.layout.spinner_gender);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            genders.setAdapter(adapter);
            genders.setSelection(0);
            genders.setFocusable(true);
            genders.setFocusableInTouchMode(true);
            genders.requestFocus();

            genders.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if(position == 0){
                        genderType = NewUser.TYPE_MALE;
                    }else{
                        genderType = NewUser.TYPE_FEMALE;
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            type.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openDialogEmployeesTypes();
                }
            });
            type.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if(hasFocus){
                        openDialogEmployeesTypes();
                    }
                }
            });

            startTime.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showTimePickerDialog(startTime);
                }
            });
            startTime.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if(hasFocus){
                        showTimePickerDialog(startTime);
                    }
                }
            });

            endTime.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showTimePickerDialog(endTime);
                }
            });
            endTime.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if(hasFocus){
                        showTimePickerDialog(endTime);
                    }
                }
            });

            view.findViewById(R.id.sendEmployees).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sendEmployees();
                }
            });

            getFunctions();
            willBeEdited();
        }
    }

    private void willBeEdited(){
        if(employees != null){
            name.setText(employees.getName());
            type.setText(employees.getType().getName());
            employeesTypeId = employees.getDomestics_type_id();
            rg.setText(employees.getDocument());
            cpf.setText(employees.getCpf());
            startTime.setText(employees.getStartTime());
            endTime.setText(employees.getEndTime());
            Picasso.with(getContext()).load(Volley.getUrlByImage(employees.getImage())).placeholder(R.drawable.placeholder_camera).into(image);

            if(!TextUtils.isEmpty(employees.getWorkDays())){
                String[] split = employees.getWorkDays().split(",");
                for(String day: split){
                    if(day.equals("0")){
                        D.setActivated(true);
                    }
                    if(day.equals("1")){
                        S.setActivated(true);
                    }
                    if(day.equals("2")){
                        T.setActivated(true);
                    }
                    if(day.equals("3")){
                        Q.setActivated(true);
                    }
                    if(day.equals("4")){
                        Q5.setActivated(true);
                    }
                    if(day.equals("5")){
                        S6.setActivated(true);
                    }
                    if(day.equals("6")){
                        S7.setActivated(true);
                    }
                }
            }
        }
    }

    private void alterImage(){
        CharSequence[] options = {getString(R.string.takePhoto), getString(R.string.chooseGallery)};
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity());

        builder.setTitle("")
                .setItems(options, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if(which == 0) {
                            PicturesHelper.Camera(getActivity());
                        } else if(which == 1) {
                            PicturesHelper.Gallery(getActivity());
                        }else{
                            image.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.placeholder_camera));
                        }
                    }
                });
        builder.create().show();
    }

    public void showTimePickerDialog(final CustomEditText editText){
        Calendar calendar = Calendar.getInstance();
        final int hour = calendar.get(Calendar.HOUR_OF_DAY);
        final int minute = calendar.get(Calendar.MINUTE);
        TimePickerDialog timePickerDialog = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                editText.setText(
                        String.format("%s:%s", (selectedHour < 10 ? "0" : "") + String.valueOf(selectedHour), (selectedMinute < 10 ? "0" : "") + String.valueOf(selectedMinute))
                );
            }
        }, hour, minute, true); //Yes 24 hour time
        timePickerDialog.setTitle(getString(R.string.titleTimePickerDialog));
        timePickerDialog.show();
    }


    public static EmployeesAddFragment newInstance(){
        return new EmployeesAddFragment();
    }


    public void onActivityResultFragment(int requestCode, int resultCode, Intent data) {
        if(requestCode == EZPhotoPick.PHOTO_PICK_REQUEST_CODE && EmployeesAddFragment.image != null){
            try {
                EZPhotoPickStorage ezPhotoPickStorage = new EZPhotoPickStorage(getActivity());
                bitmap = ezPhotoPickStorage.loadLatestStoredPhotoBitmap();

                EmployeesAddFragment.image.setImageBitmap(bitmap);
                EmployeesAddFragment.image.setVisibility(View.VISIBLE);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void sendEmployees(){
        try{
            if(TextUtils.isEmpty(name.getText())){
                throw new Exception(getString(R.string.error_name_employees));
            }

            if(employeesTypeId <= 0){
                throw new Exception(getString(R.string.error_employees_error_type));
            }

            if(TextUtils.isEmpty(rg.getText())){
                throw new Exception(getString(R.string.error_rg_employees));
            }

            if(TextUtils.isEmpty(cpf.getText())){
                throw new Exception(getString(R.string.error_cpf_employees));
            }

            if(TextUtils.isEmpty(startTime.getText())){
                throw new Exception(getString(R.string.error_start_time_employees));
            }

            if(TextUtils.isEmpty(endTime.getText())){
                throw new Exception(getString(R.string.error_end_time_employees));
            }

            String workDay = "";
            if(D.isActivated()){
                workDay = "0";
            }

            if(S.isActivated()){
                if(TextUtils.isEmpty(workDay)){
                    workDay += "1";
                }else{
                    workDay += ",1";
                }
            }

            if(T.isActivated()){
                if(TextUtils.isEmpty(workDay)){
                    workDay += "2";
                }else{
                    workDay += ",2";
                }
            }

            if(Q.isActivated()){
                if(TextUtils.isEmpty(workDay)){
                    workDay += "3";
                }else{
                    workDay += ",3";
                }
            }

            if(Q5.isActivated()){
                if(TextUtils.isEmpty(workDay)){
                    workDay += "4";
                }else{
                    workDay += ",4";
                }
            }

            if(S6.isActivated()){
                if(TextUtils.isEmpty(workDay)){
                    workDay += "5";
                }else{
                    workDay += ",5";
                }
            }

            if(S7.isActivated()){
                if(TextUtils.isEmpty(workDay)){
                    workDay += "6";
                }else{
                    workDay += ",6";
                }
            }

            HashMap<String, String> hashMap = new HashMap<>();
            if(employees != null && employees.getId() > 0){
                hashMap.put("id", String.valueOf(employees.getId()));
            }
            hashMap.put("name", name.getText().toString());
            hashMap.put("domestics_type_id", String.valueOf(employeesTypeId));
            hashMap.put("document", rg.getText().toString());
            hashMap.put("cpf", cpf.getText().toString());
            hashMap.put("gender", genderType);
            hashMap.put("work_days", workDay);
            hashMap.put("start_time", startTime.getText().toString());
            hashMap.put("end_time", endTime.getText().toString());


            File finalFile = Util.createImageFile();
            if(bitmap != null && finalFile != null){
                Util.sendBitmapToFile(finalFile, bitmap);
            }

            Volley volley = new Volley(this);
            volley.showDialog(getActivity());
            volley.request(Request.Method.POST, hashMap, 0, finalFile, "image");
        }catch (Exception e){
            SendSuccessDialog.show(e.getMessage(), null, getFragmentManager(), null, SendSuccessDialog.TYPE_ERROR);
        }
    }


    private void openDialogEmployeesTypes() {
        AutoCompleteDialog.show(getFragmentManager(), employeesType, new AutoCompleteDialog.Click() {
            @Override
            public void click(AutoComplete autoComplete) {
                employeesTypeId = autoComplete.getId();
                type.setText(autoComplete.getName());
            }
        }, getString(R.string.choose_function));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.D:
                if(D.isActivated()){
                    D.setActivated(false);
                }else{
                    D.setActivated(true);
                }
                break;
            case R.id.S:
                if(S.isActivated()){
                    S.setActivated(false);
                }else{
                    S.setActivated(true);
                }
                break;
            case R.id.T:
                if(T.isActivated()){
                    T.setActivated(false);
                }else{
                    T.setActivated(true);
                }
                break;
            case R.id.Q:
                if(Q.isActivated()){
                    Q.setActivated(false);
                }else{
                    Q.setActivated(true);
                }
                break;
            case R.id.Q5:
                if(Q5.isActivated()){
                    Q5.setActivated(false);
                }else{
                    Q5.setActivated(true);
                }
                break;
            case R.id.S6:
                if(S6.isActivated()){
                    S6.setActivated(false);
                }else{
                    S6.setActivated(true);
                }
                break;
            case R.id.S7:
                if(S7.isActivated()){
                    S7.setActivated(false);
                }else{
                    S7.setActivated(true);
                }
                break;
        }
    }

    public void getFunctions(){
        Volley volley = new Volley(new Volley.Callback() {
            @Override
            public void result(String result, int type) {
                if(result != null){
                    EmployeesTypeRequest employeesTypeRequest = App.getGson().fromJson(result, EmployeesTypeRequest.class);
                    if(employeesTypeRequest != null){
                        employeesType = employeesTypeRequest.getObject();
                    }
                }
            }
            @Override
            public String uri() {
                return URI.EMPLOYEES_TYPE;
            }
        });
        volley.request(Request.Method.GET, null);
    }

    @Override
    public void result(String result, int type) {
        if(result != null){
            BaseGson baseGson = App.getGson().fromJson(result, BaseGson.class);
            if(baseGson.getStatus() == Volley.STATUS_OK){
                SendSuccessDialog.show(getString(R.string.success_employees), null, getFragmentManager(), new SendSuccessDialog.Actions(){
                    @Override
                    public void back() {
                        getActivity().finish();
                    }
                });
            }else{
                SendSuccessDialog.show(getString(R.string.error_generic), null, getFragmentManager(), null, SendSuccessDialog.TYPE_ERROR);
            }
        }
    }

    @Override
    public String uri() {
        return URI.EMPLOYEES;
    }

}
