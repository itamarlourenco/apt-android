package br.com.apt.ui.MyAdm.myadmFragments.administrator;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import br.com.apt.R;
import br.com.apt.application.BaseFragment;
import br.com.apt.widget.CustomTextView;
import br.com.apt.widget.CustomTextViewLight;

/**
 * Created by adminbs on 7/28/17.
 */

public class AdministratorKnowMoreFragment  extends BaseFragment {
    private CustomTextViewLight description;
    private ImageView logo;
    private CompanyData companyData;

    public void setCompanyData(CompanyData companyData) {
        this.companyData = companyData;
    }

    public static AdministratorKnowMoreFragment newInstance() {
        return new AdministratorKnowMoreFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_administratir_know_more, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if(view != null){
            logo = (ImageView) view.findViewById(R.id.logo);
            description = (CustomTextViewLight) view.findViewById(R.id.description);

            Picasso.with(getContext()).load(companyData.logo_url).placeholder(R.drawable.default_camera).into(logo);
            description.setText(companyData.getCompanyAppPresentationText());
        }
    }




}
