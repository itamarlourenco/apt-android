package br.com.apt.ui.MyAdm;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by adminbs on 7/31/17.
 */

public class Occurrences implements Parcelable {
    @SerializedName("id")
    public String id;

    @SerializedName("protocol_number")
    public int protocolNumber;

    @SerializedName("title")
    public String title;

    @SerializedName("body")
    public String body;

    @SerializedName("type")
    public String type;

    @SerializedName("status")
    public String status;

    @SerializedName("view_by")
    public int viewBy;

    @SerializedName("closed")
    public boolean closed;

    @SerializedName("deadline")
    public String deadline;

    @SerializedName("deadline_counter")
    public String deadlineCounter;

    @SerializedName("created_at")
    public Date createdAt;

    @SerializedName("updated_at")
    public String updatedAt;

    @SerializedName("condominium")
    public Condominium condominium;

    @SerializedName("theme")
    public Theme theme;

    @SerializedName("subject")
    public Subject subject;

    @SerializedName("priority")
    public Priority priority;

    @SerializedName("created_by")
    public User createdBy;

    @SerializedName("update_by")
    public User updatedBy;

    @SerializedName("owned_by")
    public List<User> ownedBy;

    @SerializedName("rating")
    public int rating;

    @SerializedName("attachments")
    public List<Attachments> attachments;

    private int notified;

    public int getNotified() {
        return notified;
    }

    public boolean isNotified() {
        return notified > 0;
    }

    public void setNotified(int notified) {
        this.notified = notified;
    }

    public static class Condominium implements Parcelable {
        private long id;
        private String name;
        private String location;
        private String phone;
        private String zipcode;
        private String address;
        private String email;
        private float finance;
        private String image;
        private String latitude;
        private String longitude;


        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getZipcode() {
            return zipcode;
        }

        public void setZipcode(String zipcode) {
            this.zipcode = zipcode;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public float getFinance() {
            return finance;
        }

        public void setFinance(float finance) {
            this.finance = finance;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        protected Condominium(Parcel in) {
            id = in.readLong();
            name = in.readString();
            location = in.readString();
            phone = in.readString();
            zipcode = in.readString();
            address = in.readString();
            email = in.readString();
            finance = in.readFloat();
            image = in.readString();
            latitude = in.readString();
            longitude = in.readString();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeLong(id);
            dest.writeString(name);
            dest.writeString(location);
            dest.writeString(phone);
            dest.writeString(zipcode);
            dest.writeString(address);
            dest.writeString(email);
            dest.writeFloat(finance);
            dest.writeString(image);
            dest.writeString(latitude);
            dest.writeString(longitude);
        }

        @SuppressWarnings("unused")
        public static final Parcelable.Creator<Condominium> CREATOR = new Parcelable.Creator<Condominium>() {
            @Override
            public Condominium createFromParcel(Parcel in) {
                return new Condominium(in);
            }

            @Override
            public Condominium[] newArray(int size) {
                return new Condominium[size];
            }
        };
    }

    public static class Theme implements Parcelable {
        @SerializedName("id")
        public String id;

        @SerializedName("name")
        public String name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        protected Theme(Parcel in) {
            id = in.readString();
            name = in.readString();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(id);
            dest.writeString(name);
        }

        @SuppressWarnings("unused")
        public static final Parcelable.Creator<Theme> CREATOR = new Parcelable.Creator<Theme>() {
            @Override
            public Theme createFromParcel(Parcel in) {
                return new Theme(in);
            }

            @Override
            public Theme[] newArray(int size) {
                return new Theme[size];
            }
        };
    }

    public static class Subject implements Parcelable {
        @SerializedName("id")
        public String id;

        @SerializedName("name")
        public String name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        protected Subject(Parcel in) {
            id = in.readString();
            name = in.readString();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(id);
            dest.writeString(name);
        }

        @SuppressWarnings("unused")
        public static final Parcelable.Creator<Subject> CREATOR = new Parcelable.Creator<Subject>() {
            @Override
            public Subject createFromParcel(Parcel in) {
                return new Subject(in);
            }

            @Override
            public Subject[] newArray(int size) {
                return new Subject[size];
            }
        };
    }

    public static class Priority implements Parcelable {
        @SerializedName("id")
        public String id;

        @SerializedName("name")
        public String name;

        protected Priority(Parcel in) {
            id = in.readString();
            name = in.readString();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(id);
            dest.writeString(name);
        }

        @SuppressWarnings("unused")
        public static final Parcelable.Creator<Priority> CREATOR = new Parcelable.Creator<Priority>() {
            @Override
            public Priority createFromParcel(Parcel in) {
                return new Priority(in);
            }

            @Override
            public Priority[] newArray(int size) {
                return new Priority[size];
            }
        };
    }

    public static class User implements Parcelable {

        @SerializedName("id")
        public String id;

        @SerializedName("name")
        public String name;

        @SerializedName("email")
        public String email;

        @SerializedName("document")
        public String document;

        @SerializedName("mobile")
        public String mobile;

        @SerializedName("gender")
        public String gender;

        @SerializedName("role")
        public Role role;

        @SerializedName("photo")
        public Photo photo;

        @SerializedName("company_group")
        public List<CompanyGroup> companyGroup;

        @SerializedName("condominiumkey")
        public String condominiumKey;

        public Photo getPhoto() {
            return photo;
        }

        public void setPhoto(Photo photo) {
            this.photo = photo;
        }

        public static class CompanyGroup implements Parcelable {
            private String id;
            private String name;
            private String description;
            private int user_quantity;
            private boolean is_admin;

            protected CompanyGroup(Parcel in) {
                id = in.readString();
                name = in.readString();
                description = in.readString();
                user_quantity = in.readInt();
                is_admin = in.readByte() != 0x00;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(id);
                dest.writeString(name);
                dest.writeString(description);
                dest.writeInt(user_quantity);
                dest.writeByte((byte) (is_admin ? 0x01 : 0x00));
            }

            @SuppressWarnings("unused")
            public static final Parcelable.Creator<CompanyGroup> CREATOR = new Parcelable.Creator<CompanyGroup>() {
                @Override
                public CompanyGroup createFromParcel(Parcel in) {
                    return new CompanyGroup(in);
                }

                @Override
                public CompanyGroup[] newArray(int size) {
                    return new CompanyGroup[size];
                }
            };


        }

        public static class Role implements Parcelable {

            @SerializedName("name")
            public String name;

            @SerializedName("key")
            public String key;

            protected Role(Parcel in) {
                name = in.readString();
                key = in.readString();
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(name);
                dest.writeString(key);
            }

            @SuppressWarnings("unused")
            public static final Parcelable.Creator<Role> CREATOR = new Parcelable.Creator<Role>() {
                @Override
                public Role createFromParcel(Parcel in) {
                    return new Role(in);
                }

                @Override
                public Role[] newArray(int size) {
                    return new Role[size];
                }
            };
        }

        public static class Photo implements Parcelable {

            @SerializedName("etag")
            public String etag;

            @SerializedName("url")
            public String url;

            protected Photo(Parcel in) {
                etag = in.readString();
                url = in.readString();
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(etag);
                dest.writeString(url);
            }

            @SuppressWarnings("unused")
            public static final Parcelable.Creator<Photo> CREATOR = new Parcelable.Creator<Photo>() {
                @Override
                public Photo createFromParcel(Parcel in) {
                    return new Photo(in);
                }

                @Override
                public Photo[] newArray(int size) {
                    return new Photo[size];
                }
            };
        }

        protected User(Parcel in) {
            id = in.readString();
            name = in.readString();
            email = in.readString();
            document = in.readString();
            mobile = in.readString();
            gender = in.readString();
            role = (Role) in.readValue(Role.class.getClassLoader());
            photo = (Photo) in.readValue(Photo.class.getClassLoader());
            if (in.readByte() == 0x01) {
                companyGroup = new ArrayList<CompanyGroup>();
                in.readList(companyGroup, CompanyGroup.class.getClassLoader());
            } else {
                companyGroup = null;
            }
            condominiumKey = in.readString();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(id);
            dest.writeString(name);
            dest.writeString(email);
            dest.writeString(document);
            dest.writeString(mobile);
            dest.writeString(gender);
            dest.writeValue(role);
            dest.writeValue(photo);
            if (companyGroup == null) {
                dest.writeByte((byte) (0x00));
            } else {
                dest.writeByte((byte) (0x01));
                dest.writeList(companyGroup);
            }
            dest.writeString(condominiumKey);
        }

        @SuppressWarnings("unused")
        public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {
            @Override
            public User createFromParcel(Parcel in) {
                return new User(in);
            }

            @Override
            public User[] newArray(int size) {
                return new User[size];
            }
        };
    }

    public static class Attachments implements Parcelable {
        private String name;
        private String etag;
        private String url;



        public String getEtag() {
            return etag;
        }

        public void setEtag(String etag) {
            this.etag = etag;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public Attachments(String name, String etag, String url) {
            this.name = name;
            this.etag = etag;
            this.url = url;
        }

        protected Attachments(Parcel in) {
            etag = in.readString();
            url = in.readString();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(etag);
            dest.writeString(url);
        }

        @SuppressWarnings("unused")
        public static final Parcelable.Creator<Attachments> CREATOR = new Parcelable.Creator<Attachments>() {
            @Override
            public Attachments createFromParcel(Parcel in) {
                return new Attachments(in);
            }

            @Override
            public Attachments[] newArray(int size) {
                return new Attachments[size];
            }
        };
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getProtocolNumber() {
        return protocolNumber;
    }

    public void setProtocolNumber(int protocolNumber) {
        this.protocolNumber = protocolNumber;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getViewBy() {
        return viewBy;
    }

    public void setViewBy(int viewBy) {
        this.viewBy = viewBy;
    }

    public boolean isClosed() {
        return closed;
    }

    public void setClosed(boolean closed) {
        this.closed = closed;
    }

    public String getDeadline() {
        return deadline;
    }

    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }

    public String getDeadlineCounter() {
        return deadlineCounter;
    }

    public void setDeadlineCounter(String deadlineCounter) {
        this.deadlineCounter = deadlineCounter;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Condominium getCondominium() {
        return condominium;
    }

    public void setCondominium(Condominium condominium) {
        this.condominium = condominium;
    }

    public Theme getTheme() {
        return theme;
    }

    public void setTheme(Theme theme) {
        this.theme = theme;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public Priority getPriority() {
        return priority;
    }

    public void setPriority(Priority priority) {
        this.priority = priority;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public User getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(User updatedBy) {
        this.updatedBy = updatedBy;
    }

    public List<User> getOwnedBy() {
        return ownedBy;
    }

    public void setOwnedBy(List<User> ownedBy) {
        this.ownedBy = ownedBy;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public List<Attachments> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<Attachments> attachments) {
        this.attachments = attachments;
    }

    protected Occurrences(Parcel in) {
        id = in.readString();
        protocolNumber = in.readInt();
        title = in.readString();
        body = in.readString();
        type = in.readString();
        status = in.readString();
        viewBy = in.readInt();
        notified = in.readInt();
        closed = in.readByte() != 0x00;
        deadline = in.readString();
        deadlineCounter = in.readString();
        long tmpCreatedAt = in.readLong();
        createdAt = tmpCreatedAt != -1 ? new Date(tmpCreatedAt) : null;
        updatedAt = in.readString();
        condominium = (Condominium) in.readValue(Condominium.class.getClassLoader());
        theme = (Theme) in.readValue(Theme.class.getClassLoader());
        subject = (Subject) in.readValue(Subject.class.getClassLoader());
        priority = (Priority) in.readValue(Priority.class.getClassLoader());
        createdBy = (User) in.readValue(User.class.getClassLoader());
        updatedBy = (User) in.readValue(User.class.getClassLoader());
        if (in.readByte() == 0x01) {
            ownedBy = new ArrayList<User>();
            in.readList(ownedBy, User.class.getClassLoader());
        } else {
            ownedBy = null;
        }
        rating = in.readInt();
        if (in.readByte() == 0x01) {
            attachments = new ArrayList<Attachments>();
            in.readList(attachments, Attachments.class.getClassLoader());
        } else {
            attachments = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeInt(protocolNumber);
        dest.writeString(title);
        dest.writeString(body);
        dest.writeString(type);
        dest.writeString(status);
        dest.writeInt(viewBy);
        dest.writeInt(notified);
        dest.writeByte((byte) (closed ? 0x01 : 0x00));
        dest.writeString(deadline);
        dest.writeString(deadlineCounter);
        dest.writeLong(createdAt != null ? createdAt.getTime() : -1L);
        dest.writeString(updatedAt);
        dest.writeValue(condominium);
        dest.writeValue(theme);
        dest.writeValue(subject);
        dest.writeValue(priority);
        dest.writeValue(createdBy);
        dest.writeValue(updatedBy);
        if (ownedBy == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(ownedBy);
        }
        dest.writeInt(rating);
        if (attachments == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(attachments);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Occurrences> CREATOR = new Parcelable.Creator<Occurrences>() {
        @Override
        public Occurrences createFromParcel(Parcel in) {
            return new Occurrences(in);
        }

        @Override
        public Occurrences[] newArray(int size) {
            return new Occurrences[size];
        }
    };
}