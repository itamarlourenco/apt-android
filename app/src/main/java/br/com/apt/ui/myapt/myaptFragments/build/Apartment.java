package br.com.apt.ui.myapt.myaptFragments.build;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;
import br.com.apt.application.ModelObject;

/**
 * Created by adminbs on 5/10/16.
 */
public class Apartment implements ModelObject, Parcelable {
    private String id;

    @SerializedName("building_id")
    private String buildingId;
    private String identification;
    private String code;
    private String complement;

    @SerializedName("dweller_count")
    private int dwellerCount;

    @SerializedName("img_plan")
    private String imgPlan;

    private int floor;

    @SerializedName("square_meters")
    private String squareMeters;

    private float condominium;

    @SerializedName("created_at")
    private String createdAt;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBuildingId() {
        return buildingId;
    }

    public void setBuildingId(String buildingId) {
        this.buildingId = buildingId;
    }

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getComplement() {
        return complement;
    }

    public void setComplement(String complement) {
        this.complement = complement;
    }

    public int getDwellerCount() {
        return dwellerCount;
    }

    public void setDwellerCount(int dwellerCount) {
        this.dwellerCount = dwellerCount;
    }

    public String getImgPlan() {
        return imgPlan;
    }

    public void setImgPlan(String imgPlan) {
        this.imgPlan = imgPlan;
    }

    public int getFloor() {
        return floor;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }

    public String getSquareMeters() {
        return squareMeters;
    }

    public void setSquareMeters(String squareMeters) {
        this.squareMeters = squareMeters;
    }

    public float getCondominium() {
        return condominium;
    }

    public void setCondominium(float condominium) {
        this.condominium = condominium;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    protected Apartment(Parcel in) {
        id = in.readString();
        buildingId = in.readString();
        identification = in.readString();
        code = in.readString();
        complement = in.readString();
        dwellerCount = in.readInt();
        imgPlan = in.readString();
        floor = in.readInt();
        squareMeters = in.readString();
        condominium = in.readFloat();
        createdAt = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(buildingId);
        dest.writeString(identification);
        dest.writeString(code);
        dest.writeString(complement);
        dest.writeInt(dwellerCount);
        dest.writeString(imgPlan);
        dest.writeInt(floor);
        dest.writeString(squareMeters);
        dest.writeFloat(condominium);
        dest.writeString(createdAt);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Apartment> CREATOR = new Parcelable.Creator<Apartment>() {
        @Override
        public Apartment createFromParcel(Parcel in) {
            return new Apartment(in);
        }

        @Override
        public Apartment[] newArray(int size) {
            return new Apartment[size];
        }
    };
}