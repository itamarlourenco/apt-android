package br.com.apt.ui.MyAdm.myadmFragments.sac;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.URLUtil;
import android.widget.LinearLayout;

import java.util.List;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.ui.MyAdm.OccurrenceRequest;
import br.com.apt.ui.MyAdm.Occurrences;
import br.com.apt.ui.MyAdm.OccurrencesRequest;
import br.com.apt.ui.MyAdm.OccurrencesSend;
import br.com.apt.ui.MyAdm.OccurrencesSingle;
import br.com.apt.ui.MyAdm.myadmFragments.OccurrencesSingleRequest;
import br.com.apt.ui.myCondominium.myCondominiumFragments.legislation.LegislationActivity;
import br.com.apt.util.Logger;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomButton;
import br.com.apt.widget.CustomTextViewLight;
import br.com.apt.widget.SendSuccessDialog;

/**
 * Created by adminbs on 8/16/17.
 */

public class DialogSacDetails extends DialogFragment implements View.OnClickListener {

    /** Tag for this fragment. */
    private static final String FRAG_TAG = "dialogSacDetailsTag";
    private Occurrences occurrence;
    private CustomTextViewLight opening;
    private CustomTextViewLight subject;
    private CustomTextViewLight theme;
    private CustomTextViewLight title;
    private CustomTextViewLight description;
    private CustomTextViewLight status;
    private LinearLayout attachment;

    private CustomButton close;

    /**
     * Shows an instance of this fragment to present the Terms of Use to the user.
     * @param fragmentManager a {@link FragmentManager} object to show this {@link DialogFragment}.
     */
    public static void show(FragmentManager fragmentManager, Occurrences occurrences) {
        if (fragmentManager.findFragmentByTag(FRAG_TAG) == null) {
            DialogSacDetails fragment = new DialogSacDetails();
            fragment.show(fragmentManager, FRAG_TAG);
            fragment.setOccurrences(occurrences);
        }
    }

    public void setOccurrences(Occurrences occurrence) {
        this.occurrence = occurrence;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_sac_details, container, false);
        Window window = getDialog().getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if (view != null) {
            opening = (CustomTextViewLight) view.findViewById(R.id.opening);
            description = (CustomTextViewLight) view.findViewById(R.id.description);
            subject = (CustomTextViewLight) view.findViewById(R.id.subject);
            theme = (CustomTextViewLight) view.findViewById(R.id.theme);
            status = (CustomTextViewLight) view.findViewById(R.id.status);
            title = (CustomTextViewLight) view.findViewById(R.id.title);
            description = (CustomTextViewLight) view.findViewById(R.id.description);
            close = (CustomButton) view.findViewById(R.id.close);
            attachment = (LinearLayout) view.findViewById(R.id.attachment);

            opening.setText(
                    new StringBuffer(Util.dateFormatted(occurrence.getCreatedAt()))
                            .append(" às ")
                            .append(Util.timeFormatted(occurrence.getCreatedAt()))
            );
            title.setText(occurrence.getTitle());
            description.setText(occurrence.getBody());


            close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getDialog().dismiss();
                }
            });

            Occurrences.Subject subject = occurrence.getSubject();
            if (subject != null) {
                this.subject.setText(subject.getName());
            }
            Occurrences.Theme theme = occurrence.getTheme();
            if (theme != null) {
                this.theme.setText(theme.getName());
            }

            String status = occurrence.getStatus();
            if(!TextUtils.isEmpty(status)) {
                if (status.equals("Vencido")) {
                    this.status.setText("Andamento");
                    this.status.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.border_radius_green));
                    this.status.setTextColor(ContextCompat.getColor(getContext(), R.color.pale_olive_green));
                } else if (status.equals("Finalizado com atraso")) {
                    this.status.setText("Finalizado");
                    this.status.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.border_radius_blue));
                    this.status.setTextColor(ContextCompat.getColor(getContext(), R.color.faded_blue));
                } else {
                    this.status.setText("Aberto");
                    this.status.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.border_radius_yellow));
                    this.status.setTextColor(ContextCompat.getColor(getContext(), R.color.leather));
                }
            }

            getOccurrence();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void handleAttachment(final OccurrencesSingle occurrencesSingle) {
        attachment.removeAllViews();
        if(occurrencesSingle != null && occurrencesSingle.attachments != null){
            for(final OccurrencesSingle.Attachments attachments:  occurrencesSingle.attachments){
                final CustomTextViewLight customTextViewLight = new CustomTextViewLight(getContext());
                customTextViewLight.setTextColor(ContextCompat.getColor(getActivity(), R.color.text_color_attachment));
                customTextViewLight.setText(attachments.getPhoto_name());
                customTextViewLight.setTextSize(16);
                customTextViewLight.setMaxLines(1);
                customTextViewLight.setPadding(0, 0, 0, 30);

                if(attachments.getPhoto_name().contains(".pdf")){
                    Drawable drawable = ContextCompat.getDrawable(getContext(),R.drawable.icon_pdf);
                    Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
                    Drawable drawableFinal = new BitmapDrawable(this.getResources(), Bitmap.createScaledBitmap(bitmap, 70, 70, true));
                    customTextViewLight.setCompoundDrawablePadding(30);
                    customTextViewLight.setCompoundDrawablesWithIntrinsicBounds(drawableFinal, null, null, null);
                }else{
                    Drawable drawable = ContextCompat.getDrawable(getContext(),R.drawable.icon_image);
                    Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
                    Drawable drawableFinal = new BitmapDrawable(this.getResources(), Bitmap.createScaledBitmap(bitmap, 30, 30, true));
                    customTextViewLight.setCompoundDrawablePadding(10);
                    customTextViewLight.setCompoundDrawablesWithIntrinsicBounds(drawableFinal, null, null, null);
                }

                customTextViewLight.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String url = attachments.getPhoto_url();
                        if(URLUtil.isValidUrl(url)){
                            Uri parse = Uri.parse(url);

                            Intent intent = new Intent(getActivity(), LegislationActivity.class);
                            intent.putExtra("title", "ANEXO");
                            intent.putExtra("url", url);
                            startActivity(intent);

//                            startActivity(new Intent(Intent.ACTION_VIEW, parse));
                        }else{
                            Logger.t(getString(R.string.error_attachment));
                        }
                    }
                });

                attachment.addView(customTextViewLight);
            }
        }else{
            CustomTextViewLight customTextViewLight = new CustomTextViewLight(getContext());
            customTextViewLight.setTextColor(ContextCompat.getColor(getActivity(), R.color.text_color_attachment));
            customTextViewLight.setText(R.string.without_attachment);
            customTextViewLight.setTextSize(16);
            customTextViewLight.setMaxLines(1);

            attachment.addView(customTextViewLight);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.closeDialog:
                closeDialog();
                break;
        }
    }

    private void closeDialog() {
        dismiss();
    }

    @Override
    public int getTheme() {
        return R.style.CustomDialog;
    }

    public void getOccurrence() {
        Volley volley = new Volley(new Volley.Callback() {
            @Override
            public void result(String result, int type) {
                try{
                    OccurrencesSingleRequest occurrencesRequest = App.getGson().fromJson(result, OccurrencesSingleRequest.class);
                    OccurrencesSingle object = occurrencesRequest.getObject();
                    if (object != null) {
                        handleAttachment(object);
                    }
                }catch (Exception e){
                    SendSuccessDialog.show(getString(R.string.error_generic), null, getFragmentManager(), new SendSuccessDialog.Actions() {
                        @Override
                        public void back() {
                            getActivity().finish();
                        }
                    }, SendSuccessDialog.TYPE_ERROR);
                }
            }

            @Override
            public String uri() {
                return String.format(URI.OCCURRENCE, occurrence.getId());
            }
        });
        volley.isMeuADM(true);
        volley.showDialog(getActivity());
        volley.request();
    }
}
