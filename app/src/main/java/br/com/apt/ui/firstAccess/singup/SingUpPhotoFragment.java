package br.com.apt.ui.firstAccess.singup;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import java.io.IOException;

import br.com.apt.R;
import br.com.apt.application.BaseFragment;
import br.com.apt.application.PicturesHelper;
import de.hdodenhof.circleimageview.CircleImageView;
import siclo.com.ezphotopicker.api.EZPhotoPick;
import siclo.com.ezphotopicker.api.EZPhotoPickStorage;

/**
 * Created by adminbs on 9/15/16.
 */
public class SingUpPhotoFragment extends BaseFragment implements View.OnClickListener {

    public static Bitmap bitmap;
    public static String urlImage;
    private RelativeLayout basePhoto;
    private static CircleImageView circlePhoto;
    private boolean takePhoto = false;

    public static SingUpPhotoFragment newInstance() {
        return new SingUpPhotoFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.sing_up_photo, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if(view != null){
            SingUpPhotoFragment.circlePhoto = (CircleImageView) view.findViewById(R.id.circlePhoto);
            basePhoto = (RelativeLayout) view.findViewById(R.id.basePhoto);
            basePhoto.setOnClickListener(this);
        }
    }


    public void onActivityResultFragment(int requestCode, int resultCode, Intent data) {
        if(requestCode == EZPhotoPick.PHOTO_PICK_REQUEST_CODE){
            try {
                EZPhotoPickStorage ezPhotoPickStorage = new EZPhotoPickStorage(getActivity());
                SingUpPhotoFragment.bitmap = ezPhotoPickStorage.loadLatestStoredPhotoBitmap();

                SingUpPhotoFragment.circlePhoto.setImageBitmap(SingUpPhotoFragment.bitmap);
                SingUpPhotoFragment.circlePhoto.setVisibility(View.VISIBLE);
                takePhoto = true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    private void pickPictureOrPickFromGallery() {
        CharSequence[] options = {getString(R.string.takePhoto), getString(R.string.chooseGallery)};
        if(takePhoto){
            options = new CharSequence[]{getString(R.string.takePhoto), getString(R.string.chooseGallery), Html.fromHtml("<font color='#FF0000'>" + getString(R.string.remove_photo) + "</font>")};
        }


        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("")
                .setItems(options, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if(which == 0) {
                            PicturesHelper.Camera(getActivity());
                        } else if(which == 1) {
                            PicturesHelper.Gallery(getActivity());
                        }else{
                            circlePhoto.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.circle_photo));
                            takePhoto = false;
                        }
                    }
                });
        builder.create().show();
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.basePhoto){
            pickPictureOrPickFromGallery();
        }
    }

}
