package br.com.apt.ui.MyAdm.myadmFragments.term;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.android.volley.Request;
import com.github.ybq.android.spinkit.SpinKitView;


import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.BaseGsonMeuAdm;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.model.user.User;
import br.com.apt.model.user.UserData;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomButton;
import br.com.apt.widget.SendSuccessDialog;

/**
 * Created by adminbs on 7/21/17.
 */

public class TermFragment extends br.com.apt.widget.PagerItemFragment implements Volley.Callback {

    private WebView webView;
    private CustomButton sendTerm;
    private SpinKitView loader;
    private User user = UserData.getUser();


    @Override
    protected String getPagerTitle() {
        return App.getContext().getString(R.string.terms);
    }

    @Override
    protected int getPagerIcon() {
        return R.drawable.ico_menu_termo_quitacao;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_terms_discharge, container, false);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if(view != null){
            webView = (WebView) view.findViewById(R.id.webView);
            sendTerm = (CustomButton) view.findViewById(R.id.sendTerm);
            loader = (SpinKitView) view.findViewById(R.id.loader);
            WebSettings webSettings = webView.getSettings();
            webSettings.setJavaScriptEnabled(true);
            webSettings.setBuiltInZoomControls(true);
            webView.clearCache(true);

            view.findViewById(R.id.sendTerm).setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    sendTerm();
                }
            });

            Volley volley = new Volley(this);
            volley.isMeuADM(true);
            volley.request(Request.Method.POST, null);
        }
    }

    private void sendTerm() {
        Volley volley = new Volley(new Volley.Callback() {
            @Override
            public void result(String result, int type) {
                try{
                    if(result != null){
                        BaseGsonMeuAdm baseGsonMeuAdm = App.getGson().fromJson(result, BaseGsonMeuAdm.class);
                        SendSuccessDialog.show(baseGsonMeuAdm.getMessage(), null, getFragmentManager(), new SendSuccessDialog.Actions() {
                            @Override
                            public void back() {
                                getActivity().finish();
                            }
                        });
                    }
                }catch (Exception e){
                    SendSuccessDialog.show(getString(R.string.error_generic), null, getFragmentManager(), new SendSuccessDialog.Actions() {
                        @Override
                        public void back() {
                            getActivity().finish();
                        }
                    }, SendSuccessDialog.TYPE_ERROR);
                }
            }

            @Override
            public String uri() {
                return String.format(URI.MAIL_TERM, user.getCondominiumId(), user.getApto());
            }
        });
        volley.showDialog(getActivity());
        volley.isMeuADM(true);
        volley.request(Request.Method.POST, null);
    }

    @Override
    public void result(String result, int type) {
        try{
            if(result != null){
                Util.changeVisibility(loader, View.GONE, true);
                Util.changeVisibility(webView, View.VISIBLE, true);
                Util.changeVisibility(sendTerm, View.VISIBLE, true);

                TermsDischargeRequest cashFlowRequest = App.getGson().fromJson(result, TermsDischargeRequest.class);
                webView.loadData(cashFlowRequest.getObject(),  "text/html; charset=utf-8", "UTF-8");
            }
        }catch (Exception e){
            SendSuccessDialog.show(getString(R.string.error_generic), null, getFragmentManager(), new SendSuccessDialog.Actions() {
                @Override
                public void back() {
                    getActivity().finish();
                }
            }, SendSuccessDialog.TYPE_ERROR);
        }
    }

    @Override
    public String uri() {
        return String.format(URI.TERMS_DISCHARGE, user.getCondominiumId(), user.getApto());
    }
}
