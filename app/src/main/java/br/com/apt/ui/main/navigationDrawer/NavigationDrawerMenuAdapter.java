package br.com.apt.ui.main.navigationDrawer;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.ui.main.DisabledFunction;
import br.com.apt.ui.main.MainActivity;
import br.com.apt.ui.main.menu.MainMenuFragment;
import br.com.apt.ui.main.navigationDrawer.holder.ViewItemHolder;
import br.com.apt.ui.main.navigationDrawer.holder.ViewOutHolder;
import br.com.apt.ui.main.navigationDrawer.holder.ViewSettingHolder;

/**
 * Created by itamarlourenco on 02/01/16.
 */
public class NavigationDrawerMenuAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int VIEW_SETTING = 0;
    private static final int VIEW_ITEM = 1;
    private static final int VIEW_OUT = 2;

    private Context context;
    public List<NavigationDrawerMenu> items = App.getItems();
    private OnClickItemMenu onClickItemMenu;

    public NavigationDrawerMenuAdapter(Context context) {
        this.context = context;
    }

    public NavigationDrawerMenuAdapter(Context context, OnClickItemMenu onClickItemMenu){
        this.context = context;
        this.onClickItemMenu = onClickItemMenu;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType){
            case VIEW_SETTING:
                return new ViewSettingHolder(
                        ViewSettingHolder.getView(context, parent), onClickItemMenu
                );
            case VIEW_ITEM:
                return new ViewItemHolder(
                        ViewItemHolder.getView(context, parent), onClickItemMenu
                );
            case VIEW_OUT:
                return new ViewOutHolder(
                        ViewOutHolder.getView(context, parent), onClickItemMenu
                );
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)){
            case VIEW_SETTING:
                ViewSettingHolder viewSettingHolder = (ViewSettingHolder) holder;
                break;
            case VIEW_ITEM:
                ViewItemHolder viewItemHolder = (ViewItemHolder) holder;
                viewItemHolder.textView.setText(getItem(position).toString());
                viewItemHolder.imageView.setImageResource(getItem(position).icon());

                if(MainMenuFragment.disabledFunctions != null && MainMenuFragment.disabledFunctions.size() > 0) {
                    for (DisabledFunction disabledFunction : MainMenuFragment.disabledFunctions) {


                        if(TextUtils.equals(disabledFunction.getFunction(), DisabledFunction.LOBBY)){
                            if(getItem(position).id() == R.id.relativeLobby){
                                viewItemHolder.textView.setEnabled(false);
                                viewItemHolder.imageView.setEnabled(false);
                            }
                        }

                        if(TextUtils.equals(disabledFunction.getFunction(), DisabledFunction.RESERVATIONS)){
                            if(getItem(position).id() == R.id.relativeReservations){
                                viewItemHolder.textView.setEnabled(false);
                                viewItemHolder.imageView.setEnabled(false);
                            }
                        }

                        if(TextUtils.equals(disabledFunction.getFunction(), DisabledFunction.NOTIFICATION)){
                            if(getItem(position).id() == R.id.relativeNotification){
                                viewItemHolder.textView.setEnabled(false);
                                viewItemHolder.imageView.setEnabled(false);
                            }
                        }

                        if(TextUtils.equals(disabledFunction.getFunction(), DisabledFunction.TRAVEL)){
                            if(getItem(position).id() == R.id.relativeAdministrator){
                                viewItemHolder.textView.setEnabled(false);
                                viewItemHolder.imageView.setEnabled(false);
                            }
                        }

                        if(TextUtils.equals(disabledFunction.getFunction(), DisabledFunction.MY_APT)){
                            if(getItem(position).id() == R.id.relativeMyApt){
                                viewItemHolder.textView.setEnabled(false);
                                viewItemHolder.imageView.setEnabled(false);
                            }
                        }
                    }
                }
                break;
            case VIEW_OUT:
                ViewOutHolder viewOutHolder = (ViewOutHolder) holder;
                break;
        }
    }


    @Override
    public int getItemCount() {
        return items != null ? items.size() : 0;
    }

    @Override
    public int getItemViewType(int position) {
        if(position == 0){
            return VIEW_SETTING;
        }else if(position == (items.size() - 1)){
            return VIEW_OUT;
        }

        return VIEW_ITEM;
    }

    public NavigationDrawerMenu getItem(int position){
        return items.get(position);
    }

    public interface OnClickItemMenu{
        void clickClose(View view, int position);
    }
}
