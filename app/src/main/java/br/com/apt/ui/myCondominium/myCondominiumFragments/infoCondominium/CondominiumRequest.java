package br.com.apt.ui.myCondominium.myCondominiumFragments.infoCondominium;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.apt.application.BaseGson;
import br.com.apt.ui.neighborhood.neighborhoodFragment.model.Neighborhood;

/**
 * Created by adminbs on 9/3/17.
 */

public class CondominiumRequest extends BaseGson {
    private Condominium object;

    public Condominium getObject() {
        return object;
    }

    public void setObject(Condominium object) {
        this.object = object;
    }
}
