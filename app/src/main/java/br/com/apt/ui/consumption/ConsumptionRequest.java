package br.com.apt.ui.consumption;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import br.com.apt.application.BaseGson;
import br.com.apt.application.ModelObject;

/**
 * Created by itamarlourenco on 16/01/16.
 */
public class ConsumptionRequest extends BaseGson implements ModelObject {
    private AllConsumption Object;

    public AllConsumption getObject() {
        return Object;
    }

    public void setObject(AllConsumption object) {
        Object = object;
    }

    protected ConsumptionRequest(Parcel in) {
        Object = (AllConsumption) in.readValue(AllConsumption.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        try{
            dest.writeValue(Object);
        }catch (Exception e){}

    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<ConsumptionRequest> CREATOR = new Parcelable.Creator<ConsumptionRequest>() {
        @Override
        public ConsumptionRequest createFromParcel(Parcel in) {
            return new ConsumptionRequest(in);
        }

        @Override
        public ConsumptionRequest[] newArray(int size) {
            return new ConsumptionRequest[size];
        }
    };

    public class AllConsumption{
        @SerializedName("agua")
        public Consumption water;

        @SerializedName("energia")
        public Consumption energy;
        public Consumption gas;

        public Consumption getWater() {
            return water;
        }

        public void setWater(Consumption water) {
            this.water = water;
        }

        public Consumption getEnergy() {
            return energy;
        }

        public void setEnergy(Consumption energy) {
            this.energy = energy;
        }

        public Consumption getGas() {
            return gas;
        }

        public void setGas(Consumption gas) {
            this.gas = gas;
        }
    }

}
