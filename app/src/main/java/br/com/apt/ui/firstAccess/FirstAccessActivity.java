package br.com.apt.ui.firstAccess;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import br.com.apt.application.BaseActivity;
import br.com.apt.ui.reservations.CalendarFragment;

public class FirstAccessActivity extends BaseActivity {

    public static Intent newIntent(Context context) {
        return new Intent(context, FirstAccessActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public FirstAccessFragment getFragment(){
        return FirstAccessFragment.newInstance();
    }

    @Override
    protected boolean showNavigationDrawer() {
        return false;
    }

    @Override
    protected boolean showToolbarLogo() {
        return true;
    }

    @Override
    protected boolean checkLogin() {
        return false;
    }
}