package br.com.apt.ui.MyAdm.myadmFragments.cashflow;

import br.com.apt.application.BaseGsonMeuAdm;
import br.com.apt.ui.MyAdm.myadmFragments.Billet.BilletDetails;

/**
 * Created by adminbs on 8/2/17.
 */

public class CashFlowRequest extends BaseGsonMeuAdm {
    private String object;

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }
}