package br.com.apt.ui.myapt.myaptFragments.chat;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import com.android.volley.Request;

import org.jivesoftware.smack.*;
import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Presence;


import java.util.ArrayList;
import java.util.List;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.BaseGson;
import br.com.apt.application.Config;
import br.com.apt.application.Volley;
import br.com.apt.application.chat.AptChatMessages;
import br.com.apt.application.chat.AptChatWrapper;
import br.com.apt.application.chat.Authenticate;
import br.com.apt.application.chat.ChatMessageAdapter;
import br.com.apt.application.chat.XMPPConnectionCustom;
import br.com.apt.model.user.User;
import br.com.apt.ui.myapt.MyAptActivity;
import br.com.apt.ui.myapt.myaptFragments.chat.adapter.ChatAdapter;
import br.com.apt.util.Logger;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomEditText;
import br.com.apt.widget.CustomTextView;
import br.com.apt.widget.PagerItemFragment;


public class ChatFragment extends PagerItemFragment implements AdapterView.OnItemClickListener, XMPPConnectionCustom.ConnectionListener, ChatManagerListener, MessageListener {

    private ListView listView;
    private ImageView buttonSend;
    private CustomTextView feedback;
    private ArrayList<RosterEntry> rosterEntries;
    private XMPPConnectionCustom xmppConnection;
    private CustomEditText chatMessage;
    private ListView listChat;
    private ChatMessageAdapter chatMessageAdapter;
    private List<AptChatMessages> messages;
    private Roster roster;
    private String userParticipant;

    private static final String USER_OFFLINE = "unavailable";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_chat, container, false);
    }

    @Override
    protected int getIndicator() {
        return 0;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //App.getNotificationSharedPreferences().clearCountVisit();

        User user = getUser();
        View view = getView();
        if(view != null){
            listView = (ListView) view.findViewById(R.id.listView);
            listChat = (ListView) view.findViewById(R.id.listChat);
            listView.setOnItemClickListener(this);
            feedback = (CustomTextView) view.findViewById(R.id.feedBack);
            feedback.setText(R.string.connecting);
            buttonSend = (ImageView) view.findViewById(R.id.buttonSend);
            chatMessage = (CustomEditText) view.findViewById(R.id.chatMessage);

            messages = AptChatMessages.getMessagens();
            chatMessageAdapter = new ChatMessageAdapter(getContext(), messages);
            listChat.setAdapter(chatMessageAdapter);
        }

        if(user != null){
            App.setXmppConnection(new XMPPConnectionCustom(new ConnectionConfiguration(Config.SMACK.HOST, Config.SMACK.PORT)));
            xmppConnection = App.getXmppConnection();
            xmppConnection.setAuthenticate(Authenticate.createAuthenticate(user));
            xmppConnection.setActivity(getActivity());
            xmppConnection.setConnectionListener(this);
            xmppConnection.connect();
        }
    }

    @Override
    protected String getPagerTitle() {
        return App.getContext().getString(R.string.chat);
    }

    @Override
    protected int getPagerIcon() {
        return R.drawable.ic_myapt_chat;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        View fragmentView = getView();
        if(fragmentView != null && rosterEntries != null && rosterEntries.size() > 0) {
            final View openChat = fragmentView.findViewById(R.id.openChat);
            Util.changeVisibility(listView, View.GONE, true);
            Util.changeVisibility(openChat, View.VISIBLE, true);

            final RosterEntry rosterEntry = rosterEntries.get(position);
            initChat(rosterEntry);

            fragmentView.findViewById(R.id.closeChat).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Util.changeVisibility(listView, View.VISIBLE, true);
                    Util.changeVisibility(openChat, View.GONE, true);
                }
            });
        }
    }

    private void initChat(final RosterEntry rosterEntry) {
        if(xmppConnection != null && xmppConnection.isConnected() && rosterEntry != null){
            userParticipant = rosterEntry.getUser();

//            refreshListView();
//            if(MyAptActivity.getChat() == null){
//                userParticipant = rosterEntry.getUser();
//                Chat chat = xmppConnection.getChatManager().createChat(userParticipant, this);
//                MyAptActivity.setChat(chat);
//            }else{
//                MyAptActivity.getChat().removeMessageListener(this);
//                MyAptActivity.getChat().addMessageListener(this);
//            }
//            buttonSend.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    sendMessage(MyAptActivity.getChat(), rosterEntry);
//                }
//            });
//
//            View view = getView();
//            if(view != null){
//                ((CustomTextView) view.findViewById(R.id.nameUserChat)).setText(rosterEntry.getName());
//                String apto = "Apto. " + Integer.parseInt(Util.getAptoByUser(rosterEntry.getUser()));
//                ((CustomTextView) view.findViewById(R.id.aptoUserChat)).setText(apto);
//            }
        }
    }

    private void sendMessage(Chat chat, RosterEntry rosterEntry) {
        String messageSend = chatMessage.getText().toString();
        if (!TextUtils.isEmpty(messageSend)) {
            try {
                if(xmppConnection.isConnected()){
                    Presence presence = roster.getPresence(rosterEntry.getUser());
                    if(presence.getType().toString().equals(USER_OFFLINE)){
                        senderMessageToUserOffline(messageSend);
                    }else{
                        chat.sendMessage(messageSend);
                        if (AptChatWrapper.saveMessageChat(messageSend, xmppConnection.getAuthenticate().getLogin(), userParticipant, xmppConnection.getAuthenticate().getLogin()) > 0) {
                            refreshListView();
                            Util.hideKeyboard(getContext(), getView());
                            chatMessage.setText("");
                        }
                    }
                }else{
                    Logger.t(R.string.error_sender_message_chat);
                    getActivity().finish();
                }

                return;
            } catch (XMPPException e) {
                Logger.e(e.getMessage());
            }
            Logger.t(R.string.error_send_message);
        }
    }

    private void senderMessageToUserOffline(String message) {
        User user = getUser();
        if(user != null){
            MessagePushChat messagePushChat = new MessagePushChat();

            messagePushChat.setBuildId(user.getBuildId());
            messagePushChat.setBlockId(user.getBlockId());
            messagePushChat.setMessage(message);
            messagePushChat.setApto(Util.getAptoByUser(userParticipant));

            if (AptChatWrapper.saveMessageChat(message, xmppConnection.getAuthenticate().getLogin(), userParticipant, xmppConnection.getAuthenticate().getLogin()) > 0) {
                refreshListView();
                Util.hideKeyboard(getContext(), getView());
                chatMessage.setText("");
            }

            Volley volley = new Volley(new Volley.Callback() {
                @Override
                public void result(String result, int type) {
                    if(result != null){
                        BaseGson baseGson = App.getGson().fromJson(result, BaseGson.class);
                        if(baseGson.getStatus() == Volley.STATUS_OK){
                            Logger.d("Message chat push notification sender");
                        }
                    }
                }

                @Override
                public String uri() {
                    return "messagepushchat";
                }
            });
            volley.request(Request.Method.POST, messagePushChat.toJson());
        }
    }

    @Override
    public void connected(XMPPConnectionCustom xmppConnectionCustom) {
        if(xmppConnectionCustom != null){
            xmppConnectionCustom.getChatManager().addChatListener(this);
            View view = getView();
            roster = xmppConnectionCustom.getRoster();
            if(view != null && roster != null){
                rosterEntries = new ArrayList<>(roster.getEntries());
                ChatAdapter chatAdapter = new ChatAdapter(getContext(), rosterEntries);
                Util.hideFeedBackFragment(view, listView);
                feedback.setVisibility(View.GONE);
                listView.setAdapter(chatAdapter);
                listView.setVisibility(View.VISIBLE);
            }
        }else{
            feedback.setText(R.string.error_generic);
        }
    }

    @Override
    public void chatCreated(Chat chat, boolean b) {
//        if(chat != null){
//            if(MyAptActivity.getChat() == null){
//                MyAptActivity.setChat(chat);
//            }else{
//                if(!chat.getThreadID().equals(MyAptActivity.getChat().getThreadID())){
//                    MyAptActivity.setChat(chat);
//                }
//            }
//
//            MyAptActivity.getChat().removeMessageListener(this);
//            MyAptActivity.getChat().addMessageListener(this);
//        }
    }

    public void receiveMessage(Message message){
//        if(message != null){
//            if (AptChatWrapper.saveMessageChat(message.getBody(), xmppConnection.getAuthenticate().getLogin(),userParticipant, MyAptActivity.getChat().getParticipant()) > 0) {
//                refreshListView();
//            }
//        }
    }

    private void refreshListView() {
//        if (messages != null && xmppConnection != null && MyAptActivity.getChat() != null) {
//            messages.clear();
//            messages.addAll(AptChatMessages.getChatByFrom(userParticipant));
//            chatMessageAdapter.notifyDataSetChanged();
//        }
    }

    @Override
    public void processMessage(Chat chat, final Message message) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                receiveMessage(message);
            }
        });
    }
}
