package br.com.apt.ui.myinfo.myinfofragment.resident;

import java.util.List;

import br.com.apt.application.BaseGson;
import br.com.apt.ui.myinfo.myinfofragment.AutoComplete;

/**
 * Created by adminbs on 12/29/17.
 */

public class ResidentTypeRequest  extends BaseGson {
    public List<AutoComplete> Object;

    public List<AutoComplete> getObject() {
        return Object;
    }
}