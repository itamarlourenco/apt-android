package br.com.apt.ui.myCondominium.myCondominiumFragments.legalDocuments;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;

import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.apt.R;
import br.com.apt.application.Volley;
import br.com.apt.ui.lobby.lobbyFragments.deliveries.DeliveriesListViewAdapter;
import br.com.apt.ui.lobby.lobbyFragments.deliveries.Delivery;
import br.com.apt.ui.myCondominium.myCondominiumFragments.Attachments;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomTextView;
import br.com.apt.widget.CustomTextViewLight;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by adminbs on 8/6/17.
 */

public class LegalDocumentsAdapter extends BaseAdapter {

    private Context context;
    private List<Attachments> attachmentsList;

    public LegalDocumentsAdapter(Context context, List<Attachments> attachmentsList) {
        this.context = context;
        this.attachmentsList = attachmentsList;
    }

    @Override
    public int getCount() {
        return attachmentsList != null ? attachmentsList.size() : 0;
    }

    @Override
    public Attachments getItem(int position) {
        return attachmentsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        LegalDocumentsAdapter.ViewHolder holder;

        if(convertView == null){
            view = LayoutInflater.from(context).inflate(R.layout.row_documents, parent, false);
            holder = new LegalDocumentsAdapter.ViewHolder(view);
            view.setTag(holder);
        }else{
            view = convertView;
            holder = (LegalDocumentsAdapter.ViewHolder) view.getTag();
        }

        bindList(holder, position);
        return view;
    }

    private void bindList(LegalDocumentsAdapter.ViewHolder holder, int position) {
        Attachments item = getItem(position);
        holder.item.setText(item.getTitle());
    }

    public class ViewHolder{
        private CustomTextViewLight item;

        public ViewHolder(View view) {
            item = (CustomTextViewLight) view.findViewById(R.id.item);
        }
    }
}
