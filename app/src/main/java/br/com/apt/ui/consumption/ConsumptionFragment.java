package br.com.apt.ui.consumption;

import android.os.Bundle;
import com.android.volley.Request;
import java.util.ArrayList;
import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.model.user.User;
import br.com.apt.ui.consumption.consumptionsFragments.EnergyFragment;
import br.com.apt.ui.consumption.consumptionsFragments.GasFragment;
import br.com.apt.ui.consumption.consumptionsFragments.WaterFragment;
import br.com.apt.widget.PagerFragment;
import br.com.apt.widget.PagerItemFragment;

public class ConsumptionFragment extends PagerFragment implements Volley.Callback {

    private ConsumptionRequest consumptionRequest;

    public static ConsumptionFragment newInstance() {
        return new ConsumptionFragment();
    }

    @Override
    public String uri() {
        return URI.CONSUMPTIONS;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        User user = getUser();
        if(user != null){
            Volley volley = new Volley(this);
            volley.showDialog(getActivity());
            volley.request(Request.Method.POST, null);
        }
    }

    @Override
    public ArrayList<PagerItemFragment> getPagerItems() {
        ArrayList<PagerItemFragment> pagerItems = new ArrayList<>();

        pagerItems.add(WaterFragment.newInstance(consumptionRequest));
        pagerItems.add(EnergyFragment.newInstance(consumptionRequest));
        pagerItems.add(GasFragment.newInstance(consumptionRequest));

        return pagerItems;
    }

    @Override
    protected int getDrawableSelected() {
        return R.drawable.background_tab_selected_consumption;
    }

    @Override
    public void result(String result, int type) {
        if(result != null){
            consumptionRequest = App.getGson().fromJson(result, ConsumptionRequest.class);
            createPagerView();
        }
    }

    @Override
    protected boolean isCreatePagerView() {
        return false;
    }
}
