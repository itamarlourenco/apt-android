package br.com.apt.ui.MyAdm.myadmFragments.Billet;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.android.volley.Request;
import com.github.ybq.android.spinkit.SpinKitView;

import java.util.List;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.model.user.User;
import br.com.apt.ui.ExceptionWithoutResult;
import br.com.apt.ui.MyAdm.Occurrences;
import br.com.apt.ui.MyAdm.myadmFragments.sac.SolicitationAddActivity;
import br.com.apt.util.Util;

/**
 * Created by adminbs on 7/21/17.
 */

public class BilletFragment extends br.com.apt.widget.PagerItemFragment implements Volley.Callback {

    private SpinKitView loader;
    private ListView listView;
    private User user = getUser();
    private Billet billet;
    private LinearLayout baseList;

    @Override
    protected String getPagerTitle() {
        return App.getContext().getString(R.string.billet);
    }

    @Override
    protected int getPagerIcon() {
        return R.drawable.ico_menu_boleto;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_billits, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if(view != null){
            loader = (SpinKitView) view.findViewById(R.id.loader);
            listView = (ListView) view.findViewById(R.id.listView);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    List<Billet.Items> items = billet.getItems();
                    startActivity(BilletCodeActivity.newIntent(getContext(), items.get(position)));
                }
            });


            view.findViewById(R.id.showQuotas).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(QuotasActivity.newIntent(getContext()));
                    getActivity().overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
                }
            });
            handleButtonQuotas(view);

            baseList = (LinearLayout) view.findViewById(R.id.baseList);
        }
        if (user != null) {
            Volley volley = new Volley(this);
            volley.isMeuADM(true);
            volley.request();
        }
    }

    private void handleButtonQuotas(final View view){
        view.findViewById(R.id.showQuotas).setVisibility(View.GONE);
        Volley volley = new Volley(new Volley.Callback() {
            @Override
            public void result(String result, int type) {
                if (result != null) {
                    QuotasRequest quotasRequest = App.getGson().fromJson(result, QuotasRequest.class);
                    Quotas quotas = quotasRequest.getObject();
                    if(quotas.getUnits() != null && quotas.getUnits().size() > 0){
                        view.findViewById(R.id.showQuotas).setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public String uri() {
                return String.format(URI.FINANCE, user.getCondominiumId());
            }
        });
        volley.isMeuADM(true);
        volley.request();
    }

    @Override
    public void result(String result, int type) {
        View view = getView();
        try{
            Util.hidePlaceholder(view);
            Util.changeVisibility(loader, View.GONE, true);
            Util.changeVisibility(baseList, View.VISIBLE, true);
            BilletRequest billetRequest = App.getGson().fromJson(result, BilletRequest.class);
            billet = billetRequest.getObject();

            if(billet == null || billet.getItems().size() <= 0){
                throw new ExceptionWithoutResult(getString(R.string.message_without_billit), R.drawable.my_adm_billet);
            }
            listView.setAdapter(new BilletAdapter(getContext(), billet));
        }catch (ExceptionWithoutResult exceptionWithoutResult){
            Util.showPlaceholder(view, getActivity(), exceptionWithoutResult.getDrawableId(), exceptionWithoutResult.getMessage());
            if(getView() != null){
                getView().findViewById(R.id.message).setVisibility(View.GONE);
            }
        }catch (Exception exception){
            Util.handlePlaceholderError(view, getActivity());
        }
    }

    @Override
    public String uri() {
        return String.format(URI.BILLET, user.getCondominiumId());
    }
}
