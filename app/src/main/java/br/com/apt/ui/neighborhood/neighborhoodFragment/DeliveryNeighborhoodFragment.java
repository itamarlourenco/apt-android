package br.com.apt.ui.neighborhood.neighborhoodFragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.android.volley.Request;

import java.util.ArrayList;
import java.util.List;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.Volley;
import br.com.apt.model.user.User;
import br.com.apt.ui.neighborhood.neighborhoodFragment.model.RequestNeighborhoodDelivery;
import br.com.apt.ui.neighborhood.neighborhoodFragment.model.SendNeighborhoodDelivery;
import br.com.apt.ui.neighborhood.neighborhoodFragment.adapter.NeighborhoodAdapter;
import br.com.apt.ui.neighborhood.neighborhoodFragment.model.Neighborhood;
import br.com.apt.util.Util;
import br.com.apt.widget.PagerItemFragment;

public class DeliveryNeighborhoodFragment extends NeighborhoodsFragment{

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_neighborhood, container, false);
    }

    @Override
    protected String getPagerTitle() {
        return App.getContext().getString(R.string.neighborhood_delivery);
    }

    @Override
    protected int getPagerIcon() {
        return R.drawable.ic_neighborhood_delivery;
    }

    @Override
    public String getStringService() {
        return "2";
    }

}
