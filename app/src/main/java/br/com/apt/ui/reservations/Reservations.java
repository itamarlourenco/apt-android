package br.com.apt.ui.reservations;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

import br.com.apt.application.SendRequest;
import br.com.apt.ui.reservations.Installations;

public class Reservations extends SendRequest implements Parcelable {

    public static final String TYPE_CREATED = "CREATED";
    public static final String TYPE_NOT_APPROVED = "NOT_APPROVED";
    public static final String TYPE_APPROVED = "APPROVED";

    private String id;
    private String created_user_id;
    private String created_external_user_name;
    private String note;
    private int installation_id;
    private String block_id;
    private String unit_id;
    private String start;
    private String end;
    private String approved;
    private String reason;
    private int canceled;
    private Date created_at;
    private Unit unit;
    private Block block;
    private Installations installation;
    private int read;

    public Reservations() {
    }



    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Installations getInstallation() {
        return installation;
    }

    public void setInstallation(Installations installation) {
        this.installation = installation;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreated_user_id() {
        return created_user_id;
    }

    public void setCreated_user_id(String created_user_id) {
        this.created_user_id = created_user_id;
    }

    public String getCreated_external_user_name() {
        return created_external_user_name;
    }

    public void setCreated_external_user_name(String created_external_user_name) {
        this.created_external_user_name = created_external_user_name;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public int getInstallation_id() {
        return installation_id;
    }

    public void setInstallation_id(int installation_id) {
        this.installation_id = installation_id;
    }

    public String getBlock_id() {
        return block_id;
    }

    public void setBlock_id(String block_id) {
        this.block_id = block_id;
    }

    public String getUnit_id() {
        return unit_id;
    }

    public void setUnit_id(String unit_id) {
        this.unit_id = unit_id;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public String getApproved() {
        return approved;
    }

    public void setApproved(String approved) {
        this.approved = approved;
    }

    public int getCanceled() {
        return canceled;
    }

    public void setCanceled(int canceled) {
        this.canceled = canceled;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public Block getBlock() {
        return block;
    }

    public void setBlock(Block block) {
        this.block = block;
    }

    public int getRead() {
        return read;
    }

    public void setRead(int read) {
        this.read = read;
    }

    public boolean isRead() {
        return (this.read > 0);
    }

    public static Creator<Reservations> getCREATOR() {
        return CREATOR;
    }

    public static class Unit implements Parcelable {
        private String id;
        private String external_id;
        private String name;
        private Date created_at;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getExternal_id() {
            return external_id;
        }

        public void setExternal_id(String external_id) {
            this.external_id = external_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Date getCreated_at() {
            return created_at;
        }

        public void setCreated_at(Date created_at) {
            this.created_at = created_at;
        }

        protected Unit() {

        }

        protected Unit(Parcel in) {
            id = in.readString();
            external_id = in.readString();
            name = in.readString();
            long tmpCreated_at = in.readLong();
            created_at = tmpCreated_at != -1 ? new Date(tmpCreated_at) : null;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(id);
            dest.writeString(external_id);
            dest.writeString(name);
            dest.writeLong(created_at != null ? created_at.getTime() : -1L);
        }

        @SuppressWarnings("unused")
        public static final Parcelable.Creator<Unit> CREATOR = new Parcelable.Creator<Unit>() {
            @Override
            public Unit createFromParcel(Parcel in) {
                return new Unit(in);
            }

            @Override
            public Unit[] newArray(int size) {
                return new Unit[size];
            }
        };
    }

    public static class Block implements Parcelable {
        private long id;
        private String external_id;
        private String name;
        private Date created_at;

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public String getExternal_id() {
            return external_id;
        }

        public void setExternal_id(String external_id) {
            this.external_id = external_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Date getCreated_at() {
            return created_at;
        }

        public void setCreated_at(Date created_at) {
            this.created_at = created_at;
        }

        public Block() {
        }


        protected Block(Parcel in) {
            id = in.readLong();
            external_id = in.readString();
            name = in.readString();
            long tmpCreated_at = in.readLong();
            created_at = tmpCreated_at != -1 ? new Date(tmpCreated_at) : null;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeLong(id);
            dest.writeString(external_id);
            dest.writeString(name);
            dest.writeLong(created_at != null ? created_at.getTime() : -1L);
        }

        @SuppressWarnings("unused")
        public static final Parcelable.Creator<Block> CREATOR = new Parcelable.Creator<Block>() {
            @Override
            public Block createFromParcel(Parcel in) {
                return new Block(in);
            }

            @Override
            public Block[] newArray(int size) {
                return new Block[size];
            }
        };
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.created_user_id);
        dest.writeString(this.created_external_user_name);
        dest.writeString(this.note);
        dest.writeInt(this.installation_id);
        dest.writeString(this.block_id);
        dest.writeString(this.unit_id);
        dest.writeString(this.start);
        dest.writeString(this.end);
        dest.writeString(this.approved);
        dest.writeString(this.reason);
        dest.writeInt(this.canceled);
        dest.writeLong(this.created_at != null ? this.created_at.getTime() : -1);
        dest.writeParcelable(this.unit, flags);
        dest.writeParcelable(this.block, flags);
        dest.writeParcelable(this.installation, flags);
        dest.writeInt(this.read);
    }

    protected Reservations(Parcel in) {
        this.id = in.readString();
        this.created_user_id = in.readString();
        this.created_external_user_name = in.readString();
        this.note = in.readString();
        this.installation_id = in.readInt();
        this.block_id = in.readString();
        this.unit_id = in.readString();
        this.start = in.readString();
        this.end = in.readString();
        this.approved = in.readString();
        this.reason = in.readString();
        this.canceled = in.readInt();
        long tmpCreated_at = in.readLong();
        this.created_at = tmpCreated_at == -1 ? null : new Date(tmpCreated_at);
        this.unit = in.readParcelable(Unit.class.getClassLoader());
        this.block = in.readParcelable(Block.class.getClassLoader());
        this.installation = in.readParcelable(Installations.class.getClassLoader());
        this.read = in.readInt();
    }

    public static final Creator<Reservations> CREATOR = new Creator<Reservations>() {
        @Override
        public Reservations createFromParcel(Parcel source) {
            return new Reservations(source);
        }

        @Override
        public Reservations[] newArray(int size) {
            return new Reservations[size];
        }
    };
}