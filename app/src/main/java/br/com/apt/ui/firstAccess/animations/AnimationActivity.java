package br.com.apt.ui.firstAccess.animations;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import br.com.apt.application.FullScreenBaseActivity;
import br.com.apt.ui.reservations.CalendarFragment;

/**
 * Created by adminbs on 9/12/16.
 */
public class AnimationActivity  extends FullScreenBaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static Intent newIntent(Context context){
        return new Intent(context, AnimationActivity.class);
    }

    @Override
    protected boolean checkLogin() {
        return false;
    }

    @Override
    protected Fragment getFragment() {
        return AnimationFragment.newInstance();
    }
}
