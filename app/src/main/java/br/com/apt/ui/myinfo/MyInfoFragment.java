package br.com.apt.ui.myinfo;

import android.content.Intent;
import android.os.Bundle;

import java.util.ArrayList;

import br.com.apt.R;


import br.com.apt.ui.myinfo.myinfofragment.FragmentMyInfoAdd;
import br.com.apt.ui.myinfo.myinfofragment.employee.EmployeesListFragment;
import br.com.apt.ui.myinfo.myinfofragment.pet.MyPetListFragment;
import br.com.apt.ui.myinfo.myinfofragment.resident.ResidentFragment;
import br.com.apt.ui.myinfo.myinfofragment.UserFragment;
import br.com.apt.ui.myinfo.myinfofragment.vehicle.VehicleListFragment;
import br.com.apt.widget.PagerFragment;
import br.com.apt.widget.PagerItemFragment;

/**
 * Created by fabricio.bezerra on 22/09/2016.
 */
public class MyInfoFragment extends PagerFragment {

    private FragmentMyInfoAdd userFragment = new UserFragment();
    private FragmentMyInfoAdd residentFragment = new ResidentFragment();
    private FragmentMyInfoAdd myPetFragment = new MyPetListFragment();
    private FragmentMyInfoAdd employeesFragment = new EmployeesListFragment();
    private FragmentMyInfoAdd vehicleFragment = new VehicleListFragment();

    public static MyInfoFragment newInstance() {
        return new MyInfoFragment();
    }


    @Override
    public ArrayList<PagerItemFragment> getPagerItems() {
        ArrayList<PagerItemFragment> pagerItems = new ArrayList<>();
        //pagerItems.add(userFragment);
        pagerItems.add(residentFragment);
        pagerItems.add(myPetFragment);
        pagerItems.add(employeesFragment);
        pagerItems.add(vehicleFragment);

        return pagerItems;
    }

    @Override
    protected int getDrawableSelected() {
        return R.drawable.background_tab_selected_my_info;
    }

    public void onActivityResultFragment(int requestCode, int resultCode, Intent data) {
        userFragment.onActivityResultFragment(requestCode, resultCode, data);
        myPetFragment.onActivityResultFragment(requestCode, resultCode, data);
        employeesFragment.onActivityResultFragment(requestCode, resultCode, data);
        vehicleFragment.onActivityResultFragment(requestCode, resultCode, data);
    }

    @Override
    protected boolean blockSwitch() {
        return true;
    }
}
