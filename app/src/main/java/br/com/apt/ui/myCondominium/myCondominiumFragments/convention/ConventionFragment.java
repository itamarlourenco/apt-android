package br.com.apt.ui.myCondominium.myCondominiumFragments.convention;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.model.user.User;
import br.com.apt.ui.myCondominium.myCondominiumFragments.AttachmentRequest;
import br.com.apt.ui.myCondominium.myCondominiumFragments.Attachments;
import br.com.apt.widget.PagerItemFragment;
import br.com.apt.widget.SendSuccessDialog;

/**
 * Created by adminbs on 8/5/17.
 */

public class ConventionFragment extends PagerItemFragment implements Volley.Callback {
    private User user = getUser();

    @Override
    protected String getPagerTitle() {
        return App.getContext().getString(R.string.convention);
    }

    @Override
    protected int getPagerIcon() {
        return R.drawable.ico_convention;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_convention, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Volley volley = new Volley(this);
        volley.showDialog(getActivity());
        volley.isMeuADM(true);
        volley.request();
    }

    @Override
    public void result(String result, int type) {
        try{
            AttachmentRequest attachmentsRequest = App.getGson().fromJson(result, AttachmentRequest.class);
            Attachments attachments = attachmentsRequest.getObject();
            View view = getView();
            if (view != null) {
                WebView webView = (WebView) view.findViewById(R.id.webView);
                webView.getSettings().setJavaScriptEnabled(true);
                webView.loadUrl("https://drive.google.com/viewerng/viewer?embedded=true&url=" + attachments.getUrl());
            }

        }catch (Exception e){
            SendSuccessDialog.show(getString(R.string.error_generic), null, getFragmentManager(), new SendSuccessDialog.Actions() {
                @Override
                public void back() {
                    getActivity().finish();
                }
            }, SendSuccessDialog.TYPE_ERROR);
        }
    }

    @Override
    public String uri() {
        return String.format(URI.CONVENTION, user.getCondominiumId());
    }
}
