package br.com.apt.ui.lobby.lobbyFragments.visits;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TimePicker;

import com.android.volley.Request;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.BaseActivity;
import br.com.apt.application.BaseFragment;
import br.com.apt.application.BaseGson;
import br.com.apt.application.PicturesHelper;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.ui.firstAccess.singup.SingUpPhotoFragment;
import br.com.apt.ui.myinfo.AutoCompleteDialog;
import br.com.apt.ui.myinfo.myinfofragment.AutoComplete;
import br.com.apt.ui.myinfo.myinfofragment.pet.PetTypeRequest;
import br.com.apt.util.Logger;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomButton;
import br.com.apt.widget.CustomEditText;
import br.com.apt.widget.SendSuccessDialog;
import siclo.com.ezphotopicker.api.EZPhotoPick;
import siclo.com.ezphotopicker.api.EZPhotoPickStorage;

/**
 * Created by adminbs on 7/10/17.
 */

public class VisitsScheduleFragment extends BaseFragment implements DatePickerDialog.OnDateSetListener {

    private static ImageView visitsImage;

    private CustomEditText name;
    private CustomEditText dateVisits;
    private CustomEditText timeVisits;
    private DatePickerDialog datePickerDialog;
    private TimePickerDialog timePickerDialog;
    private CustomButton sendVisits;
    private CustomEditText type;
    private static Bitmap imageVisitsBitmap;
    private Date originalDateSelected = new Date();
    private List<AutoComplete> visitsType;
    private int visitTypeId = 0;


    public static VisitsScheduleFragment newInstance(){
        return new VisitsScheduleFragment();
    }

    @Override
    protected String getTitleActivity() {
        return getString(R.string.add_visitor);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_schedule_visits, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if (view != null) {
            visitsImage = (ImageView) view.findViewById(R.id.visitsImage);
            view.findViewById(R.id.canvasAlterImage).setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    alterImage();
                }
            });
            visitsImage.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    alterImage();
                }
            });

            name = (CustomEditText) view.findViewById(R.id.name);
            dateVisits = (CustomEditText) view.findViewById(R.id.dateVisits);
            dateVisits.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if(hasFocus){
                        showDatePickerDialog();
                    }
                }
            });
            dateVisits.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    showDatePickerDialog();
                }
            });

            timeVisits = (CustomEditText) view.findViewById(R.id.timeVisits);
            timeVisits.setOnFocusChangeListener(new View.OnFocusChangeListener(){
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if(hasFocus){
                        showTimePickerDialog();
                    }
                }
            });
            timeVisits.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    showTimePickerDialog();
                }
            });

            sendVisits = (CustomButton) view.findViewById(R.id.sendVisits);
            sendVisits.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    sendVisits();
                }
            });

            type = (CustomEditText) view.findViewById(R.id.type);
            type.setOnFocusChangeListener(new View.OnFocusChangeListener(){
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if(hasFocus) {
                        openDialogVisitsTypes();
                    }
                }
            });
            type.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openDialogVisitsTypes();
                }
            });
        }

        setDatePickerDialog();
        getVisitsType();
    }

    public void getVisitsType(){
        Volley volley = new Volley(new Volley.Callback() {
            @Override
            public void result(String result, int type) {
                if(result != null){
                    VisitTypeRequest visitTypeRequest = App.getGson().fromJson(result, VisitTypeRequest.class);
                    if(visitTypeRequest != null){
                        visitsType = visitTypeRequest.getObject();
                    }
                }
            }
            @Override
            public String uri() {
                return URI.VISITS_TYPE;
            }
        });
        volley.showDialog(getActivity());
        volley.request(Request.Method.GET, null);
    }

    private void openDialogVisitsTypes() {
        AutoCompleteDialog.show(getFragmentManager(), visitsType, new AutoCompleteDialog.Click() {
            @Override
            public void click(AutoComplete autoComplete) {
                visitTypeId = autoComplete.getId();
                type.setText(autoComplete.getName());
            }
        }, getString(R.string.choose_type_visits));
    }

    public void showDatePickerDialog(){
        Util.hideKeyboard(getContext(), getView());
        datePickerDialog.show();
    }

    public void showTimePickerDialog(){
        Util.hideKeyboard(getContext(), getView());
        Calendar calendar = Calendar.getInstance();
        final int hour = calendar.get(Calendar.HOUR_OF_DAY);
        final int minute = calendar.get(Calendar.MINUTE);
        timePickerDialog = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                VisitsScheduleFragment.this.onTimeSet(timePicker, selectedHour, selectedMinute);
            }
        }, hour, minute, true); //Yes 24 hour time
        timePickerDialog.setTitle(getString(R.string.titleTimePickerDialog));
        timePickerDialog.show();
    }

    public void setDatePickerDialog(){
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        datePickerDialog = new DatePickerDialog(getContext(), this, year, month, day);
    }

    public void alterImage(){
        CharSequence[] options = {getString(R.string.takePhoto), getString(R.string.chooseGallery)};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle("")
                .setItems(options, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if(which == 0) {
                            PicturesHelper.Camera(getActivity());
                        } else if(which == 1) {
                            PicturesHelper.Gallery(getActivity());
                        }else{
                            visitsImage.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.placeholder_camera));
                        }
                    }
                });
        builder.create().show();
    }

    public void onActivityResultFragment(int requestCode, int resultCode, Intent data) {
        if(requestCode == EZPhotoPick.PHOTO_PICK_REQUEST_CODE && resultCode != 0){
            try {
                EZPhotoPickStorage ezPhotoPickStorage = new EZPhotoPickStorage(getActivity());
                imageVisitsBitmap = ezPhotoPickStorage.loadLatestStoredPhotoBitmap();

                if(VisitsScheduleFragment.visitsImage != null){
                    VisitsScheduleFragment.visitsImage.setImageBitmap(imageVisitsBitmap);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        String data = String.format("%s/%s/%s", ((dayOfMonth < 10 ? "0" : "") + String.valueOf(dayOfMonth)), ((monthOfYear+1 < 10 ? "0" : "") + String.valueOf(monthOfYear+1)), String.valueOf(year));
        dateVisits.setText(data);

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        calendar.set(Calendar.MONTH, monthOfYear);
        originalDateSelected = calendar.getTime();
    }

    private void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
        String time = String.format("%s:%s", (selectedHour < 10 ? "0" : "") + String.valueOf(selectedHour), (selectedMinute < 10 ? "0" : "") + String.valueOf(selectedMinute));
        timeVisits.setText(time);
    }

    private void sendVisits(){
        try{
            if(TextUtils.isEmpty(name.getText())){
                throw new Exception(getString(R.string.error_name_visits));
            }

            if(TextUtils.isEmpty(dateVisits.getText())){
                throw new Exception(getString(R.string.error_date_visits));
            }

            if(TextUtils.isEmpty(timeVisits.getText())){
                throw new Exception(getString(R.string.error_time_visits));
            }

            HashMap<String, String> hashMap = handleResults(name.getText().toString(), timeVisits.getText().toString());

            File finalFile = Util.createImageFile();
            if(imageVisitsBitmap != null && finalFile != null){
                Util.sendBitmapToFile(finalFile, imageVisitsBitmap);
            }

            Volley volley = new Volley(new Volley.Callback() {
                @Override
                public void result(String result, int type) {
                    if(result != null){
                        BaseGson baseGson = App.getGson().fromJson(result, BaseGson.class);
                        final boolean requestOK = baseGson.getStatus() == Volley.STATUS_OK;
                        if(getActivity() != null) {
                            SendSuccessDialog.show(baseGson.getMessage(), null, getFragmentManager(), new SendSuccessDialog.Actions() {
                                @Override
                                public void back() {
                                    if(requestOK){
                                        getActivity().finish();
                                    }
                                }
                            }, requestOK ? SendSuccessDialog.TYPE_DIALOG : SendSuccessDialog.TYPE_ERROR);
                        }
                    }
                }

                @Override
                public String uri() {
                    return URI.MORE_VISITS;
                }
            });
            volley.showDialog(getActivity());
            volley.request(Request.Method.POST, hashMap, 0, finalFile, "image");
        }catch (Exception e){
            SendSuccessDialog.show(e.getMessage(), null, getFragmentManager(), null, SendSuccessDialog.TYPE_ERROR);
        }
    }

    private HashMap<String, String>  handleResults(String name, String timeVisits){
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("name", name);
        hashMap.put("arrival_date", Util.dateFormatted(originalDateSelected, "yyyy-MM-dd"));
        hashMap.put("arrival_time", timeVisits);
        hashMap.put("visit_type_id", String.valueOf(visitTypeId));

        return hashMap;
    }
}
