package br.com.apt.ui.MyAdm.myadmFragments.sac;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.com.apt.R;
import br.com.apt.application.BaseFragment;
import br.com.apt.ui.MyAdm.myadmFragments.sac.billet.BilletActivity;

/**
 * Created by adminbs on 7/31/17.
 */

public class SolicitationAddFragment  extends BaseFragment {

    private static final String SUBJECT_ID_EMPLOYEES = "e87baede-10ab-11e7-a9ce-b58c8680d4c2";
    private static final String SUBJECT_ID_ASSEMBLY = "e87bb10e-10ab-11e7-8f18-eb7612b5e337";
    private static final String SUBJECT_ID_MAINTENANCE= "14f6bb53-35fe-43db-990f-d7792e15ef66";
    private static final String SUBJECT_DOCUMENTS = "353f485a-9a38-4e09-a7db-0c175226fab8";


    public static SolicitationAddFragment newInstance() {
        return new SolicitationAddFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_add_solicitation, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if(view != null){
            view.findViewById(R.id.occurrencesSubjectsBillet).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    occurrencesSubjectsBillet();
                }
            });
            view.findViewById(R.id.occurrencesSubjectsPersonalDep).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    occurrencesSubjectsPersonalDep();
                }
            });
            view.findViewById(R.id.occurrencesSubjectsAssembly).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    occurrencesSubjectsAssembly();
                }
            });
            view.findViewById(R.id.occurrencesSubjectsMaintenance).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    occurrencesSubjectsMaintenance();
                }
            });
            view.findViewById(R.id.occurrencesSubjectsDocuments).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    occurrencesSubjectsDocuments();
                }
            });
            view.findViewById(R.id.others).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    others();
                }
            });

        }
    }

    private void others() {
        startActivity(OthersOccurrencesActivity.newIntent(getActivity()));
        getActivity().overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
    }

    private void occurrencesSubjectsDocuments() {
        startActivity(MakeSacActivity.newIntent(getContext(), R.drawable.my_adm_personal_department, SolicitationAddFragment.SUBJECT_DOCUMENTS, getString(R.string.my_adm_subjects_documents)));
        getActivity().overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
    }

    private void occurrencesSubjectsMaintenance() {
        startActivity(MakeSacActivity.newIntent(getContext(), R.drawable.my_adm_maintenance, SolicitationAddFragment.SUBJECT_ID_MAINTENANCE, getString(R.string.my_adm_subjects_maintenance)));
        getActivity().overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
    }

    private void occurrencesSubjectsAssembly() {
        startActivity(MakeSacActivity.newIntent(getContext(), R.drawable.my_adm_assembly, SolicitationAddFragment.SUBJECT_ID_ASSEMBLY, getString(R.string.assemly)));
        getActivity().overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
    }

    private void occurrencesSubjectsPersonalDep() {
        startActivity(MakeSacActivity.newIntent(getContext(), R.drawable.my_adm_personal_department, SolicitationAddFragment.SUBJECT_ID_EMPLOYEES, getString(R.string.employee)));
        getActivity().overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
    }

    private void occurrencesSubjectsBillet() {
        startActivity(BilletActivity.newIntent(getContext()));
        getActivity().overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
    }
}
