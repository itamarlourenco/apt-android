package br.com.apt.ui.MyAdm.myadmFragments.sac;

import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

import br.com.apt.ui.MyAdm.Occurrences;

/**
 * Created by adminbs on 8/4/17.
 */

public class History {

    @SerializedName("user")
    public Occurrences.User user;

    @SerializedName("action")
    public String action;

    @SerializedName("by")
    public String by;

    @SerializedName("comment")
    public String comment;

    @SerializedName("title")
    public String title;

    @SerializedName("forward_users")
    public List<Occurrences.User> forward_users;

    @SerializedName("changed_at")
    public Date changedAt;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getBy() {
        return by;
    }

    public void setBy(String by) {
        this.by = by;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Occurrences.User> getForward_users() {
        return forward_users;
    }

    public void setForward_users(List<Occurrences.User> forward_users) {
        this.forward_users = forward_users;
    }

    public Date getChangedAt() {
        return changedAt;
    }

    public void setChangedAt(Date changedAt) {
        this.changedAt = changedAt;
    }

    public Occurrences.User getUser() {
        return user;
    }

    public void setUser(Occurrences.User user) {
        this.user = user;
    }
}
