package br.com.apt.ui.login;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import br.com.apt.application.BaseGson;
import br.com.apt.application.ModelObject;

/**
 * Created by itamarlourenco on 14/01/16.
 */
public class Login extends BaseGson implements ModelObject {
    @SerializedName("username")
    private String userName;
    private String password;
    private String apns;

    public Login(String email, String password, String apns) {
        this.userName = email;
        this.password = password;
        this.apns = apns;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getApns() {
        return apns;
    }

    public void setApns(String apns) {
        this.apns = apns;
    }

    protected Login(Parcel in) {
        userName = in.readString();
        password = in.readString();
        apns = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(userName);
        dest.writeString(password);
        dest.writeString(apns);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Login> CREATOR = new Parcelable.Creator<Login>() {
        @Override
        public Login createFromParcel(Parcel in) {
            return new Login(in);
        }

        @Override
        public Login[] newArray(int size) {
            return new Login[size];
        }
    };
}