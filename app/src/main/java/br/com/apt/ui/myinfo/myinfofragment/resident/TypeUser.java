package br.com.apt.ui.myinfo.myinfofragment.resident;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

/**
 * Created by adminbs on 12/30/17.
 */

public class TypeUser implements Parcelable {
    private int id;
    private String name;
    private Date created_at;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeLong(this.created_at != null ? this.created_at.getTime() : -1);
    }

    public TypeUser() {
    }

    protected TypeUser(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        long tmpCreated_at = in.readLong();
        this.created_at = tmpCreated_at == -1 ? null : new Date(tmpCreated_at);
    }

    public static final Parcelable.Creator<TypeUser> CREATOR = new Parcelable.Creator<TypeUser>() {
        @Override
        public TypeUser createFromParcel(Parcel source) {
            return new TypeUser(source);
        }

        @Override
        public TypeUser[] newArray(int size) {
            return new TypeUser[size];
        }
    };
}
