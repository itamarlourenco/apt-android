package br.com.apt.ui.MyAdm.myadmFragments.Billet;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.List;

import br.com.apt.R;
import br.com.apt.application.BaseFragment;
import br.com.apt.widget.CustomTextView;
import br.com.apt.widget.CustomTextViewLight;

/**
 * Created by adminbs on 8/1/17.
 */

public class BankFragment extends BaseFragment implements View.OnClickListener {

    private LinearLayout itau;
    private LinearLayout santander;
    private LinearLayout bradesco;
    private LinearLayout bancoDoBrasil;
    private LinearLayout caixas;
    private CustomTextViewLight code;


    private BilletDetails billetDetails;

    public static Fragment newInstance(BilletDetails billetDetails) {
        BankFragment bankFragment = new BankFragment();
        bankFragment.setBilletDetails(billetDetails);
        return bankFragment;
    }

    public void setBilletDetails(BilletDetails billetDetails) {
        this.billetDetails = billetDetails;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_banks, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if(view != null){
            itau = (LinearLayout) view.findViewById(R.id.itau);
            itau.setOnClickListener(this);
            santander = (LinearLayout) view.findViewById(R.id.santander);
            santander.setOnClickListener(this);
            bradesco = (LinearLayout) view.findViewById(R.id.bradesco);
            bradesco.setOnClickListener(this);
            bancoDoBrasil = (LinearLayout) view.findViewById(R.id.bancoDoBrasil);
            bancoDoBrasil.setOnClickListener(this);
            caixas = (LinearLayout) view.findViewById(R.id.caixas);
            caixas.setOnClickListener(this);

            code = (CustomTextViewLight) view.findViewById(R.id.code);
            code.setText(billetDetails.getPay_line());
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.itau:
                findUrlByName("Itaú");
                break;
            case R.id.santander:
                findUrlByName("Santander");
                break;
            case R.id.bradesco:
                findUrlByName("Bradesco");
                break;
            case R.id.bancoDoBrasil:
                findUrlByName("Banco do Brasil");
                break;
            case R.id.caixas:
                findUrlByName("Caixa Econônimca Federal");
                break;
        }
    }

    private void findUrlByName(String name) {
        List<BilletDetails.Banks> banks = billetDetails.getBanks();
        for(BilletDetails.Banks bank : banks){
            if(TextUtils.equals(bank.getName(), name)){
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(bank.getUrl()));
                startActivity(intent);
            }
        }
    }
}
