package br.com.apt.ui.firstAccess;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;

import org.w3c.dom.Text;

import br.com.apt.BuildConfig;
import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.BaseFragment;
import br.com.apt.application.BaseGson;
import br.com.apt.application.SendRequest;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.ui.firstAccess.singup.SingUpActivity;
import br.com.apt.ui.notHaveCode.NotHaveCodeActivity;
import br.com.apt.util.Util;
import br.com.apt.widget.CustomEditText;

public class FirstAccessFragment extends BaseFragment implements View.OnClickListener {

    public CustomEditText code;

    public static FirstAccessFragment newInstance() {
        return new FirstAccessFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.first_access_fragment, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if (view != null) {
            view.findViewById(R.id.back).setOnClickListener(this);
            view.findViewById(R.id.notHaveCode).setOnClickListener(this);
            view.findViewById(R.id.submit).setOnClickListener(this);
            code = (CustomEditText) view.findViewById(R.id.code);

            if(BuildConfig.DEBUG){
                code.setText("186447");
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                backActivity();
                break;

            case R.id.notHaveCode:
                notHaveCode();
                break;

            case R.id.submit:
                submit();
                break;
        }
    }

    private void submit() {
        final String stringCode = code.getText().toString();

        if(TextUtils.isEmpty(stringCode)){
            Util.showDialog(getActivity(), R.string.error_first_access);
            return;
        }

        Volley volley = new Volley(new Volley.Callback() {
            @Override
            public void result(String result, int type) {
                if(result != null){
                    BaseGson baseGson = App.getGson().fromJson(result, BaseGson.class);
                    if(baseGson.getStatus() == Volley.STATUS_OK){
//                        startActivity(SingUpActivity.newIntent(getContext(), code));
//                        getActivity().finish();
                    }else{
                        Util.showDialog(getActivity(), baseGson.getMessage());
                    }
                }
            }

            @Override
            public String uri() {
                return URI.CODE;
            }
        });
        CodeSend codeSend = new CodeSend();
        codeSend.setCode(stringCode);
        volley.showDialog(getActivity());
        volley.request(Request.Method.POST, codeSend.toJson());
    }

    private void backActivity() {
        getActivity().finish();
    }

    private void notHaveCode() {
        startActivity(NotHaveCodeActivity.newIntent(getActivity()));
    }

    public class CodeSend extends SendRequest{
        private String code;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }
    }

}
