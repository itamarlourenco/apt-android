package br.com.apt.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Base64;
import android.webkit.URLUtil;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

/**
 * Created by adminbs on 10/26/17.
 */

public class Base64ImageView extends ImageView {
    public Base64ImageView(Context context) {
        super(context);
    }

    public Base64ImageView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public Base64ImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public Base64ImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void handleBase64(String url){
        if(URLUtil.isValidUrl(url)){
            Picasso.with(getContext()).load(url).into(this);
        }
    }
}
