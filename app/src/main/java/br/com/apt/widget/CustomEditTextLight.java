package br.com.apt.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

public class CustomEditTextLight extends EditText {
    public CustomEditTextLight(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFont(context);
    }

    public CustomEditTextLight(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setFont(context);
    }

    public CustomEditTextLight(Context context) {
        super(context);
        setFont(context);
    }

    public void setFont(Context context) {
        this.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/NettoOT-Light.otf"));
    }
}
