package br.com.apt.widget;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by itamarlourenco on 09/07/17.
 */

public class CustomLoadImageView extends ImageView {
    public CustomLoadImageView(Context context) {
        super(context);
    }

    public CustomLoadImageView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomLoadImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public CustomLoadImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }
}
