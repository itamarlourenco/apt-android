package br.com.apt.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.AttributeSet;
import android.util.Base64;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by adminbs on 10/26/17.
 */

public class Base64CircleImageView extends CircleImageView {
    public Base64CircleImageView(Context context) {
        super(context);
    }

    public Base64CircleImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public Base64CircleImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void handleBase64(String base64){
        byte[] decodedString = Base64.decode(base64, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        setImageBitmap(decodedByte);
    }
}
