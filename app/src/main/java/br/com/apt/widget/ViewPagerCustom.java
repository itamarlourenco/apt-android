package br.com.apt.widget;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import com.viewpagerindicator.CirclePageIndicator;

import br.com.apt.R;

public class ViewPagerCustom extends ViewPager implements View.OnClickListener{

    private boolean onSwitch = true;

    public ViewPagerCustom(Context context) {
        super(context);
    }

    public ViewPagerCustom(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setAdapter(PagerAdapter adapter, View view) {
        this.setAdapter(adapter);

        ImageView actionPagerViewLeft = (ImageView) view.findViewById(R.id.actionPagerViewLeft);
        ImageView actionPagerViewRight = (ImageView) view.findViewById(R.id.actionPagerViewRight);
        if(actionPagerViewLeft != null){
            actionPagerViewLeft.setOnClickListener(this);
        }
        if(actionPagerViewRight != null){
            actionPagerViewRight.setOnClickListener(this);
        }

        CirclePageIndicator bannerPageIndicator = (CirclePageIndicator) view.findViewById(R.id.pagerViewIndicator);
        if(bannerPageIndicator != null){
            bannerPageIndicator.setViewPager(this);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.actionPagerViewLeft:
                actionBannerLeft();
                break;
            case R.id.actionPagerViewRight:
                actionBannerRight();
                break;
        }
    }

    private int getItemPosition(int i) {
        return getCurrentItem() + i;
    }

    public void actionBannerLeft(){
        setCurrentItem(getItemPosition(-1), true);
    }

    public void actionBannerRight(){
        setCurrentItem(getItemPosition(+1), true);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        if(onSwitch){
            return super.onInterceptTouchEvent(event);
        }
        return false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if(onSwitch){
            return super.onTouchEvent(event);
        }
        return false;
    }

    public void setOnSwitch(boolean onSwitch) {
        this.onSwitch = onSwitch;
    }


}
