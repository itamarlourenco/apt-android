package br.com.apt.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class CustomTextView extends android.support.v7.widget.AppCompatTextView {

    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFont(context);

    }

    public CustomTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setFont(context);
    }

    public CustomTextView(Context context) {
        super(context);
        setFont(context);
    }

    public void setFont(Context context){
        if(getTypeface() != null){
            if(getTypeface().isItalic()) {
                this.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/NettoOT-Italic.otf"));
            } else if(getTypeface().isBold()) {
                this.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/NettoOT-Bold.otf"));
            } else {
                this.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/NettoOT.ttf"));
            }
        }

    }
}
