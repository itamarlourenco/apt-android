package br.com.apt.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.TextView;

public class CustomTextViewLight extends android.support.v7.widget.AppCompatTextView {
    public CustomTextViewLight(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFont(context);
    }

    public CustomTextViewLight(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setFont(context);
    }

    public CustomTextViewLight(Context context) {
        super(context);
        setFont(context);
    }

    public void setFont(Context context){
        this.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/NettoOT-Light.otf"));
    }
}
