package br.com.apt.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import com.uncopt.android.widget.text.justify.JustifiedTextView;

/**
 * Created by Zarp System on 25/09/2017.
 */

public class CustomJustifiedTextView extends JustifiedTextView {

    public CustomJustifiedTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFont(context);

    }

    public CustomJustifiedTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setFont(context);
    }

    public CustomJustifiedTextView(Context context) {
        super(context);
        setFont(context);
    }

    public void setFont(Context context){
        if(getTypeface() != null){
            if(getTypeface().isItalic()) {
                this.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/NettoOT-Italic.otf"));
            } else if(getTypeface().isBold()) {
                this.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/NettoOT-Bold.otf"));
            } else {
                this.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/NettoOT.ttf"));
            }
        }

    }

}
