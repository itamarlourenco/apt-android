package br.com.apt.widget;

import android.graphics.Bitmap;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;

import br.com.apt.R;
import br.com.apt.application.BaseFragment;

/**
 * Created by itamarlourenco on 10/01/16.
 */
public abstract class PagerItemFragment extends BaseFragment {
    protected abstract String getPagerTitle();
    protected abstract int getPagerIcon();

    protected CustomTextViewLight pageIndicator;

    protected int getIndicator() {
        return 0;
    }

    protected String getImageByWeb(){
        return null;
    }

    protected void onSelected() { }
}
