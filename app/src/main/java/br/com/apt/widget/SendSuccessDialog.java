package br.com.apt.widget;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.ColorRes;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;

import br.com.apt.R;
import br.com.apt.widget.CustomButton;
import br.com.apt.widget.CustomTextView;
import br.com.apt.widget.CustomTextViewBold;

/**
 * Created by  on 09/05/17.
 */

public class SendSuccessDialog extends DialogFragment {
    private static final String FRAG_TAG = "SendSuccess";
    public static final int TYPE_DIALOG = 0;
    public static final int TYPE_ERROR = 1;
    public static final int TYPE_DYNAMIC = 2;
    public static final int TYPE_DYNAMIC_ERROR = 3;
    private ViewHolder viewHolder;
    @ColorRes private int color;
    private String message;
    private String title;
    private int type;
    private SendSuccessDialog.Actions actions;


    public static void show(String title, String message, FragmentManager fragmentManager){
        SendSuccessDialog.show(title, message, fragmentManager, null, SendSuccessDialog.TYPE_DIALOG);
    }

    public static void show(String title, String message, FragmentManager fragmentManager, SendSuccessDialog.Actions actions){
        SendSuccessDialog.show(title, message, fragmentManager, actions, SendSuccessDialog.TYPE_DIALOG);
    }

    public static void show(String title, String message, FragmentManager fragmentManager, SendSuccessDialog.Actions actions, int type){
        if(fragmentManager.findFragmentByTag(FRAG_TAG) == null){
            SendSuccessDialog dialog = new SendSuccessDialog();
            dialog.color = R.color.colorAccent;
            dialog.message = message;
            dialog.title = title;
            dialog.type = type;
            dialog.actions = actions;
            dialog.show(fragmentManager, FRAG_TAG);
        }
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        Window window = getDialog().getWindow();
        if(window != null){
            window.requestFeature(Window.FEATURE_NO_TITLE);
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }

        return inflater.inflate(R.layout.dialog_send_success, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (viewHolder == null){
            viewHolder = new ViewHolder();
        }

        View view = getView();
        if (view != null){
            viewHolder.sendSuccessRoot = (LinearLayout) view.findViewById(R.id.sendSuccessRoot);

            viewHolder.sendSuccessImage = (ImageView) view.findViewById(R.id.sendSuccessImage);

            //viewHolder.sendSuccessImage.setColorFilter(ContextCompat.getColor(getContext(), color));
            viewHolder.sendSuccessTitle = (CustomTextViewLight) view.findViewById(R.id.sendSuccessTitle);
            if (title != null) {
                if (title.isEmpty()) {
                    viewHolder.sendSuccessTitle.setText("");
                } else {
                    viewHolder.sendSuccessTitle.setText(title);
                }
            }

            viewHolder.sendSuccessMessage = (CustomTextViewLight) view.findViewById(R.id.sendSuccessMessage);
            if (message != null) {
                if (message.isEmpty()) {
                    viewHolder.sendSuccessMessage.setText("");
                } else {
                    viewHolder.sendSuccessMessage.setText(message);
                }
            }else{
                viewHolder.sendSuccessMessage.setVisibility(View.GONE);
            }


            viewHolder.sendSuccessConfirm = (CustomButton) view.findViewById(R.id.sendSuccessConfirm);
            viewHolder.sendSuccessConfirm.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    dismiss();
                    if(actions != null){
                        actions.back();
                    }
                }
            });
            //viewHolder.sendSuccessConfirm.setTextColor(ContextCompat.getColor(getContext(), color));

            if(type == SendSuccessDialog.TYPE_ERROR || type == SendSuccessDialog.TYPE_DYNAMIC_ERROR){
                viewHolder.sendSuccessImage.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.popup_error));
                viewHolder.sendSuccessTitle.setTextColor(ContextCompat.getColor(getContext(), R.color.red));
            }


            viewHolder.sendSuccessBack = (CustomButton) view.findViewById(R.id.sendSuccessBack);
            if(type == SendSuccessDialog.TYPE_DYNAMIC || type == SendSuccessDialog.TYPE_DYNAMIC_ERROR){
                viewHolder.sendSuccessBack.setVisibility(View.VISIBLE);
                viewHolder.sendSuccessBack.setOnClickListener(new OnClickListener(){
                    @Override
                    public void onClick(View v) {
                        dismiss();
                    }
                });

            }else{
                viewHolder.sendSuccessBack.setVisibility(View.GONE);
            }

        }
    }

    public interface Actions{
        void back();
    }

    private class ViewHolder{
        public LinearLayout sendSuccessRoot;
        public ImageView sendSuccessImage;
        public CustomTextViewLight sendSuccessTitle;
        public CustomTextViewLight sendSuccessMessage;
        public CustomButton sendSuccessBack;
        public CustomButton sendSuccessConfirm;
    }
}
