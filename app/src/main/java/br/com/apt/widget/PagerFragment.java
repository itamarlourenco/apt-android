package br.com.apt.widget;

import android.app.ActionBar;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.ActionMenuView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.BaseActivity;
import br.com.apt.application.BaseFragment;
import br.com.apt.application.services.PushNotificationLocalBroadcast;
import br.com.apt.util.Logger;

import static br.com.apt.R.id.slidingText;

public abstract class PagerFragment extends BaseFragment {
    private int positionFragment = 0;
    private CustomTextViewLight indicatorPagerItem;
    protected ViewPagerCustom pager;

    public abstract ArrayList<PagerItemFragment> getPagerItems();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.pager_fragment_default, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Bundle arguments = getArguments();
        if(arguments != null){
            positionFragment = arguments.getInt(BaseActivity.POSITION_FRAGMENT);
        }

        if(isCreatePagerView()){
            createPagerView();
        }
        handlePagerIndicator();
    }

    private void handlePagerIndicator() {
        PushNotificationLocalBroadcast.initLocalBroadcast(getContext(), new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                updateIndicator();
            }
        });
    }

    protected void createPagerView(){
        View view = getView();
        if(view != null && getPagerItems() != null){
            pager = (ViewPagerCustom) view.findViewById(getIdViewpager());
            if(blockSwitch()){
                pager.setOnSwitch(false);
            }
            pager.setAdapter(getFragments());
            PagerSlidingTabStrip tabStrip = (PagerSlidingTabStrip) view.findViewById(getIdTabs());
            tabStrip.setViewPager(pager);
            tabStrip.setOnPageChangeListener(getCustomOnPageChangeListener(tabStrip));

            if(positionFragment > 0){
                pager.setCurrentItem(positionFragment);
            }
        } else {
            Logger.d("Please create your Method getPagerItems");
        }
    }

    protected boolean blockSwitch(){
        return false;
    }

    protected CustomOnPageChangeListener getCustomOnPageChangeListener(PagerSlidingTabStrip tabStrip){
        return new CustomOnPageChangeListener(tabStrip);
    }

    protected PagerAdapter getFragments(){
        return new PagerAdapter(getChildFragmentManager());
    }

    protected int getIdViewpager() {
        return R.id.viewPager;
    }

    protected int getIdTabs() {
        return R.id.tabs;
    }

    protected abstract int getDrawableSelected();

    public View getPageViewCustom(int position) {
        PagerItemFragment pagerItem = getPagerItems().get(position);
        try {
            View view = LayoutInflater.from(getActivity()).inflate(R.layout.page_sliding, null);

            ImageView imageView = (ImageView) view.findViewById(R.id.slidingIcon);
            if(pagerItem.getPagerIcon() != 0){
                imageView.setImageResource(pagerItem.getPagerIcon());
                imageView.setColorFilter(ContextCompat.getColor(getContext(), R.color.gray_menu));
            }else{
                if(!TextUtils.isEmpty(pagerItem.getImageByWeb())){
                    Picasso.with(getContext()).load(pagerItem.getImageByWeb()).into(imageView);
                }
            }


            CustomTextViewLight slidingText = (CustomTextViewLight) view.findViewById(R.id.slidingText);
            slidingText.setText(pagerItem.getPagerTitle().toUpperCase());

            pagerItem.pageIndicator = (CustomTextViewLight) view.findViewById(R.id.indicatorPagerItem);
            if(pagerItem.getIndicator() > 0){
                pagerItem.pageIndicator.setText(String.valueOf(pagerItem.getIndicator()));
                pagerItem.pageIndicator.setVisibility(View.VISIBLE);
            }else{
                pagerItem.pageIndicator.setVisibility(View.INVISIBLE);
            }

            View itemPager = view.findViewById(R.id.itemPager);
            itemPager.setBackgroundColor(Color.parseColor("#46463f"));

            if(getPagerItems().size() > 3){
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) itemPager.getLayoutParams();
                WindowManager wm = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
                Display display = wm.getDefaultDisplay();
                params.width = (int) (display.getWidth() / 3.5);
            }

            return view;

        } catch (IndexOutOfBoundsException e) {
            Logger.d(e.getMessage());
        }
        return null;
    }

    public void updateIndicator(){
            for (final PagerItemFragment pagerItem:getPagerItems()) {
                if(pagerItem.getIndicator() > 0){
                    try {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                pagerItem.pageIndicator.setText(String.valueOf(pagerItem.getIndicator()));
                                pagerItem.pageIndicator.setVisibility(View.VISIBLE);
                            }
                        });
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                }else{
                    try{
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                pagerItem.pageIndicator.setVisibility(View.INVISIBLE);
                                 }
                        });
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }

    }


    private class CustomOnPageChangeListener implements ViewPager.OnPageChangeListener{
        private PagerSlidingTabStrip tabStrip;
        private int previousPage = 0;

        public CustomOnPageChangeListener(PagerSlidingTabStrip tabStrip){
            this.tabStrip = tabStrip;
            try{
                View view = ((LinearLayout)tabStrip.getChildAt(0)).getChildAt(previousPage);
                view.findViewById(R.id.itemPager).setBackgroundResource(getDrawableSelected());
                ImageView imageView = (ImageView) view.findViewById(R.id.slidingIcon);
                imageView.setColorFilter(ContextCompat.getColor(getContext(), R.color.blue_menu));
                ((CustomTextViewLight) view.findViewById(R.id.slidingText)).setTextColor(ContextCompat.getColor(getContext(), R.color.gray_text_menu));
            }catch (NullPointerException e){
                Logger.e(e.getMessage());
            }
        }

        @Override
        public void onPageScrolled(int i, float v, int i2) {}

        @Override
        public void onPageSelected(int i) {
            try{
                positionFragment = i;
                View view = ((LinearLayout)tabStrip.getChildAt(0)).getChildAt(i);
                View viewPrevious = ((LinearLayout)tabStrip.getChildAt(0)).getChildAt(previousPage);

                viewPrevious.findViewById(R.id.itemPager).setBackgroundColor(Color.parseColor("#46463f"));
                view.findViewById(R.id.itemPager).setBackgroundResource(getDrawableSelected());

                ImageView imageViewPrevious = (ImageView) viewPrevious.findViewById(R.id.slidingIcon);
                imageViewPrevious.setColorFilter(ContextCompat.getColor(getContext(), R.color.gray_menu));
                ((CustomTextViewLight) viewPrevious.findViewById(R.id.slidingText)).setTextColor(ContextCompat.getColor(getContext(), R.color.gray_text_menu_hover));

                ImageView imageView = (ImageView) view.findViewById(R.id.slidingIcon);
                imageView.setColorFilter(ContextCompat.getColor(getContext(), R.color.blue_menu));
                ((CustomTextViewLight) view.findViewById(R.id.slidingText)).setTextColor(ContextCompat.getColor(getContext(), R.color.gray_text_menu));

                getPagerItems().get(i).onSelected();

            }catch (NullPointerException e){
                Logger.d(e.getMessage());
            }
            previousPage=i;
        }

        @Override
        public void onPageScrollStateChanged(int i) {}
    }

    private class PagerAdapter extends FragmentPagerAdapter implements PagerSlidingTabStrip.ViewTabProvider{
        private ArrayList<PagerItemFragment> pagerItems;
        public PagerAdapter(FragmentManager fm) {
            super(fm);
            pagerItems = getPagerItems();
        }

        @Override
        public Fragment getItem(int position) {
            return pagerItems.get(position);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return pagerItems.get(position).getPagerTitle();
        }

        @Override
        public int getCount() {
            return pagerItems == null ? 0 : pagerItems.size();
        }

        @Override
        public View getPageView(int position) {
            return getPageViewCustom(position);
        }
    }

    protected boolean isCreatePagerView(){
        return true;
    }

    public int getPositionFragment() {
        return positionFragment;
    }

    public static PagerFragment  instanceWithPosition(PagerFragment pagerFragment, int position){
        Bundle bundle = new Bundle();
        bundle.putInt(BaseActivity.POSITION_FRAGMENT, position);
        pagerFragment.setArguments(bundle);

        return pagerFragment;
    }

    public CustomTextViewLight getIndicatorPagerItem() {
        return indicatorPagerItem;
    }

    public void setIndicatorPagerItem(String total) {
        this.indicatorPagerItem.setText(total);
    }
}
