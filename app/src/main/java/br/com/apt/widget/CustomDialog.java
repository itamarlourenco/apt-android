package br.com.apt.widget;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import br.com.apt.R;
import br.com.apt.application.Config;
import br.com.apt.util.Util;

/**
 * Created by adminbs on 7/12/17.
 */

public class CustomDialog extends DialogFragment {
    public static final int TYPE_INFORMATION = 0;

    private static final String FRAG_TAG = "customDialogFragment";

    private String message;

    public void setMessage(String message){
        this.message = message;
    }

    public String getMessage(){
        return message;
    }

    public static void show(FragmentManager fragmentManager) {
        if (fragmentManager.findFragmentByTag(FRAG_TAG) == null) {
            CustomDialog fragment = new CustomDialog();
            fragment.show(fragmentManager, FRAG_TAG);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_custom_dialog, null);
        CustomButton closeDialog = (CustomButton) view.findViewById(R.id.closeDialog);

        final WebView webView = (WebView) view.findViewById(R.id.webView);

        final ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(Config.About.URL);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                Util.changeVisibility(progressBar, View.GONE, false);
                Util.changeVisibility(webView, View.VISIBLE, true);
            }
        });
        return view;
    }

}
