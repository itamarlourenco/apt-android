package br.com.apt.widget;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import br.com.apt.util.Logger;

/**
 * Created by adminbs on 9/7/17.
 */

public class ListViewRangeCustom extends ListView {
    public ListViewRangeCustom(Context context) {
        super(context);
        init();
    }

    public ListViewRangeCustom(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ListViewRangeCustom(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public ListViewRangeCustom(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    public void init(){

    }
}
