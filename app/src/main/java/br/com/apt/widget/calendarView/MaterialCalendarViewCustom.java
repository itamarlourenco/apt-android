package br.com.apt.widget.calendarView;

import android.content.Context;
import android.util.AttributeSet;

import com.prolificinteractive.materialcalendarview.MaterialCalendarView;

/**
 * Created by itamarlourenco on 25/01/16.
 */
public class MaterialCalendarViewCustom extends MaterialCalendarView {
    public MaterialCalendarViewCustom(Context context) {
        super(context);
    }

    public MaterialCalendarViewCustom(Context context, AttributeSet attrs) {
        super(context, attrs);
    }


}
