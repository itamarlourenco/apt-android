package br.com.apt.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

public class CustomEditText extends EditText {
    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFont(context);
    }

    public CustomEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setFont(context);
    }

    public CustomEditText(Context context) {
        super(context);
        setFont(context);
    }

    public void setFont(Context context) {
        this.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/NettoOT-Light.otf"));

//        if(getTypeface() != null){
//            if(getTypeface().isItalic()) {
//                this.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/NettoOT-Italic.otf"));
//            } else if(getTypeface().isBold()) {
//                this.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/NettoOT-Bold.otf"));
//            } else {
//                this.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/NettoOT.ttf"));
//            }
//        }
    }


}
