package br.com.apt.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

public class CustomEditTextV2 extends EditText {
    public CustomEditTextV2(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomEditTextV2(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public CustomEditTextV2(Context context) {
        super(context);
    }
}
