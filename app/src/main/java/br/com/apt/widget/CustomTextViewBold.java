package br.com.apt.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by itamarlourenco on 14/01/16.
 */
public class CustomTextViewBold extends TextView {
    public CustomTextViewBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFont(context);
    }

    public CustomTextViewBold(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setFont(context);
    }

    public CustomTextViewBold(Context context) {
        super(context);
        setFont(context);
    }

    public void setFont(Context context){
        this.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/NettoOT-Bold.otf"));
    }
}
