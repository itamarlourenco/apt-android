package br.com.apt.widget;

import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import br.com.apt.R;

/**
 * Created by itamarlourenco on 16/01/16.
 */
public class ViewHolderLoaderImageView {
    public ImageView mainImageView;
    public ProgressBar imageViewProgressBar;

    public ViewHolderLoaderImageView(View view) {
        mainImageView = (ImageView) view.findViewById(R.id.mainImageView);
        imageViewProgressBar = (ProgressBar) view.findViewById(R.id.imageViewProgressBar);
    }
}
