package br.com.apt.application;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import br.com.apt.ui.reservations.CalendarFragment;

/**
 * Created by adminbs on 9/3/17.
 */

public class OpenURL extends BaseActivity {

    private static final String EXTRA_URL = "EXTRA_URL";

    public static Intent newIntent(Context context, String url) {
        Intent intent = new Intent(context, OpenURL.class);
        intent.putExtra(EXTRA_URL, url);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public OpenURLFragment getFragment(){
        Intent intent = getIntent();
        if(intent != null){
            String url = intent.getStringExtra(EXTRA_URL);
            return OpenURLFragment.newInstance(url);
        }
        return null;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}