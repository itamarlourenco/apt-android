package br.com.apt.application.notification;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import br.com.apt.ui.myapt.MyAptActivity;

/**
 * Created by itamarlourenco on 21/01/16.
 */
public class NotificationChat extends Notification {
    public NotificationChat(Context context, Bundle data) {
        super(context, data);
    }

    @Override
    protected Intent getIntent() {
        return MyAptActivity.newIntent(context);
    }
}
