package br.com.apt.application;

import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

import br.com.apt.R;
import br.com.apt.util.Logger;
import br.com.apt.util.ZoomImageDialog;

/**
 * Created by verificar on 1/12/17.
 */

public class ZoomImageView extends ImageView {

    public static final int APP_AREA_APT = 0;
    public static final int APP_AREA_LOBBY = 1;
    private boolean showZoom = true;

    private int appArea;

    public ZoomImageView(Context context) {
        super(context);
        zoomImage(context);
    }

    public ZoomImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        zoomImage(context);
    }

    public void setShowZoom(boolean showZoom) {
        this.showZoom = showZoom;
    }

    public ZoomImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        zoomImage(context);
    }

    public void zoomImage(final Context context){
        setOnClickListener(new OnClickListener(){
            @Override
            public void onClick(View view) {
                if(!showZoom) return;
                try {
                    android.support.v4.app.FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
                    BitmapDrawable drawable = (BitmapDrawable) getDrawable();
                    Drawable drawablePlaceholder = ContextCompat.getDrawable(context, R.drawable.placeholder_camera);

                    if(drawable != null){
                        if(drawable.getConstantState() == drawablePlaceholder.getConstantState()){
                            return;
                        }

                        Bitmap bitmap = ((BitmapDrawable) getDrawable()).getBitmap();
                        ZoomImageDialog.show(fragmentManager, bitmap, appArea);
                    }
                } catch (ClassCastException e) {
                    Logger.d(e.getMessage());
                }
            }
        });
    }

    public int getAppArea() {
        return appArea;
    }

    public void setAppArea(int appArea) {
        this.appArea = appArea;
    }


}
