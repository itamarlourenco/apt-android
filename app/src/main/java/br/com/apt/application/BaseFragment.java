package br.com.apt.application;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;

import br.com.apt.R;
import br.com.apt.model.user.User;
import br.com.apt.model.user.UserData;
import br.com.apt.util.Logger;
import br.com.apt.widget.CustomTextViewLight;

public class BaseFragment extends Fragment {

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        updateTitle();
    }

    public void updateTitle(){
        if(TextUtils.isEmpty(getTitleActivity())){
            Activity activity = getActivity();
            if(activity != null){
                CharSequence title = activity.getTitle();
                if(title != null){
                    setTitleActivity(title.toString().toUpperCase());
                }
            }

        }else{
            setTitleActivity(getTitleActivity().toUpperCase());
        }
    }


    public User getUser(){
        return UserData.getUser();
    }

    protected LayoutInflater getLayoutInflater() {
        return LayoutInflater.from(getActivity());
    }

    public static void handleDeliveriesFragments(int id, BaseFragment baseFragment, FragmentActivity activity) {
        handleDeliveriesFragments(id, baseFragment, activity, 0, 0);
    }
    public static void handleDeliveriesFragments(int id, BaseFragment baseFragment, FragmentActivity fragmentActivity, int firstAnimation, int secondAnimation) {
        if(fragmentActivity != null && baseFragment != null){
            FragmentTransaction fragmentTransaction = fragmentActivity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(firstAnimation, secondAnimation);
            fragmentTransaction.replace(id, baseFragment);
            fragmentTransaction.commit();
        }
    }

    public CustomTextViewLight getCustomTextViewTitle(){
        BaseActivity baseActivity = (BaseActivity) getActivity();
        if(baseActivity != null){
            Toolbar toolbar = baseActivity.getToolBar();
            if(toolbar != null){
                return (CustomTextViewLight) toolbar.findViewById(R.id.toolbar_title);
            }
        }
        return null;
    }

    private String setTitleActivity(String title){
        CustomTextViewLight customTitle = getCustomTextViewTitle();
        if(customTitle != null){
            customTitle.setText(title);
        }
        return "";
    }

    protected String getTitleActivity(){
        return "";
    }
}
