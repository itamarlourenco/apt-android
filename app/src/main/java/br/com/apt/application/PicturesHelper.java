package br.com.apt.application;

import android.app.Activity;

import siclo.com.ezphotopicker.api.EZPhotoPick;
import siclo.com.ezphotopicker.api.models.EZPhotoPickConfig;
import siclo.com.ezphotopicker.api.models.PhotoSource;

/**
 * Created by adminbs on 1/19/17.
 */

public class PicturesHelper {
    public static void Camera(Activity activity){
        EZPhotoPickConfig config = new EZPhotoPickConfig();
        config.photoSource = PhotoSource.CAMERA;
        config.needToExportThumbnail = true;
        config.exportingThumbSize = 600;
        config.exportingSize = 1000;
        EZPhotoPick.startPhotoPickActivity(activity, config);
    }

    public static void Gallery(Activity activity){
        EZPhotoPickConfig config = new EZPhotoPickConfig();
        config.photoSource = PhotoSource.GALERY;
        config.needToExportThumbnail = true;
        config.exportingThumbSize = 600;
        config.exportingSize = 1000;
        EZPhotoPick.startPhotoPickActivity(activity, config);
    }
}
