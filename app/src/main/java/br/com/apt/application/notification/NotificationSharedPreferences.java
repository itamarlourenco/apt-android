package br.com.apt.application.notification;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Observable;

import br.com.apt.application.App;
import br.com.apt.ui.main.Counter;
import br.com.apt.util.Logger;

/**
 * Created by itamarlourenco on 21/01/16.
 */
public class NotificationSharedPreferences extends Observable {
    public static int totalVisits = 0;
    public static int totalDelivery = 0;

    public static int totalPoll = 0;
    public static int totalNotices = 0;


    public void setCountNotices(int total) {
        NotificationSharedPreferences.totalNotices = total;
        setChanged();
        notifyObservers();
    }

    public static int getCountNotices() {
        return NotificationSharedPreferences.totalNotices;
    }

    public void setCountPoll(int total) {
        NotificationSharedPreferences.totalPoll = total;
        setChanged();
        notifyObservers();
    }

    public static int getCountPoll() {
        return NotificationSharedPreferences.totalPoll;
    }


    public void setCountDelivery(int total) {
        NotificationSharedPreferences.totalDelivery = total;
        setChanged();
        notifyObservers();
    }

    public static int getCountDelivery() {
        return NotificationSharedPreferences.totalDelivery;
    }

    public void setCountVisit(int total) {
        try{
            NotificationSharedPreferences.totalVisits = total;
            setChanged();
            notifyObservers();
        }catch (Exception e){
            e.printStackTrace();
//            Logger.e(e.getMessage());
        }
    }

    public static int getCountVisit() {
        return NotificationSharedPreferences.totalVisits;
    }



    public static int getTotalLobby() {
        return getCountVisit() + getCountDelivery();
    }

    public static int getTotalMyApt() {
        return getCountNotices() + getCountPoll();
    }


    public static String handleNotification(String myapt, String reception){
        try {
            JSONObject myaptJson = new JSONObject(myapt);
            JSONObject receptionJson = new JSONObject(reception);

            JSONObject all = new JSONObject();
            all.put("myapt", myaptJson);
            all.put("reception", receptionJson);

            return all.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }


    public static void updateNotification(Counter counter){
        try {
            if (counter != null) {
                Counter.Reception reception = counter.getReception();
                if (reception != null) {
                    App.getNotificationSharedPreferences().setCountVisit(reception.getVisits());
                    App.getNotificationSharedPreferences().setCountDelivery(reception.getDeliveries());
                }


                Counter.MyApt myapt = counter.getMyapt();
                if (myapt != null) {
                    App.getNotificationSharedPreferences().setCountPoll(myapt.getPolls());
                    App.getNotificationSharedPreferences().setCountNotices(myapt.getNotices());
                }

            }
        }catch (Exception e){
            Logger.d(e.getMessage());
        }
    }
}
