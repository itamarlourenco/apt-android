package br.com.apt.application;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.android.volley.Request;
import com.github.ybq.android.spinkit.SpinKitView;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.File;
import java.util.List;

import br.com.apt.BuildConfig;
import br.com.apt.R;
import br.com.apt.model.user.User;
import br.com.apt.model.user.UserData;
import br.com.apt.model.user.UserMeuadm;
import br.com.apt.ui.firstAccess.singup.DialogTermsOfUse;
import br.com.apt.ui.login.LoginActivity;
import br.com.apt.ui.main.MainActivity;
import br.com.apt.ui.main.navigationDrawer.NavigationDrawerMenuAdapter;
import br.com.apt.ui.myCondominium.MyCondominiumActivity;
import br.com.apt.ui.myinfo.MyInfoActivity;
import br.com.apt.util.Logger;
import br.com.apt.util.Util;
import br.com.apt.widget.Base64ImageView;
import br.com.apt.widget.CustomButton;
import br.com.apt.widget.CustomTextViewLight;
import siclo.com.ezphotopicker.api.EZPhotoPick;
import siclo.com.ezphotopicker.api.EZPhotoPickStorage;

public class BaseActivity extends AppCompatActivity implements NavigationDrawerMenuAdapter.OnClickItemMenu{
    public static final String POSITION_FRAGMENT = "POSITION_FRAGMENT";

    private Toolbar toolBar;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle drawerToggle;
    private int positionFragment = 0;
    private CustomTextViewLight menuBuilding;
    private CustomTextViewLight menuApartment;
    private Base64ImageView profileImage;
    private SpinKitView profileLoader;
    private boolean photoByHere = false;

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        if(intent != null){
            positionFragment = intent.getIntExtra(BaseActivity.POSITION_FRAGMENT, 0);
        }

        if(checkLogin()){
            User user = getUser();
            if(user == null){
                Logger.t(getString(R.string.error_authenticate));
                startActivity(LoginActivity.newIntent(this));
                finish();
            }
        }

        setContentView(getIdLayoutActivity());

        if (savedInstanceState == null) {
            handleFragment();
            if (showToolbar()) {
                handleToolbar();
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    Window window = getWindow();
                    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                    window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                    window.setStatusBarColor(ContextCompat.getColor(this, R.color.status_bar_color));
                }
            }
            if(showNavigationDrawer()){
                handleNavigationDrawer();
            }
        }
    }

    @android.support.annotation.RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void handleNavigationDrawer() {
        if(toolBar != null){
            toolBar.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
            drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolBar, R.string.drawer_open, R.string.drawer_close){
                public void onDrawerClosed(View view) {
                    super.onDrawerClosed(view);
                    invalidateOptionsMenu();
                }

                public void onDrawerOpened(View drawerView) {
                    super.onDrawerOpened(drawerView);
                    invalidateOptionsMenu();
                }
            };
            drawerLayout.setDrawerListener(drawerToggle);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                drawerToggle.getDrawerArrowDrawable().setColor(getColor(R.color.drawer_toggle_color));
            } else {
                drawerToggle.getDrawerArrowDrawable().setColor(getResources().getColor(R.color.drawer_toggle_color));
            }

            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.setDisplayHomeAsUpEnabled(true);
                actionBar.setHomeButtonEnabled(true);
                actionBar.setDisplayShowTitleEnabled(false);
            }

            toolBar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    closeDrawable();
                }
            });

            //handleItemsNavigationDrawer();
            handleDrawerMenu();
        }
    }

    private void handleDrawerMenu() {
        ImageView closeMenu = (ImageView) findViewById(R.id.closeMenu);
        if(closeMenu != null){
            closeMenu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    closeDrawable();
                }
            });
        }

        profileImage = (Base64ImageView) findViewById(R.id.profileImage);
        profileLoader = (SpinKitView) findViewById(R.id.profileLoader);
        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickPictureOrPickFromGallery();
            }
        });

        User user = getUser();
        if(user == null) return;


        Picasso.with(this).load(Volley.getUrlByImage(user.getImage())).placeholder(R.drawable.placeholder_camera).into(profileImage);

        CustomButton btnLogoff = (CustomButton) findViewById(R.id.btnLogoff);
        if(btnLogoff != null){
            btnLogoff.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    outUser();
                    closeDrawable();
                }
            });
        }

        CustomButton terms = (CustomButton) findViewById(R.id.terms);
        if(terms != null){
            terms.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DialogTermsOfUse.show(getSupportFragmentManager());
                }
            });
        }

        CustomButton btnMyInfo = (CustomButton) findViewById(R.id.btnMyInfo);
        if(btnMyInfo != null){
            btnMyInfo.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    startActivity(MyInfoActivity.newIntent(getBaseContext()));
                    closeDrawable();
                }
            });
        }

        CustomButton myCondominium = (CustomButton) findViewById(R.id.myCondominium);
        if(myCondominium != null){
            myCondominium.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(MyCondominiumActivity.newIntent(getBaseContext()));
                    closeDrawable();
                }
            });
        }
        if(App.isApt())
            myCondominium.setVisibility(View.VISIBLE);

        CustomButton tradeUnits = (CustomButton) findViewById(R.id.tradeUnits);
        if(tradeUnits != null){
            tradeUnits.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showSelectUnits();
                }
            });

            if(getUser().getUnits() == null || getUser().getUnits().size() <= 1){
                tradeUnits.setVisibility(View.GONE);
            }
        }

        CustomTextViewLight version = (CustomTextViewLight) findViewById(R.id.version);
        version.setText(BuildConfig.VERSION_NAME + " - " + BuildConfig.VERSION_CODE);

//        CustomButton btoAbout = (CustomButton) findViewById(R.id.btoAbout);
//        if(btoAbout != null){
//            btoAbout.setOnClickListener(new View.OnClickListener(){
//                @Override
//                public void onClick(View v) {
//                    startActivity(AboutActivity.newIntent(getBaseContext()));
//                    closeDrawable();
//                }
//            });
//        }
//
//        CustomButton btoRegulation = (CustomButton) findViewById(R.id.regulation);
//        if(btoRegulation != null){
//            btoRegulation.setOnClickListener(new View.OnClickListener(){
//                @Override
//                public void onClick(View v) {
//                    startActivity(RegulationActivity.newIntent(getBaseContext()));
//                    closeDrawable();
//                }
//            });
//        }




        CustomTextViewLight menuName = (CustomTextViewLight) findViewById(R.id.menuName);
        if(menuName != null){
            menuName.setText(user.getName());
        }

        menuApartment = (CustomTextViewLight) findViewById(R.id.menuApartment);
        if(menuApartment != null){
            menuApartment.setText(getString(R.string.apartment) + " " + user.getApto());
        }


        menuBuilding = (CustomTextViewLight) findViewById(R.id.menuBuilding);
        if(menuBuilding != null){
            menuBuilding.setText(user.getBuildingName());
        }

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if(drawerToggle != null){
            drawerToggle.syncState();
        }
    }

    public void showSelectUnits() {
        showSelectUnits(false);
    }

    public void showSelectUnits(final boolean init) {
        User user = getUser();
        if(init && !TextUtils.isEmpty(user.getApto())){
            return;
        }

        final List<UserMeuadm.Units> unitsUser = user.getUnits();
        if(unitsUser == null){
            return;
        }

        if(unitsUser.size() <= 1){
            return;
        }

        if(unitsUser.size() > 0){
            try {
                String[] listUnits = new String[unitsUser.size()];
                int i=0;
                for(UserMeuadm.Units units: user.getUnits()){
                    listUnits[i] = units.getAddress().getBlocoNome().trim() + " - " + units.getBloco() + " - " + units.getUnidade();
                    i++;
                }

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(Html.fromHtml(getString(R.string.select_unit)))
                        .setItems(listUnits, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                final UserMeuadm.Units units = unitsUser.get(which);
                                Volley volley = new Volley(new Volley.Callback() {
                                    @Override
                                    public void result(String result, int type) {
                                        menuApartment.setText(getString(R.string.apartment) + " " + units.getUnidade());
                                        User user = getUser();
                                        user.setApto(units.getUnidade());
                                        user.setCurrentBlock(units.getBloco());
                                        UserData.clear();
                                        UserData.saveUser(user);
                                        startActivity(MainActivity.newIntent(getBaseContext()));
                                        finish();
                                    }

                                    @Override
                                    public String uri() {
                                        return URI.UNIT;
                                    }
                                });
                                volley.isMeuADM(true);
                                volley.showDialog(BaseActivity.this);
                                volley.request(Request.Method.PUT, App.getGson().toJson(units));

                            }
                        });
                if(init) {
                    builder.setCancelable(false);
                }
                builder.create().show();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if(drawerToggle != null) {
            drawerToggle.onConfigurationChanged(newConfig);
        }
    }

    private void handleToolbar() {
        toolBar = (Toolbar) findViewById(R.id.toolbar);
        if (toolBar != null) {
            setSupportActionBar(toolBar);
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.setDisplayShowTitleEnabled(false);
            }
            if(showToolbarLogo()){
                toolBar.findViewById(R.id.toolbar_title).setVisibility(View.GONE);
                toolBar.findViewById(R.id.toolbar_back).setVisibility(View.GONE);
                toolBar.findViewById(R.id.toolbar_logo).setVisibility(View.VISIBLE);
            }else{
                CustomTextViewLight customTextView = (CustomTextViewLight) toolBar.findViewById(R.id.toolbar_title);
                customTextView.setVisibility(View.VISIBLE);
                customTextView.setText(getTitle().toString().toUpperCase());
                customTextView.setTextColor(ContextCompat.getColor(this, getTitleToolbarColor()));

                toolBar.findViewById(R.id.toolbar_logo).setVisibility(View.GONE);
                ImageView toolBarBack = (ImageView) toolBar.findViewById(R.id.toolbar_back);
                toolBarBack.setVisibility(View.VISIBLE);
                toolBarBack.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onBackPressed();
                    }
                });
            }
        }
    }

    public void setTitleToolBar(String title) {
        if (toolBar != null) {
            toolBar.setTitle(title);
        }
    }

    public Toolbar getToolBar() {
        return toolBar;
    }

    protected boolean showToolbar() {
        return true;
    }

    protected Fragment getFragment(){
        return null;
    }

    protected boolean showNavigationDrawer() {
        return true;
    }

    private void handleFragment() {
        handleFragment(getFragment());
    }

    public void handleFragment(Fragment fragment) {
        if(fragment != null){
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(getIdFragmentContainer(), getFragment());
            fragmentTransaction.commit();
        }
    }

    public void alterFragment(Fragment fragment) {
        alterFragment(fragment, true);
    }
    public void alterFragment(Fragment fragment, boolean enter) {
        if(fragment != null){
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            if(enter){
                fragmentTransaction.setCustomAnimations(R.anim.slide_out_left, R.anim.slide_in_right);
            }else{
                fragmentTransaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
            }
            fragmentTransaction.replace(getIdFragmentContainer(), fragment);
            fragmentTransaction.commit();
        }
    }

    protected int getIdFragmentContainer(){
        return R.id.fragmentContainer;
    }

    protected int getIdLayoutActivity(){
        return R.layout.activity_base;
    }

    protected DrawerLayout getDrawerLayout(){
        return drawerLayout;
    }

    private void handleItemsNavigationDrawer() {
//        RecyclerView recycleViewNavigationDrawer = (RecyclerView) findViewById(R.id.recycleViewNavigationDrawer);
//        if(recycleViewNavigationDrawer != null){
//            recycleViewNavigationDrawer.setLayoutManager(new LinearLayoutManager(this));
//            recycleViewNavigationDrawer.setAdapter(new NavigationDrawerMenuAdapter(this, this));
//            recycleViewNavigationDrawer.setNestedScrollingEnabled(false);
//        }
    }

    @Override
    public void clickClose(View view, int position) {
        switch (view.getId()){
            case R.id.closeDrawer:
                closeDrawable();
                break;

            case R.id.baseItem:
                openNavItem(position);
                finish();
                break;

            case R.id.out:
                outUser();
                break;
        }
    }

    private void outUser() {
        UserData.clear();
        startActivity(LoginActivity.newIntent(this));
        finish();
    }

    private void openNavItem(int position) {
        if(App.getItem(position) != null){
            MainActivity.openActivity(this, App.getItem(position));
        }
    }

    private void closeDrawable() {
        if(drawerLayout != null){
            if (drawerLayout.isDrawerOpen(GravityCompat.END)) {
                drawerLayout.closeDrawer(GravityCompat.END);
            } else {
                drawerLayout.openDrawer(GravityCompat.END);
            }
        }
    }

    protected User getUser(){
        return UserData.getUser();
    }

    protected boolean showToolbarLogo(){
        return false;
    }

    protected int getTitleToolbarColor() {
        return R.color.text_default_color;
    }

    protected boolean checkLogin(){
        return true;
    }

    public int getPositionFragment() {
        return positionFragment;
    }

    public static Intent intentWithPosition(Intent intent, int positionFragment){
        intent.putExtra(BaseActivity.POSITION_FRAGMENT, positionFragment);
        return intent;
    }


    private void pickPictureOrPickFromGallery() {
        CharSequence[] options = {getString(R.string.takePhoto), getString(R.string.chooseGallery)};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("")
                .setItems(options, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if(which == 0) {
                            PicturesHelper.Camera(BaseActivity.this);
                            photoByHere = true;
                        } else if(which == 1) {
                            PicturesHelper.Gallery(BaseActivity.this);
                            photoByHere = true;
                        }
                    }
                });
        builder.create().show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == EZPhotoPick.PHOTO_PICK_REQUEST_CODE && photoByHere){
            try {
                EZPhotoPickStorage ezPhotoPickStorage = new EZPhotoPickStorage(this);
                Bitmap bitmap = ezPhotoPickStorage.loadLatestStoredPhotoBitmap();
                profileImage.setImageBitmap(null);
                profileLoader.setVisibility(View.VISIBLE);
                new TaskUploadS3(bitmap).execute();
                photoByHere = false;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    @SuppressLint("StaticFieldLeak")
    private class TaskUploadS3 extends AsyncTask<Void, Void, Void> {
        private String urlImage;
        private Bitmap bitmap;

        public TaskUploadS3(Bitmap bitmap) {
            this.bitmap = bitmap;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            final File photoUser = Util.createImageFile();
            if(bitmap != null && photoUser != null){
                Util.sendBitmapToFile(photoUser, bitmap);
                String namePhoto = Util.generateName("user_android");
                urlImage = Config.Amazon.handleLocation(namePhoto);
                AmazonS3Client s3Client = new AmazonS3Client(new BasicAWSCredentials(Config.Amazon.ACCESS_KEY, Config.Amazon.PUBLIC_KEY));
                PutObjectRequest requestBillet = new PutObjectRequest(Config.Amazon.BUCKET, String.format(Config.Amazon.PATH, namePhoto), photoUser);
                s3Client.putObject(requestBillet);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            BaseActivity.this.sendImage(urlImage);
        }
    }


    private void sendImage(final String image){
        try{
            final User user = getUser();
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("name", user.getName());
            jsonObject.put("email", user.getEmail());
            jsonObject.put("document", user.getDoc());
            JSONObject photos = new JSONObject();
            photos.put("etag", "12312312");
            photos.put("url", image);
            jsonObject.put("photo", photos);

            Volley volley = new Volley(new Volley.Callback() {
                @Override
                public void result(String result, int type) {
                    user.setImage(image);
                    UserData.clear();
                    UserData.saveUser(user);
                    User user = getUser();
                    if(user != null && profileImage != null){
                        profileLoader.setVisibility(View.INVISIBLE);
                        profileImage.handleBase64(user.getImage());
                    }
                }

                @Override
                public String uri() {
                    User user = UserData.getUser();
                    if(user != null){
                        return URI.USER_EXTERNAL + "/" + user.getId();
                    }
                    return "";
                }
            });
            volley.isMeuADM(true);
            volley.request(Request.Method.PUT, jsonObject.toString());

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        User user = getUser();
        if(user != null && profileImage != null){
            profileImage.handleBase64(user.getImage());
        }
    }
}
