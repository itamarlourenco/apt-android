package br.com.apt.application.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;

/**
 * Created by adminbs on 11/26/17.
 */

public class PushNotificationLocalBroadcast {

    public static final String NOTIFICATION_LOCAL_BROAD_CAST = "NOTIFICATION_LOCAL_BROAD_CAST";
    public static final String NOTIFICATION_LOCAL_BROAD_CAST_COUNT = "NOTIFICATION_LOCAL_BROAD_CAST_COUNT";

    public static void initLocalBroadcast(Context context, BroadcastReceiver broadcastReceiver){
        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(context);
        localBroadcastManager.registerReceiver(broadcastReceiver, new IntentFilter(PushNotificationLocalBroadcast.NOTIFICATION_LOCAL_BROAD_CAST));
    }
}
