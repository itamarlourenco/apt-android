package br.com.apt.application;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.provider.Settings;
import android.support.multidex.MultiDexApplication;
import android.text.TextUtils;

import com.activeandroid.ActiveAndroid;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.XMPPConnection;

import br.com.apt.BuildConfig;
import br.com.apt.application.chat.XMPPConnectionCustom;
import br.com.apt.application.notification.NotificationSharedPreferences;
import br.com.apt.ui.myinfo.myinfofragment.pet.CustomHurlStack;
import io.fabric.sdk.android.Fabric;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;
import java.util.Locale;

import br.com.apt.ui.main.navigationDrawer.NavigationDrawerMenu;
import io.realm.Realm;
import io.realm.RealmConfiguration;

public class App extends MultiDexApplication {
    public static final String TAG = "br.com.apt.application.App";

    private static App instance;
    private static AppPreferences appPreferences;
    private static RequestQueue requestQueue;
    private static Gson gson;
    private static NotificationSharedPreferences notificationSharedPreferences;
    private static XMPPConnectionCustom xmppConnection;

    @Override
    public void onCreate() {
        super.onCreate();
        if(!BuildConfig.DEBUG){
            Fabric.with(this, new Crashlytics());
        }
        instance = this;
        appPreferences = new AppPreferences();
        gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
        notificationSharedPreferences = new NotificationSharedPreferences();
        xmppConnection = new XMPPConnectionCustom(new ConnectionConfiguration(Config.SMACK.HOST, Config.SMACK.PORT));
        Realm.init(this);
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
                .name("meuapt.realm")
                .schemaVersion(0)
                .build();
        Realm.setDefaultConfiguration(realmConfiguration);
        //ActiveAndroid.initialize(this);
    }

    public static Context getContext() {
        return instance;
    }

    public static App getInstance() {
        return instance;
    }

    public static AppPreferences getAppPreferences() {
        return appPreferences;
    }

    public RequestQueue getRequestQueue() {
        if(requestQueue == null){
            CustomHurlStack customHurlStack = new CustomHurlStack();
            requestQueue = Volley.newRequestQueue(getApplicationContext(), customHurlStack);
        }
        return requestQueue;
    }

    public static Gson getGson() {
        return gson;
    }

    public <T> void addToRequestQueue(Request<T> request, String tag) {
        request.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(request);
    }

    public <T> void addToRequestQueue(Request<T> request) {
        request.setTag(TAG);
        getRequestQueue().add(request);
    }

    public static List<NavigationDrawerMenu> getItems(){
        return new ArrayList<>(EnumSet.allOf(NavigationDrawerMenu.class));
    }

    public static NavigationDrawerMenu getItem(int position) {
        return getItems().get(position);
    }

    public static NotificationSharedPreferences getNotificationSharedPreferences() {
        return notificationSharedPreferences;
    }

    public static XMPPConnectionCustom getXmppConnection() {
        return xmppConnection;
    }

    public static void setXmppConnection(XMPPConnectionCustom xmppConnection) {
        App.xmppConnection = xmppConnection;
    }

    public static SimpleDateFormat getSimpleDateFormat() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", new Locale("pt", "BR"));
    }

    public static boolean isRelease(){
        return !BuildConfig.isDemo;
    }

    public static boolean isAdbens(){
        return BuildConfig.FLAVOR.equals("adbens");
    }

    public static boolean isApt(){
        return BuildConfig.FLAVOR.equals("apt");
    }

    public static boolean isDebug(){
        return BuildConfig.DEBUG;
    }

    @SuppressLint("HardwareIds")
    public static String getDeviceId(){
        return Settings.Secure.getString(getContext().getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static boolean isDeviceHml(){
        String[] deviceId = new String[]{
            //"d3eb48daaac92d5c"
        };
        return Arrays.asList(deviceId).contains(App.getDeviceId());
    }
}
