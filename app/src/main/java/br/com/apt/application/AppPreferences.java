package br.com.apt.application;

import android.content.Context;
import android.content.SharedPreferences;

public class AppPreferences {

    private static String SHARED_PREFERENCE_NAME = "AppPreferences";

    private static String PREFERENCE_IS_FIRST_ACCESS = "firstAccess";
    private static String APNS = "apns";

    private Context context;
    private static SharedPreferences sharedPrefs;

    public AppPreferences() {
        sharedPrefs = App.getContext().getSharedPreferences(SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE);
    }

    public void setFirstAccess(boolean isFirstAccess) {
        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putBoolean(PREFERENCE_IS_FIRST_ACCESS, isFirstAccess);
        editor.apply();
    }

    public boolean isFirstAccess() {
        return sharedPrefs != null && sharedPrefs.getBoolean(PREFERENCE_IS_FIRST_ACCESS, false);
    }

    public void setAPNS(String apns) {
        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putString(APNS, apns);
        editor.apply();
    }

    public String getAPNS() {
        if (sharedPrefs != null) {
            return sharedPrefs.getString(APNS, null);
        }
        return null;
    }
}
