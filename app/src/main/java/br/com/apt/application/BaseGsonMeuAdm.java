package br.com.apt.application;

/**
 * Created by itamarlourenco on 14/01/16.
 */
public class BaseGsonMeuAdm extends SendRequest {
    public static final int OK = 200;

    private int code;
    private int status;
    private boolean success;
    private String message;

    public static int getOK() {
        return OK;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
