package br.com.apt.application.chat;

import android.text.TextUtils;

import java.text.Normalizer;

import br.com.apt.application.Config;
import br.com.apt.model.user.User;

/**
 * Created by adminbs on 1/22/16.
 */
public class Authenticate {
    private String login;
    private String password;

    public Authenticate(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public String getLogin() {
        login = Normalizer.normalize(login, Normalizer.Form.NFD);
        login = login.replaceAll("\\s+","");
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        password = Normalizer.normalize(password, Normalizer.Form.NFD);
        password = password.replaceAll("\\s+","");
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isCorrect(){
        return !TextUtils.isEmpty(login) && !TextUtils.isEmpty(password);
    }

    public static Authenticate createAuthenticate(User user){
        String login = user.getName() + user.getApto() + "@" + Config.SMACK.HOST;
        String password = user.getName() + "_" + user.getBuildId() + "_" + user.getBlockId();

        return new Authenticate(login, password);
    }
}
