package br.com.apt.application.services;

import android.Manifest;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.NotificationCompat;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.model.user.UserData;
import br.com.apt.ui.main.Counter;
import br.com.apt.util.Logger;
import br.com.apt.util.Util;


/**
 * Created by adminbs on 2/28/18.
 */

public class LocationNotificationService extends IntentService implements LocationListener {
    private LocationManager locationManager;
    private String provider;
    private double DISTANCE = 1000d;
    private int TIME_NEXT_NOTIFICATION_IN_HOUR = (1000 * 60 * 6);

    private static final String NAME = "LOCATION_NOTIFICATION_SERVICE";

    public static void startService(Context context) {
        //context.startService(new Intent(context, LocationNotificationService.class));
    }

    public LocationNotificationService() {
        super(LocationNotificationService.NAME);
    }

    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
        locationNotificationCheck();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

    }

    private void locationNotificationCheck() {
        LocationManager service = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (service != null) {
            boolean enabled = service.isProviderEnabled(LocationManager.GPS_PROVIDER);
        }

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        provider = locationManager.getBestProvider(criteria, false);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        Location location = locationManager.getLastKnownLocation(provider);
        onLocationChanged(location);

        locationManager.requestLocationUpdates(provider, 400, 1, this);
    }

    @Override
    public void onLocationChanged(Location location) {
        if(location == null) return;
        double lat = (location.getLatitude());
        double lng = (location.getLongitude());


        ManegeData manegeData = new ManegeData();
        //double distanceBetweenPoints = (meterDistanceBetweenPoints(manegeData.getLat(), manegeData.getLon(), lat, lng) / 1000);
        double distanceBetweenPoints = (meterDistanceBetweenPoints(-23.6422878, -46.5394675, lat, lng) / 1000);


        long diff = System.currentTimeMillis() - (new ManegeData()).getSendTimestamp();
        if(distanceBetweenPoints < DISTANCE){
            if((new ManegeData()).getSendTimestamp() == 0){
                checkServIfExistDelivery();
            }else if(diff >= TIME_NEXT_NOTIFICATION_IN_HOUR){
                checkServIfExistDelivery();
            }
        }
    }

    private void checkServIfExistDelivery() {
        Volley volley = new Volley(new Volley.Callback() {
            @Override
            public void result(String result, int type) {
                try{
                    PushNotificationCountRequest pushNotificationCountRequest = App.getGson().fromJson(result, PushNotificationCountRequest.class);
                    Counter object = pushNotificationCountRequest.getObject();
                    if (object != null) {
                        Counter counter = pushNotificationCountRequest.getObject();
                        Counter.Reception reception = counter.getReception();
                        if(reception.getDeliveries() > 0){
                            showNotification();
                        }
                    }
                }catch (Exception e){
                    Logger.e(e.getCause());
                }
            }

            @Override
            public String uri() {
                return URI.PUSH_NOTIFICATION_COUNT;
            }
        });
        volley.request();
    }

    private void showNotification() {
        int num = (int) System.currentTimeMillis();
        PendingIntent resultPendingIntent = PendingIntent.getActivity(this, num, new Intent(), PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder mBuilder =
                (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                        .setVibrate(new long[] { 100, 100 })
                        .setLights(Color.RED, 1000, 1000)
                        .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                        .setSmallIcon(R.drawable.notification_status_bar)
                        .setLargeIcon(BitmapFactory.decodeResource(App.getContext().getResources(), R.drawable.ic_launcher))
                        .setContentTitle(getString(R.string.geo_location_delivery))
                        .setContentText(getString(R.string.geo_location_delivery_description))
                        .setContentIntent(resultPendingIntent);


        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        assert mNotificationManager != null;
        mNotificationManager.notify(num, mBuilder.build());

        (new ManegeData()).saveSendTimestamp(System.currentTimeMillis());
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    public static void sheduleAlarm(Activity activity){
        Intent intent = new Intent(activity, LocationNotificationService.class);
        PendingIntent pintent = PendingIntent.getService(activity, 0, intent, 0);

        int alarmType = AlarmManager.ELAPSED_REALTIME;
        final int FIFTEEN_SEC_MILLIS = (1000 * 60 * 10);
        AlarmManager alarmManager = (AlarmManager) activity.getSystemService(ALARM_SERVICE);

        assert alarmManager != null;
        alarmManager.setRepeating(alarmType, SystemClock.elapsedRealtime() + FIFTEEN_SEC_MILLIS, 60000, pintent);
    }

    private double meterDistanceBetweenPoints(double latA, double lngA, double latB, double lngB) {
        float pk = (float) (180.f/Math.PI);

        double a1 = latA / pk;
        double a2 = lngA / pk;
        double b1 = latB / pk;
        double b2 = lngB / pk;

        double t1 = Math.cos(a1) * Math.cos(a2) * Math.cos(b1) * Math.cos(b2);
        double t2 = Math.cos(a1) * Math.sin(a2) * Math.cos(b1) * Math.sin(b2);
        double t3 = Math.sin(a1) * Math.sin(b1);
        double tt = Math.acos(t1 + t2 + t3);

        return (6366000 * tt);
    }

    public static class ManegeData{
        public static String LOCATION_NOTIFICATION_DATA = "LOCATION_NOTIFICATION_DATA";
        public static String LOCATION_NOTIFICATION_LON = "LOCATION_NOTIFICATION_LON";
        public static String LOCATION_NOTIFICATION_LAT = "LOCATION_NOTIFICATION_LAT";
        public static String LOCATION_NOTIFICATION_SEND_TIMESTAMP = "LOCATION_NOTIFICATION_SEND_TIMESTAMP";

        private SharedPreferences pref = App.getContext().getSharedPreferences(LocationNotificationService.ManegeData.LOCATION_NOTIFICATION_DATA, Context.MODE_PRIVATE);
        private SharedPreferences.Editor editor = pref.edit();

        public void saveData(float lat, float lon){
            editor.putFloat(ManegeData.LOCATION_NOTIFICATION_LAT, lat);
            editor.putFloat(ManegeData.LOCATION_NOTIFICATION_LON, lon);
            editor.apply();
        }

        public float getLat(){
            return pref.getFloat(ManegeData.LOCATION_NOTIFICATION_LAT, 0);
        }

        public float getLon(){
            return pref.getFloat(ManegeData.LOCATION_NOTIFICATION_LON, 0);
        }

        public void saveSendTimestamp(long timeStamp){
            editor.putLong(ManegeData.LOCATION_NOTIFICATION_SEND_TIMESTAMP, timeStamp);
            editor.apply();
        }

        public long getSendTimestamp(){
            return pref.getLong(ManegeData.LOCATION_NOTIFICATION_SEND_TIMESTAMP, 0);
        }
    }
}