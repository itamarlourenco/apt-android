package br.com.apt.application.chat;

import android.os.Parcel;
import android.os.Parcelable;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

import br.com.apt.application.ModelObject;

/**
 * Created by adminbs on 1/22/16.
 */
@Table(name = "Chat")
public class AptChatMessages extends Model implements ModelObject {
    @Column(name = "meUser")
    private String meUser;

    @Column(name = "participant")
    private String participant;

    @Column(name = "message")
    private String message;

    @Column(name = "sender")
    private String sender;

    public AptChatMessages() {
    }

    public AptChatMessages(String fromUser, String fromName, String message) {
        this.meUser = fromUser;
        this.participant = fromName;
        this.message = message;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMeUser() {
        return meUser;
    }

    public void setMeUser(String from) {
        this.meUser = from;
    }

    public String getParticipant() {
        return participant;
    }

    public void setParticipant(String participant) {
        this.participant = participant;
    }

    public static List<AptChatMessages>  getChatByFrom(String participant){
        return new Select().from(AptChatMessages.class).where("participant = ?", participant).limit(100).execute();
    }

    public static List<AptChatMessages> getMessagens(){
        return new Select().from(AptChatMessages.class).execute();
    }

    public static AptChatMessages getChatById(Long chatId) {
        return new Select().from(AptChatMessages.class).where("id = ?", chatId).executeSingle();
    }

    protected AptChatMessages(Parcel in) {
        meUser = in.readString();
        participant = in.readString();
        message = in.readString();
        sender = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(meUser);
        dest.writeString(participant);
        dest.writeString(message);
        dest.writeString(sender);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<AptChatMessages> CREATOR = new Parcelable.Creator<AptChatMessages>() {
        @Override
        public AptChatMessages createFromParcel(Parcel in) {
            return new AptChatMessages(in);
        }

        @Override
        public AptChatMessages[] newArray(int size) {
            return new AptChatMessages[size];
        }
    };
}

