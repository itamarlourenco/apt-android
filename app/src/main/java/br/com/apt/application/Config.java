package br.com.apt.application;

import br.com.apt.model.user.User;
import br.com.apt.model.user.UserData;

/**
 * Created by itamarlourenco on 20/01/16.
 */
public class Config {


    public static class GCM{
        public static final String SENDER_ID = "864586428854";
    }

    public static class SMACK{
        public static final String HOST = "aptchat.cloudapp.net";
        public static final int PORT = 5222;
    }

    public static class About{
        public static final String URL = "https://drive.google.com/viewerng/viewer?embedded=true&url=https://meuapt.com.br/site/terms";
    }

    public static class Regulation{
        public static final String URL = "https://drive.google.com/viewerng/viewer?embedded=true&url=https://meuapt.com.br/site/regulation?condominium_id=";
        public static String URL(String blockId){
            return URL + blockId;
        }
    }

    public static class Amazon{
        public static final String ACCESS_KEY = "AKIAI3WXP3CG5DD4N4BQ";
        public static final String PUBLIC_KEY = "JnczI8h9chBFDT5w45BKohi3lj34TAzZ6JRYmhhN";
        public static final String BUCKET = "meuadm";
        public static final String PATH = "image-users/%s.jpg";
        public static final String LOCATION = "https://s3-us-west-2.amazonaws.com/";
        public static String handleLocation(String file){
            return Amazon.LOCATION + Amazon.BUCKET + "/" + String.format(PATH, file);
        }
    }
}
