package br.com.apt.application;

import android.app.Activity;
import android.text.TextUtils;
import android.util.Base64;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Random;

import br.com.apt.R;
import br.com.apt.model.user.User;
import br.com.apt.model.user.UserData;
import br.com.apt.util.CustomMultipartRequest;
import br.com.apt.util.Logger;
import br.com.apt.util.Util;
import dmax.dialog.SpotsDialog;
import android.util.Base64;
import android.webkit.URLUtil;

/**
 * Created by itamarlourenco on 14/01/16.
 */
public class Volley {
    public static final String KEY_API = "auth-key";
    private static final int TIME_OUT = 60000;
    private String url = null;
    private Volley.Callback callback;
    public static final int STATUS_OK = 200;
    public static final int STATUS_INVALID_TOKEN = 300;
    public static final int NOT_FOUND = 404;
    private SpotsDialog dialog;
    private boolean isMeuADM = false;
    private boolean isReservation = false;
    private boolean isPolls = false;
    private Object[] paramsGet = new Objects[]{};
    private String loginMeuAdm;
    private String passwordMeuAdm;
    private User user = UserData.getUser();

    public void setLoginMeuAdm(String loginMeuAdm) {
        this.loginMeuAdm = loginMeuAdm;
    }

    public void setPasswordMeuAdm(String passwordMeuAdm) {
        this.passwordMeuAdm = passwordMeuAdm;
    }

    public void isMeuADM(boolean isMeuADM) {
        this.isMeuADM = isMeuADM;
    }

    public void isReservation(boolean reservation) {
        isReservation = reservation;
    }

    public void isPoll(boolean isPolls){
        this.isPolls = isPolls;
    }

    public Volley(Volley.Callback callback, Object... paramsGet) {
        this.paramsGet = paramsGet;
        this.callback = callback;
    }

    public static String urlAttachment(String uri){
        return "https://meuadm.s3.amazonaws.com/image-users/" + uri;
    }

    private void handleUrl(Volley.Callback callback, Object... paramsGet){
        if(callback == null) return;

        if(paramsGet != null && paramsGet.length > 0){
            String params = TextUtils.join("/", paramsGet);
            url = url() + callback.uri() + "/" + params;
        }else{
            url = url() + callback.uri();
        }
    }

    public String url(){
        if(isMeuADM){
            if(App.isDeviceHml()){
                return "http://192.168.0.118:8080/v1/";
            }
            return "https://api.meuadm.com.br/v1/";
        }

        if(isPolls){
            return "https://enquetes.meuapt.com.br/";
        }

        if(isReservation){
            return "https://reservas.meuapt.com.br/";
        }

        if(App.isRelease()){
            return "https://v2.meuapt.com.br/api/";
        }

        return "https://v2.meuapt.com.br/api/";
    }

    public String urlBase(){
        if(isMeuADM){
            if(App.isDeviceHml()){
                return "http://192.168.0.118:8080/v1/";
            }
            return "http://api.meuadm.com.br/";
        }

        if(isPolls){
            return "https://enquetes.meuapt.com.br/";
        }

        if(isReservation){
            return "https://reservas.meuapt.com.br/";
        }

        if(App.isDeviceHml()){
            return "https://v2.meuapt.com.br/";
        }

        return "https://v2.meuapt.com.br/";
    }

    public void showDialog(Activity activity){
        try{
            dialog = new SpotsDialog(activity, R.style.AlertDialogCustom);
            dialog.show();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void setPolls(boolean polls) {
        isPolls = polls;
    }

    public void dismissDialog(){
        if(dialog != null){
            dialog.dismiss();
        }
    }

    public void request(){
        request(Request.Method.GET, null, 0);
    }

    public void request(int method, final String params){
        request(method, params, 0);
    }

    public void request(final int method, final String params, final int type){
        handleUrl(callback, paramsGet);
        final StringRequest stringRequest = new StringRequest(method == Request.Method.DELETE ? Request.Method.POST : method, this.url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try{
                            response = Util.convertToUTF8(response);
                                if(dialog != null && dialog.isShowing()){
                                dialog.dismiss();
                            }
                            if(response == null){
                                Logger.t(App.getContext().getString(R.string.error_internet));
                                return;
                            }
                            callback.result(response, type);
                        }catch(Exception e){
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error){
                        String response = null;
                        try {
                            if(error != null){
                                if(error.networkResponse != null){
                                    response = new String(error.networkResponse.data, "UTF-8");
                                }
                            }
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                        if(dialog != null && dialog.isShowing()){
                            dialog.dismiss();
                        }

                        try{
                            BaseGson baseGson = App.getGson().fromJson(response, BaseGson.class);
                            if(baseGson != null && baseGson.getStatus() == STATUS_INVALID_TOKEN){
                                //Logger.t(App.getContext().getString(R.string.error_authenticate));
                                //UserData.clear();
                            }
                        }catch (Exception ignored){}


                        callback.result(response, type);
                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();

                if(method == Request.Method.DELETE){
                    params.put("X-HTTP-Method-Override","DELETE");
                }

                if(Volley.this.user != null && Volley.this.user.isADM()){
                    String token = Volley.this.user.getToken();
                    params.put("Authorization", "Basic " + token);
                    params.put("unit", user.getApto());
                    return params;
                }else{
                    User user = UserData.getUser();
                    if(user != null){
                        String token = user.getToken();
                        params.put(KEY_API, token);
                        params.put("unit", user.getApto());
                    }


                    if(!TextUtils.isEmpty(loginMeuAdm) && !TextUtils.isEmpty(passwordMeuAdm)){
                        byte[] encodedBytes = Base64.encode((loginMeuAdm+":"+passwordMeuAdm).getBytes(), 0);
                        String basic = new String(encodedBytes);
                        params.put("Authorization", "Basic " + basic);
                    }


                    return params;
                }

            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=UTF-8";
            }

            @Override
            public byte[] getBody() {
                if(params != null){
                    return params.getBytes();
                }
                return null;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                Volley.TIME_OUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        );
        App.getInstance().addToRequestQueue(stringRequest);
    }

    public void request(final int method, final HashMap<String, String> params, final int type, File file, String nameFile){
        handleUrl(callback, paramsGet);

        MultipartEntity entity = new MultipartEntity();

        if(file != null){
            FileBody fileBody = new FileBody(file);
            entity.addPart(nameFile, fileBody);
        }

        if(params != null && params.size() > 0){
            for (Map.Entry<String,String> entry : params.entrySet()) {
                String key = Util.convertToUTF8params(entry.getKey());
                String value = Util.convertToUTF8params(entry.getValue());

                entity.addPart(key, new StringBody(value, ContentType.TEXT_PLAIN));
            }
        }

        final CustomMultipartRequest req = new CustomMultipartRequest(this.url, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if(dialog != null && dialog.isShowing()){
                    dialog.dismiss();
                }

                String response = null;
                try {
                    if(error != null){
                        if(error.networkResponse != null){
                            response = new String(error.networkResponse.data, "UTF-8");
                        }
                    }
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

                callback.result(response, type);
            }
        }, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(dialog != null && dialog.isShowing()){
                    dialog.dismiss();
                }
                callback.result(response, type);
            }
        }, entity){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();

                if(Volley.this.user != null && Volley.this.user.isADM()){
                    String token = Volley.this.user.getToken();
                    params.put("Authorization", "Basic " + token);
                    return params;
                }


                User user = UserData.getUser();
                if(user != null){
                    String token = user.getToken();
                    params.put(Volley.KEY_API, token);
                    return params;
                }




                return params;
            }
        };
        req.setRetryPolicy(new DefaultRetryPolicy(
                Volley.TIME_OUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        );
        App.getInstance().addToRequestQueue(req);
    }

    public interface Callback{
        void result(String result, int type);
        String uri();
    }

    public static String getUrlByImage(String file, boolean forceDownload){
        String sufix = "";

        if(forceDownload){
            Random rand = new Random();
            int n = rand.nextInt(999999);
            sufix = "?v=" + n;
        }


        return URLUtil.isValidUrl(file) ? file : (new Volley(null)).urlBase() + file + sufix;
    }


    public static String getUrlByImage(String file){
        return getUrlByImage(file,false);
    }

}
