package br.com.apt.application.notification;

import android.content.Context;
import android.os.Bundle;

import com.google.android.gms.gcm.GcmListenerService;

import br.com.apt.application.App;
import br.com.apt.application.notification.type.NotificationDelivery;
import br.com.apt.application.notification.type.NotificationNotice;
import br.com.apt.application.notification.type.NotificationPoll;
import br.com.apt.application.notification.type.NotificationVisit;
import br.com.apt.application.services.NotificationService;
import br.com.apt.ui.main.Counter;
import br.com.apt.ui.main.CounterRequest;
import br.com.apt.ui.trip.TripPreference;
import br.com.apt.util.Logger;

/**
 * Created by itamarlourenco on 20/01/16.
 */
public class GcmMessageHandler extends GcmListenerService {
    @Override
    public void onMessageReceived(String from, Bundle data) {
        try {

            NotificationService.startService(getApplicationContext());

            String myapt = data.getString("myapt");
            String reception = data.getString("reception");
            String notifications = NotificationSharedPreferences.handleNotification(myapt, reception);

            Counter counter = App.getGson().fromJson(notifications, Counter.class);
            NotificationSharedPreferences.updateNotification(counter);


            String stringType = data.getString("type");
            int type = Integer.parseInt(stringType == null ? "0" : stringType);
            Notification notification = null;
            Context context = App.getContext();

            switch (type){
                case Notification.TYPE_LOBBY_DELIVERY:
                    notification = new NotificationDelivery(context, data);
//                    App.getNotificationSharedPreferences().setCountDelivery(+1);
                    break;

                case Notification.TYPE_LOBBY_VISIT:
                    notification = new NotificationVisit(context, data);
//                    App.getNotificationSharedPreferences().setCountVisit(+1);
                    break;

                case Notification.TYPE_MYAPT_POLL:
                    notification = new NotificationPoll(context, data);
//                    App.getNotificationSharedPreferences().setCountPoll(+1);
                    break;

                case Notification.TYPE_MYAPT_NOTICE:
                    notification = new NotificationNotice(context, data);
//                    App.getNotificationSharedPreferences().setCountNotices(+1);
                    break;

                case Notification.TYPE_MYAPT_CHAT:
                    notification = new NotificationChat(context, data);
                    break;

                default:
                    notification = new Notification(context, data);
            }

            NotificationWrapper.createNotification(context, notification);

        }catch (Exception e){
            Logger.e(e.getMessage());
        }
    }
}