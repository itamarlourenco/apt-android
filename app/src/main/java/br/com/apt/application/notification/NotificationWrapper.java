package br.com.apt.application.notification;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.provider.Settings;
import android.support.v7.app.NotificationCompat;

import java.util.Random;

import br.com.apt.R;
import br.com.apt.application.App;

/**
 * Created by itamarlourenco on 21/01/16.
 */
public class NotificationWrapper {
    public static final int MESSAGE_NOTIFICATION_ID = 0;

    public static void createNotification(Context context, Notification notification) {

        int num = (int) System.currentTimeMillis();

        PendingIntent resultPendingIntent = PendingIntent.getActivity(context, num, notification.getIntent(), PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder mBuilder =
                (NotificationCompat.Builder) new NotificationCompat.Builder(context)

                        .setVibrate(new long[] { 100, 100 })
                        .setLights(Color.RED, 1000, 1000)
                        .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)

                        .setSmallIcon(R.drawable.notification_status_bar)
                        .setLargeIcon(BitmapFactory.decodeResource(App.getContext().getResources(), R.drawable.ic_launcher))
                        .setContentTitle(context.getString(R.string.app_name))
                        .setContentIntent(resultPendingIntent)
                        .setContentText(notification.getAlert());


        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(num, mBuilder.build());
    }
}
