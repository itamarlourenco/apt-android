package br.com.apt.application.chat;

import android.app.Activity;
import android.os.AsyncTask;

import org.apache.harmony.javax.security.auth.callback.CallbackHandler;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Presence;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.util.Logger;

/**
 * Created by adminbs on 1/22/16.
 */
public class XMPPConnectionCustom extends XMPPConnection {
    private ConnectionListener connectionListener;
    private Authenticate authenticate;
    private Activity activity;

    public XMPPConnectionCustom(String s, CallbackHandler callbackHandler) {
        super(s, callbackHandler);
    }

    public XMPPConnectionCustom(String s) {
        super(s);
    }

    public XMPPConnectionCustom(ConnectionConfiguration connectionConfiguration) {
        super(connectionConfiguration);
    }

    public XMPPConnectionCustom(ConnectionConfiguration connectionConfiguration, CallbackHandler callbackHandler) {
        super(connectionConfiguration, callbackHandler);
    }

    public void setConnectionListener(ConnectionListener connectionListener) {
        this.connectionListener = connectionListener;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public void setAuthenticate(Authenticate authenticate){
        this.authenticate = authenticate;
    }

    public Authenticate getAuthenticate() {
        return authenticate;
    }

    @Override
    public void connect(){
        if(activity == null){
            connectionListener.connected(null);
            Logger.e("Please enter with Activity");
            return;
        }

        try{
            if(authenticate.isCorrect()){
                new AsyncTask<Void, Void, Void>(){
                    @Override
                    protected Void doInBackground(Void... params) {
                        try {
                            if(!isConnected()) {
                                XMPPConnectionCustom.super.connect();
                                login(authenticate.getLogin(), authenticate.getPassword());

                                if (isConnected()) {
                                    activity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            connectionListener.connected(XMPPConnectionCustom.this);
                                        }
                                    });
                                    return null;
                                }
                            }else{
                                activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        connectionListener.connected(XMPPConnectionCustom.this);
                                    }
                                });
                            }
                        } catch (Exception e) {
                            Logger.e(e.getMessage());
                        }
                        return null;
                    }
                }.execute();
            }else{
                Logger.e(App.getContext().getString(R.string.enter_with_authenticate_chat));
            }
        }catch (Exception e){
            Logger.e(e.getMessage());
        }
    }

    public interface ConnectionListener{
        void connected(XMPPConnectionCustom xmppConnectionCustom);
    }
}
