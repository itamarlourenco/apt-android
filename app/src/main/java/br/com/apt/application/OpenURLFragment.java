package br.com.apt.application;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.github.ybq.android.spinkit.SpinKitView;

import br.com.apt.R;

/**
 * Created by adminbs on 9/3/17.
 */

public class OpenURLFragment extends BaseFragment {
    private String url;

    public static OpenURLFragment newInstance(String url) {
        OpenURLFragment openURLFragment = new OpenURLFragment();
        openURLFragment.setUrl(url);
        return openURLFragment;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.open_url, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if(view != null){
            WebView webView = (WebView) view.findViewById(R.id.webView);
            WebSettings webSettings = webView.getSettings();
            webSettings.setJavaScriptEnabled(true);
            webView.clearCache(true);
            webView.loadUrl(url);
        }
    }
}
