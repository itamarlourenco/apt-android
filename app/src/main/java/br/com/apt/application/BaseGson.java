package br.com.apt.application;

/**
 * Created by itamarlourenco on 14/01/16.
 */
public class BaseGson extends SendRequest {
    public static final int OK = 200;

    public int Status;
    public String Message;

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }
}
