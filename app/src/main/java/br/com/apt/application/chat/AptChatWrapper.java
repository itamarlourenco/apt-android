package br.com.apt.application.chat;

import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.packet.Message;

import br.com.apt.util.Util;

/**
 * Created by adminbs on 1/22/16.
 */
public class AptChatWrapper {

    public static long saveMessageChat(String message, String meUser, String participant, String sender){
        AptChatMessages aptChat = new AptChatMessages();
        aptChat.setMeUser(meUser);
        aptChat.setParticipant(participant);
        aptChat.setMessage(message);
        aptChat.setSender(sender);

        return aptChat.save();
    }

}
