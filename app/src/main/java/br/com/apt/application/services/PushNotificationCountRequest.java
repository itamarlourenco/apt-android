package br.com.apt.application.services;

import br.com.apt.application.BaseGson;
import br.com.apt.ui.main.Counter;
import br.com.apt.ui.myapt.myaptFragments.build.Apartment;

/**
 * Created by adminbs on 11/26/17.
 */

public class PushNotificationCountRequest extends BaseGson {
    public Counter Object;

    public Counter getObject() {
        return Object;
    }
}
