package br.com.apt.application.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;

import org.json.JSONObject;

import br.com.apt.application.App;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.model.user.User;
import br.com.apt.model.user.UserData;
import br.com.apt.ui.main.Counter;
import br.com.apt.util.Logger;

/**
 * Created by adminbs on 11/26/17.
 */

public class NotificationService extends IntentService {

    private static final String NAME = "NOTIFICATION_SERVICE";

    public static void startService(Context context){
        context.startService(new Intent(context, NotificationService.class));
    }

    public NotificationService() {
        super(NotificationService.NAME);
    }

    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
        notificationCount();
        return super.onStartCommand(intent, flags, startId);
    }

    private void notificationCount() {
        Volley volley = new Volley(new Volley.Callback() {
            @Override
            public void result(String result, int type) {
                try{
                    PushNotificationCountRequest pushNotificationCountRequest = App.getGson().fromJson(result, PushNotificationCountRequest.class);
                    Counter object = pushNotificationCountRequest.getObject();
                    if (object != null) {
                        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(
                                (new Intent(PushNotificationLocalBroadcast.NOTIFICATION_LOCAL_BROAD_CAST))
                                        .putExtra(PushNotificationLocalBroadcast.NOTIFICATION_LOCAL_BROAD_CAST_COUNT, object)
                        );
                        getCountNotification(object);
                    }
                }catch (Exception e){
                    Logger.e(e.getCause());
                }
            }

            @Override
            public String uri() {
                return URI.PUSH_NOTIFICATION_COUNT;
            }
        });
        volley.request();
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
    }

    public void getCountNotification(final Counter counterObject) {
        Volley volley = new Volley(new Volley.Callback() {
            @Override
            public void result(String result, int type) {
                try{
                    if(!TextUtils.isEmpty(result)){
                        JSONObject jsonObject = new JSONObject(result);
                        JSONObject object = jsonObject.getJSONObject("object");
                        int number = Integer.parseInt(object.getString("number"));
                        counterObject.setNotificationCount(number);

                        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(
                                (new Intent(PushNotificationLocalBroadcast.NOTIFICATION_LOCAL_BROAD_CAST))
                                        .putExtra(PushNotificationLocalBroadcast.NOTIFICATION_LOCAL_BROAD_CAST_COUNT, counterObject)
                        );
                    }
                }catch (Exception e){
                    Logger.e(e.getCause());
                }
                getCountReservations(counterObject);
            }

            @Override
            public String uri() {
                return URI.PUSH_NOTIFICATION_COUNT_MEUADM;
            }
        });
        volley.isMeuADM(true);
        volley.request();
    }

    private void getCountReservations(final Counter counterObject) {
        Volley volley = new Volley(new Volley.Callback() {
            @Override
            public void result(String result, int type) {
                try{
                    JSONObject jsonObject = new JSONObject(result);
                    JSONObject object = jsonObject.getJSONObject("object");
                    int total = Integer.parseInt(object.getString("total"));
                    counterObject.setReservation(new Counter.Reservation(total));

                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(
                            (new Intent(PushNotificationLocalBroadcast.NOTIFICATION_LOCAL_BROAD_CAST))
                                    .putExtra(PushNotificationLocalBroadcast.NOTIFICATION_LOCAL_BROAD_CAST_COUNT, counterObject)
                    );
                }catch (Exception e){
                    Logger.e(e.getCause());
                }
            }

            @Override
            public String uri() {
                User user = UserData.getUser();
                if(user != null){
                    return String.format(URI.COUNT_RESERVATION, user.getId());
                }
                return null;
            }
        });
        volley.isReservation(true);
        volley.request();
    }
}
