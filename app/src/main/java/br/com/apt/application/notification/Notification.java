package br.com.apt.application.notification;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import br.com.apt.ui.lobby.LobbyActivity;
import br.com.apt.ui.main.MainActivity;

/**
 * Created by itamarlourenco on 20/01/16.
 */
public class Notification {
    public static final String EXTRA_FROM = "from";
    public static final String EXTRA_TYPE = "type";
    public static final String EXTRA_ALERT = "alert";
    public static final String EXTRA_BADGE = "badge";
    public static final String EXTRA_SOUND = "sound";
    public static final String EXTRA_COLLAPSE_KEY = "collapse_key";

    public static final int TYPE_LOBBY_DELIVERY = 0;
    public static final int TYPE_LOBBY_VISIT = 1;
    public static final int TYPE_MYAPT_POLL = 2;
    public static final int TYPE_MYAPT_NOTICE = 3;
    public static final int TYPE_MYAPT_CHAT = 4;

    private String from;
    private int type;
    private String alert;
    private int badge;
    private String sound;
    private String collapse_key;
    private String description;

    protected Context context;

    public Notification(Context context, Bundle data) {
        this.context = context;
        if(data != null){
            setFrom(data.getString(EXTRA_FROM));
            setType(data.getInt(EXTRA_TYPE, 0));
            setAlert(data.getString(EXTRA_ALERT));
            setBadge(data.getInt(EXTRA_BADGE, 0));
            setSound(data.getString(EXTRA_SOUND));
            setCollapseKey(data.getString(EXTRA_COLLAPSE_KEY));
        }
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getAlert() {
        return alert;
    }

    public void setAlert(String alert) {
        this.alert = alert;
        setDescription(alert);
    }

    public int getBadge() {
        return badge;
    }

    public void setBadge(int badge) {
        this.badge = badge;
    }

    public String getSound() {
        return sound;
    }

    public void setSound(String sound) {
        this.sound = sound;
    }

    public String getCollapseKey() {
        return collapse_key;
    }

    public void setCollapseKey(String collapse_key) {
        this.collapse_key = collapse_key;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    protected Intent getIntent(){
        return MainActivity.newIntent(context);
    }

}
