package br.com.apt.application.chat;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.List;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.widget.CustomTextView;

/**
 * Created by adminbs on 1/22/16.
 */
public class ChatMessageAdapter extends BaseAdapter {
    private Context context;
    private List<AptChatMessages> messages;
    private static final int LAYOUT_LEFT = 1;
    private static final int LAYOUT_RIGHT = 0;

    public ChatMessageAdapter(Context context, List<AptChatMessages> messages) {
        this.context = context;
        this.messages = messages;
    }

    @Override
    public int getCount() {
        return messages == null ? 0 : messages.size();
    }

    @Override
    public AptChatMessages getItem(int position) {
        return messages.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder viewHolder;
        int type = getItemViewType(position);

        if(convertView == null){
            if(type == LAYOUT_LEFT){
                view = LayoutInflater.from(context).inflate(R.layout.adapter_chat_message, parent, false);
            }else{ //LAYOUT_RIGHT
                view = LayoutInflater.from(context).inflate(R.layout.adapter_chat_message_right, parent, false);
            }

            convertView = view;
            viewHolder = new ViewHolder();
            viewHolder.message = (CustomTextView) view.findViewById(R.id.textView);

            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }

        AptChatMessages item = getItem(position);
        if(item != null){
            viewHolder.message.setText(item.getMessage());
        }

        return convertView;
    }


    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        AptChatMessages item = getItem(position);
        XMPPConnectionCustom xmppConnection = App.getXmppConnection();
        if(item != null && xmppConnection != null && xmppConnection.getAuthenticate() != null) {
            if (item.getSender().equals(xmppConnection.getAuthenticate().getLogin())) {
                return LAYOUT_LEFT;
            }
            return LAYOUT_RIGHT;
        }
        return LAYOUT_LEFT;
    }

    public class ViewHolder{
        public CustomTextView message;
    }
}
