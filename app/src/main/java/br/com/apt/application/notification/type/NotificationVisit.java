package br.com.apt.application.notification.type;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import br.com.apt.application.notification.Notification;
import br.com.apt.ui.lobby.LobbyActivity;

/**
 * Created by itamarlourenco on 21/01/16.
 */
public class NotificationVisit extends Notification {
    public NotificationVisit(Context context, Bundle data) {
        super(context, data);
    }

    @Override
    protected Intent getIntent() {
        return LobbyActivity.newIntent(context, 1);
    }
}
