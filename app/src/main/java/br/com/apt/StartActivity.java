package br.com.apt;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.crashlytics.android.Crashlytics;

import br.com.apt.application.BaseActivity;
import br.com.apt.model.user.User;
import br.com.apt.ui.firstAccess.animations.AnimationActivity;
import br.com.apt.ui.main.MainActivity;
import br.com.apt.util.Logger;
import io.fabric.sdk.android.Fabric;

public class StartActivity extends BaseActivity {

    public static Intent newIntent(Context context){
        return new Intent(context, StartActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent;

        Fabric.with(this, new Crashlytics());

        User user = getUser();
        if(user != null){
            startActivity(MainActivity.newIntent(this));
            finish();
            return;
        }

        intent = AnimationActivity.newIntent(this);
        startActivity(intent);
        finish();
    }

    @Override
    protected boolean showToolbarLogo() {
        return true;
    }

    @Override
    protected boolean checkLogin() {
        return false;
    }
}