package br.com.apt.model.reservations;

import android.content.Context;

import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.List;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.model.OnReceivedListener;
import br.com.apt.model.ResponseModel;
import br.com.apt.model.user.User;
import br.com.apt.model.user.UserData;
import br.com.apt.util.Logger;

/**
 * Created by andre on 5/17/17.
 */

public class Installation implements Serializable {

    public int iconResource;
    public int stringResource;

    @SerializedName("id")
    public int id;
    @SerializedName("company_key")
    public String companyKey;
    @SerializedName("condominium_id")
    public int condominiumId;
    @SerializedName("name")
    public String name;
    @SerializedName("icon")
    public String icon;
    @SerializedName("color")
    public String color;
    @SerializedName("fraction_rental_time")
    public String fractionRentalTime;
    @SerializedName("rental_lock_time")
    public String rentalLockTime;
    @SerializedName("amount_lock")
    public String amountLock;
    @SerializedName("opening_time")
    public String openingTime;
    @SerializedName("closed_time")
    public String closedTime;
    @SerializedName("maximum_day_reservation")
    public int maximumDayReservation;
    @SerializedName("minimum_day_reservation")
    public int minimumDayReservation;
    @SerializedName("cancellation_period")
    public int cancellationPeriod;
    @SerializedName("enabled_use")
    public int enabledUse;
    @SerializedName("auto_approved")
    public int autoApproved;
    @SerializedName("created_at")
    public String createdAt;

    public Installation(int icon, int string) {
        iconResource = icon;
        stringResource = string;
    }

    public static void requestInstallations(final Context context, final OnReceivedListener<List<Installation>> listener) {
        final User user = UserData.getUser();

        if(user != null){
            Volley volley = new Volley(new Volley.Callback() {
                @Override
                public void result(String result, int type) {
                    if (result == null) {
                        Logger.t(context.getResources().getString(R.string.internet_error));
                        return;
                    }

                    Type responseType = new TypeToken<ResponseModel<List<Installation>>>() {}.getType();
                    ResponseModel<List<Installation>> responseModel = App.getGson().fromJson(result, responseType);

                    if (responseModel != null && responseModel.status == Volley.STATUS_OK) {
                        listener.received(responseModel.contents);
                    } else {
                        listener.failed(responseModel.code, responseModel.message);
                    }
                }

                @Override
                public String uri() {
                    return URI.INSTALLATIONS_LIST + "/search?condominium_id=" + user.getCondominiumId();
                }
            });

            volley.isReservation(true);
            volley.request();
        }
    }

}
