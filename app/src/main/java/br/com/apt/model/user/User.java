package br.com.apt.model.user;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Base64;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.ModelObject;
import br.com.apt.model.build.Build;
import br.com.apt.ui.myCondominium.myCondominiumFragments.Attachments;

/**
 * Created by itamarlourenco on 14/01/16.
 */
public class User implements ModelObject{
    private String id;

    @SerializedName("nome")
    private String name;
    private String email;

    @SerializedName("Edificios")
    private Build build;

    @SerializedName("senha")
    private String password;

    @SerializedName("telefone")
    private String phone;

    @SerializedName("documento")
    private String doc;

    @SerializedName("tipoUsuario")
    private String typeUser;

    @SerializedName("edificioId")
    private String buldId;

    @SerializedName("apto")
    private String apto;

    @SerializedName("token")
    private String token;

    @SerializedName("ativado")
    private boolean isActivated;

    private String apns;

    @SerializedName("blocoId")
    private String blockId;

    @SerializedName("viagem")
    private boolean isTrip;

    @SerializedName("chatToken")
    private String chatToken;

    @SerializedName("image")
    private String image;

    private String buildingName;

    private String condominiumId;

    private boolean isADM;

    private String companyKey;

    private String code;

    private String currentBlock;

    private List<UserMeuadm.Units> units;

    public User(){

    }

    public User(String id, String name, String condominiumId,  String email, String password, String phone, String doc, String typeUser, String buldId, String apto, String token, boolean isActivated, String apns, String blockId, boolean isTrip, String chatToken, String code, String image, boolean isADM, String currentBlock) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.condominiumId = condominiumId;
        this.password = password;
        this.phone = phone;
        this.doc = doc;
        this.currentBlock = currentBlock;
        this.typeUser = typeUser;
        this.buldId = buldId;
        this.apto = apto;
        this.token = token;
        this.isActivated = isActivated;
        this.apns = apns;
        this.blockId = blockId;
        this.isTrip = isTrip;
        this.chatToken = chatToken;
        this.code = code;
        this.image = image;
        this.isADM = isADM;
    }


    public String getCompanyKey() {
        if (companyKey != null) {
            return companyKey.trim();
        }
        return null;
    }

    public void setCompanyKey(String companyKey) {
        this.companyKey = companyKey;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Build getBuild() {
        return build;
    }

    public void setBuild(Build build) {
        this.build = build;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDoc() {
        return doc;
    }

    public void setDoc(String doc) {
        this.doc = doc;
    }

    public String getTypeUser() {
        return typeUser;
    }

    public void setTypeUser(String typeUser) {
        this.typeUser = typeUser;
    }

    public String getBuildId() {
        return buldId;
    }

    public void setBiuldId(String buldId) {
        this.buldId = buldId;
    }

    public String getCondominiumId() {
        if (condominiumId != null) {
            return condominiumId.trim();
        }
        return condominiumId;
    }

    public void setCondominiumId(String condominiumId) {
        this.condominiumId = condominiumId;
    }

    public String getApto() {
        if (apto != null) {
            return apto.trim();
        }
        return apto;
    }

    public void setApto(String apto) {
        this.apto = apto;
    }

    public String getToken() {
        if(!TextUtils.isEmpty(token)){
            token = token.trim().replaceAll("\n ", "");
        }
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public boolean isActivated() {
        return isActivated;
    }

    public void setIsActivated(boolean isActivated) {
        this.isActivated = isActivated;
    }

    public String getApns() {
        return apns;
    }

    public void setApns(String apns) {
        this.apns = apns;
    }

    public String getBlockId() {
        if (blockId != null) {
            return blockId.trim();
        }
        return blockId;
    }

    public void setBlockId(String blockId) {
        this.blockId = blockId;
    }

    public boolean isTrip() {
        return isTrip;
    }

    public void setIsTrip(boolean isTrip) {
        this.isTrip = isTrip;
    }

    public String getChatToken() {
        return chatToken;
    }

    public void setChatToken(String chatToken) {
        this.chatToken = chatToken;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getBuildingName() {
        return buildingName;
    }

    public void setBuildingName(String buildingName) {
        this.buildingName = buildingName;
    }

    public boolean isADM() {
        return isADM;
    }

    public void setADM(boolean ADM) {
        isADM = ADM;
    }

    public List<UserMeuadm.Units> getUnits() {
        return units;
    }

    public void setUnits(List<UserMeuadm.Units> units) {
        this.units = units;
    }

    public String getCurrentBlock() {
        return currentBlock;
    }

    public void setCurrentBlock(String currentBlock) {
        this.currentBlock = currentBlock;
    }

    protected User(Parcel in) {
        id = in.readString();
        name = in.readString();
        email = in.readString();
        condominiumId = in.readString();
        password = in.readString();
        phone = in.readString();
        doc = in.readString();
        typeUser = in.readString();
        buldId = in.readString();
        apto = in.readString();
        token = in.readString();
        isActivated = in.readByte() != 0x00;
        isADM = in.readByte() != 0x00;
        apns = in.readString();
        blockId = in.readString();
        isTrip = in.readByte() != 0x00;
        chatToken = in.readString();
        code = in.readString();
        image = in.readString();
        buildingName = in.readString();
        currentBlock = in.readString();

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(email);
        dest.writeString(condominiumId);
        dest.writeString(password);
        dest.writeString(phone);
        dest.writeString(doc);
        dest.writeString(typeUser);
        dest.writeString(buldId);
        dest.writeString(apto);
        dest.writeString(token);
        dest.writeByte((byte) (isActivated ? 0x01 : 0x00));
        dest.writeByte((byte) (isADM? 0x01 : 0x00));
        dest.writeString(apns);
        dest.writeString(blockId);
        dest.writeByte((byte) (isTrip ? 0x01 : 0x00));
        dest.writeString(chatToken);
        dest.writeString(code);
        dest.writeString(currentBlock);
        dest.writeString(image);
        dest.writeString(buildingName);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };


    public static User adapterUserMeuadm(UserMeuadm userMeuadm, String apns, String email, String password, final Activity activity){
        try{
            byte[] encodedBytes = Base64.encode((email+":"+password).getBytes(), 0);

            User user = new User();
            user.setId(userMeuadm.getId());
            user.setName(userMeuadm.getName());
            user.setEmail(userMeuadm.getEmail());
            user.setPhone(userMeuadm.getMobile());
            user.setDoc(userMeuadm.getDocument());
            user.setTypeUser(userMeuadm.getRole().getName());
            if(userMeuadm.getUnitsList() != null && userMeuadm.getUnitsList().size() > 0){
                user.setBiuldId(userMeuadm.getUnitsList().get(0).getBloco());
                user.setBlockId(userMeuadm.getUnitsList().get(0).getBloco());
            }
            user.setCurrentBlock(userMeuadm.getBlockKey());
            user.setToken(new String(encodedBytes));
            user.setADM(true);
            user.setApns(apns);
            user.setCompanyKey(userMeuadm.getCompanyKey());
            user.setCondominiumId(userMeuadm.getCondominiumkey());
            user.setImage(userMeuadm.getPhoto().getUrl());
            Build build = new Build();

            List<UserMeuadm.Units> unitsList  = userMeuadm.getUnitsList();
            user.setUnits(unitsList);
            if(unitsList != null && unitsList.size() > 0){
                UserMeuadm.Units units = unitsList.get(0);
                UserMeuadm.Address address = units.getAddress();
                build.setName(address.getCondominiumNome().trim());
                build.setLocation(address.getBairro());
                build.setZipcode(address.getCep());
                build.setNameBlock(address.getBlocoNome().trim());
                build.setPhone("");
                if(unitsList.size() <= 1){
                    user.setApto(units.getUnidade());
                }
                user.setBuild(build);
            }

            return user;
        }catch (Exception e){
            e.printStackTrace();
        }

        return new User();
    }
}