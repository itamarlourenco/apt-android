package br.com.apt.model;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;

import org.json.JSONException;
import org.json.JSONObject;

import br.com.apt.application.App;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.model.user.User;
import br.com.apt.model.user.UserData;
import br.com.apt.widget.CustomTextView;

/**
 * Created by adminbs on 10/9/17.
 */

public class NotificationHandle {
    public static int SAC = 0;
    public static int NOTICES = 0;
    public static int RESERVATION = 0;

    public static void getNotificationSac(final Context context, final CustomTextView indicatorMyAdm){
        NotificationHandle.SAC = 0;
        Volley volley = new Volley(new Volley.Callback() {
            @Override
            public void result(String result, int type) {
                if(result != null){
                    OccurrencesNotReadSend occurrencesNotReadSend = App.getGson().fromJson(result, OccurrencesNotReadSend.class);
                    if(occurrencesNotReadSend != null){
                        try{
                            OccurrencesNotRead object = occurrencesNotReadSend.getObject();
                            if (object.getNumber() > 0) {
                                NotificationHandle.SAC = object.getNumber();

                                indicatorMyAdm.setVisibility(View.VISIBLE);
                                indicatorMyAdm.setText(String.valueOf(object.getNumber()));

                                Intent intent = new Intent(OccurrencesNotRead.OCCURRENCES_NOT_READ_BROADCAST);
                                intent.putExtra(OccurrencesNotRead.OCCURRENCES_NOT_READ_BROADCAST_TOTAL, object.getNumber());
                                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                            }else{
                                indicatorMyAdm.setVisibility(View.GONE);
                            }
                        }catch (Exception e){
                            indicatorMyAdm.setVisibility(View.GONE);
                        }
                    }
                }else{
                    indicatorMyAdm.setVisibility(View.GONE);
                }
            }

            @Override
            public String uri() {
                return URI.OCCURRENCES_NOT_READ;
            }
        });
        volley.isMeuADM(true);
        volley.request();
    }


    public static void getNotificationReservation(final Context context, final CustomTextView indicatorReservation){
        NotificationHandle.RESERVATION = 0;
        Volley volley = new Volley(new Volley.Callback() {
            @Override
            public void result(String result, int type) {
                if(result != null){
                    ReservationNotReadSend reservationNotReadSend = App.getGson().fromJson(result, ReservationNotReadSend.class);
                    if(reservationNotReadSend != null){
                        try{
                            ReservationNotRead object = reservationNotReadSend.getObject();
                            if (object.getTotal() > 0) {
                                NotificationHandle.RESERVATION = object.getTotal();

                                indicatorReservation.setVisibility(View.VISIBLE);
                                indicatorReservation.setText(String.valueOf(object.getTotal()));
                            }else{
                                indicatorReservation.setVisibility(View.GONE);
                            }
                        }catch (Exception e){
                            indicatorReservation.setVisibility(View.GONE);
                        }
                    }
                }else{
                    indicatorReservation.setVisibility(View.GONE);
                }
            }

            @Override
            public String uri() {
                User user = UserData.getUser();
                if(user != null){
                    return String.format(URI.COUNT_RESERVATION, user.getId());
                }
                return null;
            }
        });
        volley.isReservation(true);
        volley.request();
    }


    public static void getNoticesCount(final Context context, final CustomTextView indicatorLobby){
        NotificationHandle.NOTICES = 0;
        Volley volley = new Volley(new Volley.Callback() {
            @Override
            public void result(String result, int type) {
                if(result != null){
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        JSONObject object = jsonObject.getJSONObject("Object");
                        int total = object.getInt("total");
                        if(total > 0){
                            NotificationHandle.NOTICES = total;

                            indicatorLobby.setVisibility(View.VISIBLE);
                            indicatorLobby.setText(String.valueOf(total));
                        }
                    } catch (JSONException e) {
                        indicatorLobby.setVisibility(View.GONE);
                        e.printStackTrace();
                    }
                }else{
                    indicatorLobby.setVisibility(View.GONE);
                }
            }

            @Override
            public String uri() {
                return URI.USERS_HAS_NOTICES;
            }
        });
        volley.request();
    }
}
