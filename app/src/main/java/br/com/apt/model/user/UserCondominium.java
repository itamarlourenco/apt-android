package br.com.apt.model.user;

import android.content.Context;

import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.List;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.model.OnReceivedListener;
import br.com.apt.model.ResponseModel;
import br.com.apt.util.Logger;

/**
 * Created by andre on 09/06/17.
 */

public class UserCondominium {

    @SerializedName("id")
    public int id;
    @SerializedName("name")
    public String name;
    @SerializedName("address")
    public String address;
    @SerializedName("street")
    public String street;
    @SerializedName("street_number")
    public String street_number;
    @SerializedName("neighborhood")
    public String neighborhood;
    @SerializedName("zip_code")
    public String zip_code;
    @SerializedName("city")
    public String city;
    @SerializedName("state")
    public String state;
    @SerializedName("manager_name")
    public String manager_name;
    @SerializedName("manager_email")
    public String manager_email;
    @SerializedName("manager_phone")
    public String manager_phone;
    @SerializedName("apartment_quantity")
    public String apartment_quantity;
    @SerializedName("store_quantity")
    public String store_quantity;
    @SerializedName("group_quantity")
    public String group_quantity;
    @SerializedName("document")
    public String document;
    @SerializedName("date_closing")
    public String date_closing;
    @SerializedName("category")
    public String category;
    @SerializedName("blocks")
    public List<Block> blocks;

    public static void getCondominium(String condominiumId, final Context context, final OnReceivedListener<UserCondominium> listener){
        final User user = UserData.getUser();
        if(user != null){
            Volley volley = new Volley(new Volley.Callback() {
                @Override
                public void result(String result, int type) {
                    if (result == null) {
                        Logger.t(context.getResources().getString(R.string.internet_error));
                        return;
                    }

                    Type responseType = new TypeToken<ResponseModel<UserCondominium>>() { }.getType();
                    ResponseModel<UserCondominium> responseModel = App.getGson().fromJson(result, responseType);

                    if (responseModel != null && responseModel.code == Volley.STATUS_OK) {
                        listener.received(responseModel.contents);
                    } else {
                        listener.failed(responseModel.code, responseModel.message);
                    }
                }

                @Override
                public String uri() {
                    return URI.CONDOMINIUM;
                }

            }, condominiumId);
            volley.request();
        }
    }

    public static class Block implements Serializable {
        @SerializedName("name")
        public String name;
    }
}
