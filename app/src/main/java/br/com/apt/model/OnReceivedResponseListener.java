package br.com.apt.model;

/**
 * Created by andre on 30/05/17.
 */

public interface OnReceivedResponseListener<T> {

    void received(T object);

    void failed(int errorCode, String errorMessage);
}