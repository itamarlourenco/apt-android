package br.com.apt.model;

import com.android.volley.Request;
import br.com.apt.application.SendRequest;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.util.Logger;

/**
 * Created by adminbs on 1/4/17.
 */

public class UsersHasNotices {

    public static void requestUserHasNotices(long noticesId){
        Volley volley = new Volley(new Volley.Callback() {
            @Override
            public void result(String result, int type) {
                Logger.d(result);
            }


            @Override
            public String uri() {
                return URI.USERS_HAS_NOTICES;
            }
        });
        SendUsersHasNotices sendUsersHasNotices = new SendUsersHasNotices();
        sendUsersHasNotices.setNoticesId(noticesId);
        volley.request(Request.Method.PUT, sendUsersHasNotices.toJson());
    }

    public static class SendUsersHasNotices extends SendRequest {
        private long notices_id;

        public long getNoticesId() {
            return notices_id;
        }

        public void setNoticesId(long notices_id) {
            this.notices_id = notices_id;
        }
    }
}
