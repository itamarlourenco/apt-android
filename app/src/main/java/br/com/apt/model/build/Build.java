package br.com.apt.model.build;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import br.com.apt.application.ModelObject;

/**
 * Created by adminbs on 1/15/16.
 */
public class Build implements ModelObject {
    private long id;
    @SerializedName("nome")
    private String name;

    @SerializedName("localizacao")
    private String location;

    @SerializedName("telefone")
    private String phone;

    @SerializedName("cep")
    private String zipcode;

    @SerializedName("numero")
    private String number;

    @SerializedName("emailAdm")
    private String email;

    @SerializedName("fundo")
    private String funds;

    private String nameBlock;

    public String getNameBlock() {
        return nameBlock;
    }

    public void setNameBlock(String nameBlock) {
        this.nameBlock = nameBlock;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFunds() {
        return funds;
    }

    public void setFunds(String funds) {
        this.funds = funds;
    }

    public Build() {

    }

    public Build(long id, String name, String location, String phone, String zipcode, String number, String email, String funds) {
        this.id = id;
        this.name = name;
        this.location = location;
        this.phone = phone;
        this.zipcode = zipcode;
        this.number = number;
        this.email = email;
        this.funds = funds;
    }

    protected Build(Parcel in) {
        id = in.readLong();
        name = in.readString();
        location = in.readString();
        phone = in.readString();
        zipcode = in.readString();
        number = in.readString();
        email = in.readString();
        funds = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(name);
        dest.writeString(location);
        dest.writeString(phone);
        dest.writeString(zipcode);
        dest.writeString(number);
        dest.writeString(email);
        dest.writeString(funds);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Build> CREATOR = new Parcelable.Creator<Build>() {
        @Override
        public Build createFromParcel(Parcel in) {
            return new Build(in);
        }

        @Override
        public Build[] newArray(int size) {
            return new Build[size];
        }
    };
}
