package br.com.apt.model.user;

import android.os.Parcel;
import android.os.Parcelable;

import br.com.apt.application.SendRequest;

/**
 * Created by adminbs on 5/17/16.
 */
public class Registe extends SendRequest implements Parcelable {
    public String name;
    public String username;
    public String password;
    public String code;
    public String document;
    public String cpf;
    public String phone;

    public Registe() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    protected Registe(Parcel in) {
        name = in.readString();
        username = in.readString();
        password = in.readString();
        code = in.readString();
        document = in.readString();
        cpf = in.readString();
        phone = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(username);
        dest.writeString(password);
        dest.writeString(code);
        dest.writeString(document);
        dest.writeString(cpf);
        dest.writeString(phone);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Registe> CREATOR = new Parcelable.Creator<Registe>() {
        @Override
        public Registe createFromParcel(Parcel in) {
            return new Registe(in);
        }

        @Override
        public Registe[] newArray(int size) {
            return new Registe[size];
        }
    };
}