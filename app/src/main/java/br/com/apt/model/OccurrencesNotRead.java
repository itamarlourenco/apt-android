package br.com.apt.model;

/**
 * Created by adminbs on 10/9/17.
 */

public class OccurrencesNotRead {

    public static final String OCCURRENCES_NOT_READ_BROADCAST = "OCCURRENCES_NOT_READ_BROADCAST";
    public static final String OCCURRENCES_NOT_READ_BROADCAST_TOTAL = "OCCURRENCES_NOT_READ_BROADCAST_TOTAL";

    private int number;

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}
