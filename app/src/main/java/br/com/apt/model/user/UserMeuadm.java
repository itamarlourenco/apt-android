package br.com.apt.model.user;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.apt.application.ModelObject;

/**
 * Created by adminbs on 7/24/17.
 */

public class UserMeuadm implements ModelObject {
    private String id;

    private String name;
    private String email;
    private String document;
    private String condominiumkey;
    private String mobile;
    private String gender;
    private String tutorial;
    private String companyKey;
    private Role role;
    private String blockKey;
    private String unitKey;
    private Photo photo;
    private String company_group;
    private String condominium;
    private List<Units> units;

    public String getCompanyKey() {
        return companyKey;
    }

    public void setCompanyKey(String companyKey) {
        this.companyKey = companyKey;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public String getCondominiumkey() {
        return condominiumkey;
    }

    public void setCondominiumkey(String condominiumkey) {
        this.condominiumkey = condominiumkey;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getTutorial() {
        return tutorial;
    }

    public void setTutorial(String tutorial) {
        this.tutorial = tutorial;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getBlockKey() {
        return blockKey;
    }

    public void setBlockKey(String blockKey) {
        this.blockKey = blockKey;
    }

    public String getUnitKey() {
        return unitKey;
    }

    public void setUnitKey(String unitKey) {
        this.unitKey = unitKey;
    }

    public Photo getPhoto() {
        return photo;
    }

    public void setPhoto(Photo photo) {
        this.photo = photo;
    }

    public String getCompany_group() {
        return company_group;
    }

    public void setCompany_group(String company_group) {
        this.company_group = company_group;
    }

    public String getCondominium() {
        return condominium;
    }

    public void setCondominium(String condominium) {
        this.condominium = condominium;
    }

    public List<Units> getUnitsList() {
        return units;
    }

    public void setUnitsList(List<Units> unitsList) {
        this.units = unitsList;
    }

    public static class Role{
        private String name;
        private String key;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }
    }

    public static class Photo{
        private String etag;
        private String url;

        public String getEtag() {
            return etag;
        }

        public void setEtag(String etag) {
            this.etag = etag;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
    }

    public static class Units implements Parcelable {
        private String id_clienteaptxsiga;
        private String cpf_cnpj_cliente;
        private String condominio;
        private String bloco;
        private String unidade;
        private String id_cliente_meuapt;
        private String id_administradora;
        private Date dt_integracao;
        private Date dt_alteracao;
        private String id_usuario_alteracao;
        private String ativo;
        private String nome;
        private Date dt_cadastro;
        private String nome_base;
        private Address address;

        public Address getAddress() {
            return address;
        }

        public void setAddress(Address address) {
            this.address = address;
        }

        public String getId_clienteaptxsiga() {
            return id_clienteaptxsiga;
        }

        public void setId_clienteaptxsiga(String id_clienteaptxsiga) {
            this.id_clienteaptxsiga = id_clienteaptxsiga;
        }

        public String getCpf_cnpj_cliente() {
            return cpf_cnpj_cliente;
        }

        public void setCpf_cnpj_cliente(String cpf_cnpj_cliente) {
            this.cpf_cnpj_cliente = cpf_cnpj_cliente;
        }

        public String getCondominio() {
            return condominio;
        }

        public void setCondominio(String condominio) {
            this.condominio = condominio;
        }

        public String getBloco() {
            return bloco;
        }

        public void setBloco(String bloco) {
            this.bloco = bloco;
        }

        public String getUnidade() {
            return unidade;
        }

        public void setUnidade(String unidade) {
            this.unidade = unidade;
        }

        public String getId_cliente_meuapt() {
            return id_cliente_meuapt;
        }

        public void setId_cliente_meuapt(String id_cliente_meuapt) {
            this.id_cliente_meuapt = id_cliente_meuapt;
        }

        public String getId_administradora() {
            return id_administradora;
        }

        public void setId_administradora(String id_administradora) {
            this.id_administradora = id_administradora;
        }

        public Date getDt_integracao() {
            return dt_integracao;
        }

        public void setDt_integracao(Date dt_integracao) {
            this.dt_integracao = dt_integracao;
        }

        public Date getDt_alteracao() {
            return dt_alteracao;
        }

        public void setDt_alteracao(Date dt_alteracao) {
            this.dt_alteracao = dt_alteracao;
        }

        public String getId_usuario_alteracao() {
            return id_usuario_alteracao;
        }

        public void setId_usuario_alteracao(String id_usuario_alteracao) {
            this.id_usuario_alteracao = id_usuario_alteracao;
        }

        public String getAtivo() {
            return ativo;
        }

        public void setAtivo(String ativo) {
            this.ativo = ativo;
        }

        public String getNome() {
            return nome;
        }

        public void setNome(String nome) {
            this.nome = nome;
        }

        public Date getDt_cadastro() {
            return dt_cadastro;
        }

        public void setDt_cadastro(Date dt_cadastro) {
            this.dt_cadastro = dt_cadastro;
        }

        public String getNome_base() {
            return nome_base;
        }

        public void setNome_base(String nome_base) {
            this.nome_base = nome_base;
        }

        protected Units(Parcel in) {
            id_clienteaptxsiga = in.readString();
            cpf_cnpj_cliente = in.readString();
            condominio = in.readString();
            bloco = in.readString();
            unidade = in.readString();
            id_cliente_meuapt = in.readString();
            id_administradora = in.readString();
            long tmpDt_integracao = in.readLong();
            dt_integracao = tmpDt_integracao != -1 ? new Date(tmpDt_integracao) : null;
            long tmpDt_alteracao = in.readLong();
            dt_alteracao = tmpDt_alteracao != -1 ? new Date(tmpDt_alteracao) : null;
            id_usuario_alteracao = in.readString();
            ativo = in.readString();
            nome = in.readString();
            long tmpDt_cadastro = in.readLong();
            dt_cadastro = tmpDt_cadastro != -1 ? new Date(tmpDt_cadastro) : null;
            nome_base = in.readString();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(id_clienteaptxsiga);
            dest.writeString(cpf_cnpj_cliente);
            dest.writeString(condominio);
            dest.writeString(bloco);
            dest.writeString(unidade);
            dest.writeString(id_cliente_meuapt);
            dest.writeString(id_administradora);
            dest.writeLong(dt_integracao != null ? dt_integracao.getTime() : -1L);
            dest.writeLong(dt_alteracao != null ? dt_alteracao.getTime() : -1L);
            dest.writeString(id_usuario_alteracao);
            dest.writeString(ativo);
            dest.writeString(nome);
            dest.writeLong(dt_cadastro != null ? dt_cadastro.getTime() : -1L);
            dest.writeString(nome_base);
        }

        @SuppressWarnings("unused")
        public static final Parcelable.Creator<Units> CREATOR = new Parcelable.Creator<Units>() {
            @Override
            public Units createFromParcel(Parcel in) {
                return new Units(in);
            }

            @Override
            public Units[] newArray(int size) {
                return new Units[size];
            }
        };
    }

    public static class Address implements Parcelable {
        private String blocoNome;
        private String condominiumNome;
        private String endereco;
        private String bairro;
        private String cidade;
        private String estado;
        private String cep;

        public String getBlocoNome() {
            return blocoNome;
        }

        public void setBlocoNome(String blocoNome) {
            this.blocoNome = blocoNome;
        }

        public String getCondominiumNome() {
            return condominiumNome;
        }

        public void setCondominiumNome(String condominiumNome) {
            this.condominiumNome = condominiumNome;
        }

        public String getEndereco() {
            return endereco;
        }

        public void setEndereco(String endereco) {
            this.endereco = endereco;
        }

        public String getBairro() {
            return bairro;
        }

        public void setBairro(String bairro) {
            this.bairro = bairro;
        }

        public String getCidade() {
            return cidade;
        }

        public void setCidade(String cidade) {
            this.cidade = cidade;
        }

        public String getEstado() {
            return estado;
        }

        public void setEstado(String estado) {
            this.estado = estado;
        }

        public String getCep() {
            return cep;
        }

        public void setCep(String cep) {
            this.cep = cep;
        }

        protected Address(Parcel in) {
            blocoNome = in.readString();
            condominiumNome = in.readString();
            endereco = in.readString();
            bairro = in.readString();
            cidade = in.readString();
            estado = in.readString();
            cep = in.readString();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(blocoNome);
            dest.writeString(condominiumNome);
            dest.writeString(endereco);
            dest.writeString(bairro);
            dest.writeString(cidade);
            dest.writeString(estado);
            dest.writeString(cep);
        }

        @SuppressWarnings("unused")
        public static final Parcelable.Creator<Address> CREATOR = new Parcelable.Creator<Address>() {
            @Override
            public Address createFromParcel(Parcel in) {
                return new Address(in);
            }

            @Override
            public Address[] newArray(int size) {
                return new Address[size];
            }
        };
    }

    protected UserMeuadm(Parcel in) {
        id = in.readString();
        name = in.readString();
        email = in.readString();
        document = in.readString();
        condominiumkey = in.readString();
        mobile = in.readString();
        gender = in.readString();
        tutorial = in.readString();
        role = (Role) in.readValue(Role.class.getClassLoader());
        blockKey = in.readString();
        unitKey = in.readString();
        photo = (Photo) in.readValue(Photo.class.getClassLoader());
        company_group = in.readString();
        condominium = in.readString();
        if (in.readByte() == 0x01) {
            units = new ArrayList<Units>();
            in.readList(units, Units.class.getClassLoader());
        } else {
            units = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(email);
        dest.writeString(document);
        dest.writeString(condominiumkey);
        dest.writeString(mobile);
        dest.writeString(gender);
        dest.writeString(tutorial);
        dest.writeValue(role);
        dest.writeString(blockKey);
        dest.writeString(unitKey);
        dest.writeValue(photo);
        dest.writeString(company_group);
        dest.writeString(condominium);
        if (units == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(units);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<UserMeuadm> CREATOR = new Parcelable.Creator<UserMeuadm>() {
        @Override
        public UserMeuadm createFromParcel(Parcel in) {
            return new UserMeuadm(in);
        }

        @Override
        public UserMeuadm[] newArray(int size) {
            return new UserMeuadm[size];
        }
    };
}