package br.com.apt.model;

/**
 * Created by adminbs on 10/9/17.
 */

public class ReservationNotRead {

    public static final String RESERVATION_NOT_READ_BROADCAST = "RESERVATION_NOT_READ_BROADCAST";
    public static final String RESERVATION_NOT_READ_BROADCAST_TOTAL = "RESERVATION_NOT_READ_BROADCAST_TOTAL";

    private int total;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
