package br.com.apt.model.reservations;

import android.content.Context;

import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.model.OnReceivedListener;
import br.com.apt.model.ResponseModel;
import br.com.apt.util.Logger;

/**
 * Created by andre on 5/17/17.
 */

public class Rule {

    @SerializedName("id")
    public int id;
    @SerializedName("installation_id")
    public int installationId;
    @SerializedName("text")
    public String text;
    @SerializedName("rule")
    public String rule;
    @SerializedName("type")
    public String type;
    @SerializedName("created_at")
    public String createdAt;

    public Rule() {
    }

    public static void requestRules(final Context context, final Installation installation, final OnReceivedListener<List<Rule>> listener) {

        Volley volley = new Volley(new Volley.Callback() {
            @Override
            public void result(String result, int type) {
                if (result == null) {
                    Logger.t(context.getResources().getString(R.string.internet_error));
                    return;
                }

                Type responseType = new TypeToken<ResponseModel<List<Rule>>>() {}.getType();
                ResponseModel<List<Rule>> responseModel = App.getGson().fromJson(result, responseType);

                if (responseModel != null && responseModel.status == Volley.STATUS_OK) {
                    listener.received(responseModel.contents);
                } else {
                    listener.failed(responseModel.code, responseModel.message);
                }
            }

            @Override
            public String uri() {
                return URI.RULE_PROCEDURE+installation.id;
            }

        });
        volley.isReservation(true);
        volley.request();
    }

}
