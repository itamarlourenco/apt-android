package br.com.apt.model;

import br.com.apt.application.BaseGsonMeuAdm;

/**
 * Created by adminbs on 10/9/17.
 */

public class ReservationNotReadSend extends BaseGsonMeuAdm {
    public ReservationNotRead object;

    public ReservationNotRead getObject() {
        return object;
    }

    public void setObject(ReservationNotRead object) {
        this.object = object;
    }
}

