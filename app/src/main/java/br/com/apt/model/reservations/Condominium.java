package br.com.apt.model.reservations;

import android.content.Context;

import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.io.Serializable;
import java.lang.reflect.Type;

import br.com.apt.R;
import br.com.apt.application.App;
import br.com.apt.application.URI;
import br.com.apt.application.Volley;
import br.com.apt.model.OnReceivedListener;
import br.com.apt.model.ResponseModel;
import br.com.apt.model.user.UserData;
import br.com.apt.util.Logger;

/**
 * Created by andre on 21/05/17.
 */

public class Condominium implements Serializable {

    @SerializedName("id")
    public int id;
    @SerializedName("external_id")
    public int externalId;
    @SerializedName("name")
    public String name;
    @SerializedName("address")
    public String address;
    @SerializedName("street")
    public String street;
    @SerializedName("street_number")
    public int street_number;
    @SerializedName("neighborhood")
    public String neighborhood;
    @SerializedName("zip_code")
    public long zip_code;
    @SerializedName("city")
    public String city;
    @SerializedName("state")
    public String state;
    @SerializedName("manager_name")
    public String manager_name;
    @SerializedName("manager_email")
    public String manager_email;
    @SerializedName("manager_phone")
    public String manager_phone;
    @SerializedName("apartment_quantity")
    public int apartment_quantity;
    @SerializedName("store_quantity")
    public int store_quantity;
    @SerializedName("group_quantity")
    public int group_quantity;
    @SerializedName("document")
    public String document;
    @SerializedName("date_closing")
    public String date_closing;
    @SerializedName("category")
    public String category;

    public static void getCurrentCondominium(final Context context, final OnReceivedListener<Condominium> listener){

        Volley volley = new Volley(new Volley.Callback() {

            @Override
            public void result(String result, int type) {
                if (result == null) {
                    Logger.t(context.getResources().getString(R.string.internet_error));
                    return;
                }

                Type responseType = new TypeToken<ResponseModel<Condominium>>() {
                }.getType();
                ResponseModel<Condominium> responseModel = App.getGson().fromJson(result, responseType);

                if (responseModel != null && responseModel.status == Volley.STATUS_OK) {
                    listener.received(responseModel.contents);
                } else {
                    listener.failed(responseModel.code, responseModel.message);
                }
            }

            @Override
            public String uri() {
                return URI.INSTALLATIONS_LIST;
            }
        }, String.format(URI.CONDOMINIUM, UserData.getUser().getId()));

        volley.request();
    }

}