package br.com.apt.model.reservations;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by andre on 19/05/17.
 */

public class Unit implements Serializable {

    @SerializedName("id")
    public int id;
    @SerializedName("external_id")
    public String externalId;
    @SerializedName("name")
    public String name;
    @SerializedName("created_at")
    public String createdAt;

}
