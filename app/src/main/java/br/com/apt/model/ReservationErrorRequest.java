package br.com.apt.model;

import java.util.List;

import br.com.apt.application.BaseGsonMeuAdm;
import br.com.apt.ui.MyAdm.myadmFragments.Billet.Billet;

/**
 * Created by adminbs on 9/13/17.
 */

public class ReservationErrorRequest extends BaseGsonMeuAdm{
    private List<ReservationError> object;

    public List<ReservationError> getObject() {
        return object;
    }

    public void setObject(List<ReservationError> object) {
        this.object = object;
    }
}
