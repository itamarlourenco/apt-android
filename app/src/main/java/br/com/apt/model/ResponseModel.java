package br.com.apt.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by adminbs on 7/27/17.
 */

public class ResponseModel<T> {
    @SerializedName("message")
    public String message;

    @SerializedName("code")
    public int code;

    @SerializedName("status")
    public int status;

    @SerializedName("object")
    public T contents;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public T getContents() {
        return contents;
    }

    public void setContents(T contents) {
        this.contents = contents;
    }
}