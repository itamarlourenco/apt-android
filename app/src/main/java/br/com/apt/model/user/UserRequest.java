package br.com.apt.model.user;

import com.google.gson.annotations.SerializedName;

import br.com.apt.application.BaseGson;
import br.com.apt.model.build.Build;

/**
 * Created by adminbs on 1/15/16.
 */
public class UserRequest extends BaseGson{
    private User Object;

    public User getObject() {
        return Object;
    }

    public void setObject(User object) {
        this.Object = object;
    }
}
