package br.com.apt.model.user;

import br.com.apt.application.BaseGson;

/**
 * Created by adminbs on 1/15/16.
 */
public class UserMeuadmRequest {

    private int code;
    private boolean success;
    private String message;
    private UserMeuadm object;

    public UserMeuadm getObject() {
        return object;
    }

    public void setObject(UserMeuadm object) {
        this.object = object;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
