package br.com.apt.model;

/**
 * Created by andre on 5/18/17.
 */

public interface OnReceivedListener<T> {
    //void received();
    void received(T object);

    void failed(int errorCode, String errorMessage);
}

