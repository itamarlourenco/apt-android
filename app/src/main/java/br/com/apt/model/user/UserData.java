package br.com.apt.model.user;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import br.com.apt.application.App;
import br.com.apt.model.build.Build;
import br.com.apt.util.Logger;

/**
 * Created by itamarlourenco on 15/01/16.
 */
public class UserData {

    public static void saveUser(UserRequest user){
        User userObject = user.getObject();
        Build build = userObject.getBuild();
        if(build != null){
            userObject.setCondominiumId( String.valueOf(userObject.getBuild().getId()) );
        }
        saveUser(userObject);
    }

    public static void saveUser(User user){
        try{
            SharedPreferences pref = App.getContext().getSharedPreferences(UserDataPref.USER_PREF, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = pref.edit();

            editor.putString(UserDataPref.ID, user.getId());
            editor.putString(UserDataPref.NAME, user.getName());
            editor.putString(UserDataPref.EMAIL, user.getEmail());
            editor.putString(UserDataPref.PASSWORD, user.getPassword());
            editor.putString(UserDataPref.PHONE, user.getPhone());
            editor.putString(UserDataPref.DOC, user.getDoc());
            editor.putString(UserDataPref.TYPE_USER, user.getTypeUser());
            editor.putString(UserDataPref.BUILD_ID, user.getBuildId());
            editor.putString(UserDataPref.APTO, user.getApto());
            editor.putString(UserDataPref.TOKEN, user.getToken());
            editor.putBoolean(UserDataPref.IS_ACTIVATED, user.isActivated());
            editor.putString(UserDataPref.APNS, user.getApns());
            editor.putString(UserDataPref.BLOCK_ID, user.getBlockId());
            editor.putBoolean(UserDataPref.IS_TRIP, user.isTrip());
            editor.putBoolean(UserDataPref.ADM, user.isADM());
            editor.putString(UserDataPref.CHAT_TOKEN, user.getChatToken());
            editor.putString(UserDataPref.CODE, user.getCode());
            editor.putString(UserDataPref.IMAGE, user.getImage());
            editor.putString(UserDataPref.BUILDING_NAME, user.getBuildingName());
            editor.putString(UserDataPref.CONDOMINIUM_ID, user.getCondominiumId());
            editor.putString(UserDataPref.COMPANY_KEY, user.getCompanyKey());
            editor.putString(UserDataPref.UNITS, App.getGson().toJson(user.getUnits()));
            editor.putString(UserDataPref.CURRENT_BLOCK, user.getCurrentBlock());

            Build build = user.getBuild();
            if(build != null){
                SharedPreferences prefBuild = App.getContext().getSharedPreferences(UserDataPref.UserDataBuildPref.USER_PREF_BUILD, Context.MODE_PRIVATE);
                SharedPreferences.Editor editorBuild = prefBuild.edit();
                editorBuild.putLong(UserDataPref.UserDataBuildPref.ID, build.getId());
                editorBuild.putString(UserDataPref.UserDataBuildPref.EMAIL, build.getEmail());
                editorBuild.putString(UserDataPref.UserDataBuildPref.FUNDS, build.getFunds());
                editorBuild.putString(UserDataPref.UserDataBuildPref.LOCATION, build.getLocation());
                editorBuild.putString(UserDataPref.UserDataBuildPref.NAME, build.getName());
                editorBuild.putString(UserDataPref.UserDataBuildPref.NUMBER, build.getNumber());
                editorBuild.putString(UserDataPref.UserDataBuildPref.PHONE, build.getPhone());
                editorBuild.putString(UserDataPref.UserDataBuildPref.ZIPCODE, build.getZipcode());
                editorBuild.putString(UserDataPref.UserDataBuildPref.BLOCK_NAME, build.getNameBlock());

                editorBuild.apply();
            }

            editor.apply();
        }catch (Exception e){
            Logger.e(e.getMessage());
        }
    }

    public static User getUser(){
        User user = new User();
        try {
            SharedPreferences pref = App.getContext().getSharedPreferences(UserDataPref.USER_PREF, Context.MODE_PRIVATE);

            user.setId(pref.getString(UserDataPref.ID, ""));
            user.setName(pref.getString(UserDataPref.NAME, ""));
            user.setEmail(pref.getString(UserDataPref.EMAIL, ""));
            user.setPassword(pref.getString(UserDataPref.PASSWORD, ""));
            user.setPhone(pref.getString(UserDataPref.PHONE, ""));
            user.setDoc(pref.getString(UserDataPref.DOC, ""));
            user.setTypeUser(pref.getString(UserDataPref.TYPE_USER, ""));
            user.setBiuldId(pref.getString(UserDataPref.BUILD_ID, ""));
            user.setApto(pref.getString(UserDataPref.APTO, ""));
            user.setToken(pref.getString(UserDataPref.TOKEN, ""));
            user.setIsActivated(pref.getBoolean(UserDataPref.IS_ACTIVATED, false));
            user.setApns(pref.getString(UserDataPref.APNS, ""));
            user.setBlockId(pref.getString(UserDataPref.BLOCK_ID, ""));
            user.setIsTrip(pref.getBoolean(UserDataPref.IS_TRIP, false));
            user.setADM(pref.getBoolean(UserDataPref.ADM, false));
            user.setChatToken(pref.getString(UserDataPref.CHAT_TOKEN, ""));
            user.setCode(pref.getString(UserDataPref.CODE, ""));
            user.setImage(pref.getString(UserDataPref.IMAGE, ""));
            user.setBuildingName(pref.getString(UserDataPref.BUILDING_NAME, ""));
            user.setCondominiumId(pref.getString(UserDataPref.CONDOMINIUM_ID, ""));
            user.setCompanyKey(pref.getString(UserDataPref.COMPANY_KEY, ""));
            user.setCurrentBlock(pref.getString(UserDataPref.CURRENT_BLOCK, ""));

            String listUnits = pref.getString(UserDataPref.UNITS, "");
            Type type = new TypeToken<List<UserMeuadm.Units>>() {
            }.getType();
            List<UserMeuadm.Units> units = App.getGson().fromJson(listUnits, type);
            user.setUnits(units);


            Build build = new Build();
            SharedPreferences prefBuild = App.getContext().getSharedPreferences(UserDataPref.UserDataBuildPref.USER_PREF_BUILD, Context.MODE_PRIVATE);
            build.setId(prefBuild.getLong(UserDataPref.UserDataBuildPref.ID, 0));
            build.setEmail(prefBuild.getString(UserDataPref.UserDataBuildPref.EMAIL, ""));
            build.setFunds(prefBuild.getString(UserDataPref.UserDataBuildPref.FUNDS, ""));
            build.setLocation(prefBuild.getString(UserDataPref.UserDataBuildPref.LOCATION, ""));
            build.setName(prefBuild.getString(UserDataPref.UserDataBuildPref.NAME, ""));
            build.setNumber(prefBuild.getString(UserDataPref.UserDataBuildPref.NUMBER, ""));
            build.setPhone(prefBuild.getString(UserDataPref.UserDataBuildPref.PHONE, ""));
            build.setZipcode(prefBuild.getString(UserDataPref.UserDataBuildPref.ZIPCODE, ""));
            build.setNameBlock(prefBuild.getString(UserDataPref.UserDataBuildPref.BLOCK_NAME, ""));
            user.setBuild(build);

            if (TextUtils.isEmpty(user.getEmail()) || TextUtils.isEmpty(user.getBuildId())) {
                return null;
            }
        }catch (Exception e){
            Logger.e(e.getMessage());
        }

        return user;
    }

    public static void clear() {
        SharedPreferences pref = App.getContext().getSharedPreferences(UserDataPref.USER_PREF, Context.MODE_PRIVATE);
        SharedPreferences prefBuild = App.getContext().getSharedPreferences(UserDataPref.UserDataBuildPref.USER_PREF_BUILD, Context.MODE_PRIVATE);
        pref.edit().clear().apply();

        prefBuild.edit().clear().apply();
    }

    public static void addNoticeRead(long id){
        SharedPreferences prefNotice = App.getContext().getSharedPreferences(UserDataPref.UserDataNoticePref.USER_PREF_NOTICE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editorNotice = prefNotice.edit();
        editorNotice.putBoolean(UserDataPref.UserDataNoticePref.PREFIXE + id, true);
        editorNotice.apply();
    }

    public static boolean isNoticeRead(long id){
        SharedPreferences prefNotice = App.getContext().getSharedPreferences(UserDataPref.UserDataNoticePref.USER_PREF_NOTICE, Context.MODE_PRIVATE);
        return prefNotice.getBoolean(UserDataPref.UserDataNoticePref.PREFIXE + id, false);
    }

    public class UserDataPref{
        public static final String USER_PREF = "USER_PREF";

        public static final String ID = "ID_USER";
        public static final String NAME = "NAME";
        public static final String EMAIL = "EMAIL";
        public static final String PASSWORD = "PASSWORD";
        public static final String PHONE = "PHONE";
        public static final String DOC = "DOC";
        public static final String TYPE_USER = "TYPE_USER";
        public static final String BUILD_ID = "BUILD_ID";
        public static final String APTO = "APTO";
        public static final String TOKEN = "TOKEN";
        public static final String IS_ACTIVATED = "IS_ACTIVATED";
        public static final String APNS = "APNS";
        public static final String BLOCK_ID = "BLOCK_ID";
        public static final String IS_TRIP = "IS_TRIP";
        public static final String CHAT_TOKEN = "CHAT_TOKEN";
        public static final String CODE = "CODE";
        public static final String IMAGE = "IMAGE";
        public static final String ADM = "ADM";
        public static final String BUILDING_NAME = "BUILDING_NAME";
        public static final String CONDOMINIUM_ID = "CONDOMINIUM_ID";
        public static final String COMPANY_KEY = "COMPANY_KEY";
        public static final String UNITS = "UNITS";
        public static final String CURRENT_BLOCK = "CURRENT_BLOCK";

        public class UserDataBuildPref{
            public static final String USER_PREF_BUILD = "USER_PREF_BUILD";

            public static final String ID = "ID";
            public static final String EMAIL = "EMAIL";
            public static final String FUNDS = "FUNDS";
            public static final String LOCATION = "LOCATION";
            public static final String NAME = "NAME";
            public static final String NUMBER = "NUMBER";
            public static final String PHONE = "PHONE";
            public static final String ZIPCODE = "ZIPCODE";
            public static final String BLOCK_NAME = "BLOCK_NAME";
        }

        public class UserDataNoticePref{
            public static final String USER_PREF_NOTICE = "USER_PREF_NOTICE";

            public static final String PREFIXE = "notice_id_";
        }
    }
}
