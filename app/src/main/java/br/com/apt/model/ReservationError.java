package br.com.apt.model;

import br.com.apt.application.BaseGsonMeuAdm;

/**
 * Created by adminbs on 9/13/17.
 */

public class ReservationError {
    private String field;
    private String message;

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
