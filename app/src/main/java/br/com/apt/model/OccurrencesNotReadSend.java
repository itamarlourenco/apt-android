package br.com.apt.model;

import br.com.apt.application.BaseGsonMeuAdm;
import br.com.apt.ui.MyAdm.myadmFragments.register.Register;

/**
 * Created by adminbs on 10/9/17.
 */

public class OccurrencesNotReadSend extends BaseGsonMeuAdm {
    public OccurrencesNotRead object;

    public OccurrencesNotRead getObject() {
        return object;
    }

    public void setObject(OccurrencesNotRead object) {
        this.object = object;
    }
}

