package br.com.apt.model;

/**
 * Created by andre on 30/06/17.
 */

public interface OnReceivedResponseModel<T> {

    void received(ResponseModel<T> reponse);
}
