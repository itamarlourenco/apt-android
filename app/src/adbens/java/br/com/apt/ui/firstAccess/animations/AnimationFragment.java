package br.com.apt.ui.firstAccess.animations;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import br.com.apt.R;
import br.com.apt.application.BaseFragment;
import br.com.apt.util.Logger;
import br.com.apt.util.Util;
import br.com.apt.util.VideoViewCustom;
import br.com.apt.widget.CustomButton;
import br.com.apt.widget.CustomTextView;

/**
 * Created by adminbs on 9/12/16.
 */
public class AnimationFragment extends BaseFragment {

    public static boolean loop;

    public static Fragment newInstance() {
        return new AnimationFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.animation_fragment, container, false);
    }


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final View view = getView();
        if(view != null){
            view.findViewById(R.id.doLogin).setOnClickListener(new OnClickListener(){
                @Override
                public void onClick(View v) {
                    openActivity(BaseFragmentActivity.FRAGMENT_LOGIN);
                }
            });

            view.findViewById(R.id.codeAccess).setOnClickListener(new OnClickListener(){
                @Override
                public void onClick(View v) {
                    openActivity(BaseFragmentActivity.FRAGMENT_CODE);
                }
            });

        }
    }

    private void openActivity(int fragmentType) {
        startActivity(BaseFragmentActivity.newIntent(getContext(), fragmentType));
        getActivity().overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
    }
}
